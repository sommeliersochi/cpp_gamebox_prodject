// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class CPP_SkillBoxTarget : TargetRules
{
	public CPP_SkillBoxTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		// bWithPushModel = true - working on engine from sources
		ExtraModuleNames.Add("CPP_SkillBox");
	}
}

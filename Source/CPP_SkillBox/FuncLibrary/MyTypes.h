// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "MyTypes.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Idle_State UMETA(DisplayName = "Idle State"),
	Aim_State UMETA(DisplayName ="Aim State"),
	AimWalk_State UMETA(DisplayName = "Aim Run Stare"),
	Walk_State UMETA(DisplayName ="Walk State"),
	Run_State UMETA(DisplayName ="Run State"),
	SprintRun_State UMETA(DisplayName = "Sprint State"),
	Roll_State UMETA(DisplayName = "Rolling State"),
	Crouch_State UMETA(DisplayName = "Crouch State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float AimSpeedNormal = 250.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float WalkSpeedNormal = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float RunSpeedNormal = 400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float AimSpeedWalk = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float SprintSpeedRun = 400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float RollSpeed = 900.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float CrouchSpeed = 200.0f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ProjectileSettings")
	TSubclassOf<class AProjectile> Projectile = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ProjectileSettings")
	float ProjectileDamage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ProjectileSettings")
	float ProjectileLife = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ProjectileSettings")
	float ProjectileInitialSpeed = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ProjectileSettings")
	bool bIsBomb = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ProjectileSettings")
	float ProjectileRadiusDamage = 200.0f;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float DispersionAimStart = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float DispersionAimMax = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float DispersionAimMin = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float DispersionAimShootCoef = 1.0f;
};

USTRUCT(BlueprintType)
struct FWeaponeInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Class")
	TSubclassOf<class AWeapon> WeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="State")
	float RateOfFire = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="State")
	float ReloadTime = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="State")
	int32 MaxRound = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Dispersion")
	FWeaponDispersion DispersionWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Sound")
	USoundBase* SoundFireBase = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Sound")
	USoundBase* SoundReloadBase = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	UParticleSystem* EffectFireWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Projectile")
	FProjectileInfo ProjectileSettings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Trace")
	float WeaponDamage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Trace")
	float DistanceTrace = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="HitEffect")
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Animations")
	UAnimMontage* AnimCharFire = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Mesh")
	UStaticMesh* MagazineDrop = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Mesh")
	UStaticMesh* SleeveBullet = nullptr;
};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Weapone State")
	int32 Round = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= items)
	int32 Starting9mmAmmo = 85;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= items)
	int32 StartingARAmmo = 120;
};

UCLASS()
class CPP_SKILLBOX_API UMyTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};

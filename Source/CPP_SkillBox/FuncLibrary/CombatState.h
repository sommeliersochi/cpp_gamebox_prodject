#pragma once

UENUM(BlueprintType)
enum class ECombatState : uint8
{
	ECS_Unoccupied UMETA(DisplayName = "Unoccupied"),
	ECS_Reloading UMETA(DisplayName = "Reloading"),
	ECS_SwappingWeapons UMETA(DisplayName = "Swapping Weapons"),
	ECS_ThrowingGrenade UMETA(DisplayName = "Throw Grenade"),
	ECS_KnifeAttack UMETA(DisplayName = "Knife Attack"),
	ECS_UseFirstAidKit UMETA(DisplayName = "Use First Aid Kit"),

	ECS_MAX UMETA(DisplayName = "DefaultMAX")
};

﻿#pragma once

#include <Kismet/BlueprintFunctionLibrary.h>
#include <Engine/DataTable.h>
#include "BaseDefinitions.generated.h"

UENUM(BlueprintType)
enum class EBarrierType : uint8
{
	None UMETA(DisplayName = "None"),
	Edge UMETA(DisplayName ="Edge"),
	Small UMETA(DisplayName = "Small"),
	Basic UMETA(DisplayName ="Basic")
};

UENUM(BlueprintType)
enum class EBarrierState : uint8
{
	FullHealth UMETA(DisplayName = "FullHealth"),
	Damaged UMETA(DisplayName ="Damaged"),
	MeshDamaged UMETA(DisplayName = "MeshDamaged"),
	Destroyed UMETA(DisplayName ="Destroyed")
};

USTRUCT(BlueprintType)
struct FBarrierInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BarrierSettings")
	int32 Price = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BarrierSettings")
	FName Name = "None";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BarrierSettings")
	float MaxHP = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BarrierSettings||Mesh")
	UStaticMesh* Mesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BarrierSettings||Mesh")
	UStaticMesh* MeshDamaged = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BarrierSettings||Mesh")
	UStaticMesh* MeshDestroyed = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BarrierSettings||Mesh")
	UMaterial* MaterialDamaged = nullptr;
};

USTRUCT(BlueprintType)
struct FBaseDefenceInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DefenceSettings")
	float TimeToStartDefence = 60.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DefenceSettings")
	float SpawnRate = 5.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DefenceSettings")
	float TimeBetweenWavesAttack = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DefenceSettings")
	TArray<int32> FirstWaveGroupNpc = {1, 2, 3};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DefenceSettings")
	TArray<int32> SecondWaveGroupNpc = {2, 3, 4};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DefenceSettings")
	TArray<int32> ThirdWaveGroupNpc = {3, 4, 5};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DefenceSettings")
	TArray<int32> FourthWaveGroupNpc = {3, 4, 5};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DefenceSettings")
	TArray<int32> FifthWaveGroupNpc = {3, 4, 5};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DefenceSettings")
	TArray<int32> SixthWaveGroupNpc = {3, 4, 5};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DefenceSettings")
	int32 NumWaves = 3;
};

UCLASS()
class CPP_SKILLBOX_API UBaseDefinitions : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};

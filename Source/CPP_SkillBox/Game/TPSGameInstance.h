// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <Engine/DataTable.h>
#include <Kismet/BlueprintPlatformLibrary.h>

#include "MyTypes.h"
#include "TPSGameInstance.generated.h"


UCLASS(Blueprintable, BlueprintType)
class CPP_SKILLBOX_API UTPSGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
	public:

	virtual void Init() override;	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "WeaponInfo")
	UDataTable* WeaponInfoTable = nullptr;
	
	UFUNCTION()
	bool GetWeaponInfoByName(FName WeaponName, FWeaponeInfo& OutInfo);
	
	private:
	
	void ScreenSetup();
	
	
};

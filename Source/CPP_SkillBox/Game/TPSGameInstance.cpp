// Fill out your copyright notice in the Description page of Project Settings.

#include "TPSGameInstance.h"

#include <GameFramework/GameUserSettings.h>
#include <Kismet/GameplayStatics.h>

#ifdef WIN32

#include <Windows/AllowWindowsPlatformTypes.h>

#endif

void UTPSGameInstance::Init(){

	Super::Init();

	ScreenSetup();
}

bool UTPSGameInstance::GetWeaponInfoByName(FName WeaponName, FWeaponeInfo& OutInfo)
{
	bool bIsFind =false;
	FWeaponeInfo* WeaponInfoRoW;
	WeaponInfoRoW = WeaponInfoTable-> FindRow<FWeaponeInfo>(WeaponName, "", true );
	if(WeaponInfoRoW)
	{
		bIsFind = true;
		OutInfo = *WeaponInfoRoW;
		
	}
	return bIsFind;
}

void UTPSGameInstance::ScreenSetup()
{
#ifdef WIN32
	if (UGameplayStatics::GetPlatformName() == TEXT("Windows"))
	{
		FIntPoint  ScreenSize;
		RECT       Win32_DesktopSize;
		const HWND Win32_HDesktop = GetDesktopWindow();
		GetWindowRect(Win32_HDesktop, &Win32_DesktopSize);

		ScreenSize.X = Win32_DesktopSize.right;
		ScreenSize.Y = Win32_DesktopSize.bottom;

		FIntPoint DesiredSize{  1920, 1080, };

		UE_LOG(LogTemp, Log, TEXT("ScreenSize: %dx%d"), ScreenSize.X, ScreenSize.Y);

		while (ScreenSize.Y < DesiredSize.Y)
		{
			DesiredSize /= 2;
		}

		auto* GameUserSettings = UGameUserSettings::GetGameUserSettings();
		GameUserSettings->SetFullscreenMode(EWindowMode::Type::Windowed);
		GameUserSettings->SetScreenResolution(DesiredSize);
		GameUserSettings->ApplySettings(false);

		UE_LOG(LogTemp, Log, TEXT("WindowSize: %dx%d"), DesiredSize.X, DesiredSize.Y);
	}
#endif	
}

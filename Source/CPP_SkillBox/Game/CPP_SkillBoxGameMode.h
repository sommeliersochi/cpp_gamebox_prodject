// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <CoreMinimal.h>
#include <EnvironmentQuery/EnvQueryTypes.h>
#include <GameFramework/GameModeBase.h>

#include "CPP_SkillBoxPlayerController.h"
#include "CPP_SkillBoxGameMode.generated.h"

class UEnvQueryInstanceBlueprintWrapper;
class UEnvQuery;
class UCurveFloat;

UCLASS(minimalapi)
class ACPP_SkillBoxGameMode : public AGameModeBase
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditDefaultsOnly, Category= "AI")
	TSubclassOf<AActor> MinionClass;

	UPROPERTY(EditDefaultsOnly, Category= "AirBomb")
	TSubclassOf<AActor> AirBombSpawnerClass;

	UPROPERTY(EditDefaultsOnly, Category= "AI")
	UEnvQuery* SpawnBotQuery;

	UPROPERTY(EditDefaultsOnly, Category= "AirBomb")
	UEnvQuery* SpawnBombQuery;

	UPROPERTY(EditDefaultsOnly, Category= "AI")
	UCurveFloat* DifficultyCurve;

	FTimerHandle TimerHandle_SpawnBot;

	FTimerHandle TimerHandle_SpawnBomb;

	UPROPERTY(EditDefaultsOnly, Category= "AI")
	float SpawnTimerInterval = 2.0f;

	UPROPERTY(EditDefaultsOnly, Category= "AirBomb")
	float SpawnBombTimerIntervalMin;

	UPROPERTY(EditDefaultsOnly, Category= "AirBomb")
	float SpawnBombTimerIntervalMax;

	UPROPERTY(EditDefaultsOnly, Category = "Powerups")
	UEnvQuery* PowerupSpawnQuery;

	UPROPERTY(EditDefaultsOnly, Category = "Powerups")
	float RequiredPowerupDistance;

	UPROPERTY(EditDefaultsOnly, Category = "Powerups")
	int32 DesiredPowerupCount;

	UPROPERTY(EditDefaultsOnly, Category = "PlayerState | Credits")
	int32 CreditPerKill = 100;

	UPROPERTY(EditDefaultsOnly, Category = "Powerups")
	TArray<TSubclassOf<AActor>> ChestClasses;

	UPROPERTY()
	TArray<ACPP_SkillBoxPlayerController*> Players;

	UFUNCTION()
	void SpawnBotTimerElapsed();

	UFUNCTION()
	void SpawnBombTimerElapsed();

	UFUNCTION()
	void OnQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance, EEnvQueryStatus::Type QueryStatus);

	UFUNCTION()
	void OnQueryCompletedForBomb(UEnvQueryInstanceBlueprintWrapper* QueryInstance, EEnvQueryStatus::Type QueryStatus);

	UFUNCTION()
	void OnPowerupSpawnQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance,
	                                  EEnvQueryStatus::Type QueryStatus);

	UFUNCTION()
	void RespawnPlayerElapsed(AController* Controller);

public:
	void SpawnCard(UClass* FlashCardClass, FVector Location);
	void SetNewPlayerStartTagForAllPlayers(FName PlayerStart);

	virtual void OnActorKilled(AActor* VictimActor, AActor* Killer, ACPP_SkillBoxPlayerController* KillerController);

	ACPP_SkillBoxGameMode();

	virtual void StartPlay() override;

	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;
	virtual void RestartPlayer(AController* NewPlayer) override;
};

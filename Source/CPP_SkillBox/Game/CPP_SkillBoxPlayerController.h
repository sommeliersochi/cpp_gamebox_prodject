// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PlayerHUD.h"
#include "GameFramework/PlayerController.h"

#include "CPP_SkillBoxPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPawnChanged, APawn*, NewPawn);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerStateChanged, APlayerState*, NewPlayerState);

UCLASS()
class ACPP_SkillBoxPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	ACPP_SkillBoxPlayerController();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginPlay() override;

	virtual void PlayerTick(float DeltaTime) override;

	virtual void SetupInputComponent() override;

	virtual void SetPawn(APawn* InPawn) override;

	virtual void OnRep_PlayerState() override;

protected:
	void ControlRotationTick(float DeltaTime);

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category="PlayerStart")
	FName PlayerStartTag = "None";

	UPROPERTY(Replicated, BlueprintReadWrite)
	FVector MousePose;

	UPROPERTY(BlueprintReadWrite)
	bool bCanRotate = true;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UUserWidget> HUDOverlayClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Widgets, meta = (AllowPrivateAccess = "true"))
	UUserWidget* HUDOverlay;

	UPROPERTY()
	UPlayerInput* MyPlayerInput;

	UPROPERTY()
	APlayerHUD* PlayerHUD;

	UPROPERTY()
	UCharacterOverlay* CharacterOverlay;

	float HUDCarriedAmmo = 0.0f;
	bool bInitializeCarriedAmmo = false;

	float HUDWeaponAmmo = 0.0f;
	bool bInitializeWeaponAmmo = false;

protected:
	UPROPERTY(BlueprintAssignable)
	FOnPawnChanged OnPawnChanged;

	UPROPERTY(BlueprintAssignable)
	FOnPlayerStateChanged OnPlayerStateReceived;
};

// Copyright Epic Games, Inc. All Rights Reserved.

#include "CPP_SkillBoxPlayerController.h"

#include "Blueprint/UserWidget.h"
#include "Net/UnrealNetwork.h"


ACPP_SkillBoxPlayerController::ACPP_SkillBoxPlayerController()
{
	bShowMouseCursor = false;
}

void ACPP_SkillBoxPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ACPP_SkillBoxPlayerController, MousePose);
	DOREPLIFETIME(ACPP_SkillBoxPlayerController, PlayerStartTag);
}

void ACPP_SkillBoxPlayerController::BeginPlay()
{
	Super::BeginPlay();

	PlayerHUD = GetHUD<APlayerHUD>();
}

void ACPP_SkillBoxPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	ControlRotationTick(DeltaTime);
}

void ACPP_SkillBoxPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
}

void ACPP_SkillBoxPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	OnPawnChanged.Broadcast(InPawn);
}

void ACPP_SkillBoxPlayerController::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	OnPlayerStateReceived.Broadcast(PlayerState);
}

void ACPP_SkillBoxPlayerController::ControlRotationTick(float DeltaTime)
{
	if (bCanRotate)
	{
		if (const APawn* PlayerPawn = GetPawn())
		{
			FVector MouseWorldLocation = FVector::ZeroVector;
			FVector MouseWorldDirection = FVector::ZeroVector;
			DeprojectMousePositionToWorld(MouseWorldLocation, MouseWorldDirection);

			const FVector PawnLocation = PlayerPawn->GetActorLocation();

			const double DotProduct1 = FVector::DotProduct(PawnLocation - MouseWorldLocation, FVector::UpVector);
			const double DotProduct2 = FVector::DotProduct(MouseWorldDirection, FVector::UpVector);

			if (DotProduct2 != 0.0)
			{
				MousePose = MouseWorldLocation + (DotProduct1 / DotProduct2) * MouseWorldDirection;

				SetControlRotation((MousePose - PawnLocation).Rotation());
			}
		}
	}
}

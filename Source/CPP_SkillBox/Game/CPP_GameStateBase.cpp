#include "Game/CPP_GameStateBase.h"

#include "Net/UnrealNetwork.h"

void ACPP_GameStateBase::UpdateWidget_Multicast_Implementation(EGameState State, int32 Items)
{
	OnUpdateWidget.Broadcast(State,Items, MaxItems);
}

void ACPP_GameStateBase::IncrementCurrentItem_OnServer_Implementation(int32 Delta)
{
	CurrentItems += Delta;
	UpdateWidget_Multicast(CurrentState, CurrentItems);
}

void ACPP_GameStateBase::ChangeState_OnServer_Implementation(EGameState State, int32 MaxItem)
{
	CurrentState = State;
	MaxItems = MaxItem;
	UpdateWidget_Multicast(CurrentState, CurrentItems);
}

void ACPP_GameStateBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ThisClass, CurrentItems);
	DOREPLIFETIME(ThisClass, CurrentState);
}

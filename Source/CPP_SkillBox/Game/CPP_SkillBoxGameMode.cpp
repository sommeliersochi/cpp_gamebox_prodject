// Copyright Epic Games, Inc. All Rights Reserved.

#include "CPP_SkillBoxGameMode.h"

#include <EngineUtils.h>
#include <EnvironmentQuery/EnvQueryManager.h>
#include <GameFramework/GameStateBase.h>
#include <GameFramework/PlayerState.h>
#include <GameFramework/PlayerStart.h>

#include "CPP_SkillBoxCharacter.h"
#include "CPP_SkillBoxPlayerController.h"
#include "CPP_SkillBox/Base/InteractParentBase.h"
#include "Inventory/InventoryComponent.h"
#include "SAttributeComponent.h"
#include "SPlayerState.h"
#include "AI/SAICharacterBase.h"

ACPP_SkillBoxGameMode::ACPP_SkillBoxGameMode()
{
	PlayerControllerClass = ACPP_SkillBoxPlayerController::StaticClass();
	DefaultPawnClass = ACPP_SkillBoxCharacter::StaticClass();
	HUDClass = APlayerHUD::StaticClass();
	PlayerStateClass = ASPlayerState::StaticClass();
}

void ACPP_SkillBoxGameMode::StartPlay()
{
	Super::StartPlay();

	GetWorldTimerManager().SetTimer(
		TimerHandle_SpawnBot,
		this,
		&ACPP_SkillBoxGameMode::SpawnBotTimerElapsed,
		SpawnTimerInterval,
		true);

	GetWorldTimerManager().SetTimer(
		TimerHandle_SpawnBomb,
		this,
		&ACPP_SkillBoxGameMode::SpawnBombTimerElapsed,
		FMath::FRandRange(SpawnBombTimerIntervalMin, SpawnBombTimerIntervalMax),
		true);

	if (ensure(ChestClasses.Num() > 0))
	{
		// Run EQS to find potential power-up spawn locations
		UEnvQueryInstanceBlueprintWrapper* QueryInstance = UEnvQueryManager::RunEQSQuery(
			this, PowerupSpawnQuery, this, EEnvQueryRunMode::AllMatching, nullptr);
		if (ensure(QueryInstance))
		{
			QueryInstance->GetOnQueryFinishedEvent().AddDynamic(
				this, &ACPP_SkillBoxGameMode::OnPowerupSpawnQueryCompleted);
		}
	}
}

void ACPP_SkillBoxGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	if (ACPP_SkillBoxPlayerController* PC = Cast<ACPP_SkillBoxPlayerController>(NewPlayer))
	{
		Players.Add(PC);
	}
	if (GameState)
	{
		int32 NumberOfPlayers = GameState.Get()->PlayerArray.Num();

		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(
				-1,
				60.f,
				FColor::Yellow,
				FString::Printf(TEXT("Player in Game: %d "), NumberOfPlayers)
			);

			APlayerState* PlayerState = NewPlayer->GetPlayerState<APlayerState>();
			if (PlayerState)
			{
				FString PlayerName = PlayerState->GetPlayerName();

				GEngine->AddOnScreenDebugMessage(
					-1,
					60.f,
					FColor::Cyan,
					FString::Printf(TEXT("%s has Join the Game "), *PlayerName)
				);
			}
		}
	}
}

void ACPP_SkillBoxGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);
	if (ACPP_SkillBoxPlayerController* PC = Cast<ACPP_SkillBoxPlayerController>(Exiting))
	{
		Players.Remove(PC);
	}
	APlayerState* PlayerState = Exiting->GetPlayerState<APlayerState>();
	if (PlayerState)
	{
		int32 NumberOfPlayers = GameState.Get()->PlayerArray.Num();
		GEngine->AddOnScreenDebugMessage(
			-1,
			60.f,
			FColor::Yellow,
			FString::Printf(TEXT("Player in Game: %d "), NumberOfPlayers - 1)
		);

		FString PlayerName = PlayerState->GetPlayerName();

		GEngine->AddOnScreenDebugMessage(
			-1,
			60.f,
			FColor::Cyan,
			FString::Printf(TEXT("%s has Exited the Game "), *PlayerName)
		);
	}
}

void ACPP_SkillBoxGameMode::RestartPlayer(AController* NewPlayer)
{
	if (IsValid(NewPlayer))
	{
		AActor* StartSpot = nullptr;
		ACPP_SkillBoxPlayerController* PC = Cast<ACPP_SkillBoxPlayerController>(NewPlayer);
		if (PC)
		{
			TArray<APlayerStart*> StartSpotsWithTag;
			for (TActorIterator<APlayerStart> It(GetWorld()); It; ++It)
			{
				APlayerStart* PS = *It;
				if (PS && PS->PlayerStartTag == PC->PlayerStartTag)
				{
					StartSpotsWithTag.Add(PS);
				}
			}
			if (StartSpotsWithTag.Num() > 0)
			{
				StartSpot = StartSpotsWithTag[FMath::RandRange(0, StartSpotsWithTag.Num() - 1)];
			}
		}
		if (StartSpot == nullptr)
		{
			if (NewPlayer->StartSpot != nullptr)
			{
				StartSpot = NewPlayer->StartSpot.Get();
				UE_LOG(LogGameMode, Warning, TEXT("RestartPlayer: Player start not found, using last start spot"));
			}
		}
		RestartPlayerAtPlayerStart(NewPlayer, StartSpot);
	}
}

void ACPP_SkillBoxGameMode::SpawnBotTimerElapsed()
{
	int32 NrOfAliveBots = 0;
	for (TActorIterator<ASAICharacterBase> It(GetWorld()); It; ++It)
	{
		const USAttributeComponent* AttributeComp = It->GetAttributeComp();
		if (AttributeComp && AttributeComp->IsAlive())
		{
			NrOfAliveBots++;
		}
	}

	float MaxBotCount = 10.f;
	if (DifficultyCurve)
	{
		MaxBotCount = DifficultyCurve->GetFloatValue(GetWorld()->TimeSeconds);
	}

	if (NrOfAliveBots >= MaxBotCount)
	{
		return;
	}

	UEnvQueryInstanceBlueprintWrapper* QueryInstance = UEnvQueryManager::RunEQSQuery(
		this, SpawnBotQuery, this, EEnvQueryRunMode::RandomBest5Pct, nullptr);
	if (ensure(QueryInstance))
	{
		QueryInstance->GetOnQueryFinishedEvent().AddDynamic(this, &ACPP_SkillBoxGameMode::OnQueryCompleted);
	}
}

void ACPP_SkillBoxGameMode::SpawnBombTimerElapsed()
{
	UEnvQueryInstanceBlueprintWrapper* QueryInstance = UEnvQueryManager::RunEQSQuery(
		this, SpawnBombQuery, this, EEnvQueryRunMode::RandomBest5Pct, nullptr);
	if (ensure(QueryInstance))
	{
		QueryInstance->GetOnQueryFinishedEvent().AddDynamic(this, &ACPP_SkillBoxGameMode::OnQueryCompletedForBomb);
	}
}

void ACPP_SkillBoxGameMode::OnQueryCompletedForBomb(UEnvQueryInstanceBlueprintWrapper* QueryInstance,
                                                    EEnvQueryStatus::Type QueryStatus)
{
	if (QueryStatus != EEnvQueryStatus::Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawn Bomb EQS Query Failed!"))
		return;
	}

	TArray<FVector> Location = QueryInstance->GetResultsAsLocations();
	if (Location.IsValidIndex(0))
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.ObjectFlags = RF_Transient;

		GetWorld()->SpawnActor<AActor>(AirBombSpawnerClass, Location[0] + FVector(0.0f, 0.0f, 1000.f),
		                               FRotator::ZeroRotator, SpawnParams);
		DrawDebugSphere(GetWorld(), Location[0], 128, 12, FColor::Cyan, false, 10.0f);
	}
}

void ACPP_SkillBoxGameMode::OnQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance,
                                             EEnvQueryStatus::Type QueryStatus)
{
	if (QueryStatus != EEnvQueryStatus::Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawn Bot EQS Query Failed!"))
		return;
	}

	TArray<FVector> Location = QueryInstance->GetResultsAsLocations();
	if (Location.IsValidIndex(0))
	{
		GetWorld()->SpawnActor<AActor>(MinionClass, Location[0] + FVector(0.0f, 0.0f, 50.f), FRotator::ZeroRotator);
	}
}

void ACPP_SkillBoxGameMode::OnPowerupSpawnQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance,
                                                         EEnvQueryStatus::Type QueryStatus)
{
	if (QueryStatus != EEnvQueryStatus::Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawn Item EQS Query failed!"))
		return;
	}

	TArray<FVector> Locations = QueryInstance->GetResultsAsLocations();

	TArray<FVector> UsedLocation;

	int32 SpawnCounter = 0;

	while (SpawnCounter < DesiredPowerupCount && Locations.Num() > 0)
	{
		int32 RandomLocationIndex = FMath::RandRange(0, Locations.Num() - 1);

		FVector PickedLocation = Locations[RandomLocationIndex];

		Locations.RemoveAt(RandomLocationIndex);

		bool bValidLocations = true;
		for (FVector OtherLocation : UsedLocation)
		{
			float DistanceTo = (PickedLocation - OtherLocation).Size();

			if (DistanceTo < RequiredPowerupDistance)
			{
				bValidLocations = false;
				break;
			}
		}

		if (!bValidLocations)
		{
			continue;
		}

		int32 RandomClassIndex = FMath::RandRange(0, ChestClasses.Num() - 1);
		TSubclassOf<AActor> RandomPowerupClass = ChestClasses[RandomClassIndex];

		FActorSpawnParameters SpawnParam;
		SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		GetWorld()->SpawnActor<AActor>(RandomPowerupClass, PickedLocation, FRotator::ZeroRotator, SpawnParam);

		UsedLocation.Add(PickedLocation);
		SpawnCounter++;
	}
}


void ACPP_SkillBoxGameMode::RespawnPlayerElapsed(AController* Controller)
{
	if (ensure(Controller))
	{
		Controller->UnPossess();
		RestartPlayer(Controller);
	}
}

void ACPP_SkillBoxGameMode::SpawnCard(UClass* FlashCardClass, FVector Location)
{
	GetWorld()->SpawnActor(FlashCardClass,&Location);
}

void ACPP_SkillBoxGameMode::OnActorKilled(AActor* VictimActor, AActor* Killer,
                                          ACPP_SkillBoxPlayerController* KillerController)
{
	ACPP_SkillBoxCharacter* VictimCharacter = Cast<ACPP_SkillBoxCharacter>(VictimActor);
	ACPP_SkillBoxCharacter* KillerCharacter = Cast<ACPP_SkillBoxCharacter>(Killer);

	if (VictimCharacter)
	{
		if (VictimCharacter->bIsPlayer)
		{
			FTimerHandle TimerHandle_RespawnDelay;

			FTimerDelegate Delegate;
			Delegate.BindUFunction(this, "RespawnPlayerElapsed", VictimCharacter->GetController());

			float RespawnDelay = 3.0f;
			GetWorldTimerManager().SetTimer(TimerHandle_RespawnDelay, Delegate, RespawnDelay, false);
		}
	}

	if (KillerCharacter)
	{
		UInventoryComponent* KillerInventory = KillerCharacter->GetInventoryComponent();
		if (KillerInventory)
		{
			KillerInventory->AddCredits(CreditPerKill);
		}
	}
}

void ACPP_SkillBoxGameMode::SetNewPlayerStartTagForAllPlayers(FName PlayerStart)
{
	for (auto Player : Players)
	{
		Player->PlayerStartTag = PlayerStart;
	}
}

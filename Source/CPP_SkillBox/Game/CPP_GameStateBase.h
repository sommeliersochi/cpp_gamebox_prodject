#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "CPP_GameStateBase.generated.h"

UENUM(BlueprintType)
enum class EGameState : uint8
{
	Phase1 UMETA(DisplayName = "Phase1"),
	Phase2 UMETA(DisplayName = "Phase2"),
	Phase3 UMETA(DisplayName = "Phase3"),
	Phase4 UMETA(DisplayName = "Phase4"),
	Phase5 UMETA(DisplayName = "Phase5")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnUpdateWidget, EGameState, State, int32, Items, int32, MaxItems);

UCLASS()
class CPP_SKILLBOX_API ACPP_GameStateBase : public AGameStateBase
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, Replicated)
	EGameState CurrentState = EGameState::Phase1;
	UPROPERTY(BlueprintReadWrite, Replicated)
	int32 CurrentItems = 0;
	UPROPERTY(BlueprintReadWrite, Replicated)
	int32 MaxItems = 3;
	UPROPERTY(BlueprintAssignable)
	FOnUpdateWidget OnUpdateWidget;
	UFUNCTION(BlueprintCallable, NetMulticast, Reliable)
	void UpdateWidget_Multicast(EGameState State, int32 Items);
	UFUNCTION(BlueprintCallable, Server, Reliable)
	void IncrementCurrentItem_OnServer(int32 Delta = 1);
	UFUNCTION(BlueprintCallable, Server, Reliable)
	void ChangeState_OnServer(EGameState State, int32 MaxItem);
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};


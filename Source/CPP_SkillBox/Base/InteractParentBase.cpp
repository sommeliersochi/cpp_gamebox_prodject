#include "CPP_SkillBox/Base/InteractParentBase.h"

#include <Components/DecalComponent.h>
#include <Kismet/GameplayStatics.h>
#include <Kismet/KismetMathLibrary.h>
#include <Net/UnrealNetwork.h>
#include <EngineUtils.h>

#include "CPP_SkillBoxCharacter.h"
#include "CPP_SkillBoxGameMode.h"
#include "SAttributeComponent.h"
#include "Inventory/InventoryComponent.h"
#include "BaseChest.h"
#include "AI/SAICharacterBase.h"

AInteractParentBase::AInteractParentBase()
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	SetRootComponent(Sphere);
	Sphere->SetCollisionResponseToAllChannels(ECR_Ignore);
	Sphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Sphere->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	Sphere->OnComponentBeginOverlap.AddDynamic(this, &AInteractParentBase::SphereBeginOverlap);
	Sphere->OnComponentEndOverlap.AddDynamic(this, &AInteractParentBase::SphereEndOverlap);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);

	InputComponent = CreateDefaultSubobject<UInputComponent>(TEXT("InputComponent"));
}

void AInteractParentBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AInteractParentBase, Faction);
	DOREPLIFETIME(AInteractParentBase, bIsMenuOpen);
	DOREPLIFETIME(AInteractParentBase, OwnerCharacter);
}

void AInteractParentBase::PrimaryInteractPressed()
{
	UE_LOG(LogTemp, Warning, TEXT("Base_InputPressed"));
}

void AInteractParentBase::BeginPlay()
{
	Super::BeginPlay();

	InputComponent->BindAction(TEXT("PrimaryInteract"), IE_Pressed, this, &AInteractParentBase::PrimaryInteractPressed);
	InputComponent->BindAction(TEXT("PrimaryInteract"), IE_Released, this,
	                           &AInteractParentBase::PrimaryInteractReleased);
}

void AInteractParentBase::PrimaryInteractReleased()
{
	RunCheckOverlapTimer_OnServer();
	ACPP_SkillBoxCharacter* PlayerCharacter = Cast<ACPP_SkillBoxCharacter>(GetOwner());
	if (PlayerCharacter)
	{
		PlayerCharacter->PrimaryInteractReleased();
		UE_LOG(LogTemp, Warning, TEXT("Base_InputReleased"));
		DisableInput(PlayerCharacter->GetLocalViewingPlayerController());
	}
	SetOwner(nullptr);
	OwnerCharacter = nullptr;
}

void AInteractParentBase::CheckOverlapActors()
{
	TArray<AActor*> PlayerCharacters;
	Sphere->GetOverlappingActors(PlayerCharacters, ACPP_SkillBoxCharacter::StaticClass());
	for (int i = 0; i < PlayerCharacters.Num(); ++i)
	{
		ACPP_SkillBoxCharacter* PlayerCharacter = Cast<ACPP_SkillBoxCharacter>(PlayerCharacters[i]);
		if (PlayerCharacter && PlayerCharacter->bIsPlayer && PlayerCharacter->bIsTryAnnex)
		{
			GetWorldTimerManager().ClearTimer(CheckOverlapActorsTimer);
			SetOwner(PlayerCharacter);
			OwnerCharacter = PlayerCharacter;

			if (PlayerCharacter == GetOwner())
			{
				EnableInput(PlayerCharacter->GetLocalViewingPlayerController());
				PrimaryInteractPressed();
			}
			else
			{
				RunCheckOverlapTimer_Multi();
			}
			return;
		}
		else if (PlayerCharacter && PlayerCharacter->bIsPlayer && !PlayerCharacter->bIsTryAnnex)
		{
			SetOwner(nullptr);
		}
	}
	if (DebugTimer)
	{
		if (HasAuthority())
		{
			UE_LOG(LogTemp, Warning, TEXT("EnableTimer_Server"));
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("EnableTimer_Client"));
		}
	}
	if (PlayerCharacters.Num() == 0)
		GetWorldTimerManager().ClearTimer(CheckOverlapActorsTimer);
}

void AInteractParentBase::RunCheckOverlapTimer_OnServer_Implementation() { RunCheckOverlapTimer_Multi(); }

void AInteractParentBase::RunCheckOverlapTimer_Multi_Implementation()
{
	GetWorldTimerManager().SetTimer(CheckOverlapActorsTimer, this, &AInteractParentBase::CheckOverlapActors,
	                                GetWorld()->GetDeltaSeconds(), true);
}

void AInteractParentBase::StopCheckOverlapTimer_OnServer_Implementation() { StopCheckOverlapTimer_OnMulti(); }

void AInteractParentBase::StopCheckOverlapTimer_OnMulti_Implementation()
{
	GetWorldTimerManager().ClearTimer(CheckOverlapActorsTimer);
}

void AInteractParentBase::CreateInteractWidget(ACPP_SkillBoxCharacter* OwnerPlayer)
{
	if (OwnerPlayer->CurrentInteractWidget)
	{
		OwnerPlayer->CurrentInteractWidget->RemoveFromParent();
	}
	OwnerPlayer->CreateInteractBaseWidget(InteractWidgetClass.LoadSynchronous());
	if (OwnerPlayer->CurrentInteractWidget)
	{
		if (Faction == EFaction::Enemy)
		{
			OwnerPlayer->CurrentInteractWidget->UpdateSpecification("Press To Annexation Base");
			OwnerPlayer->CurrentInteractWidget->AddToViewport();
		}
	}
}

void AInteractParentBase::SphereBeginOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
                                             UPrimitiveComponent* PrimitiveComponent1, int I, bool bArg,
                                             const FHitResult& HitResult)
{
	ACPP_SkillBoxCharacter* OverlapCharacter = Cast<ACPP_SkillBoxCharacter>(Actor);
	if (OverlapCharacter && OverlapCharacter->bIsPlayer)
	{
		CreateInteractWidget(OverlapCharacter);
		GetWorldTimerManager().SetTimer(CheckOverlapActorsTimer, this, &AInteractParentBase::CheckOverlapActors,
		                                GetWorld()->GetDeltaSeconds(), true);
	}
}

void AInteractParentBase::SphereEndOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
                                           UPrimitiveComponent* PrimitiveComponent1, int I)
{
	ACPP_SkillBoxCharacter* OverlapCharacter = Cast<ACPP_SkillBoxCharacter>(Actor);

	if (OverlapCharacter && OverlapCharacter->bIsPlayer)
	{
		DisableInput(OverlapCharacter->GetLocalViewingPlayerController());
		if (OverlapCharacter->CurrentInteractWidget)
			OverlapCharacter->CurrentInteractWidget->CloseWidget();
	}
	if (OverlapCharacter == OwnerCharacter)
	{
		GetWorldTimerManager().SetTimer(CheckOverlapActorsTimer, this, &AInteractParentBase::CheckOverlapActors,
		                                GetWorld()->GetDeltaSeconds(), true);
		SetOwner(nullptr);
		OwnerCharacter = nullptr;

		TArray<AActor*> PlayerCharacters;
		Sphere->GetOverlappingActors(PlayerCharacters, ACPP_SkillBoxCharacter::StaticClass());
		if (PlayerCharacters.Num() == 0)
		{
			GetWorldTimerManager().ClearTimer(CheckOverlapActorsTimer);
			SetOwner(nullptr);
			OwnerCharacter = nullptr;
		}
	}
}

//Base====================================================================================
//========================================================================================
ABase::ABase()
{
	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("TimerWidget"));
	WidgetComponent->SetupAttachment(RootComponent);
}

void ABase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ABase, BeginAnnex);
	DOREPLIFETIME(ABase, CanAnnex);
	DOREPLIFETIME(ABase, CurrentAnnexTime);
	DOREPLIFETIME(ABase, CurrentActorsToSpawn);
	DOREPLIFETIME(ABase, AIActors);
}

void ABase::RunAnnexTimer_OnServer_Implementation()
{
	RunAnnexTimer_Multi();
	GetWorldTimerManager().SetTimer(AnnexTimer, this, &ABase::AnnexProgress, GetWorld()->GetDeltaSeconds(), true);
}

void ABase::RunAnnexTimer_Multi_Implementation()
{
	WidgetComponent->SetVisibility(true);
	ChangeInteractWidgetForAllOverlapActor_Multicast(false);
}

void ABase::UpdateWidget_Multicast_Implementation()
{
	ProgressWidget->SetTime(CurrentAnnexTime, TimeToAnnexBase);
}

void ABase::StopAnnexTimer_OnServer_Implementation()
{
	StopAnnexTimer_Multicast();
}

void ABase::StopAnnexTimer_Multicast_Implementation()
{
	GetWorldTimerManager().ClearTimer(AnnexTimer);
	CurrentAnnexTime = TimeToAnnexBase;
	UpdateWidget_Multicast();
	if (Faction == EFaction::Enemy)
		ChangeInteractWidgetForAllOverlapActor_Multicast(true);
	WidgetComponent->SetVisibility(false);
}

void ABase::RotateWidget_Multicast_Implementation()
{
	if (ProgressWidget->GetOwningPlayerCameraManager())
	{
		FRotator NewRot = UKismetMathLibrary::FindLookAtRotation(WidgetComponent->GetComponentLocation(),
		                                                         ProgressWidget->GetOwningPlayerCameraManager()->
		                                                         GetCameraLocation());
		NewRot = FRotator(0.f, NewRot.Yaw, 0.f);
		WidgetComponent->SetWorldRotation(NewRot);
	}
}

void ABase::AnnexProgress()
{
	CurrentAnnexTime -= GetWorld()->GetDeltaSeconds();
	if (CurrentAnnexTime <= 0)
	{
		StopAnnexTimer_OnServer();
		SetBeginAnnex_OnServer(false);
		SetFaction_OnServer(EFaction::Friendly);
	}
	if (!GetOwner())
	{
		StopAnnexTimer_OnServer();
		SetBeginAnnex_OnServer(false);
	}
	UpdateWidget_Multicast();
	RotateWidget_Multicast();
}

void ABase::ChangeInteractWidgetForAllOverlapActor_Multicast_Implementation(bool On) const
{
	TArray<AActor*> PlayerCharacters;
	Sphere->GetOverlappingActors(PlayerCharacters, ACPP_SkillBoxCharacter::StaticClass());
	if (PlayerCharacters.Num() > 0)
	{
		for (AActor* Actor : PlayerCharacters)
		{
			ACPP_SkillBoxCharacter* PlayerCharacter = Cast<ACPP_SkillBoxCharacter>(Actor);
			if (On)
			{
				if (Faction == EFaction::Enemy)
				{
					if (PlayerCharacter->CurrentInteractWidget)
					{
						PlayerCharacter->CurrentInteractWidget->UpdateSpecification("Press To Annexation Base");
					}
					else
					{
						PlayerCharacter->CreateInteractBaseWidget(InteractWidgetClass.LoadSynchronous());
					}
					if (PlayerCharacter->CurrentInteractWidget && !PlayerCharacter->CurrentInteractWidget->
						IsInViewport())
						PlayerCharacter->CurrentInteractWidget->AddToViewport();
				}
			}
			else
			{
				if (PlayerCharacter && PlayerCharacter->CurrentInteractWidget)
					PlayerCharacter->CurrentInteractWidget->CloseWidget();
			}
		}
	}
}

void ABase::BeginPlay()
{
	Super::BeginPlay();
	ProgressWidget = Cast<UAnnexProgressBarWidget>(WidgetComponent->GetWidget());
	if (HasAuthority())
	{
		RunTimersSpawnAI();
	}
	for (TActorIterator<ABaseChest> It(GetWorld()); It; ++It)
	{
		ABaseChest* Chest = *It;
		if (Chest && Chest->ActorHasTag(BaseTagForRespawn))
		{
			Chest->SetOwner(this);
		}
	}
}

void ABase::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	CurrentActorsToSpawn = AdditionalActorsToSpawn;
	CurrentAnnexTime = TimeToAnnexBase;
}

void ABase::PointDistribution(ASAICharacterBase* AICharacter)
{
	if (Cast<AAICharacterGuardian>(AICharacter))
	{
		int i = 0;
		AIGuardianActors.Add(AICharacter);
		AIGuardianActors.Find(AICharacter,i);
		if (AIGuardianSpawn.IsValidIndex(i))
		{
			AICharacter->PatrolLocation = GetActorLocation() + AIGuardianSpawn[i];
		}
		else
		{
			if (AIPatrolSpawn.Num() > 0)
			{
				AICharacter->PatrolLocation = GetActorLocation() + AIPatrolSpawn[FMath::RandRange(0,AIPatrolSpawn.Num() - 1)];
			}
		}
		return;
	}
	if (Cast<AAICharacterPatrol>(AICharacter))
	{
		int i = 0;
		AIPatrolActors.Add(AICharacter);
		AIPatrolActors.Find(AICharacter,i);
		if (AIPatrolSpawn.IsValidIndex(i))
		{
			AICharacter->PatrolLocation = GetActorLocation() + AIPatrolSpawn[i];
		}
		else
		{
			if (AIPatrolSpawn.Num() > 0)
			{
				AICharacter->PatrolLocation = GetActorLocation() + AIPatrolSpawn[FMath::RandRange(0,AIPatrolSpawn.Num() - 1)];
			}
		}
	}
}

void ABase::SpawnAIChasing()
{
	FVector Loc = GetActorLocation() + AIAdditionalPoint;
	SpawnAIFromClass(PatrolClass.LoadSynchronous(), FTransform(Loc));
}

void ABase::SpawnAIGuardian()
{
	FVector Loc = GetActorLocation() + AIAdditionalPoint;
	SpawnAIFromClass(GuardianClass.LoadSynchronous(), FTransform(Loc));
}

void ABase::SpawnAIFromClass(UClass* Class, FTransform Transform)
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	ASAICharacterBase* AIChar = GetWorld()->SpawnActorDeferred<ASAICharacterBase>(Class, Transform, nullptr, nullptr, SpawnParameters.SpawnCollisionHandlingOverride);
	//ASAICharacterBase* AIChar = GetWorld()->SpawnActor<ASAICharacterBase>(Class, Transform, SpawnParameters);
	AIChar->UpdateFaction(Faction);
	UGameplayStatics::FinishSpawningActor(AIChar,Transform);
	AIActors.Add(AIChar);
	PointDistribution(AIChar);
	AIChar->GetAttributeComp()->OnDeath.AddDynamic(this, &ABase::RespawnAIOnDeath);
}

void ABase::RunTimersSpawnAI()
{
	TArray<FTimerHandle> ArrayTimersChasing;
	TArray<FTimerHandle> ArrayTimersGuardian;
	float Rate = 0.01f;
	for (int i = 0; i < AIPatrolSpawn.Num(); i++)
	{
		ArrayTimersChasing.Emplace();
		GetWorldTimerManager().SetTimer(ArrayTimersChasing[i], this, &ABase::SpawnAIChasing, Rate, false);
		Rate += SpawnRate;
	}
	for (int i = 0; i < AIGuardianSpawn.Num(); i++)
	{
		ArrayTimersGuardian.Emplace();
		GetWorldTimerManager().SetTimer(ArrayTimersGuardian[i], this, &ABase::SpawnAIGuardian, Rate, false);
		Rate += SpawnRate;
	}
}

void ABase::RespawnAIOnDeath(AActor* Actor, AActor* Actor1)
{
	if (HasAuthority())
	{
		ASAICharacterBase* AIChar = Cast<ASAICharacterBase>(Actor);
		if (AIChar)
		{
			if (Cast<AAICharacterGuardian>(AIChar))
			{
				AIGuardianActors.Remove(AIChar);
			}
			if (Cast<AAICharacterPatrol>(AIChar))
			{
				AIPatrolActors.Remove(AIChar);
			}
			AIActors.Remove(AIChar);
			
			SpawnAdditionalAI();
			if (AIActors.Num() == 0)
			{
				CanAnnex = true;
			}
		}
	}
}

void ABase::SpawnAdditionalAI()
{
	if (CurrentActorsToSpawn > 0)
	{
		FVector Loc = GetActorLocation() + AIAdditionalPoint;
		UClass* AIClass = nullptr;
		if (FMath::RandBool())
		{
			AIClass = PatrolClass.LoadSynchronous();
		}
		else
		{
			AIClass = GuardianClass.LoadSynchronous();
		}
		SpawnAIFromClass(AIClass, FTransform(Loc));
		CurrentActorsToSpawn --;
	}
}

//BeginAnnexChange
void ABase::SetBeginAnnex_OnServer_Implementation(bool Value) { SetBeginAnnex_Multicast(Value); }
void ABase::SetBeginAnnex_Multicast_Implementation(bool Value) { BeginAnnex = Value; }
//CanAnnexChange
void ABase::SetCanAnnex_OnServer_Implementation(bool Value) { SetCanAnnex_Multicast(Value); }
void ABase::SetCanAnnex_Multicast_Implementation(bool Value) { CanAnnex = Value; }
//FactionChange
void ABase::SetFaction_OnServer_Implementation(EFaction NewFaction)
{
	Faction = NewFaction;
	AnnexEnd();
	RunTimersSpawnAI();
	CurrentActorsToSpawn = AdditionalActorsToSpawn;
}

void ABase::PrimaryInteractPressed()
{
	Super::PrimaryInteractPressed();
	if (Faction != EFaction::Friendly && CanAnnex)
	{
		if (!BeginAnnex)
		{
			RunAnnexTimer_OnServer();
			SetBeginAnnex_OnServer(true);
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("Base_InputPressed"));
}

void ABase::PrimaryInteractReleased()
{
	Super::PrimaryInteractReleased();

	if (Faction != EFaction::Friendly)
	{
		SetBeginAnnex_OnServer(false);
		SetCanAnnex_OnServer(true);
	}
	StopAnnexTimer_OnServer();
}

void ABase::AnnexEnd()
{
	ACPP_SkillBoxGameMode* GameMode = Cast<ACPP_SkillBoxGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (GameMode)
	{
		if (CanChangeRespawn)
		{
			GameMode->SetNewPlayerStartTagForAllPlayers(BaseTagForRespawn);
		}
		if (FlashCardClass)
		{
			GameMode->SpawnCard(FlashCardClass, FlashCardLocation + GetActorLocation());
		}
	}
	for (TActorIterator<ABaseChest> It(GetWorld()); It; ++It)
	{
		ABaseChest* Chest = *It;
		if (Chest && Chest->ActorHasTag(BaseTagForRespawn))
		{
			Chest->ChangeCollisionForInteract_Multicast();
		}
	}
}

void ABase::SphereBeginOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
                               UPrimitiveComponent* PrimitiveComponent1, int I, bool bArg, const FHitResult& HitResult)
{
	if (CanAnnex)
	{
		Super::SphereBeginOverlap(PrimitiveComponent, Actor, PrimitiveComponent1, I, bArg, HitResult);
	}
}

void ABase::SphereEndOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
                             UPrimitiveComponent* PrimitiveComponent1, int I)
{
	Super::SphereEndOverlap(PrimitiveComponent, Actor, PrimitiveComponent1, I);

	const ACPP_SkillBoxCharacter* OverlapCharacter = Cast<ACPP_SkillBoxCharacter>(Actor);
	if (OverlapCharacter && OverlapCharacter->bIsPlayer)
	{
		if (Faction != EFaction::Friendly)
		{
			SetBeginAnnex_OnServer(false);
		}
		StopAnnexTimer_OnServer();
	}
}


//Barrier=================================================================================
//========================================================================================
ABarrier::ABarrier()
{
	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("TimerWidget"));
	WidgetComponent->SetupAttachment(RootComponent);

	HealthComponent = CreateDefaultSubobject<USAttributeComponent>(TEXT("AttributeComponent"));
	HealthComponent->OnHealthChanged.AddDynamic(this, &ABarrier::HealthChange);

	bIsBarrier = true;

	for (int i = 0; i < 4; i++)
	{
		Levels.Add(BarrierInfo);
	}
}

void ABarrier::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ABarrier, CurrentLevel);
	DOREPLIFETIME(ABarrier, HealthStatus);
	DOREPLIFETIME(ABarrier, CurrentHP);
}


void ABarrier::BeginPlay()
{
	Super::BeginPlay();
	HealtBarWidget = Cast<UAnnexProgressBarWidget>(WidgetComponent->GetWidget());

	UpdateLevel();
	AppearAnimIn();
}

void ABarrier::HealthChange(AActor* InstigatorActor, USAttributeComponent* OwningComp, float NewHealth, float Damage)
{
	UpdateBarrierState_OnServer();
}

void ABarrier::UpdateLevel()
{
	if (Levels.Num() > 0 && Levels.IsValidIndex(CurrentLevel))
	{
		if (Levels[CurrentLevel].Mesh &&
			Levels[CurrentLevel].MaterialDamaged &&
			Levels[CurrentLevel].MeshDamaged &&
			Levels[CurrentLevel].MeshDestroyed)
		{
			StaticMesh->SetStaticMesh(Levels[CurrentLevel].Mesh);
			StaticMesh->SetMaterial(0, Levels[CurrentLevel].Mesh->GetMaterial(0));
			HealthComponent->SetMaxHealth(Levels[CurrentLevel].MaxHP);
			float HP = HealthComponent->GetHealthMax() - HealthComponent->GetHealth();
			HealthComponent->ApplyHealthChange(this, HP);
		}
	}
}

void ABarrier::AppearAnimIn()
{
	Materials.Empty();
	Materials.Add(Cast<UMaterial>(StaticMesh->GetMaterial(0)));
	StaticMesh->SetMaterial(0, nullptr); //WTF??????????
	FTimerHandle Timer;
	GetWorldTimerManager().SetTimer(Timer, this, &ABarrier::AppearAnimOut, 0.1f, false);
}

void ABarrier::AppearAnimOut()
{
	if (Materials.Num() > 0 && Materials.IsValidIndex(0) && Materials[0])
		StaticMesh->SetMaterial(0, Materials[0]);
}

bool ABarrier::HaveCredits(int32 Price, int32& SubstractCredits)
{
	if (OwnerCharacter)
	{
		const UInventoryComponent* CharacterInventory = OwnerCharacter->GetInventoryComponent();
		return CharacterInventory && CharacterInventory->HasCredits(Price);
	}
	return false;
}

void ABarrier::SetCredits(float Substract)
{
	if (OwnerCharacter)
	{
		UInventoryComponent* CharacterInventory = OwnerCharacter->GetInventoryComponent();
		if (CharacterInventory)
		{
			CharacterInventory->RemoveCredits(Substract);
		}
	}
}

void ABarrier::Repair(FName& Result)
{
	int32 SubstractValue;
	if (HaveCredits(BarrierInfo.MaxHP - CurrentHP, SubstractValue))
	{
		HealthComponent->ApplyHealthChange(this, HealthComponent->GetHealthMax());
		SetCredits(BarrierInfo.MaxHP - CurrentHP);
		Result = "";
	}
	else
	{
		Result = "You can't afford it";
	}
}

void ABarrier::MeshUpdate()
{
	if (StaticMesh && BarrierInfo.Mesh)
	{
		switch (HealthStatus)
		{
		case EBarrierState::FullHealth:
			StaticMesh->SetStaticMesh(BarrierInfo.Mesh);
			break;
		case EBarrierState::Damaged:
			StaticMesh->SetStaticMesh(BarrierInfo.Mesh);
			StaticMesh->SetMaterial(0, BarrierInfo.MaterialDamaged);
			break;
		case EBarrierState::MeshDamaged:
			StaticMesh->SetStaticMesh(BarrierInfo.MeshDamaged);
			break;
		case EBarrierState::Destroyed:
			StaticMesh->SetStaticMesh(BarrierInfo.MeshDestroyed);
			break;
		default: ;
		}
	}
}

void ABarrier::UpdateBarrierState_OnServer_Implementation() { UpdateBarrierState_Multicast(); }
void ABarrier::UpdateBarrierState_Multicast_Implementation() { UpdateBarrierState(); }

void ABarrier::UpdateBarrierState()
{
	EBarrierState CurrentStatus = HealthStatus;
	if (HealthComponent->GetHealth() < HealthComponent->GetHealthMax() * 0.6f)
	{
		HealthStatus = EBarrierState::Damaged;
		if (HealthComponent->GetHealth() < HealthComponent->GetHealthMax() * 0.25f)
		{
			HealthStatus = EBarrierState::MeshDamaged;
			if (HealthComponent->GetHealth() < HealthComponent->GetHealthMax() * 0.05f)
			{
				HealthStatus = EBarrierState::Destroyed;
			}
		}
		if (HealthStatus != CurrentStatus)
		{
			MeshUpdate();
		}
	}
	else
	{
		HealthStatus = EBarrierState::FullHealth;
		MeshUpdate();
	}
}

bool ABarrier::CanUpgradeBarrier(FName& Result, int32& Price)
{
	if (Levels.IsValidIndex(CurrentLevel + 1))
	{
		if (CurrentLevel < Levels.Num()) //CurrentLevel < MaxLevel //in process
		{
			if (HealthComponent->GetHealth() == HealthComponent->GetHealthMax())
			{
				int32 SubstractCredits;
				if (HaveCredits(Levels[CurrentLevel + 1].Price, SubstractCredits))
				{
					Result = "Upgraded";
					Price = Levels[CurrentLevel + 1].Price;
					return true;
				}
				else
				{
					Result = "Not enough money";
					Price = 0;
					return false;
				}
			}
			else
			{
				Result = "Barrier Damaged";
				Price = 0;
				return false;
			}
		}
		else
		{
			Result = "You should upgrade base first";
			Price = 0;
			return false;
		}
	}
	else
	{
		Result = "Max";
		Price = 0;
		return false;
	}
}

void ABarrier::UpgradeBarrier_OnServer_Implementation() { UpgradeBarrier_Multicast(); }
void ABarrier::UpgradeBarrier_Multicast_Implementation() { UpgradeBarrier(); }

void ABarrier::UpgradeBarrier()
{
	FName Result;
	int32 Price = 0;
	if (CanUpgradeBarrier(Result, Price))
	{
		SetCredits(Price);
		CurrentLevel++;
		UpdateLevel();
		AppearAnimIn();
	}
}

bool ABarrier::CanRestore(FName& Result)
{
	if (HealthComponent->GetHealth() == HealthComponent->GetHealthMax())
	{
		Result = "NOT DAMAGED";
		return false;
	}
	else
	{
		int32 SubstractValue;
		int32 Value = UKismetMathLibrary::FTrunc(HealthComponent->GetHealth() - HealthComponent->GetHealthMax());
		if (HaveCredits(Value, SubstractValue))
		{
			Result = "REPAIRED";
			return true;
		}
		else
		{
			Result = "YOU CAN'T AFFORD IT";
			return false;
		}
	}
}

bool ABarrier::CanAddWeapon(FName& Result)
{
	int32 SubstractValue;
	if (HaveCredits(WeaponPrice, SubstractValue))
	{
		Result = "PURCHAISED";
		return true;
	}
	else
	{
		Result = "Not enough money";
		return false;
	}
}

void ABarrier::AddWeapon()
{
}

void ABarrier::SpawnBomb()
{
}

bool ABarrier::CanAddBomb(FName& Result, int32& Quantity)
{
	return false;
}

void ABarrier::PrimaryInteractPressed()
{
	Super::PrimaryInteractPressed();
}

void ABarrier::PrimaryInteractReleased()
{
	Super::PrimaryInteractReleased();
}

void ABarrier::SphereBeginOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
                                  UPrimitiveComponent* PrimitiveComponent1, int I, bool bArg,
                                  const FHitResult& HitResult)
{
	Super::SphereBeginOverlap(PrimitiveComponent, Actor, PrimitiveComponent1, I, bArg, HitResult);
}

void ABarrier::SphereEndOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
                                UPrimitiveComponent* PrimitiveComponent1, int I)
{
	Super::SphereEndOverlap(PrimitiveComponent, Actor, PrimitiveComponent1, I);
}

#include "CPP_SkillBox/Base/BaseChest.h"

#include <Components/SphereComponent.h>
#include <Kismet/GameplayStatics.h>
#include "CombatComponent.h"
#include "Inventory/InventoryComponent.h"


ABaseChest::ABaseChest()
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComp");
	RootComponent = SceneComponent;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>("MeshComp");
	MeshComp->SetupAttachment(RootComponent);
	MeshComp->SetCollisionObjectType(ECC_WorldStatic);

	SphereComp = CreateDefaultSubobject<USphereComponent>("SphereComp");
	SphereComp->SetCollisionProfileName("Powerup");
	SphereComp->SetCollisionObjectType(ECC_WorldStatic);
	SphereComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SphereComp->SetupAttachment(RootComponent);
}

void ABaseChest::Interact_Implementation(APawn* InstigatorPawn)
{
	ISGamplayInterface::Interact_Implementation(InstigatorPawn);
	if (!ensure(InstigatorPawn))
	{
		return;
	}
	UE_LOG(LogTemp, Display, TEXT("Activate == ABaseChest::Interact_Implementation "))
	Interact_Server(InstigatorPawn);
}

void ABaseChest::ChangeCollisionForInteract_Multicast_Implementation()
{
	SphereComp->SetCollisionObjectType(ECC_WorldDynamic);
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void ABaseChest::PlaySound_Client_Implementation(USoundBase* Sound)
{
	if (Sound)
		UGameplayStatics::PlaySound2D(GetWorld(), Sound);
}

AAmmoChest::AAmmoChest()
{
	Cost = 300;
	ContainsAmmoType.Emplace(EAmmoType::EAT_5_45, 30);
	ContainsAmmoType.Emplace(EAmmoType::EAT_5_56, 30);
	ContainsAmmoType.Emplace(EAmmoType::EAT_7_62, 71);
	ContainsAmmoType.Emplace(EAmmoType::EAT_9, 44);
	ContainsAmmoType.Emplace(EAmmoType::EAT_12, 8);
}

void AAmmoChest::Interact_Server(APawn* InstigatorPawn)
{
	UInventoryComponent* InventoryComp = InstigatorPawn->FindComponentByClass<UInventoryComponent>();
	if (InventoryComp)
	{
		if (InventoryComp->RemoveCredits(Cost))
		{
			for (auto& Ammo : ContainsAmmoType)
			{
				InventoryComp->AddAmmo(Ammo.Key, Ammo.Value);
			}
			PlaySound_Client(ToUpSound);
		}
		else
		{
			PlaySound_Client(FailureSound);
		}
	}
}

AGrenadeChest::AGrenadeChest()
{
	Cost = 450;
	ContainsGrenadeType.Emplace(EGrenadeSubType::TypeRGD, 2);
	ContainsGrenadeType.Emplace(EGrenadeSubType::TypeF1, 2);
	ContainsGrenadeType.Emplace(EGrenadeSubType::Smoke, 1);
	ContainsGrenadeType.Emplace(EGrenadeSubType::Light, 1);

	ContainsAmmoType.Emplace(EAmmoType::EAT_GrenadeLauncher, 6);
	ContainsAmmoType.Emplace(EAmmoType::EAT_RPG, 1);
}

void AGrenadeChest::Interact_Server(APawn* InstigatorPawn)
{
	UInventoryComponent* InventoryComp = InstigatorPawn->FindComponentByClass<UInventoryComponent>();
	if (InventoryComp)
	{
		if (InventoryComp->RemoveCredits(Cost))
		{
			for (auto& Grenade : ContainsGrenadeType)
			{
				InventoryComp->AddGrenade(Grenade.Key, Grenade.Value);
			}
			for (auto& Ammo : ContainsAmmoType)
			{
				InventoryComp->AddAmmo(Ammo.Key, Ammo.Value);
			}
			PlaySound_Client(ToUpSound);
		}
		else
		{
			PlaySound_Client(FailureSound);
		}
	}
}

AFirstAidChest::AFirstAidChest()
{
	Cost = 50;
}

void AFirstAidChest::Interact_Server(APawn* InstigatorPawn)
{
	UInventoryComponent* InventoryComp = InstigatorPawn->FindComponentByClass<UInventoryComponent>();
	if (InventoryComp)
	{
		if (InventoryComp->RemoveCredits(Cost))
		{
			InventoryComp->AddFirstAidKits(FirstAidNum);
			PlaySound_Client(ToUpSound);
		}
		else
		{
			PlaySound_Client(FailureSound);
		}
	}
}

AArmorChest::AArmorChest()
{
	Cost = 100;
}

void AArmorChest::Interact_Server(APawn* InstigatorPawn)
{
	UInventoryComponent* InventoryComp = InstigatorPawn->FindComponentByClass<UInventoryComponent>();
	if (InventoryComp)
	{
		if (InventoryComp->RemoveCredits(Cost) && InventoryComp->PickupArmor(NumPlates))
		{
			PlaySound_Client(ToUpSound);
		}
		else
		{
			PlaySound_Client(FailureSound);
		}
	}
}

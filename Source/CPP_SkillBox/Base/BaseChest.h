#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Actor.h>

#include "SGamplayInterface.h"
#include "BaseChest.generated.h"

enum class EGrenadeSubType : uint8;
enum class EAmmoType : uint8;
class USphereComponent;
UCLASS()
class CPP_SKILLBOX_API ABaseChest : public AActor, public ISGamplayInterface
{
	GENERATED_BODY()

public:
	ABaseChest();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Component")
	USceneComponent* SceneComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Component")
	USphereComponent* SphereComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Component")
	UStaticMeshComponent* MeshComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Ammo")
	int32 Cost = 300;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Sound")
	TObjectPtr<USoundBase> ToUpSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Sound")
	TObjectPtr<USoundBase> FailureSound = nullptr;

	virtual void Interact_Implementation(APawn* InstigatorPawn) override;

	virtual void Interact_Server(APawn* InstigatorPawn)
	{
	};

	UFUNCTION(Client, Unreliable)
	void PlaySound_Client(USoundBase* Sound);

	UFUNCTION(NetMulticast, Reliable)
	void ChangeCollisionForInteract_Multicast();
};

UCLASS()
class CPP_SKILLBOX_API AAmmoChest : public ABaseChest
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Ammo")
	TMap<EAmmoType, int32> ContainsAmmoType;

	AAmmoChest();
	virtual void Interact_Server(APawn* InstigatorPawn) override;
};

UCLASS()
class CPP_SKILLBOX_API AGrenadeChest : public ABaseChest
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade")
	TMap<EGrenadeSubType, int32> ContainsGrenadeType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade for weapon")
	TMap<EAmmoType, int32> ContainsAmmoType;

	AGrenadeChest();
	virtual void Interact_Server(APawn* InstigatorPawn) override;
};

UCLASS()
class CPP_SKILLBOX_API AFirstAidChest : public ABaseChest
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FirstAidNum")
	int32 FirstAidNum = 1;

	AFirstAidChest();
	virtual void Interact_Server(APawn* InstigatorPawn) override;
};

UCLASS()
class CPP_SKILLBOX_API AArmorChest : public ABaseChest
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FirstAidNum")
	int32 NumPlates = 1;

	AArmorChest();
	virtual void Interact_Server(APawn* InstigatorPawn) override;
};

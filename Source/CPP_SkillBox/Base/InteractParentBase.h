// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <Components/SphereComponent.h>
#include <Components/WidgetComponent.h>
#include <GameFramework/Actor.h>
#include "BaseDefinitions.h"
#include "CPP_SkillBox/CPP_SkillBox.h"
#include "CPP_SkillBox/UI/AnnexProgressBarWidget.h"
#include "CPP_SkillBox/UI/InteractBaseWidgetParent.h"
#include "InteractParentBase.generated.h"

class ACPP_SkillBoxCharacter;
class ASAICharacterBase;
class ASMine;
class USAttributeComponent;
class ABarrier;
class UInteractBaseWidgetParent;
enum class EFaction : uint8;

UCLASS()
class CPP_SKILLBOX_API AInteractParentBase : public AActor
{
	GENERATED_BODY()

public:
	AInteractParentBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Interact")
	TObjectPtr<USphereComponent> Sphere;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Interact")
	TObjectPtr<UStaticMeshComponent> StaticMesh;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Replicated, Category="Faction")
	EFaction Faction;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category="Widget")
	TSoftClassPtr<UInteractBaseWidgetParent> InteractWidgetClass;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category="Widget")
	TSoftClassPtr<UInteractBaseWidgetParent> InteractMenuWidgetClass;

	UPROPERTY(BlueprintReadOnly, Replicated)
	bool bIsMenuOpen = false;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category="Debug")
	bool DebugTimer = false;
	UPROPERTY(BlueprintReadOnly, Replicated)
	TObjectPtr<ACPP_SkillBoxCharacter> OwnerCharacter = nullptr;

	//Actor type
	bool bIsBarrier;

	UPROPERTY()
	FTimerHandle CheckOverlapActorsTimer;

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override { Super::Tick(DeltaTime); };

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	//Inputs
	virtual void PrimaryInteractPressed();
	virtual void PrimaryInteractReleased();

	virtual void CheckOverlapActors();

	//CheckOverlapTimer
	UFUNCTION(Reliable, Server)
	void RunCheckOverlapTimer_OnServer();
	UFUNCTION(Reliable, NetMulticast)
	void RunCheckOverlapTimer_Multi();
	UFUNCTION(Reliable, Server)
	void StopCheckOverlapTimer_OnServer();
	UFUNCTION(Reliable, NetMulticast)
	void StopCheckOverlapTimer_OnMulti();

	// Create Interact Widget
	virtual void CreateInteractWidget(ACPP_SkillBoxCharacter* OwnerPlayer);

	// Overlaps
	UFUNCTION()
	virtual void SphereBeginOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
	                                UPrimitiveComponent* PrimitiveComponent1, int I, bool bArg,
	                                const FHitResult& HitResult);
	UFUNCTION()
	virtual void SphereEndOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
	                              UPrimitiveComponent* PrimitiveComponent1, int I);
};

// Base=================================================================================================================
//======================================================================================================================
UCLASS()
class CPP_SKILLBOX_API ABase : public AInteractParentBase
{
	GENERATED_BODY()

public:
	ABase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Widget")
	TObjectPtr<UWidgetComponent> WidgetComponent;
	UPROPERTY()
	TObjectPtr<UAnnexProgressBarWidget> ProgressWidget;

	UPROPERTY(BlueprintReadWrite, Replicated)
	bool BeginAnnex = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	bool CanAnnex = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool CanChangeRespawn = true;

	UPROPERTY(BlueprintReadWrite, Replicated)
	float CurrentAnnexTime = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Annexation")
	float TimeToAnnexBase = 3.0f;

	UPROPERTY()
	FTimerHandle AnnexTimer;

	//Player start
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="PlayerStart")
	FName BaseTagForRespawn = "Base1";

	// Category AISpawn
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AISpawn", Meta = (MakeEditWidget = true))
	FVector AIAdditionalPoint = FVector(0);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AISpawn")
	int32 AdditionalActorsToSpawn = 1;
	UPROPERTY(BlueprintReadWrite, Replicated)
	int32 CurrentActorsToSpawn = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AISpawn")
	float SpawnRate = 1.0f;

	// Guardian
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AISpawn", Meta = (MakeEditWidget = true))
	TArray<FVector> AIGuardianSpawn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AISpawn")
	TSoftClassPtr<ASAICharacterBase> GuardianClass;
	
	TArray<ASAICharacterBase*>AIGuardianActors;

	// Patrol
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AISpawn", Meta = (MakeEditWidget = true))
	TArray<FVector> AIPatrolSpawn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AISpawn")
	TSoftClassPtr<ASAICharacterBase> PatrolClass;
	
	TArray<ASAICharacterBase*> AIPatrolActors;
	
	UPROPERTY(BlueprintReadWrite, Replicated, Category="AISpawn")
	TArray<ASAICharacterBase*> AIActors;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FlashCardSpawn")
	TSubclassOf<AActor> FlashCardClass = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FlashCardSpawn", Meta = (MakeEditWidget = true))
	FVector FlashCardLocation{0};

protected:
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;
	
public:

	// AI Spawn
	void PointDistribution(ASAICharacterBase* AICharacter);
	void SpawnAIChasing();
	void SpawnAIGuardian();
	void SpawnAIFromClass(UClass* Class, FTransform Transform);
	void RunTimersSpawnAI();
	UFUNCTION()
	void RespawnAIOnDeath(AActor* Actor, AActor* Actor1);
	void SpawnAdditionalAI();

	//void SpawnPlayerStart();

	//BeginAnnexChange
	UFUNCTION(Reliable, Server)
	void SetBeginAnnex_OnServer(bool Value);
	UFUNCTION(Reliable, NetMulticast)
	void SetBeginAnnex_Multicast(bool Value);

	//CanAnnexChange
	UFUNCTION(Reliable, Server)
	void SetCanAnnex_OnServer(bool Value);
	UFUNCTION(Reliable, NetMulticast)
	void SetCanAnnex_Multicast(bool Value);

	//FactionChange

	UFUNCTION(Reliable, Server)
	void SetFaction_OnServer(EFaction NewFaction);

	//AnnexTimer
	UFUNCTION(Reliable, Server)
	void RunAnnexTimer_OnServer();
	UFUNCTION(Reliable, NetMulticast)
	void RunAnnexTimer_Multi();
	UFUNCTION(Reliable, Server)
	void StopAnnexTimer_OnServer();
	UFUNCTION(Reliable, NetMulticast)
	void StopAnnexTimer_Multicast();

	// Rotate Widget On Owner Camera
	UFUNCTION(Unreliable, NetMulticast)
	void RotateWidget_Multicast();

	// Update Widget	
	UFUNCTION(Unreliable, NetMulticast)
	void UpdateWidget_Multicast();

	void AnnexProgress();

	// Change Interact Widget Multi
	UFUNCTION(Reliable, NetMulticast)
	void ChangeInteractWidgetForAllOverlapActor_Multicast(bool On) const;

	// Inputs
	virtual void PrimaryInteractPressed() override;
	virtual void PrimaryInteractReleased() override;

	void AnnexEnd();

	// Overlap	
	virtual void SphereBeginOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
	                                UPrimitiveComponent* PrimitiveComponent1, int I, bool bArg,
	                                const FHitResult& HitResult) override;
	virtual void SphereEndOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
	                              UPrimitiveComponent* PrimitiveComponent1, int I) override;
};

// Barrier==============================================================================================================
//======================================================================================================================
UCLASS()
class CPP_SKILLBOX_API ABarrier : public AInteractParentBase
{
	GENERATED_BODY()

public:
	ABarrier();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TObjectPtr<USAttributeComponent> HealthComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Widget")
	TObjectPtr<UWidgetComponent> WidgetComponent;
	UPROPERTY()
	TObjectPtr<UAnnexProgressBarWidget> HealtBarWidget;
	UPROPERTY(Replicated)
	float CurrentHP = 0.f;
	UPROPERTY(BlueprintReadOnly)
	FBarrierInfo BarrierInfo;
	UPROPERTY(BlueprintReadOnly)
	APawn* Weapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon")
	TSoftClassPtr<APawn> WeaponClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BarrierSettings")
	TArray<FBarrierInfo> Levels;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category="BarrierSettings")
	int32 CurrentLevel = 0;
	UPROPERTY()
	TArray<UMaterial*> Materials;
	UPROPERTY()
	int32 WeaponPrice = 0;
	//UPROPERTY()
	// APawn* Weapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BarrierSettings||Bomb", Meta = (MakeEditWidget = true))
	TArray<FVector> BombSpot;
	UPROPERTY(BlueprintReadWrite, Category="BarrierSettings||Bomb")
	TArray<ASMine*> Bombs;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category="BarrierSettings")
	EBarrierState HealthStatus = EBarrierState::FullHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BarrierSettings")
	int32 MaxLevel = 6;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="BarrierSettings||Bomb")
	int32 BombPrice = 100;

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION()
	void HealthChange(AActor* InstigatorActor, USAttributeComponent* OwningComp, float NewHealth, float Damage);
	void UpdateLevel();
	void AppearAnimIn();
	void AppearAnimOut();
	bool HaveCredits(int32 Price, int32& SubstractCredits);
	void SetCredits(float Substract);
	UFUNCTION(BlueprintCallable)
	void Repair(FName& Result);
	void MeshUpdate();
	UFUNCTION(Reliable, Server)
	void UpdateBarrierState_OnServer();
	UFUNCTION(Reliable, NetMulticast)
	void UpdateBarrierState_Multicast();
	void UpdateBarrierState();
	UFUNCTION(BlueprintCallable)
	bool CanUpgradeBarrier(FName& Result, int32& Price);
	UFUNCTION(BlueprintCallable, Reliable, Server)
	void UpgradeBarrier_OnServer();
	UFUNCTION(BlueprintCallable, Reliable, NetMulticast)
	void UpgradeBarrier_Multicast();
	void UpgradeBarrier();
	UFUNCTION(BlueprintCallable)
	bool CanRestore(FName& Result);
	UFUNCTION(BlueprintCallable)
	bool CanAddWeapon(FName& Result);
	UFUNCTION(BlueprintCallable)
	void AddWeapon();
	UFUNCTION(BlueprintCallable)
	void SpawnBomb();
	UFUNCTION(BlueprintCallable)
	bool CanAddBomb(FName& Result, int32& Quantity);


	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void PrimaryInteractPressed() override;
	virtual void PrimaryInteractReleased() override;

	// Overlap	
	virtual void SphereBeginOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
	                                UPrimitiveComponent* PrimitiveComponent1, int I, bool bArg,
	                                const FHitResult& HitResult) override;
	virtual void SphereEndOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
	                              UPrimitiveComponent* PrimitiveComponent1, int I) override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "ProjectileGrenade.generated.h"

class AProjectileGrenade;

UENUM(BlueprintType)
enum class EGrenadeType : uint8
{
	Combat,
	Tactical
};

UENUM(BlueprintType)
enum class EGrenadeSubType : uint8
{
	TypeF1,
	TypeRGD,
	Smoke,
	Light
};

USTRUCT(BlueprintType)
struct FGrenadeInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade")			//класс для спавна предмета
	TSoftClassPtr<AProjectileGrenade> GrenadeClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade")			//кол-во в одной упаковке если подбираем
	int32 Count = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade")			//тип гранаты
	EGrenadeType GrenadeType;

	//подтип гранаты - взрывная
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade") //,meta=(EditCondition="GrenadeType == EGrenadeType::Combat", EditConditionHides))
	EGrenadeSubType GrenadeSubType;
	//урон
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade", meta=(ClampMin="0", UIMin="0", EditCondition="GrenadeType == EGrenadeType::Combat", EditConditionHides))
	float GrenadeDamage = 0.0f;
	//порождает осколки?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade", meta=(EditCondition="GrenadeType == EGrenadeType::Combat", EditConditionHides))
	bool bMustSpawnPart = false;
	//кол-во осколков
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade", meta=(ClampMin="0", UIMin="0", EditCondition="GrenadeType == EGrenadeType::Combat && bMustSpawnPart == true", EditConditionHides))
	int32 SpawnedPart = 0;
	//начальный радиус поражения
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade", meta=(ClampMin="0", UIMin="0", EditCondition="GrenadeType == EGrenadeType::Combat", EditConditionHides))
	float DamageRadius = 0.0f;

	//общие настройки
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade", meta=(EditCondition="ParticleEffect == nullptr", EditConditionHides))	//Эффект Ниагара
	TSoftObjectPtr<UNiagaraSystem> NiagaraEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade", meta=(EditCondition="NiagaraEffect == nullptr", EditConditionHides))	//Эффект Частиц
	TSoftObjectPtr<UParticleSystem> ParticleEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade", meta=(ClampMin="0", UIMin="0"))			//время до срабатывания реакции
	float TimeToReact = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade")
	TArray<AActor*> IgnoredActors; // TODO: remove
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade")			//название предмета для виджета
	FText ItemName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade")			//звук отскока от поверхности
	TSoftObjectPtr<USoundCue> BounceSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade")			//звук применения реакции (взрыв, шум, газ)
	TSoftObjectPtr<USoundCue> ExploseSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade")			//иконка для отрисовки в UI
	TSoftObjectPtr<UTexture2D> ItemIcon = nullptr;
};

UCLASS()
class CPP_SKILLBOX_API AProjectileGrenade : public AProjectile
{
	GENERATED_BODY()

public:
	AProjectileGrenade();
	//virtual void Destroyed() override;
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION()
	void ApplyEffect();

	UFUNCTION()
	void StartReactTimer();

protected:
	UFUNCTION()
	void OnBounce(const FHitResult& ImpactResult, const FVector& ImpactVelocity);

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade Settings")
	FGrenadeInfo GrenadeInfo;

	//Таймер до срабатывания эффекта
	FTimerHandle ReactTimer;
};

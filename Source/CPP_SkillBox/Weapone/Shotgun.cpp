// Fill out your copyright notice in the Description page of Project Settings.


#include "Shotgun.h"

#include <Engine/SkeletalMeshSocket.h>
#include <Kismet/GameplayStatics.h>
#include <Sound/SoundCue.h>

#include "CPP_SkillBoxCharacter.h"
#include "CPP_SkillBoxPlayerController.h"

void AShotgun::Fire(const FVector& HitTarget)
{
	AWeapon::Fire(HitTarget);

	AController* InstigatorController = GetOwnerController();

	PlayFireSound();
	if (MuzzleFlashSocket)
	{
		FTransform SocketTransform = MuzzleFlashSocket->GetSocketTransform(GetWeaponMesh());
		FVector Start = SocketTransform.GetLocation();

		TMap<ACPP_SkillBoxCharacter*, uint32> HitMap;

		FHitResult FireHit;
		for (uint32 i = 0; i <= NumberOfPellets; i++)
		{
			WeaponTraceHit(Start, HitTarget, FireHit);

			ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(FireHit.GetActor());
			if (Character && HasAuthority() && InstigatorController)
			{
				if (HitMap.Contains(Character))
				{
					HitMap[Character]++;
				}
				else
				{
					HitMap.Emplace(Character, 1);
				}
			}

			UGameplayStatics::SpawnEmitterAtLocation(this, ImpactParticles, FireHit.ImpactPoint, FireHit.ImpactNormal.Rotation());
			UGameplayStatics::PlaySoundAtLocation(this, HitSound, FireHit.ImpactPoint, -.5f, FMath::FRandRange(-.5f, .5f));
		}

		if (HasAuthority() && InstigatorController)
		{
			for (auto HitPair : HitMap)
			{
				UGameplayStatics::ApplyDamage(
					HitPair.Key,
					Damage * HitPair.Value,
					InstigatorController,
					this,
					UDamageType::StaticClass()
				);
			}
		}
	}
}

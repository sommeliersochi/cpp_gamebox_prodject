// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileRocket.h"

#include <NiagaraComponent.h>
#include <NiagaraFunctionLibrary.h>
#include <Components/AudioComponent.h>
#include <Components/BoxComponent.h>
#include <Kismet/GameplayStatics.h>
#include <PhysicsEngine/RadialForceComponent.h>
#include <Sound/SoundCue.h>

#include "RocketMovementComponent.h"

AProjectileRocket::AProjectileRocket()
{
	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>("RocketMesh");
	ProjectileMesh->SetupAttachment(RootComponent);
	ProjectileMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	RocketMovementComponent = CreateDefaultSubobject<URocketMovementComponent>("RocketMovementComp");
	RocketMovementComponent->bRotationFollowsVelocity = true;
	RocketMovementComponent->SetIsReplicated(true);

	RadialForceComp = CreateDefaultSubobject<URadialForceComponent>("Radial Force Comp");
	RadialForceComp->SetupAttachment(CollisionBox);
	RadialForceComp->SetAutoActivate(false);
	RadialForceComp->bImpulseVelChange = true;
	RadialForceComp->Radius = 500.f;
	RadialForceComp->ImpulseStrength = 1000.f;
}

void AProjectileRocket::BeginPlay()
{
	Super::BeginPlay();

	if(!HasAuthority())
	{
		CollisionBox->OnComponentHit.AddDynamic(this, &AProjectileRocket::OnHit);
	}

	SpawnTrailSystem();
	
	if(ProjectileLoopComponent && LoopingSoundAttenuation)
	{
		ProjectileLoopComponent = UGameplayStatics::SpawnSoundAttached(
			ProjectileLoop,
			GetRootComponent(),
			FName(),
			GetActorLocation(),
			EAttachLocation::KeepWorldPosition,
			false,
			1.f,
			1.f,
			0.f,
			LoopingSoundAttenuation,
			(USoundConcurrency*)nullptr,
			false
			);
	}
}

void AProjectileRocket::Destroyed()
{
	//Super::Destroyed();
	
}

void AProjectileRocket::OnHit(UPrimitiveComponent* HitComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
	if(OtherActor == GetOwner())
	{
		return;
	}

	ExplodeDamage();
	StartDestroyTimer();
	
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactData.ImpactParticles, GetActorTransform());
	UGameplayStatics::PlaySoundAtLocation(this, ImpactData.ImpactSound, GetActorLocation());
	
	if(ProjectileMesh)
	{
		ProjectileMesh->SetVisibility(false);
	}
	if(ExplodeShake)
	{
		UGameplayStatics::PlayWorldCameraShake(this, ExplodeShake, GetActorLocation(), 4000.f, 4000.f);
	}
	if(CollisionBox)
	{
		CollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	if(TrailSystemComponent && TrailSystemComponent->GetSystemInstanceController())
	{
		TrailSystemComponent->GetSystemInstanceController()->Deactivate();
	}
	if(ProjectileLoopComponent && ProjectileLoopComponent->IsPlaying())
	{
		ProjectileLoopComponent->Stop();
	}
	/*Super::OnHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);*/
	
}

void AProjectileRocket::ExplodeDamage()
{
	APawn* FiringPawn = GetInstigator();
	if (FiringPawn && HasAuthority())
	{
		AController* FiringController = FiringPawn->GetController();
		if (FiringController)
		{
			UGameplayStatics::ApplyRadialDamageWithFalloff(this,
				Damage,
				10.f,
				GetActorLocation(),
				DamageInnerRadius,
				DamageOuterRadius,
				1.f,
				UDamageType::StaticClass(),
				TArray<AActor*>(),
				this);

			RadialForceComp->FireImpulse();
		}
	}
}


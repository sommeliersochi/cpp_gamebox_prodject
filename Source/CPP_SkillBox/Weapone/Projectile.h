// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ImpactData.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystemComponent.h"
#include "Projectile.generated.h"

class URadialForceComponent;
class UNiagaraComponent;
class UNiagaraSystem;
class UBoxComponent;
class UProjectileMovementComponent;
class USoundCue;

UCLASS()
class CPP_SKILLBOX_API AProjectile : public AActor
{
	GENERATED_BODY()

public:
	AProjectile();

	virtual void Tick(float DeltaTime) override;

	virtual void Destroyed() override;

	UProjectileMovementComponent* GetProjectileMovementComponent() const;

	void SetDamage(float NewDamage);

protected:
	virtual void BeginPlay() override;

	void StartDestroyTimer();

	void DestroyTimerFinished();

	void SpawnTrailSystem();

	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

protected:
	UPROPERTY(VisibleAnywhere)
	UProjectileMovementComponent* ProjectileMovementComponent;

	float Damage = 20.f;

	UPROPERTY(EditAnywhere)
	FImpactData ImpactData;

	FHitResult HitResult;

	UPROPERTY(EditAnywhere)
	TMap<UPhysicalMaterial*, FImpactData> ImpactDataMap;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UCameraShakeBase> ExplodeShake;

	UPROPERTY(EditAnywhere)
	UBoxComponent* CollisionBox;

	UPROPERTY(EditAnywhere)
	UNiagaraSystem* TrailSystem;

	UPROPERTY()
	UNiagaraComponent* TrailSystemComponent;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* ProjectileMesh;

private:
	UPROPERTY(EditAnywhere)
	UParticleSystem* Trace;

	UPROPERTY(EditAnywhere)
	UParticleSystemComponent* TraceComp;

	FTimerHandle DestroyTimer;

	UPROPERTY(EditAnywhere)
	float DestroyTime = 3.f;
};

FORCEINLINE_DEBUGGABLE UProjectileMovementComponent* AProjectile::GetProjectileMovementComponent() const
{
	return ProjectileMovementComponent;
}
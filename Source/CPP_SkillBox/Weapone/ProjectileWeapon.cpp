// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileWeapon.h"

#include <Engine/SkeletalMeshSocket.h>
#include <Kismet/GameplayStatics.h>

#include "CPP_SkillBoxCharacter.h"


void AProjectileWeapon::Fire(const FVector& HitTarget)
{
	Super::Fire(HitTarget);

	if(!HasAuthority())
	{
		return;
	}

	UWorld* World = GetWorld();

	if(MuzzleFlashSocket && ProjectileClass && OwnerCharacter && World)
	{
		const FTransform SocketTransform = MuzzleFlashSocket->GetSocketTransform(GetWeaponMesh());
		const FVector ToTarget = HitTarget - SocketTransform.GetLocation();
		const FRotator TargetRotation = ToTarget.Rotation();

		FTransform SpawnTransform = FTransform(TargetRotation, SocketTransform.GetLocation(), FVector::OneVector);

		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = GetOwner();
		SpawnParams.Instigator = OwnerCharacter;
		SpawnParams.ObjectFlags = RF_Transient;
		SpawnParams.bDeferConstruction = true;

		AProjectile* Projectile = World->SpawnActor<AProjectile>(ProjectileClass, SpawnTransform, SpawnParams);
		Projectile->SetDamage(Damage);
		UGameplayStatics::FinishSpawningActor(Projectile, SpawnTransform);

		PlayFireSound();
	}
	
}

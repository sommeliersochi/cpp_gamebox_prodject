#include "ProjectileGrenade.h"

#include "NiagaraFunctionLibrary.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"


AProjectileGrenade::AProjectileGrenade()
{
	PrimaryActorTick.bCanEverTick = true;

	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>("Grenade Mash");
	ProjectileMesh->SetupAttachment(RootComponent);
	ProjectileMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovementComp");
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->SetIsReplicated(true);
	ProjectileMovementComponent->bShouldBounce = true;
}

void AProjectileGrenade::BeginPlay()
{
	AActor::BeginPlay();

	switch (GrenadeInfo.GrenadeType)
	{
	case (EGrenadeType::Combat):
		StartReactTimer();
		break;
	case(EGrenadeType::Tactical):
		StartReactTimer();
		break;
	default:
		break;
	}
	SpawnTrailSystem();
	StartDestroyTimer();

	if (ProjectileMovementComponent)
	{
		ProjectileMovementComponent->OnProjectileBounce.RemoveDynamic(this, &AProjectileGrenade::OnBounce);
		ProjectileMovementComponent->OnProjectileBounce.AddDynamic(this, &AProjectileGrenade::OnBounce);
	}
}

void AProjectileGrenade::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (ProjectileMovementComponent)
	{
		ProjectileMovementComponent->OnProjectileBounce.RemoveDynamic(this, &AProjectileGrenade::OnBounce);
	}

	Super::EndPlay(EndPlayReason);
}

void AProjectileGrenade::ApplyEffect()
{
	if (GrenadeInfo.GrenadeDamage > 0 && GrenadeInfo.DamageRadius > 0)
	{
		UGameplayStatics::ApplyRadialDamage(this, GrenadeInfo.GrenadeDamage, GetActorLocation(), GrenadeInfo.DamageRadius,
											UDamageType::StaticClass(), GrenadeInfo.IgnoredActors, this);
	}

	UGameplayStatics::SpawnSoundAtLocation(this, GrenadeInfo.ExploseSound.LoadSynchronous(), GetActorLocation(),
										   FRotator::ZeroRotator);
	if (GrenadeInfo.ParticleEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, GrenadeInfo.ParticleEffect.LoadSynchronous(), GetActorLocation(), FRotator::ZeroRotator,
												 FVector::OneVector, true, EPSCPoolMethod::AutoRelease, true);
	}

	if (GrenadeInfo.NiagaraEffect)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, GrenadeInfo.NiagaraEffect.LoadSynchronous(), GetActorLocation(), FRotator::ZeroRotator,
													   FVector::OneVector, true, true, ENCPoolMethod::AutoRelease);
	}
}

void AProjectileGrenade::StartReactTimer()
{
	if (!GetWorldTimerManager().IsTimerActive(ReactTimer))
	{
		GetWorldTimerManager().SetTimer(ReactTimer, this, &AProjectileGrenade::ApplyEffect, 1.0f, false, GrenadeInfo.TimeToReact);
	}
}

void AProjectileGrenade::OnBounce(const FHitResult& ImpactResult, const FVector& ImpactVelocity)
{
	UGameplayStatics::PlaySoundAtLocation(this, GrenadeInfo.BounceSound.LoadSynchronous(), GetActorLocation());
}

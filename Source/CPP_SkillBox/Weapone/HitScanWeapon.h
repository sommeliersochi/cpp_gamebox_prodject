// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "HitScanWeapon.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API AHitScanWeapon : public AWeapon
{
	GENERATED_BODY()

public:
	virtual void Fire(const FVector& HitTarget) override;

protected:

	FVector TraceEndWithScatter(const FVector& TraceStart, const FVector& HitTarget) const;
	void WeaponTraceHit(const FVector& TraceStart,const FVector& HitTarget, FHitResult& OutHit);
	
	UPROPERTY(EditAnywhere, Category= "Settings|HitScan")
	UParticleSystem* ImpactParticles;
	
	UPROPERTY(EditAnywhere, Category= "Settings|HitScan")
	USoundCue* HitSound;

	UPROPERTY(EditAnywhere, Category= "Settings|HitScan")
	UParticleSystem* BeamParticles;

	UPROPERTY(EditAnywhere, Category= "Settings|HitScan")
	float DistanceToSphere = 800.f;

	UPROPERTY(EditAnywhere, Category= "Settings|HitScan")
	float SphereRadius = 75.f;

	UPROPERTY(EditAnywhere, Category= "Settings|HitScan")
	bool bUseScatter = false;
	
};

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AirBombSpawner.generated.h"

UCLASS()
class CPP_SKILLBOX_API AAirBombSpawner : public AActor
{
	GENERATED_BODY()

public:
	AAirBombSpawner();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"

#include <Components/SphereComponent.h>
#include <Components/WidgetComponent.h>
#include <Components/CapsuleComponent.h>
#include <Engine/SkeletalMeshSocket.h>
#include <Kismet/GameplayStatics.h>
#include <Net/UnrealNetwork.h>
#include <NiagaraComponent.h>
#include <Sound/SoundCue.h>

#include "Casing.h"
#include "CombatComponent.h"
#include "CPP_SkillBoxCharacter.h"
#include "CPP_SkillBoxPlayerController.h"
#include "SAttributeComponent.h"

const FName AWeapon::WeaponMeshComponentName = FName("WeaponMesh");
const FName AWeapon::SphereCompName = FName("SphereComp");
const FName AWeapon::PickupWidgetName = FName("PickupWidget");
const FName AWeapon::LineFXComponentName = FName("LineFXComponent");
const FName AWeapon::LineImpactFXComponentName = FName("LineImpactFXComponent");

AWeapon::AWeapon(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	bReplicates = true;

	AActor::SetReplicateMovement(true);
	EnableCustomDepth(false);

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(AWeapon::WeaponMeshComponentName);
	SetRootComponent(WeaponMesh);
	WeaponMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	WeaponMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->MarkRenderStateDirty();

	SphereComp = CreateDefaultSubobject<USphereComponent>(AWeapon::SphereCompName);
	SphereComp->SetupAttachment(RootComponent);
	SphereComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	SphereComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

	PickupWidget = CreateDefaultSubobject<UWidgetComponent>(AWeapon::PickupWidgetName);
	PickupWidget->SetupAttachment(RootComponent);
	PickupWidget->SetVisibility(false);

	LaserFX = CreateDefaultSubobject<UNiagaraComponent>(AWeapon::LineFXComponentName);
	LaserFX->SetupAttachment(WeaponMesh, FName("MuzzleFlash"));
	LaserFX->bAutoActivate = false;
	LaserFX->SetOnlyOwnerSee(true);
	LaserFX->SetVisibility(false);

	LaserImpactFX = CreateDefaultSubobject<UNiagaraComponent>(AWeapon::LineImpactFXComponentName);
	LaserImpactFX->SetupAttachment(WeaponMesh);
	LaserImpactFX->bAutoActivate = false;
	LaserImpactFX->SetVisibility(false);
	LaserImpactFX->SetOnlyOwnerSee(true);
	LaserImpactFX->SetUsingAbsoluteLocation(true);
}

void AWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams Params;
	Params.RepNotifyCondition = REPNOTIFY_Always;
	DOREPLIFETIME(AWeapon, WeaponState);
	DOREPLIFETIME_WITH_PARAMS(AWeapon, Ammo, Params);
}

void AWeapon::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		SphereComp->OnComponentBeginOverlap.AddDynamic(this, &AWeapon::OnSphereOverlap);
		SphereComp->OnComponentEndOverlap.AddDynamic(this, &AWeapon::OnSphereEndOverlap);
	}

	if (GetWeaponMesh())
	{
		MuzzleFlashSocket = GetWeaponMesh()->GetSocketByName("MuzzleFlash");
	}
}

void AWeapon::SetOwner(AActor* NewOwner)
{
	Super::SetOwner(NewOwner);

	OnRep_Owner();
}

void AWeapon::OnRep_Owner()
{
	Super::OnRep_Owner();

	if (Owner == nullptr)
	{
		if (OwnerCharacter)
		{
			OwnerCharacter->ReceiveControllerChangedDelegate.RemoveDynamic(this, &AWeapon::OnOwnerReceiveControllerChanged);
		}

		OwnerCharacter = nullptr;
		OwnerController = nullptr;
	}
	else
	{
		OwnerCharacter = GetOwner<ACPP_SkillBoxCharacter>();

		if (OwnerCharacter)
		{
			OwnerCharacter->ReceiveControllerChangedDelegate.RemoveDynamic(this, &AWeapon::OnOwnerReceiveControllerChanged);
			OwnerCharacter->ReceiveControllerChangedDelegate.AddDynamic(this, &AWeapon::OnOwnerReceiveControllerChanged);
			OnOwnerReceiveControllerChanged(OwnerCharacter, nullptr, OwnerCharacter->GetController());
		}
	}
}

void AWeapon::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	TickLaser();
}

void AWeapon::ShowPickupWidget(bool bShowWidget)
{
	if (PickupWidget)
	{
		PickupWidget->SetVisibility(bShowWidget);
	}
}

void AWeapon::Dropped()
{
	SetWeaponState(EWeaponState::EWS_Dropped);
	const FDetachmentTransformRules DetachRules(EDetachmentRule::KeepWorld, true);
	WeaponMesh->DetachFromComponent(DetachRules);
	SetOwner(nullptr);
	SetActorHiddenInGame(false);
}

void AWeapon::Fire(const FVector& HitTarget)
{
	if (!OwnerCharacter)
	{
		return;
	}
	if (OwnerCharacter && OwnerCharacter->GetAttributeComp()->IsDead())
	{
		OwnerCharacter = nullptr;
		return;
	}
	UWorld* World = GetWorld();
	if (!World)
	{
		return;
	}

	if (MuzzleFlash && MuzzleFlashSocket)
	{
		const FTransform SocketTransform = MuzzleFlashSocket->GetSocketTransform(GetWeaponMesh());
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MuzzleFlash, SocketTransform);
	}

	if (FireAnimation)
	{
		WeaponMesh->PlayAnimation(FireAnimation, false);
	}

	if (FireShake)
	{
		UGameplayStatics::PlayWorldCameraShake(this, FireShake, GetActorLocation(), 4000.f, 4000.f);
	}

	if (CasingClass)
	{
		const USkeletalMeshSocket* AmmoEjectSocket = WeaponMesh->GetSocketByName(FName("AmmoEject"));
		if (AmmoEjectSocket)
		{
			const FTransform SocketTransform = AmmoEjectSocket->GetSocketTransform(WeaponMesh);
			FActorSpawnParameters SpawnParameters;
			SpawnParameters.ObjectFlags = RF_Transient;
			World->SpawnActor<ACasing>(CasingClass, SocketTransform.GetLocation(), SocketTransform.GetRotation().Rotator(), SpawnParameters);
		}
	}
	AddAmmo(-1);
}

void AWeapon::AddAmmo(const int32 AmmoToAdd)
{
	Ammo = FMath::Clamp(Ammo + AmmoToAdd, 0, MagCapacity);

	if (HasAuthority())
	{
		OnRep_Ammo();
	}
}

bool AWeapon::GetTraceData(FVector& TraceStart, FVector& TraceEnd) const
{
	const auto Player = Cast<ACharacter>(GetOwner());
	FVector PlayerLoc = Player->GetActorLocation();
	FVector PlayerLocStart = PlayerLoc + FVector(50.0f, 0.0f, 50.f);
	TraceEnd = PlayerLocStart + Player->GetActorForwardVector() * 5000.f;


	const FTransform SocketTransform = WeaponMesh->GetSocketTransform(MuzzleSocketName);
	TraceStart = SocketTransform.GetLocation();
	const FVector ShotDirection = SocketTransform.GetRotation().GetForwardVector();

	FHitResult HitResult;
	GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECC_MAX);
	if (HitResult.bBlockingHit)
	{
		TraceEnd = HitResult.ImpactPoint;
	}

	return true;
}

void AWeapon::SetWeaponState(EWeaponState State)
{
	WeaponState = State;
	OnWeaponStateSet();
}

void AWeapon::EnableCustomDepth(bool bEnable)
{
	if (WeaponMesh)
	{
		WeaponMesh->SetRenderCustomDepth(bEnable);
	}
}

void AWeapon::OnWeaponStateSet()
{
	switch (WeaponState)
	{
	case EWeaponState::EWS_Equipped:
		OnEquipped();
		break;
	case EWeaponState::EWS_EquippedSecondary:
		OnEquippedSecondary();
		break;
	case EWeaponState::EWS_Dropped:
		OnDropped();
		break;
	default:
		break;
	}
}

void AWeapon::OnEquipped()
{
	ShowPickupWidget(false);
	SphereComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->SetSimulatePhysics(false);
	WeaponMesh->SetEnableGravity(false);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	EnableCustomDepth(false);
}

void AWeapon::OnDropped()
{
	if (HasAuthority())
	{
		SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	WeaponMesh->SetSimulatePhysics(true);
	WeaponMesh->SetEnableGravity(true);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	WeaponMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	WeaponMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	WeaponMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);

	WeaponMesh->MarkRenderStateDirty();
	EnableCustomDepth(false);
}

void AWeapon::OnEquippedSecondary()
{
	ShowPickupWidget(false);
	SphereComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	EnableCustomDepth(false);
	WeaponMesh->MarkRenderStateDirty();
	WeaponMesh->SetSimulatePhysics(false);
	WeaponMesh->SetEnableGravity(false);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AWeapon::PlayFireSound()
{
	UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
}

void AWeapon::ActivateLaser()
{
	if (!OwnerCharacter || !OwnerCharacter->IsLocallyPlayerControlled())
	{
		return;
	}

	FVector TraceStart, TraceEnd;

	OwnerCharacter->GetCombat()->GetTraceData(TraceStart, TraceEnd, 700, false);

	LaserFX->Activate(true);
	LaserFX->SetVisibility(true);
	LaserFX->SetVectorParameter(LaserTargetName, TraceEnd);
}

void AWeapon::DeactivateLaser()
{
	LaserFX->Deactivate();
	LaserFX->SetVisibility(false);
}

void AWeapon::TickLaser()
{
	if (!OwnerCharacter || !OwnerCharacter->IsLocallyPlayerControlled())
	{
		return;
	}

	FVector TraceStart, TraceEnd;
	FHitResult LaserHit;

	OwnerCharacter->GetCombat()->GetTraceData(TraceStart, TraceEnd, 700, false);
	GetWorld()->LineTraceSingleByChannel(LaserHit, TraceStart, TraceEnd, ECC_Visibility);

	if (LaserFX->IsActive())
	{
		LaserFX->SetVectorParameter(LaserTargetName, TraceEnd);
	}

	TraceEnd = LaserHit.ImpactPoint;

	LaserImpactFX->SetVisibility(LaserHit.bBlockingHit);
	LaserImpactFX->SetWorldLocation(TraceEnd);
}

void AWeapon::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
							  UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(OtherActor);
	if (Character && Character->GetCapsuleComponent() == OtherComp)
	{
		Character->SetOverlappingWeaponStart(this);
	}
}

void AWeapon::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
								 UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(OtherActor);
	if (Character && Character->GetCapsuleComponent() == OtherComp)
	{
		Character->SetOverlappingWeaponEnd(this);
	}
}

void AWeapon::OnOwnerReceiveControllerChanged(APawn* Pawn, AController* OldController, AController* NewController)
{
	OwnerController = Cast<ACPP_SkillBoxPlayerController>(NewController);
}

void AWeapon::OnRep_WeaponState()
{
	OnWeaponStateSet();
}

void AWeapon::OnRep_Ammo()
{
	if (OwnerCharacter && OwnerCharacter->GetCombat() && IsFull()) //TODO: move logic in combat component
	{
		OwnerCharacter->GetCombat()->JumpToShotgunEnd();
	}

	OnChangedAmmo.Broadcast(this, Ammo);
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileBullet.h"

#include "CPP_SkillBoxCharacter.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AProjectileBullet::AProjectileBullet()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovementComp");
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->SetIsReplicated(true);
}

void AProjectileBullet::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	if(auto OwnerCharacter = GetOwner<ACPP_SkillBoxCharacter>())
	{
		if(auto OwnerController = OwnerCharacter->GetController())
		{
			UGameplayStatics::ApplyDamage(OtherActor, Damage, OwnerController, this, UDamageType::StaticClass());
			HitComp = Hit.GetComponent();
			if(HitComp && HitComp->IsSimulatingPhysics(Hit.BoneName))
			{
				FVector Direction = Hit.TraceEnd - Hit.TraceStart;
				Direction.Normalize();

				HitComp->AddImpulseAtLocation(Direction * 20000.f, Hit.ImpactPoint, Hit.BoneName);
			}
		}
	}
	Super::OnHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}



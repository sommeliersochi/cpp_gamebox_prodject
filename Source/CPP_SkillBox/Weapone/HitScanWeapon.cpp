#include "HitScanWeapon.h"

#include <Engine/SkeletalMeshSocket.h>
#include <Kismet/GameplayStatics.h>
#include <Particles/ParticleSystemComponent.h>
#include <Sound/SoundCue.h>

#include "CPP_SkillBoxCharacter.h"
#include "CPP_SkillBoxPlayerController.h"

void AHitScanWeapon::Fire(const FVector& HitTarget)
{
	Super::Fire(HitTarget);

	if (MuzzleFlashSocket)
	{
		FTransform SocketTransform = MuzzleFlashSocket->GetSocketTransform(GetWeaponMesh());
		FVector Start = SocketTransform.GetLocation();

		FHitResult FireHit;
		WeaponTraceHit(Start, HitTarget, FireHit);

		AController* InstigatorController = GetOwnerController();
		ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(FireHit.GetActor());
		if (Character && HasAuthority() && InstigatorController)
		{
			Character->MulticastHit();

			UGameplayStatics::ApplyDamage(Character, Damage, InstigatorController, this, UDamageType::StaticClass());
		}

		UWorld* World = GetWorld();
		UGameplayStatics::PlaySoundAtLocation(World, HitSound, FireHit.ImpactPoint);
		UGameplayStatics::SpawnEmitterAtLocation(World, ImpactParticles, FireHit.ImpactPoint, FireHit.ImpactNormal.Rotation());
		UGameplayStatics::SpawnEmitterAtLocation(World, MuzzleFlash, SocketTransform);
		PlayFireSound();
	}
}

void AHitScanWeapon::WeaponTraceHit(const FVector& TraceStart, const FVector& HitTarget, FHitResult& OutHit)
{
	UWorld* World = GetWorld();
	if (World)
	{
		const FVector End = bUseScatter ? TraceEndWithScatter(TraceStart, HitTarget) : TraceStart + (HitTarget - TraceStart) * 2.f;
		World->LineTraceSingleByChannel(OutHit, TraceStart, End, ECollisionChannel::ECC_Visibility);

		if (BeamParticles)
		{
			FVector BeamEnd = End;
			if (OutHit.bBlockingHit)
			{
				BeamEnd = OutHit.ImpactPoint;
			}

			UParticleSystemComponent* Beam = UGameplayStatics::SpawnEmitterAtLocation(World, BeamParticles, FTransform(TraceStart), true);
			if (Beam)
			{
				Beam->SetVectorParameter(FName("Target"), BeamEnd);
			}
		}
	}
}

FVector AHitScanWeapon::TraceEndWithScatter(const FVector& TraceStart, const FVector& HitTarget) const
{
	const FVector DirectionToTarget = (HitTarget - TraceStart).GetSafeNormal();
	const FVector SphereCenter = TraceStart + DirectionToTarget * DistanceToSphere;
	const FVector RandVec = FMath::VRand() * FMath::FRandRange(0.f, SphereRadius);
	const FVector EndLoc = SphereCenter + RandVec;
	const FVector ToEndLoc = EndLoc - TraceStart;

	return FVector(TraceStart + ToEndLoc * 5000.f / ToEndLoc.Size());
}

#pragma once

#define CUSTOM_DEPTH_PURPLE 250
#define CUSTOM_DEPTH_BLUE 251
#define CUSTOM_DEPTH_TAN 252



UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	EWT_AssaultRifle UMETA(DisplayName = "Assault Rifle"),
	EWT_RocketLauncher UMETA(DisplayName = "Rocket Launcher"),
	EWT_Pistol UMETA(DisplayName = "Pistol"),
	EWT_SubmachinGun UMETA(DisplayName = "Submachin Gun"),
	EWT_Shotgun UMETA(DisplayName = "Shotgun"),
	EWT_GrenadeLauncher UMETA(DisplayName = "Grenade Launcher"),
	EWT_Minigun UMETA(DisplayName = "Minigun"),
	EWT_Knife UMETA(DisplayName = "Knife"),
	EWT_Grenade UMETA(DisplayName = "Grenade"),
	
	EWT_MAX UMETA(DisplayName = "DefaultMAX")
};

UENUM(BlueprintType)
enum class EAmmoType : uint8
{
	EAT_None UMETA(DisplayName = "None"),
	EAT_9 UMETA(DisplayName = "9mm"),
	EAT_12 UMETA(DisplayName = "12mm"),
	EAT_45 UMETA(DisplayName = ".45"),
	EAT_5_45 UMETA(DisplayName = "5_45mm"),
	EAT_5_56 UMETA(DisplayName = "5_56mm"),
	EAT_7_62 UMETA(DisplayName = "7.62mm"),
	EAT_300 UMETA(DisplayName = ".300"),
	EAT_RPG UMETA(DisplayName = "RPG"),
	EAT_GrenadeLauncher UMETA(DisplayName = "AmmoGrenadeLauncher"),
	
	EAT_MAX UMETA(DisplayName = "DefaultMax")
};

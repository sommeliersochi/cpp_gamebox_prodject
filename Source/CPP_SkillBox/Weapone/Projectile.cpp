// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"

#include <NiagaraFunctionLibrary.h>
#include <Components/BoxComponent.h>
#include <Components/DecalComponent.h>
#include <Kismet/GameplayStatics.h>
#include <Sound/SoundCue.h>

#include "CPP_SkillBoxCharacter.h"
#include "CPP_SkillBox.h"

AProjectile::AProjectile()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	CollisionBox = CreateDefaultSubobject<UBoxComponent>("Collision Box");
	SetRootComponent(CollisionBox);
	CollisionBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	CollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	CollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	CollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);
	CollisionBox->SetCollisionResponseToChannel(ECC_SkeletalMesh, ECollisionResponse::ECR_Block);
	CollisionBox->bReturnMaterialOnMove = true;
}

void AProjectile::SetDamage(float NewDamage)
{
	Damage = NewDamage;
}

void AProjectile::BeginPlay()
{
	Super::BeginPlay();

	if (Trace)
	{
		TraceComp = UGameplayStatics::SpawnEmitterAttached(
			Trace,
			CollisionBox,
			FName(),
			GetActorLocation(),
			GetActorRotation(),
			EAttachLocation::KeepWorldPosition
		);
	}
	if (HasAuthority())
	{
		CollisionBox->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);
	}
}

void AProjectile::StartDestroyTimer()
{
	GetWorldTimerManager().SetTimer(
		DestroyTimer,
		this,
		&AProjectile::DestroyTimerFinished,
		DestroyTime
	);
}

void AProjectile::DestroyTimerFinished()
{
	Destroy();
}

void AProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                        FVector NormalImpulse, const FHitResult& Hit)
{
	HitResult = Hit;

	ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(OtherActor);

	if (Character)
	{
		Character->MulticastHit();
	}

	if (Hit.PhysMaterial.IsValid())
	{
		const auto PhysMate = Hit.PhysMaterial.Get();
		if (ImpactDataMap.Contains(PhysMate))
		{
			ImpactData = ImpactDataMap[PhysMate];
		}
	}

	Destroy();
}

void AProjectile::SpawnTrailSystem()
{
	if (TrailSystem)
	{
		TrailSystemComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(
			TrailSystem,
			GetRootComponent(),
			FName(),
			GetActorLocation(),
			GetActorRotation(),
			EAttachLocation::KeepWorldPosition,
			false);
	}
}

void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectile::Destroyed()
{
	Super::Destroyed();

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactData.ImpactParticles, GetActorTransform());

	UGameplayStatics::PlaySoundAtLocation(this, ImpactData.ImpactSound, GetActorLocation());

	const FDecalData& DecalData = ImpactData.DecalData;
	if (DecalData.ImpactDecal)
	{
		UDecalComponent* DecalImpact = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), DecalData.ImpactDecal, DecalData.Size, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), DecalData.LifeTime);
		if (DecalImpact)
		{
			DecalImpact->SetFadeOut(0.f, DecalData.FadeOutTime);
		}
	}

	if (ExplodeShake)
	{
		UGameplayStatics::PlayWorldCameraShake(this, ExplodeShake, GetActorLocation(), 4000.f, 4000.f);
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "ProjectileRocket.generated.h"

class URocketMovementComponent;
class UNiagaraComponent;
class UNiagaraSystem;
/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API AProjectileRocket : public AProjectile
{
	GENERATED_BODY()
public:
	AProjectileRocket();
	virtual void Destroyed() override;
	
protected:
	virtual void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void BeginPlay() override;
	
	void ExplodeDamage();

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Rocket")
	USoundCue* ProjectileLoop;

	UPROPERTY(VisibleAnywhere, Category = "Internal")
	UAudioComponent* ProjectileLoopComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Rocket")
	USoundAttenuation* LoopingSoundAttenuation;

	UPROPERTY(VisibleAnywhere, Category = "Internal")
	URocketMovementComponent* RocketMovementComponent;

	UPROPERTY(VisibleAnywhere, Category = "Internal")
	URadialForceComponent* RadialForceComp;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Rocket")
	float DamageInnerRadius = 200.f;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Rocket")
	float DamageOuterRadius = 500.f;

};

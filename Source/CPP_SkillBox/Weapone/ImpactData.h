#pragma once
#include "ImpactData.generated.h"

class USoundCue;

USTRUCT(BlueprintType)
struct FDecalData
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, Category= "ImpactEffectsSettings")
	UMaterialInstance* ImpactDecal = nullptr;

	UPROPERTY(EditAnywhere, Category= "ImpactEffectsSettings")
	FVector Size = FVector(10.f);

	UPROPERTY(EditAnywhere, Category= "ImpactEffectsSettings")
	float LifeTime = 10.f;

	UPROPERTY(EditAnywhere, Category= "ImpactEffectsSettings")
	float FadeOutTime = 5.f;	
};

USTRUCT(BlueprintType)
struct FImpactData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category= "ImpactEffectsSettings")
	UParticleSystem* ImpactParticles = nullptr;
	
	UPROPERTY(EditAnywhere, Category= "ImpactEffectsSettings")
	USoundCue* ImpactSound = nullptr;

	UPROPERTY(EditAnywhere, Category= "ImpactEffectsSettings")
	FDecalData DecalData;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "Weapon.h"
#include "ProjectileWeapon.generated.h"

UCLASS()
class CPP_SKILLBOX_API AProjectileWeapon : public AWeapon
{
	GENERATED_BODY()
public:

	virtual void Fire(const FVector& HitTarget) override;
private:

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Weapon")
	TSubclassOf<AProjectile> ProjectileClass;

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WeaponTypes.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

class UCombatComponent;
class ACasing;
class USoundCue;
class UNiagaraComponent;
class AAmmoPickup;
class UWidgetComponent;
class ACPP_SkillBoxCharacter;
class ACPP_SkillBoxPlayerController;
class USphereComponent;

UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	EWS_Initial UMETA(DisplayName = "Initial State"),
	EWS_Equipped UMETA(DisplayName = "Equipped"),
	EWS_EquippedSecondary UMETA(DisplayName = "Equipped Secondary"),
	EWS_Dropped UMETA(DisplayName = "Dropped"),

	EWS_MAX UMETA(DisplayName = "DefaultMAX")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FWeaponChangedAmmo, AWeapon*, Weapon, int32, NewValue);

UCLASS()
class CPP_SKILLBOX_API AWeapon : public AActor
{
	GENERATED_BODY()

public:
	AWeapon(const FObjectInitializer& ObjectInitializer);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProp) const override;

	virtual void BeginPlay() override;

	virtual void SetOwner(AActor* NewOwner) override;

	virtual void OnRep_Owner() override;

	virtual void Tick(float DeltaSeconds) override;

	void ShowPickupWidget(bool bShowWidget);

	UFUNCTION(BlueprintCallable)
	void Dropped();

	virtual void Fire(const FVector& HitTarget);

	void AddAmmo(const int32 AmmoToAdd);

	virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const;

	void SetWeaponState(EWeaponState State);

	void EnableCustomDepth(bool bEnable);

	USphereComponent* GetSphereComp() const;

	USkeletalMeshComponent* GetWeaponMesh() const;

	UFUNCTION(BlueprintCallable)
	EAmmoType GetAmmoType() const;

	UFUNCTION(BlueprintCallable)
	EWeaponType GetWeaponType() const;

	int32 GetAmmo() const;

	int32 GetMagCapacity() const;

	ACPP_SkillBoxCharacter* GetOwnerCharacter() const;

	ACPP_SkillBoxPlayerController* GetOwnerController() const;

	bool IsEmpty() const;

	bool IsFull() const;

	UFUNCTION(BlueprintPure, Category="Weapon")
	TSoftClassPtr<AAmmoPickup> GetPickupAmmoClass() const;

	void ActivateLaser();
	void DeactivateLaser();

protected:
	void TickLaser();

	virtual void OnWeaponStateSet();

	virtual void OnEquipped();
	virtual void OnDropped();
	virtual void OnEquippedSecondary();

	void PlayFireSound();

	UFUNCTION()
	virtual void OnSphereOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult
	);

	UFUNCTION()
	void OnSphereEndOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex
	);

	UFUNCTION()
	void OnOwnerReceiveControllerChanged(APawn* Pawn, AController* OldController, AController* NewController);

public:
	static const FName WeaponMeshComponentName;
	static const FName SphereCompName;
	static const FName PickupWidgetName;
	static const FName LineFXComponentName;
	static const FName LineImpactFXComponentName;

	UPROPERTY(BlueprintAssignable, Category="Weapon|Events")
	FWeaponChangedAmmo OnChangedAmmo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category= "Settings|Weapon")
	float BulletSpread = 0.f;

	UPROPERTY(EditDefaultsOnly, Category= "Settings|Crosshairs")
	TObjectPtr<UTexture2D> CrosshairsCenter;

	UPROPERTY(EditDefaultsOnly, Category= "Settings|Crosshairs")
	TObjectPtr<UTexture2D> CrosshairsLeft;

	UPROPERTY(EditDefaultsOnly, Category= "Settings|Crosshairs")
	TObjectPtr<UTexture2D> CrosshairsRight;

	UPROPERTY(EditDefaultsOnly, Category= "Settings|Crosshairs")
	TObjectPtr<UTexture2D> CrosshairsTop;

	UPROPERTY(EditDefaultsOnly, Category= "Settings|Crosshairs")
	TObjectPtr<UTexture2D> CrosshairsBottom;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Weapon")
	float FireDelay = 0.15f;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Weapon")
	bool bAutomatic = true;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Weapon")
	TObjectPtr<USoundCue> EquipSound;

	bool bDestroyWeapon = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Settings|Weapon")
	FName MuzzleSocketName = "MuzzleSocket";

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Settings|Weapon")
	bool bInfinity;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Weapon")
	TSoftClassPtr<AAmmoPickup> PickupAmmoClass;

protected:
	UPROPERTY(Transient)
	const USkeletalMeshSocket* MuzzleFlashSocket;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Weapon")
	TObjectPtr<USoundCue> FireSound;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Weapon")
	TObjectPtr<UParticleSystem> MuzzleFlash;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Weapon")
	float Damage = 20.0f;

	UPROPERTY(VisibleAnywhere, Category="Internal")
	TObjectPtr<USkeletalMeshComponent> WeaponMesh = nullptr;

	UPROPERTY(VisibleAnywhere, Category="Internal")
	TObjectPtr<USphereComponent> SphereComp = nullptr;

	UPROPERTY(VisibleAnywhere, Category="Internal")
	TObjectPtr<UWidgetComponent> PickupWidget = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category= "Internal")
	TObjectPtr<UNiagaraComponent> LaserFX = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category= "Internal")
	TObjectPtr<UNiagaraComponent> LaserImpactFX = nullptr;

	UPROPERTY(ReplicatedUsing = OnRep_WeaponState, VisibleAnywhere, Category="Internal")
	EWeaponState WeaponState;

	UFUNCTION()
	void OnRep_WeaponState();

	UPROPERTY(EditDefaultsOnly, Category="Settings|Weapon")
	TObjectPtr<UAnimationAsset> FireAnimation = nullptr;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Weapon")
	TSubclassOf<UCameraShakeBase> FireShake;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Weapon")
	TSubclassOf<ACasing> CasingClass;

	UPROPERTY(EditDefaultsOnly, ReplicatedUsing = OnRep_Ammo, Category="Settings|Weapon")
	int32 Ammo;

	UFUNCTION()
	void OnRep_Ammo();

	UPROPERTY(EditDefaultsOnly, Category="Settings|Weapon")
	int32 MagCapacity;

	TObjectPtr<ACPP_SkillBoxCharacter> OwnerCharacter;

	TObjectPtr<ACPP_SkillBoxPlayerController> OwnerController;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Weapon")
	EWeaponType WeaponType;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Weapon")
	EAmmoType AmmoType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category= "Settings|LaserFx")
	FName LaserTargetName = "LaserEnd";
};

FORCEINLINE_DEBUGGABLE USphereComponent* AWeapon::GetSphereComp() const
{
	return SphereComp;
}

FORCEINLINE_DEBUGGABLE USkeletalMeshComponent* AWeapon::GetWeaponMesh() const
{
	return WeaponMesh;
}

FORCEINLINE_DEBUGGABLE EAmmoType AWeapon::GetAmmoType() const
{
	return AmmoType;
}

FORCEINLINE_DEBUGGABLE EWeaponType AWeapon::GetWeaponType() const
{
	return WeaponType;
}

FORCEINLINE_DEBUGGABLE int32 AWeapon::GetAmmo() const
{
	return Ammo;
}

FORCEINLINE_DEBUGGABLE int32 AWeapon::GetMagCapacity() const
{
	return MagCapacity;
}

FORCEINLINE_DEBUGGABLE ACPP_SkillBoxCharacter* AWeapon::GetOwnerCharacter() const
{
	return OwnerCharacter;
}

FORCEINLINE_DEBUGGABLE ACPP_SkillBoxPlayerController* AWeapon::GetOwnerController() const
{
	return OwnerController;
}

FORCEINLINE_DEBUGGABLE TSoftClassPtr<AAmmoPickup> AWeapon::GetPickupAmmoClass() const
{
	return PickupAmmoClass;
}

FORCEINLINE_DEBUGGABLE bool AWeapon::IsEmpty() const
{
	return !bInfinity && Ammo <= 0;
}

FORCEINLINE_DEBUGGABLE bool AWeapon::IsFull() const
{
	return Ammo == MagCapacity;
}

using UnrealBuildTool;

public class CPP_SkillBox : ModuleRules
{
	public CPP_SkillBox(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core",
			"CoreUObject",
			"Engine",
			"InputCore",
			"UMG",
			"HeadMountedDisplay",
			"NavigationSystem",
			"AIModule",
			"Niagara",
			"PhysicsCore",
			"AIModule",
			"GameplayTasks",
			"GameplayTags",
			"NavigationSystem",
			"OnlineSubsystem",
			"OnlineSubsystemNull",
			"NetCore"
		});

		DynamicallyLoadedModuleNames.Add("OnlineSubsystemSteam");

		PublicIncludePaths.AddRange(new string[]
		{
			"CPP_SkillBox/Character",
			"CPP_SkillBox/Weapone",
			"CPP_SkillBox/Game",
			"CPP_SkillBox/FuncLibrary",
			"CPP_SkillBox/Components",
			"CPP_SkillBox/UI",
			"CPP_SkillBox/Items",
			"CPP_SkillBox/Actions",
			"CPP_SkillBox/Core",
			"CPP_SkillBox",
			"CPP_SkillBox/Base"
		});
	}
}
// Copyright Epic Games, Inc. All Rights Reserved.

#include "CPP_SkillBox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CPP_SkillBox, "CPP_SkillBox" );

DEFINE_LOG_CATEGORY(LogCPP_SkillBox)
DEFINE_LOG_CATEGORY(LogCPP_SkillBox_Net)
 
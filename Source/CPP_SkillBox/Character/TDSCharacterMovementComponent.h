// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "TDSCharacterMovementComponent.generated.h"

class ACPP_SkillBoxCharacter;
/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UTDSCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
public:
	virtual float GetMaxSpeed() const override;

protected:
	TObjectPtr<ACPP_SkillBoxCharacter> Character = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Movement", meta = (ClampMin = "1.5", ClampMax = "10.0"))
	float SprintModifier = 2.0f;
};

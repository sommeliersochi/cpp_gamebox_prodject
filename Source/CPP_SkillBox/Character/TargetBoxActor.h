// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TargetBoxActor.generated.h"


class UBoxComponent;

UCLASS()
class CPP_SKILLBOX_API ATargetBoxActor : public AActor
{
	GENERATED_BODY()
	
public:	

	ATargetBoxActor();

	UFUNCTION(BlueprintGetter)
	UBoxComponent* GetBoxComponent()
	{
		return Box;
	}

protected:
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Combat")
	UBoxComponent* Box;

};

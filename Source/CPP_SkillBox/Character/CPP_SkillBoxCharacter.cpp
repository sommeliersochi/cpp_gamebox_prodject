
#include "CPP_SkillBoxCharacter.h"

#include <Camera/CameraComponent.h>
#include <Components/CapsuleComponent.h>
#include <Components/DecalComponent.h>
#include <GameFramework/CharacterMovementComponent.h>
#include <GameFramework/SpringArmComponent.h>
#include <Kismet/GameplayStatics.h>
#include <Kismet/KismetMathLibrary.h>
#include <Net/UnrealNetwork.h>
#include <PhysicalMaterials/PhysicalMaterial.h>
#include <Sound/SoundCue.h>

#include "CombatComponent.h"
#include "CPP_SkillBoxPlayerController.h"
#include "CPP_SkillBox.h"
#include "Inventory/InventoryComponent.h"
#include "TDSCharacterMovementComponent.h"
#include "Weapon.h"
#include "InteractBaseWidgetParent.h"
#include "DamageIndicator/DamageIndicatorComponent.h"
#include "FirstAidKit/FirstAidKitComponent.h"
#include "SActionComponent.h"
#include "SAttributeComponent.h"
#include "SInteractionComponent.h"
#include "SWorldUserWidget.h"
#include "AI/SAICharacterBase.h"
#include "Animations/SRoll_AnimNotify.h"
#include "FindTarget/FindTargetComponent.h"
#include "KnifeTarget/FindKnifeTargetComponent.h"

const FName ACPP_SkillBoxCharacter::CameraBoomComponentName = TEXT("CameraBoom");
const FName ACPP_SkillBoxCharacter::TopDownCameraComponentName = TEXT("TopDownCamera");
const FName ACPP_SkillBoxCharacter::CombatComponentName = TEXT("CombatComp");
const FName ACPP_SkillBoxCharacter::AttributeComponentName = TEXT("AttributeComp");
const FName ACPP_SkillBoxCharacter::ActionComponentName = TEXT("ActionComp");
const FName ACPP_SkillBoxCharacter::InteractComponentName = TEXT("InteractComp");
const FName ACPP_SkillBoxCharacter::AttachedGrenadeComponentName = TEXT("Attached Grenade");
const FName ACPP_SkillBoxCharacter::AttachedKnifeComponentName = TEXT("Attached Knife");
const FName ACPP_SkillBoxCharacter::LineFXComponentName = TEXT("LineFX");
const FName ACPP_SkillBoxCharacter::LineImpactFXComponentName = TEXT("LineImpactFX");
const FName ACPP_SkillBoxCharacter::DamageIndicatorComponentName = TEXT("DamageIndicatorComponent");
const FName ACPP_SkillBoxCharacter::InventoryComponentName = TEXT("InventoryComponent");
const FName ACPP_SkillBoxCharacter::FirstAidKitComponentName = TEXT("FirstAidKitComponent");
const FName ACPP_SkillBoxCharacter::FindKnifeTargetComponentName = TEXT("FindKnifeTargetComponent");
const FName ACPP_SkillBoxCharacter::FindTargetComponentName = TEXT("FindTargetComponent");

ACPP_SkillBoxCharacter::ACPP_SkillBoxCharacter(const FObjectInitializer& ObjInit) :
	Super(ObjInit.SetDefaultSubobjectClass<UTDSCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	GetCharacterMovement()->NavAgentProps.bCanCrouch = true;

	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	GetMesh()->SetCollisionObjectType(ECC_SkeletalMesh);

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(ACPP_SkillBoxCharacter::CameraBoomComponentName);
	CameraBoom->SetupAttachment(GetMesh());
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = 2200.f;
	CameraBoom->SetRelativeRotation(FRotator(-40.f, -15.f, 0.0f));
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->bEnableCameraLag = false;
	CameraBoom->CameraLagSpeed = 2.0f;

	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(
		ACPP_SkillBoxCharacter::TopDownCameraComponentName);
	TopDownCameraComponent->SetupAttachment(CameraBoom);
	TopDownCameraComponent->bUsePawnControlRotation = false;

	CombatComp = CreateDefaultSubobject<UCombatComponent>(ACPP_SkillBoxCharacter::CombatComponentName);
	CombatComp->SetIsReplicated(true);

	AttributeComp = CreateDefaultSubobject<USAttributeComponent>(ACPP_SkillBoxCharacter::AttributeComponentName);

	ActionComp = CreateDefaultSubobject<USActionComponent>(ACPP_SkillBoxCharacter::ActionComponentName);

	InteractComp = CreateDefaultSubobject<USInteractionComponent>(ACPP_SkillBoxCharacter::InteractComponentName);

	AttachedGrenade = CreateDefaultSubobject<
		UStaticMeshComponent>(ACPP_SkillBoxCharacter::AttachedGrenadeComponentName);
	AttachedGrenade->SetupAttachment(GetMesh(), FName("GrenadeSocket"));
	AttachedGrenade->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	AttachedKnife = CreateDefaultSubobject<UStaticMeshComponent>(ACPP_SkillBoxCharacter::AttachedKnifeComponentName);
	AttachedKnife->SetupAttachment(GetMesh(), FName("KnifeSocket"));
	AttachedKnife->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	DamageIndicatorComponent = CreateDefaultSubobject<UDamageIndicatorComponent>(
		ACPP_SkillBoxCharacter::DamageIndicatorComponentName);

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(ACPP_SkillBoxCharacter::InventoryComponentName);

	FirstAidKitComponent = CreateDefaultSubobject<UFirstAidKitComponent>(
		ACPP_SkillBoxCharacter::FirstAidKitComponentName);

	FindKnifeTargetComponent = CreateDefaultSubobject<UFindKnifeTargetComponent>(
		ACPP_SkillBoxCharacter::FindKnifeTargetComponentName);

	FindTargetComponent = CreateDefaultSubobject<UFindTargetComponent>(ACPP_SkillBoxCharacter::FindTargetComponentName);
	HeadMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HeadMeshComponent"));
	BodyMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("BodyMeshComponent"));
	PantsMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PantsMeshComponent"));
	FootMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FootMeshComponent"));

	HeadMeshComponent->SetupAttachment(GetMesh());
	BodyMeshComponent->SetupAttachment(GetMesh());
	PantsMeshComponent->SetupAttachment(GetMesh());
	FootMeshComponent->SetupAttachment(GetMesh());
}

bool ACPP_SkillBoxCharacter::CanBeSeenFrom(const FVector& ObserverLocation, FVector& OutSeenLocation,
	int32& NumberOfLoSChecksPerformed, float& OutSightStrength, const AActor* IgnoreActor, const bool* bWasVisible,
	int32* UserData) const
{
	static const FName NAME_AILineOfSight = FName(TEXT("TestPawnLineOfSight"));

	FHitResult HitResult;
	FVector SocketLocation = GetMesh()->GetSocketLocation(PerceptionTarget);

	const bool bHitSocket = GetWorld()->LineTraceSingleByObjectType(HitResult, ObserverLocation, SocketLocation,
																	FCollisionObjectQueryParams(ECC_TO_BITFIELD
																   (ECC_WorldStatic) | ECC_TO_BITFIELD(ECC_WorldDynamic)),
																	FCollisionQueryParams(NAME_AILineOfSight, true,
																	IgnoreActor));

	NumberOfLoSChecksPerformed++;

	if (bHitSocket == false || (HitResult.GetActor() && HitResult.GetActor()->IsOwnedBy(this)))
	{
		OutSeenLocation = SocketLocation;
		OutSightStrength = 1;

		return true;
	}

	const bool bHit = GetWorld()->LineTraceSingleByObjectType(HitResult, ObserverLocation, GetActorLocation(),
															  FCollisionObjectQueryParams(ECC_TO_BITFIELD(ECC_WorldStatic) |
															  ECC_TO_BITFIELD(ECC_WorldDynamic)), FCollisionQueryParams
															 (NAME_AILineOfSight, true, IgnoreActor));

	NumberOfLoSChecksPerformed++;

	if (bHit == false || (HitResult.GetActor() && HitResult.GetActor()->IsOwnedBy(this)))
	{
		OutSeenLocation = GetActorLocation();
		OutSightStrength = 1;

		return true;
	}

	OutSightStrength = 0;
	return false;
}

FHitResult ACPP_SkillBoxCharacter::CapsuleTrace()
{
	FHitResult OutHit;
	TArray<AActor*> ActorsToIgnore;
	ActorsToIgnore.AddUnique(this);

	FVector EyesLoc;
	FRotator EyesRot;
	GetController()->GetPlayerViewPoint(EyesLoc, EyesRot);

	const FVector End = (EyesRot.Vector() * 2000.f) + EyesLoc + FVector(0.f, 0.f, 120.f);

	UKismetSystemLibrary::SphereTraceSingle(GetWorld(), EyesLoc, End, 20.f, ETraceTypeQuery::TraceTypeQuery_MAX, false, ActorsToIgnore, EDrawDebugTrace::None, OutHit, true, FColor::Green);
	return OutHit;
}

void ACPP_SkillBoxCharacter::ToggleCombat(const bool NewBool)
{
	GetMesh()->GetAnimInstance()->StopAllMontages(0.2f);
	
	AnimValues.bIsInCombat = NewBool;
	
	bUseControllerRotationYaw = NewBool;
	
	GetCharacterMovement()->bOrientRotationToMovement = !NewBool;
	
	FName NewSocket = NewBool ? "hand_rSocket" : "spine_03Socket";
	
	NewBool ? GetCombat()->AttachActorToRightHand(StartingWeapon) : GetCombat()->AttachActorToArmory(StartingWeapon);/*AttachWeapon(DefaultWeaponClass, NewSocket);*/
	
	GetCharacterMovement()->MaxWalkSpeed = (NewBool) ? 187.f : 94.f;
}

void ACPP_SkillBoxCharacter::ToggleCrouch(const bool NewBool)
{
	AnimValues.bIsCrouching = NewBool;
	const float Speed = AnimValues.bIsInCombat ? 187.f : WalkSpeed;
	GetCharacterMovement()->MaxWalkSpeed = (NewBool) ? CrouchedWalkSpeed : Speed;
}

void ACPP_SkillBoxCharacter::ToggleADS(const bool NewBool)
{
	AnimValues.bADS = NewBool;
}

void ACPP_SkillBoxCharacter::ToggleSprinting(bool NewBool)
{
	if (NewBool)
	{
		GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
		return;
	}
	
	GetCharacterMovement()->MaxWalkSpeed = (AnimValues.bIsInCombat) ? 187.f : WalkSpeed;
}

void ACPP_SkillBoxCharacter::SetGenericTeamId(const FGenericTeamId& TeamID)
{
	IGenericTeamAgentInterface::SetGenericTeamId(TeamID);
}

void ACPP_SkillBoxCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ACPP_SkillBoxCharacter, OverlappingWeapon, COND_OwnerOnly);
	DOREPLIFETIME(ACPP_SkillBoxCharacter, PossessedInfo);
	DOREPLIFETIME(ACPP_SkillBoxCharacter, Faction);
	DOREPLIFETIME(ACPP_SkillBoxCharacter, bIsTryAnnex);
}

void ACPP_SkillBoxCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (!AttributeComp->IsDead() && IsPlayerControlled())
	{
		//CameraInterpZoom(DeltaSeconds); 
		//UpdateMouseAim(DeltaSeconds); //TODO::For Gamepad
		//CheckCollision(DeltaSeconds);
		if (IsLocallyPlayerControlled())
		{
			MovementTick(DeltaSeconds);
			TickCameraZoom(DeltaSeconds);
		}
		//LookAtTarget(DeltaSeconds); //TODO::For Gamepad


		if (IsSprint())
		{
			AttributeComp->ChangeStamina(AttributeComp->GetTired());
		}
	}

	Direction = GetMovementDirection();

	if (!GetVelocity().IsNearlyZero())
	{
		LastDirection = Direction;

		bIsMovingForward = FMath::Abs(Direction) <= AngleError;
	}
	else
	{
		bIsMovingForward = true;
	}
}

void ACPP_SkillBoxCharacter::BeginPlay()
{
	Super::BeginPlay();
	SpawnDefaultWeapon();

	if (TopDownCameraComponent)
	{
		CameraDefaultFOV = GetTopDownCamera()->FieldOfView = CameraCurrentFOV;
	}

	if (AttachedGrenade)
	{
		AttachedGrenade->SetVisibility(false);
	}

	if (AttachedKnife)
	{
		AttachedKnife->SetVisibility(false);
	}

	PlayerControllerRef = GetController<ACPP_SkillBoxPlayerController>();

	HeadMeshComponent->SetLeaderPoseComponent(GetMesh());
	BodyMeshComponent->SetLeaderPoseComponent(GetMesh());
	PantsMeshComponent->SetLeaderPoseComponent(GetMesh());
	FootMeshComponent->SetLeaderPoseComponent(GetMesh());
}

void ACPP_SkillBoxCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	if (AttributeComp)
	{
		AttributeComp->OnHealthChanged.AddDynamic(this, &ACPP_SkillBoxCharacter::OnHealthChange);
		AttributeComp->OnDeath.AddDynamic(this, &ACPP_SkillBoxCharacter::OnDeath);
	}
	TeamId = int(Faction);
}

void ACPP_SkillBoxCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	PossessedInfo.Possessed = true;
	OnRep_PossessedInfo();
}

void ACPP_SkillBoxCharacter::UnPossessed()
{
	Super::UnPossessed();

	PossessedInfo.Possessed = false;
	OnRep_PossessedInfo();
}

void ACPP_SkillBoxCharacter::OnRep_Controller()
{
	Super::OnRep_Controller();
}

void ACPP_SkillBoxCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);
	check(NewInputComponent);
	check(CombatComp);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ACPP_SkillBoxCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ACPP_SkillBoxCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &ACPP_SkillBoxCharacter::OnStartSprint);
	NewInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &ACPP_SkillBoxCharacter::OnStopSprint);

	NewInputComponent->BindAction(TEXT("Reload"), IE_Pressed, this, &ACPP_SkillBoxCharacter::ReloadButtonPressed);
	NewInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &ACPP_SkillBoxCharacter::FireButtonPressed);
	NewInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &ACPP_SkillBoxCharacter::FireButtonRealised);

	NewInputComponent->BindAction(TEXT("DropCombatGrenade"), IE_Pressed, this,
	                              &ACPP_SkillBoxCharacter::SelectCombatGrenade);
	NewInputComponent->BindAction(TEXT("DropTacticalGrenade"), IE_Pressed, this,
	                              &ACPP_SkillBoxCharacter::SelectTacticalGrenade);
	NewInputComponent->BindAction(TEXT("ThrowGrenade"), IE_Released, this,
	                              &ACPP_SkillBoxCharacter::GrenadeButtonReleased);

	//NewInputComponent->BindAction("NextWeapon", IE_Pressed, WeaponComponent, &USWeaponComponent::NextWeapon);

	NewInputComponent->BindAction(TEXT("Aiming"), IE_Pressed, this, &ACPP_SkillBoxCharacter::AimingButtonPressed);
	NewInputComponent->BindAction(TEXT("Aiming"), IE_Released, this, &ACPP_SkillBoxCharacter::AimingButtonReleased);

	NewInputComponent->BindAction(TEXT("WeaponPickup"), IE_Pressed, this, &ACPP_SkillBoxCharacter::EquipButtonPressed);
	NewInputComponent->BindAction(TEXT("Crouch"), IE_Pressed, this, &ACPP_SkillBoxCharacter::CrouchButtonPressed);

	NewInputComponent->BindAction(TEXT("PrimaryInteract"), IE_Pressed, this, &ACPP_SkillBoxCharacter::PrimaryInteract);
	NewInputComponent->BindAction(TEXT("PrimaryInteract"), IE_Released, this,
	                              &ACPP_SkillBoxCharacter::PrimaryInteractReleased);

	NewInputComponent->BindAction(TEXT("Roll"), IE_Pressed, this, &ACPP_SkillBoxCharacter::RollPressed);

	NewInputComponent->BindAction(TEXT("KnifeAttack"), IE_Pressed, this, &ACPP_SkillBoxCharacter::KnifeAttack);

	NewInputComponent->BindAction(TEXT("CameraLengthChangeUp"), IE_Pressed, this, &ACPP_SkillBoxCharacter::CameraUp);
	NewInputComponent->
		BindAction(TEXT("CameraLengthChangeDown"), IE_Pressed, this, &ACPP_SkillBoxCharacter::CameraDown);

	NewInputComponent->BindAction(TEXT("UseFirstAidKits"), IE_Pressed, this, &ACPP_SkillBoxCharacter::UseFirstAidKits);

	NewInputComponent->BindAction(TEXT("ReplaceArmorPlate"), IE_Pressed, this, &ACPP_SkillBoxCharacter::ReplaceArmor);
}

void ACPP_SkillBoxCharacter::SetHealthBarVisibility(bool bIsVisibility)
{
	if (ActivateHealthBar)
	{
		if (bIsVisibility)
		{
			ActivateHealthBar->SetVisibility(ESlateVisibility::Visible);
		}
		else
		{
			ActivateHealthBar->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}

UAnimMontage* ACPP_SkillBoxCharacter::ReturnRandomVariation()
{
	int i = UKismetMathLibrary::RandomIntegerInRange(0, IdleVariations.Num() - 1);
	return IdleVariations[i];
}

void ACPP_SkillBoxCharacter::SetTryAnnex_OnServer_Implementation(bool bIsTry)
{
	bIsTryAnnex = bIsTry;
}

void ACPP_SkillBoxCharacter::InputAxisY(float Value)
{
	AxisY = Value;
	/*if ((Controller == NULL) || (Value == 0.0f))
	{
		return;
	}

	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);
	const FVector NewDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	AddMovementInput(NewDirection, Value);*/
}

void ACPP_SkillBoxCharacter::InputAxisX(float Value)
{
	AxisX = Value;
	/*if ((Controller == NULL) || (Value == 0.0f))
	{
		return;
	}

	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);
	const FVector NewDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	AddMovementInput(NewDirection, Value);*/
}

void ACPP_SkillBoxCharacter::RollPressed()
{
	ServerRollPressed();
}

void ACPP_SkillBoxCharacter::ServerRollPressed_Implementation()
{
	NetMulticastRollPressed();
}

void ACPP_SkillBoxCharacter::NetMulticastRollPressed_Implementation()
{
	PlayRollAnimMontage();
}

void ACPP_SkillBoxCharacter::PlayRollAnimMontage()
{
	if (!RollAnimMontage)
	{
		return;
	}

	if (!bIsCanRoll || bIsKnifeAttack)
	{
		return;
	}

	if (!GetCharacterMovement() || GetCharacterMovement()->IsFalling() || IsSprint())
	{
		return;
	}

	if (GetMesh()->GetAnimInstance()->Montage_IsPlaying(RollAnimMontage))
	{
		return;
	}

	CombatComp->FireButtonPressed(false);

	GetMesh()->GetAnimInstance()->StopAllMontages(0.01);
	bIsRolling = true;
	ChangeMovementState();
	GetCharacterMovement()->MaxAcceleration = 5000.0f;
	GetCharacterMovement()->MaxWalkSpeedCrouched = MovementInfo.RollSpeed;
	FTimerHandle RollEnd_Timer;
	GetWorldTimerManager().SetTimer(RollEnd_Timer, this, &ACPP_SkillBoxCharacter::EndRoll,
	                                RollAnimMontage->GetPlayLength(),
	                                false);
	GetWorldTimerManager().SetTimer(MoveRoll_Timer, this, &ACPP_SkillBoxCharacter::AddRollMove, 0.001, true);
	GetMesh()->GetAnimInstance()->Montage_Play(RollAnimMontage);
	//GetMesh()->GetAnimInstance()->Montage_SetEndDelegate(EndDelegate,RollAnimMontage);
	//EndDelegate.BindUObject(this,&ACPP_SkillBoxCharacter::EndRoll);
	bIsCanRoll = false;

	if (!RollAnimMontage->IsNotifyAvailable())
	{
		return;
	}

	const auto AnimNotifies = RollAnimMontage->Notifies;
	for (const auto& AnimNotify : AnimNotifies)
	{
		auto RollEndNotify = Cast<USRoll_AnimNotify>(AnimNotify.Notify);
		if (RollEndNotify)
		{
			UE_LOG(LogTemp, Warning, TEXT("EXAMPLE --- NOTIFY AVAIABLE")); // LOG ONLY FOR TEST SOME
			if (!RollEndNotify->OnNotified.IsBoundToObject(this))
			{
				RollEndNotify->OnNotified.AddUFunction(this, FName("OnRollEndNotify"), AnimNotify.NotifyName);
			}
		}
	}
}

void ACPP_SkillBoxCharacter::AddRollMove()
{
	GetCharacterMovement()->AddInputVector(GetCapsuleComponent()->GetForwardVector());
}

void ACPP_SkillBoxCharacter::EndRoll()
{
	GetWorldTimerManager().ClearTimer(MoveRoll_Timer);
	GetCharacterMovement()->MaxAcceleration = 1024.0f;
	bIsCanRoll = true;
}

FName ACPP_SkillBoxCharacter::OnRollEndNotify()
{
	UE_LOG(LogTemp, Warning, TEXT("EXAMPLE --- NOTIFY FUNCTION TEST")); // LOG ONLY FOR TEST SOME
	bIsRolling = false;
	CombatComp->SetCanFire(true);
	CombatComp->SetCombatState(ECombatState::ECS_Unoccupied);
	ChangeMovementState();
	GetWorldTimerManager().ClearTimer(MoveRoll_Timer);
	return FName();
}

void ACPP_SkillBoxCharacter::ResetMovementParam()
{
	bIsKnifeAttack = false;
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 360.f, 0.f);
}

void ACPP_SkillBoxCharacter::PlayFireAnim(UAnimMontage* Animation)
{
	PlayAnimMontage(Animation);
}

void ACPP_SkillBoxCharacter::PlayThrowGrenadeMontage()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance)
	{
		AnimInstance->Montage_Play(ThrowGrenadeMontage);
	}
}

void ACPP_SkillBoxCharacter::PlayReloadMontage()
{
	if (CombatComp == nullptr || CombatComp->GetEquippedWeapon() == nullptr || bIsRolling) return;

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance) // && ReloadMontage) not need check - auto
	{
		AnimInstance->StopAllMontages(0.f);
		AnimInstance->Montage_Play(ReloadMontage);
		FName SectionName;

		switch (CombatComp->GetEquippedWeapon()->GetWeaponType())
		{
		case EWeaponType::EWT_AssaultRifle:
			SectionName = FName("Rifle");
			break;
		case EWeaponType::EWT_RocketLauncher:
			SectionName = FName("RocketLauncher");
			break;
		case EWeaponType::EWT_Pistol:
			SectionName = FName("Pistol");
			break;
		case EWeaponType::EWT_SubmachinGun:
			SectionName = FName("Rifle");
			break;
		case EWeaponType::EWT_Shotgun:
			SectionName = FName("Shotgun");
			break;
		case EWeaponType::EWT_GrenadeLauncher:
			SectionName = FName("GrenadeLauncher");
			break;
		}
		AnimInstance->Montage_JumpToSection(SectionName);
	}
}

void ACPP_SkillBoxCharacter::PlaySwapMontage()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance) //  && SwapMontage) not need check - auto
	{
		AnimInstance->Montage_Play(SwapMontage);
	}
}

void ACPP_SkillBoxCharacter::PrimaryInteractReleased()
{
	SetTryAnnex_OnServer(false);
}

void ACPP_SkillBoxCharacter::ReplaceArmor()
{
	if (InventoryComponent)
	{
		InventoryComponent->ReloadArmor_OnServer();
	}
}

void ACPP_SkillBoxCharacter::PlayHitReactMontage()
{
	if (CombatComp == nullptr || CombatComp->GetEquippedWeapon() == nullptr) return;

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance) // && HitReactMontage) not need check - auto
	{
		AnimInstance->Montage_Play(HitReactMontage);
		FName SectionName("FromFront");
		AnimInstance->Montage_JumpToSection(SectionName);
	}
}

EPhysicalSurface ACPP_SkillBoxCharacter::GetSurfaceType()
{
	FHitResult HitResult;
	const FVector Start{GetActorLocation() + FVector(0.0f, 0.0f, -80)};
	const FVector End{Start + FVector(0.f, 0.f, -400.f)};
	FCollisionQueryParams QueryParams;
	QueryParams.bReturnPhysicalMaterial = true;
	QueryParams.AddIgnoredActor(this);

	GetWorld()->LineTraceSingleByChannel(
		HitResult,
		Start,
		End,
		ECollisionChannel::ECC_EngineTraceChannel2,
		QueryParams
	);
	DrawDebugLine(GetWorld(), Start, End, FColor::Blue, false, 1, 0, 5);
	DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 5, 5, FColor::Green, false, 1, 0, 5);
	return UPhysicalMaterial::DetermineSurfaceType(HitResult.PhysMaterial.Get());
}

void ACPP_SkillBoxCharacter::MovementTick(float DeltaTime)
{
	if (MovementState == EMovementState::SprintRun_State)
	{
		AddMovementInput(GetActorForwardVector(), 1.0f, false);
	}
	else
	{
		AddMovementInput(GetTopDownCamera()->GetForwardVector(), AxisX, false);
		AddMovementInput(GetTopDownCamera()->GetRightVector(), AxisY, false);
	}
}

void ACPP_SkillBoxCharacter::LookAtTarget(float DeltaTime)
{
	if (const auto AimActor = CombatComp->GetTargetActor())
	{
		const FVector Start = GetCapsuleComponent()->GetComponentLocation();
		const FVector Target = AimActor->GetActorLocation();
		const FRotator AimRot = UKismetMathLibrary::FindLookAtRotation(Start, Target);
		Controller->SetControlRotation(AimRot);
	}
}

void ACPP_SkillBoxCharacter::ChangeMovementSpeed()
{
	if (MovementState == EMovementState::Roll_State)
	{
		ResSpeed = MovementInfo.RollSpeed;
	}
	else if (MovementState == EMovementState::Aim_State)
	{
		ResSpeed = MovementInfo.AimSpeedNormal;
	}
	else if (MovementState == EMovementState::AimWalk_State)
	{
		ResSpeed = MovementInfo.AimSpeedWalk;
	}
	else if (MovementState == EMovementState::Walk_State)
	{
		ResSpeed = MovementInfo.WalkSpeedNormal;
	}
	else if (MovementState == EMovementState::Run_State)
	{
		ResSpeed = MovementInfo.RunSpeedNormal;
	}
	else if (MovementState == EMovementState::SprintRun_State)
	{
		ResSpeed = MovementInfo.SprintSpeedRun;
	}

	if (MovementState == EMovementState::Crouch_State)
	{
		GetCharacterMovement()->MaxWalkSpeedCrouched = ResSpeed;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

double ACPP_SkillBoxCharacter::GetMovementDirection()
{
	const auto VelocityNormal = GetVelocity().GetSafeNormal();
	const auto AngleBetween = FMath::Acos(FVector::DotProduct(GetActorForwardVector(), VelocityNormal));
	const auto CrossProduct = FVector::CrossProduct(GetActorForwardVector(), VelocityNormal);
	return FMath::RadiansToDegrees(AngleBetween) * FMath::Sign(CrossProduct.Z);
}

UCameraComponent* ACPP_SkillBoxCharacter::GetTopDownCamera() const
{
	return TopDownCameraComponent;
}

void ACPP_SkillBoxCharacter::AimingButtonPressed()
{
	if (CombatComp)
	{
		CombatComp->SetAiming(true);
	}
}

void ACPP_SkillBoxCharacter::AimingButtonReleased()
{
	if (CombatComp)
	{
		CombatComp->SetAiming(false);
	}
}

void ACPP_SkillBoxCharacter::CameraInterpZoom(float DeltaSecond)
{
	if (IsAiming())
	{
		CameraCurrentFOV = FMath::FInterpTo(CameraCurrentFOV, CameraZoomedFOV, DeltaSecond, ZoomInterpSpeed);
	}
	else
	{
		CameraCurrentFOV = FMath::FInterpTo(CameraCurrentFOV, CameraDefaultFOV, DeltaSecond, ZoomInterpSpeed);
	}
	GetTopDownCamera()->SetFieldOfView(CameraCurrentFOV);
}

void ACPP_SkillBoxCharacter::CameraUp()
{
	CameraDistance = FMath::Clamp(CameraDistance - CameraDistanceStep, MinCameraDistance, MaxCameraDistance);
}

void ACPP_SkillBoxCharacter::CameraDown()
{
	CameraDistance = FMath::Clamp(CameraDistance + CameraDistanceStep, MinCameraDistance, MaxCameraDistance);
}

FVector2D ACPP_SkillBoxCharacter::GetMouseVelocity()
{
	float DeltaX = .0f;
	float DeltaY = .0f;

	/*APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);*/
	Controller = Controller == nullptr ? Cast<ACPP_SkillBoxPlayerController>(GetController()) : Controller;
	const auto PlayerController = GetController<APlayerController>();

	if (!Controller && !PlayerController)
	{
		//UE_LOG(LogTemp, Error, TEXT(" Player Controller is NULL. ")) // 
		return FVector2D(DeltaX, DeltaY);
	}

	PlayerController->GetInputMouseDelta(DeltaX, DeltaY);
	FVector2D MouseDelta = UKismetMathLibrary::MakeVector2D(DeltaX, DeltaY);
	FVector2D ReturnVector = MouseDelta / UGameplayStatics::GetWorldDeltaSeconds(GetWorld());

	return ReturnVector;
}

FRotator ACPP_SkillBoxCharacter::GetMouseAimDirection(float DeltaSecond)
{
	/*APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);*/
	Controller = Controller == nullptr ? Cast<ACPP_SkillBoxPlayerController>(GetController()) : Controller;
	if (Controller)
	{
		FRotator PCRot = Controller->GetControlRotation();

		FRotator MouseRot = UKismetMathLibrary::MakeRotator(
			0.0f, 0.0f, UKismetMathLibrary::DegAtan2(GetMouseVelocity().X, GetMouseVelocity().Y));

		FRotator ReturnRot = FMath::RInterpTo(PCRot, MouseRot, DeltaSecond, MouseSmoothingStrength);

		return ReturnRot;
	}
	return FRotator(0.0f, 0.0f, 0.0f);
}

bool ACPP_SkillBoxCharacter::IsMouseAboveDeadZone()
{
	float Vector2dLength = UKismetMathLibrary::VSize2D(GetMouseVelocity());

	return Vector2dLength > MouseDeadZone;
}

void ACPP_SkillBoxCharacter::UpdateMouseAim(float DeltaSecond)
{
	/*APlayerController* PC = UGameplayStatics::GetPlayerController(Cast<ACharacter>(GetOwner());*/
	Controller = Controller == nullptr ? Cast<ACPP_SkillBoxPlayerController>(GetController()) : Controller;
	if (Controller && IsMouseAboveDeadZone())
	{
		Controller->SetControlRotation(GetMouseAimDirection(DeltaSecond));
	}
}

void ACPP_SkillBoxCharacter::TickCameraZoom(float DeltaSecond)
{
	if (CameraBoom)
	{
		CameraBoom->TargetArmLength = FMath::FInterpTo(CameraBoom->TargetArmLength, CameraDistance, DeltaSecond,
		                                               ZoomInterpSpeed);
	}
}

void ACPP_SkillBoxCharacter::SpawnDefaultWeapon()
{
	UWorld* World = GetWorld();
	if (World && DefaultWeaponClass && CombatComp && HasAuthority())
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Instigator = this;
		SpawnParameters.ObjectFlags = RF_Transient;

		StartingWeapon = World->SpawnActor<AWeapon>(DefaultWeaponClass.LoadSynchronous(), SpawnParameters);
		StartingWeapon->bDestroyWeapon = true;
		CombatComp->EquipWeapon(StartingWeapon);
	}
}

void ACPP_SkillBoxCharacter::OnHealthChange(AActor* InstigatorActor, USAttributeComponent* OwningComp, float NewHealth,
                                            float Delta)
{
	if (Delta < 0.0f && !AttributeComp->IsDead())
	{
		if (ActivateHealthBar)
		{
			ActivateHealthBar->SetVisibility(ESlateVisibility::Visible);
		}

		GetMesh()->SetScalarParameterValueOnMaterials(TimeToHitParamName, GetWorld()->TimeSeconds);

		FVector DecalLoc = FVector(GetActorLocation() - FVector(0.0f, 0.0f, 90.f));

		UDecalComponent* DecalBlood = UGameplayStatics::SpawnDecalAtLocation(
			GetWorld(),
			BloodDecal,
			FVector(30.f, 60.f, 60.f),
			DecalLoc,
			FRotator(-90.f, 0.f, FMath::FRandRange(-30.f, 30.f)),
			10.f);
		//DrawDebugSphere(GetWorld(), DecalLoc , 20.f,20.f, FColor::Red, true);

		if (DecalBlood)
		{
			DecalBlood->SetFadeOut(0.f, 10.0f);
		}

		if (ActivateHealthBar == nullptr)
		{
			ActivateHealthBar = CreateWidget<USWorldUserWidget>(GetWorld(), HealthBarWidgetClass);
			if (ActivateHealthBar)
			{
				ActivateHealthBar->AttachedActor = this;
				ActivateHealthBar->AddToViewport();
			}
		}
		/*if(NewHealth <= 0.0f)
		{
			AAIController* AIController = Cast<AAIController>(GetController());
			if(AIController)
			{
				AIController->GetBrainComponent()->StopLogic("Killed");
			}

			GetMesh()->SetAllBodiesSimulatePhysics(true);
			GetMesh()->SetCollisionProfileName("Ragdoll");

			GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			GetCharacterMovement()->DisableMovement();

			SetLifeSpan(4.0f);
		}*/
	}
}

bool ACPP_SkillBoxCharacter::CheckCollision(float DeltaSecond)
{
	FVector StartLoc = GetCapsuleComponent()->GetComponentLocation() - FVector(360.f, -50.f, -200.f);
	FVector TargetLoc = StartLoc;

	TArray<AActor*> IngoreActors;
	IngoreActors.Add(this);

	TArray<FHitResult> HitRetArray;

	const bool isHit = UKismetSystemLibrary::SphereTraceMultiByProfile(
		GetWorld(),
		StartLoc,
		TargetLoc,
		50.f,
		TEXT("BlockAll"),
		false,
		IngoreActors,
		EDrawDebugTrace::Type::None,
		HitRetArray,
		true
	);

	if (isHit)
	{
		CameraBoom->SetRelativeRotation(FMath::RInterpTo(CameraBoom->GetRelativeRotation(), NewCamRot, DeltaSecond, ZoomInterpSpeed));
		return true;
	}

	CameraBoom->SetRelativeRotation(FMath::RInterpTo(CameraBoom->GetRelativeRotation(), BaseCamRot, DeltaSecond, ZoomInterpSpeed));
	return false;
}

void ACPP_SkillBoxCharacter::OnDeath(AActor* VictimActor, AActor* Killer)
{
	UGameplayStatics::PlaySoundAtLocation(this, DeadSound, GetActorLocation()); // Not need check - Autocheck

	GetCharacterMovement()->DisableMovement();
	GetMesh()->SetCollisionProfileName("Ragdoll");
	GetMesh()->SetAllBodiesSimulatePhysics(true);

	/*if(Controller)
	{
		Controller->ChangeState(NAME_Spectating); // TODO::SpectatorMode?
	}*/

	/*ACPP_SkillBoxGameMode* GM = GetWorld()->GetAuthGameMode<ACPP_SkillBoxGameMode>();
	if(GM && Controller)
	{
		GM->OnActorKilled(this, Killer);
	}*/

	GetCapsuleComponent()->SetCollisionResponseToChannels(ECollisionResponse::ECR_Ignore);

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	if (bIsPlayer)
	{
		SetLifeSpan(2.5f);
	}
	else
	{
		SetLifeSpan(7.0f);
	}
	//ActionComp->RemoveAllActions();
}

void ACPP_SkillBoxCharacter::PrimaryInteract()
{
	if (InteractComp)
	{
		InteractComp->PrimaryInteract();
	}
	UE_LOG(LogTemp, Display, TEXT("[ACPP_SkillBoxCharacter::PrimaryInteract] Character_InputPressed"));
	SetTryAnnex_OnServer(true);
}

void ACPP_SkillBoxCharacter::FireButtonPressed()
{
	if (CombatComp && !bIsRolling && !IsSprint())
	{
		CombatComp->FireButtonPressed(true);
	}
}

void ACPP_SkillBoxCharacter::FireButtonRealised()
{
	if (CombatComp)
	{
		//GetWorld()->GetTimerManager().ClearTimer(CombatComp->FireTimer);
		CombatComp->FireButtonPressed(false);
	}
}

void ACPP_SkillBoxCharacter::SelectCombatGrenade() // Key E
{
	if (CombatComp)
	{
		// FIND Slot Index = Combat TYPE Grenade
		int32 Slot = CombatComp->FindIndexGrenadeByType(EGrenadeType::Combat);
		if (Slot >= 0)
		{
			// IF FOUND START DRAW DROP CURVE OR JUST DROP
			CombatComp->SetActiveGrenadeSlot(Slot);
			CombatComp->SelectGrenadeSlot(Slot);
		}
	}
}

void ACPP_SkillBoxCharacter::SelectTacticalGrenade() // Key Q
{
	if (CombatComp)
	{
		// FIND Slot Index = Tactical TYPE Grenade
		int32 Slot = CombatComp->FindIndexGrenadeByType(EGrenadeType::Tactical);
		if (Slot >= 0)
		{
			// IF FOUND START DRAW DROP CURVE OR JUST DROP
			CombatComp->SetActiveGrenadeSlot(Slot);
			CombatComp->SelectGrenadeSlot(Slot);
		}
	}
}

void ACPP_SkillBoxCharacter::GrenadeButtonReleased() // Released E or Q
{
	if (!GetMesh()->GetAnimInstance()->Montage_IsPlaying(ThrowGrenadeMontage))
	{
		int32 CurrentSlot = CombatComp->GetActiveGrenadeSlot();
		if (CurrentSlot >= 0)
		{
			UE_LOG(LogTemp, Display, TEXT("[ACPP_SkillBoxCharacter::GrenadeButtonReleased] CURRENT SLOT = %i"), CurrentSlot);
			CombatComp->ThrowGrenade(CurrentSlot);
		}
	}
}

void ACPP_SkillBoxCharacter::UseFirstAidKits()
{
	if (FirstAidKitComponent)
	{
		FirstAidKitComponent->UseFirstAidKit();
	}
}

void ACPP_SkillBoxCharacter::OnRep_OverlappingWeapon(AWeapon* LastWeapon)
{
	if (OverlappingWeapon && IsLocallyPlayerControlled())
	{
		OverlappingWeapon->ShowPickupWidget(true);
		OverlappingWeapon->EnableCustomDepth(true);
	}

	if (LastWeapon)
	{
		LastWeapon->ShowPickupWidget(false);
		LastWeapon->EnableCustomDepth(false);
	}
}

void ACPP_SkillBoxCharacter::MulticastHit_Implementation()
{
	PlayHitReactMontage();

	UGameplayStatics::PlaySoundAtLocation(this, PainSound, GetActorLocation());
}

void ACPP_SkillBoxCharacter::EquipButtonPressed()
{
	if (!CombatComp)
	{
		return;
	}

	if (CombatComp->GetCombatState() != ECombatState::ECS_Unoccupied)
	{
		return;
	}

	if (!HasLocalNetOwner())
	{
		return;
	}

	ServerEquipButtonPressed();

	if (!HasAuthority())
	{
		if (CombatComp->ShouldSwapWeapons() && OverlappingWeapon == nullptr)
		{
			PlaySwapMontage();
			CombatComp->SetCombatState(ECombatState::ECS_SwappingWeapons);
			bFinishedSwapping = false;
		}
	}
}

void ACPP_SkillBoxCharacter::ServerEquipButtonPressed_Implementation()
{
	if (CombatComp)
	{
		if (OverlappingWeapon)
		{
			CombatComp->EquipWeapon(OverlappingWeapon);
		}
		else if (CombatComp->ShouldSwapWeapons())
		{
			CombatComp->SwapWeapon();
		}
	}
}

void ACPP_SkillBoxCharacter::CrouchButtonPressed()
{
	ServerCrouchButtonPressed();
}

void ACPP_SkillBoxCharacter::ServerCrouchButtonPressed_Implementation()
{
	NetMulticastCrouchButtonPressed();
}

void ACPP_SkillBoxCharacter::NetMulticastCrouchButtonPressed_Implementation()
{
	if (bIsCrouched)
	{
		UnCrouch(true);
	}
	else
	{
		Crouch(true);
	}
	FTimerHandle DelayTimer;
	GetWorldTimerManager().SetTimer(DelayTimer, this, &ACPP_SkillBoxCharacter::ChangeMovementState, 0.1, false);
}

void ACPP_SkillBoxCharacter::ReloadButtonPressed()
{
	if (IsWeaponEquipped())
	{
		CombatComp->Reload();
	}
}

void ACPP_SkillBoxCharacter::SetOverlappingWeaponStart(AWeapon* Weapon)
{
	SetOverlappingWeaponEnd(OverlappingWeapon);
	if (Weapon)
	{
		OverlappingWeapon = Weapon;

		if (IsLocallyPlayerControlled())
		{
			OverlappingWeapon->ShowPickupWidget(true);
			OverlappingWeapon->EnableCustomDepth(true);
		}
	}
}

void ACPP_SkillBoxCharacter::SetOverlappingWeaponEnd(const AWeapon* Weapon)
{
	if (Weapon && OverlappingWeapon && OverlappingWeapon == Weapon)
	{
		if (IsLocallyPlayerControlled())
		{
			OverlappingWeapon->ShowPickupWidget(false);
			OverlappingWeapon->EnableCustomDepth(false);
		}

		OverlappingWeapon = nullptr;
	}
}

bool ACPP_SkillBoxCharacter::IsWeaponEquipped()
{
	return (CombatComp && CombatComp->GetEquippedWeapon()); //TODO::AnimPose change if EquippedWeapon != nullptr;
}

bool ACPP_SkillBoxCharacter::IsAiming()
{
	return (CombatComp && CombatComp->IsAiming());
}

AWeapon* ACPP_SkillBoxCharacter::GetEquippedWeapon()
{
	if (CombatComp == nullptr)
	{
		return nullptr;
	}

	return CombatComp->GetEquippedWeapon();
}

ECombatState ACPP_SkillBoxCharacter::GetCombatState() const
{
	if (CombatComp == nullptr) return ECombatState::ECS_MAX;
	return CombatComp->GetCombatState();
}

void ACPP_SkillBoxCharacter::ChangeMovementState()
{
	if (bIsRolling)
	{
		MovementState = EMovementState::Roll_State;
	}
	else if (!bWalkEnable && !IsAiming() && !bSprintRunEnable && !bIsCrouched)
	{
		MovementState = EMovementState::Run_State;
	}
	else if (!bWalkEnable && IsAiming() && !bIsCrouched)
	{
		MovementState = EMovementState::Aim_State;
	}
	else if (bWalkEnable && !bSprintRunEnable && !bIsCrouched)
	{
		if (!IsAiming())
		{
			MovementState = EMovementState::Walk_State;
		}
		else
		{
			MovementState = EMovementState::AimWalk_State;
		}
	}
	else if (!bSprintRunEnable && bIsCrouched)
	{
		MovementState = EMovementState::Crouch_State;
	}
	else if (!bWalkEnable && !IsAiming() && bSprintRunEnable && !bIsCrouched && (MovementState ==
		EMovementState::SprintRun_State || bIsMovingForward))
	{
		bWalkEnable = false;
		MovementState = EMovementState::SprintRun_State;
	}
	ChangeMovementSpeed();
}

void ACPP_SkillBoxCharacter::OnStartSprint()
{
	if (!bIsCrouched && !bIsRolling)
	{
		ActionComp->StartActionByName(this, "Sprint");
	}
}

void ACPP_SkillBoxCharacter::OnStopSprint()
{
	ActionComp->StopActionByName(this, "Sprint");
}

bool ACPP_SkillBoxCharacter::IsSprint() const
{
	return bSprintRunEnable && MovementState == EMovementState::SprintRun_State && !GetVelocity().IsZero() &&
		AttributeComp->IsNoTired();
}

void ACPP_SkillBoxCharacter::KnifeAttack()
{
	if (CombatComp && !bIsRolling && !bIsCrouched)
	{
		CombatComp->FireButtonPressed(false);
		CombatComp->KnifeAttack();
	}
}

void ACPP_SkillBoxCharacter::PlayKnifeAttackMontage()
{
	if (!KnifeAttackMontage && AttachedKnife == nullptr)
	{
		return;
	}

	if (KnifeAttackMontage)
	{
		bIsKnifeAttack = true;
		FTimerHandle TimerHandle;
		GetWorldTimerManager().SetTimer(TimerHandle, this, &ACPP_SkillBoxCharacter::ResetMovementParam,
		                                KnifeAttackMontage->GetPlayLength(),
		                                false);
		GetMesh()->GetAnimInstance()->Montage_Play(KnifeAttackMontage);
		if (FindKnifeTargetComponent && FindKnifeTargetComponent->KnifeTarget)
		{
			const FVector Dir = (FindKnifeTargetComponent->KnifeTarget->GetActorLocation() - GetActorLocation()) *
				70000.f;
			GetCharacterMovement()->bOrientRotationToMovement = true;
			GetCharacterMovement()->RotationRate = FRotator(0.f, 10000.f, 0.f);
			GetCharacterMovement()->AddForce(Dir);
			GetCharacterMovement()->AddInputVector(Dir);
		}
		else
		{
			GetCharacterMovement()->AddForce(GetCapsuleComponent()->GetForwardVector() * 100000.f);
		}

		if (KnifeAttackMontage->IsNotifyAvailable())
		{
			UGameplayStatics::SpawnSoundAtLocation(this, KnifeSound, GetActorLocation(), FRotator(0));
			const auto AnimNotifies = KnifeAttackMontage->Notifies;
			for (const auto& AnimNotify : AnimNotifies)
			{
				const auto KnifeAttackNotify = Cast<USAnimNotify>(AnimNotify.Notify);
				if (KnifeAttackNotify && !KnifeAttackNotify->OnNotified.IsBoundToObject(this))
				{
					KnifeAttackNotify->OnNotified.AddUFunction(this, FName("KnifeNotify"), AnimNotify.NotifyName);
				}
			}
		}
	}
}

FName ACPP_SkillBoxCharacter::KnifeNotify()
{
	UE_LOG(LogTemp, Warning, TEXT("KNIFE --- APPLY Notify"));

	if (FindKnifeTargetComponent && FindKnifeTargetComponent->KnifeTarget)
	{
		UGameplayStatics::ApplyDamage(FindKnifeTargetComponent->KnifeTarget,
		                              KnifeDamage,
		                              PlayerControllerRef,
		                              this,
		                              UDamageType::StaticClass());

		UGameplayStatics::SpawnSoundAtLocation(this, KnifeHitSound, GetActorLocation(), FRotator(0));
	}

	return FName();
}

void ACPP_SkillBoxCharacter::MakeANoise(FVector Location)
{
	MakeNoise(1.f, nullptr, Location, 0.f, "");
}

bool ACPP_SkillBoxCharacter::IsHostile(ACPP_SkillBoxCharacter* Agent)
{
	return (Faction != Agent->Faction && Agent->Faction != EFaction::Neutral);
}

void ACPP_SkillBoxCharacter::CreateInteractBaseWidget(UClass* WidgetClass)
{
	CurrentInteractWidget = Cast<UInteractBaseWidgetParent>(
		CreateWidget(this->GetLocalViewingPlayerController(), WidgetClass));
}

void ACPP_SkillBoxCharacter::CreateBaseMenuWidget(UClass* WidgetClass)
{
	BaseMenuWidget = Cast<
		UInteractBaseWidgetParent>(CreateWidget(this->GetLocalViewingPlayerController(), WidgetClass));
}

void ACPP_SkillBoxCharacter::OnRep_PossessedInfo()
{
	if (PossessedInfo.Possessed)
	{
		OnPossessedEvent.Broadcast(this);
	}
	else
	{
		OnUnPossessedEvent.Broadcast(this);
	}
}

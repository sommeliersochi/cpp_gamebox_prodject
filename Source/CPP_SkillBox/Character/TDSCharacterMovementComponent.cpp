// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSCharacterMovementComponent.h"
#include "CPP_SkillBoxCharacter.h"

float UTDSCharacterMovementComponent::GetMaxSpeed() const
{
	const float MaxSpeed = Super::GetMaxSpeed();
	const ACPP_SkillBoxCharacter* Player = Cast<ACPP_SkillBoxCharacter>(GetPawnOwner());
	return Player && Player->IsSprint() ? MaxSpeed * SprintModifier: MaxSpeed;
}

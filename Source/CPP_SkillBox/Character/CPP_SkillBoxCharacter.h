// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Character.h>

#include "CharacterDefinitions.h"
#include "CombatState.h"
#include "CPP_SkillBox.h"
#include "GenericTeamAgentInterface.h"
#include "InteractWithCrosshairsInterface.h"
#include "MyTypes.h"
#include "TurningInPlace.h"
#include "AI/AIDefinitions.h"
#include "Perception/AISightTargetInterface.h"

#include "CPP_SkillBoxCharacter.generated.h"

class UFindTargetComponent;
class UFindKnifeTargetComponent;
class UInteractBaseWidgetParent;
class UFirstAidKitComponent;
class UInventoryComponent;
class UDamageIndicatorComponent;
class ACPP_SkillBoxPlayerController;
class USoundCue;
class UCombatComponent;
class USInteractionComponent;
class USWorldUserWidget;
class USpringArmComponent;
class UCameraComponent;
class UTDSCharacterMovementComponent;
class USAttributeComponent;
class USActionComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPossessedSignature, ACPP_SkillBoxCharacter*, Character);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUnPossessedSignature, ACPP_SkillBoxCharacter*, Character);

UCLASS(Blueprintable)
class ACPP_SkillBoxCharacter : public ACharacter, public IInteractWithCrosshairsInterface, public IAISightTargetInterface, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

private:
	FGenericTeamId TeamId;
public:
	ACPP_SkillBoxCharacter(const FObjectInitializer& ObjInit);

	UFUNCTION(BlueprintCallable)
	virtual FGenericTeamId GetGenericTeamId() const override { return TeamId; }
	virtual void SetGenericTeamId(const FGenericTeamId& TeamID) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;

	virtual void PostInitializeComponents() override;

	virtual void OnRep_Controller() override;

	virtual void PossessedBy(AController* NewController) override;

	virtual void UnPossessed() override;

	UFUNCTION(BlueprintPure, Category="Character")
	bool IsLocallyPlayerControlled() const;

	UFUNCTION(BlueprintPure, Category="Character")
	bool IsPossessed() const;

	// Custom View Target For AI Perception Component
	virtual bool CanBeSeenFrom(const FVector& ObserverLocation, FVector& OutSeenLocation,
	                           int32& NumberOfLoSChecksPerformed,
	                           float& OutSightStrength, const AActor* IgnoreActor = nullptr,
	                           const bool* bWasVisible = nullptr,
	                           int32* UserData = nullptr) const;

	UFUNCTION(BlueprintCallable)
	UAnimMontage* ReturnRandomVariation();
	// Behavior Tree Asset

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AI")
	class AAISmartObject* SmartObject;

	UPROPERTY(BlueprintReadOnly, Category = "AI")
	bool Dead = false;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AI")
	FName PerceptionTarget = "spine_02";

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Components")
	TArray<UAnimMontage*> IdleVariations;

	/*UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AI")
	float Health = 100.f;*/

	/*UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AI")
	float BaseDamage = 0.01f;*/

	/*UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AI")
	float FireRate = 0.1f;*/

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AI")
	bool BulletSpread = true;

	UFUNCTION()
	FHitResult CapsuleTrace();

	UPROPERTY(BlueprintReadOnly, Category = "Animation")
	FAnimValues AnimValues;

	UFUNCTION(BlueprintCallable)
	void ToggleCombat(const bool NewBool);

	UFUNCTION(BlueprintCallable)
	void ToggleCrouch(const bool NewBool);

	UFUNCTION(BlueprintCallable)
	void ToggleADS(const bool NewBool);

	UFUNCTION(BlueprintCallable)
	void ToggleSprinting(bool NewBool);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Combat")
	UArrowComponent* NormalForwardPlane;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI | Movement")
	float SlowWalkSpeed = 94.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI | Movement")
	float SprintSpeed = 375.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI | Movement")
	float WalkSpeed = 110.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI | Movement")
	float CrouchedWalkSpeed = 100.f;

	FTimerHandle SetHitTarget_Timer;

	FVector CloseTargetLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Target")
	FVector PawnTraceImpactPoint;

	void TickCameraZoom(float DeltaSecond);

	void PlayFireAnim(UAnimMontage* Animation);

	void PlayThrowGrenadeMontage();
	void PlayReloadMontage();
	void PlaySwapMontage();

	void PrimaryInteractReleased();

	void ReplaceArmor();

	virtual void SetupPlayerInputComponent(UInputComponent* NewInputComponent) override;

	void SetHealthBarVisibility(bool bIsVisibility);

	UFUNCTION(Reliable, Server)
	void SetTryAnnex_OnServer(bool bIsTry);

	UFUNCTION()
	void InputAxisY(float Value);

	UFUNCTION()
	void InputAxisX(float Value);

	void RollPressed();

	UFUNCTION(Server, Reliable)
	void ServerRollPressed();

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticastRollPressed();

	//Rollin animation
	void PlayRollAnimMontage();

	void AddRollMove();

	void EndRoll();

	UFUNCTION()
	FName OnRollEndNotify();

	void ResetMovementParam();

	UFUNCTION()
	void OnStartSprint();

	UFUNCTION()
	void OnStopSprint();

	UFUNCTION()
	void MovementTick(float DeltaTime);

	void LookAtTarget(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void ChangeMovementSpeed();

	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	UFUNCTION(BlueprintCallable, Category="Movement")
	bool IsSprint() const;

	UFUNCTION(BlueprintCallable, Category="Movement")
	double GetMovementDirection();

	UCameraComponent* GetTopDownCamera() const;

	void AimingButtonPressed();

	void AimingButtonReleased();

	void CameraInterpZoom(float DeltaSecond);

	void CameraUp();
	void CameraDown();

	FVector2D GetMouseVelocity();

	FRotator GetMouseAimDirection(float DeltaSecond);

	bool IsMouseAboveDeadZone();

	UFUNCTION()
	void UpdateMouseAim(float DeltaSecond);

	void SpawnDefaultWeapon();

	void FireButtonPressed();
	void FireButtonRealised();

	UFUNCTION(BlueprintCallable)
	void SelectCombatGrenade();
	void SelectTacticalGrenade();
	UFUNCTION(BlueprintCallable)
	void GrenadeButtonReleased();

	UFUNCTION()
	void UseFirstAidKits();

	UCameraComponent* GetTopDownCameraComponent() const;

	USpringArmComponent* GetCameraBoom() const;

	UCombatComponent* GetCombat() const;

	UAnimMontage* GetReloadMontage() const;

	USAttributeComponent* GetAttributeComp() const;

	UStaticMeshComponent* GetAttachedGrenade() const;

	UStaticMeshComponent* GetAttachedKnife() const;

	USWorldUserWidget* GetHealthBar() const;

	UTDSCharacterMovementComponent* GetCharacterMovementComp() const;

	UDamageIndicatorComponent* GetDamageIndicatorComponent() const;

	UFUNCTION(BlueprintPure, Category="Character")
	UInventoryComponent* GetInventoryComponent() const;

	UFUNCTION(BlueprintPure, Category="Character")
	USActionComponent* GetActionComponent() const;

	UFUNCTION(BlueprintPure, Category="Character")
	UFindTargetComponent* GetFindTargetComponent() const;

	ETurningInPlace GetTurningInPlace() const;

	void SetOverlappingWeaponStart(AWeapon* Weapon);

	void SetOverlappingWeaponEnd(const AWeapon* Weapon);

	bool IsWeaponEquipped();

	bool IsAiming();

	AWeapon* GetEquippedWeapon();

	UFUNCTION(NetMulticast, Unreliable)
	void MulticastHit();

	UFUNCTION(BlueprintCallable)
	EPhysicalSurface GetSurfaceType();

	ECombatState GetCombatState() const;


	//TODO: Move to component
	void KnifeAttack();
	void PlayKnifeAttackMontage();

	UFUNCTION()
	FName KnifeNotify();

	UFUNCTION()
	void CreateInteractBaseWidget(UClass* WidgetClass);
	void CreateBaseMenuWidget(UClass* WidgetClass);

protected:
	UFUNCTION()
	void OnHealthChange(AActor* InstigatorActor, USAttributeComponent* OwningComp, float NewHealth, float Delta);

	UFUNCTION(Server, Reliable)
	void ServerEquipButtonPressed();

	bool CheckCollision(float DeltaSecond);

	void EquipButtonPressed();

	void CrouchButtonPressed();

	UFUNCTION(Server, Reliable)
	void ServerCrouchButtonPressed();

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticastCrouchButtonPressed();

	void ReloadButtonPressed();

	void PlayHitReactMontage();

	UFUNCTION()
	virtual void OnDeath(AActor* VictimActor, AActor* Killer);

	void PrimaryInteract();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category= "Components")
	TObjectPtr<UCombatComponent> CombatComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category= "Components")
	TObjectPtr<USAttributeComponent> AttributeComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category= "Components")
	TObjectPtr<USActionComponent> ActionComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	TObjectPtr<UCameraComponent> TopDownCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	TObjectPtr<USpringArmComponent> CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	TObjectPtr<UDamageIndicatorComponent> DamageIndicatorComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	TObjectPtr<UInventoryComponent> InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	TObjectPtr<UFirstAidKitComponent> FirstAidKitComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	TObjectPtr<USInteractionComponent> InteractComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	TObjectPtr<UStaticMeshComponent> AttachedGrenade;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	TObjectPtr<UStaticMeshComponent> AttachedKnife;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	TObjectPtr<UCombatComponent> CombatComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	TObjectPtr<UFindKnifeTargetComponent> FindKnifeTargetComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	TObjectPtr<UFindTargetComponent> FindTargetComponent;

	TObjectPtr<UTDSCharacterMovementComponent> CharacterMovementComp;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float MouseDeadZone = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
	float AngleError = 91.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Movement")
	float MouseSmoothingStrength = 2.2f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category= "MeshComponent")
	USkeletalMeshComponent* HeadMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category= "MeshComponent")
	USkeletalMeshComponent* BodyMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category= "MeshComponent")
	USkeletalMeshComponent* PantsMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category= "MeshComponent")
	USkeletalMeshComponent* FootMeshComponent;

public:
	static const FName CameraBoomComponentName;
	static const FName TopDownCameraComponentName;
	static const FName CombatComponentName;
	static const FName AttributeComponentName;
	static const FName ActionComponentName;
	static const FName InteractComponentName;
	static const FName AttachedGrenadeComponentName;
	static const FName AttachedKnifeComponentName;
	static const FName LineFXComponentName;
	static const FName LineImpactFXComponentName;
	static const FName DamageIndicatorComponentName;
	static const FName InventoryComponentName;
	static const FName FirstAidKitComponentName;
	static const FName FindKnifeTargetComponentName;
	static const FName FindTargetComponentName;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	bool bIsTryAnnex = false;

	UPROPERTY()
	ACPP_SkillBoxPlayerController* PlayerControllerRef;

	FOnMontageEnded OnKnifeMontageEnded;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "AI", meta = (ExposeOnSpawn="true"))
	EFaction Faction;

	int32 Distance = 1000;

	UPROPERTY(BlueprintReadOnly, Category="Info")
	bool bIsPlayer = true;

	FOnMontageEnded EndDelegate;

	UPROPERTY(BlueprintReadOnly, Category="Movement")
	double Direction = 0.0f;

	UPROPERTY(BlueprintAssignable, Category="Character|Events")
	FOnPossessedSignature OnPossessedEvent;
	UPROPERTY(BlueprintAssignable, Category="Character|Events")
	FOnUnPossessedSignature OnUnPossessedEvent;

	bool bFinishedSwapping = false;

	UPROPERTY(BlueprintReadOnly, Category="Movement Bool")
	bool bIsRolling = false;
	bool bIsKnifeAttack = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Settings|Knife")
	int32 KnifeDamage = 50;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Settings|Knife")
	USoundCue* KnifeSound = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Settings|Knife")
	USoundCue* KnifeHitSound = nullptr;

	UPROPERTY(Transient)
	UInteractBaseWidgetParent* CurrentInteractWidget;
	UPROPERTY(Transient)
	UInteractBaseWidgetParent* BaseMenuWidget;

	UFUNCTION(BlueprintCallable)
	void MakeANoise(FVector Location);

	bool IsHostile(ACPP_SkillBoxCharacter* Agent);

	UPROPERTY(BlueprintReadWrite, Category="Movement")
	bool bSprintRunEnable = false;

	int32 SelectedGrenade = -1;

protected:
	UPROPERTY(ReplicatedUsing=OnRep_PossessedInfo)
	FCharacterPossessedInfo PossessedInfo;

	UFUNCTION()
	void OnRep_PossessedInfo();

	float AxisY = 0.0f;

	float AxisX = 0.0f;

	float ResSpeed = 600.0f;

	float CameraDistance = 1800.f;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Camera", meta=(UIMin="0", ClampMin="0"))
	float MinCameraDistance = 1399.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Camera", meta=(UIMin="0", ClampMin="0"))
	float MaxCameraDistance = 2399.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Camera", meta=(UIMin="0", ClampMin="0"))
	float CameraDistanceStep = 50.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Camera", meta=(UIMin="0", ClampMin="0"))
	float ZoomInterpSpeed = 20.f;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Camera", meta=(UIMin="0", ClampMin="0"))
	float CameraDefaultFOV = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Camera", meta=(UIMin="0", ClampMin="0"))
	float CameraZoomedFOV = 75.f;

	float CameraCurrentFOV = 75.0f;

	UPROPERTY(EditDefaultsOnly, Category= "Settings|Attack")
	FName TimeToHitParamName = "TimeToHit";

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Settings|Movement")
	EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Settings|Movement")
	FCharacterSpeed MovementInfo;

	ETurningInPlace TurningInPlace;

	UPROPERTY(BlueprintReadWrite, Category="Movement")
	bool bWalkEnable = false;

	UPROPERTY(BlueprintReadOnly, Category="Movement")
	bool bIsCanRoll = true;

	UPROPERTY(BlueprintReadOnly, Category="Movement")
	float LastDirection = 0.0f;

	UPROPERTY(BlueprintReadOnly, Category="Movement")
	bool bIsMovingForward = false;

	FRotator BaseCamRot = FRotator(-40.f, -15.f, 0.0f);

	FRotator NewCamRot = FRotator(-60.f, -15.f, 0.0f);

	UPROPERTY(EditDefaultsOnly, Category="Settings|Combat")
	TObjectPtr<UAnimMontage> RollAnimMontage;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Combat")
	TObjectPtr<UAnimMontage> KnifeAttackMontage;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Combat")
	TObjectPtr<UAnimMontage> HitReactMontage;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Combat")
	TObjectPtr<USoundCue> PainSound;

	UPROPERTY(EditDefaultsOnly, Category="Settings|Combat")
	TObjectPtr<USoundCue> DeadSound;

	FTimerHandle MoveRoll_Timer;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Combat")
	TObjectPtr<UAnimMontage> SwapMontage;

	UPROPERTY(EditDefaultsOnly, Category ="Settings|Combat")
	TObjectPtr<UAnimMontage> ReloadMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Combat")
	TObjectPtr<UAnimMontage> FireWeaponMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Combat")
	TObjectPtr<UAnimMontage> ThrowGrenadeMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|FX")
	TObjectPtr<UMaterialInterface> BloodDecal;

	TObjectPtr<ACPP_SkillBoxPlayerController> CharacterPlayerController;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|UI")
	TSubclassOf<UUserWidget> HealthBarWidgetClass;

	TObjectPtr<USWorldUserWidget> ActivateHealthBar;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|DefaultWeapon")
	TSoftClassPtr<AWeapon> DefaultWeaponClass;

	TObjectPtr<AWeapon> StartingWeapon = nullptr;

private:
	UPROPERTY(ReplicatedUsing = OnRep_OverlappingWeapon)
	TObjectPtr<AWeapon> OverlappingWeapon;

	UFUNCTION()
	void OnRep_OverlappingWeapon(AWeapon* LastWeapon);
};

FORCEINLINE_DEBUGGABLE UCameraComponent* ACPP_SkillBoxCharacter::GetTopDownCameraComponent() const
{
	return TopDownCameraComponent;
}

FORCEINLINE_DEBUGGABLE USpringArmComponent* ACPP_SkillBoxCharacter::GetCameraBoom() const
{
	return CameraBoom;
}

FORCEINLINE_DEBUGGABLE UCombatComponent* ACPP_SkillBoxCharacter::GetCombat() const
{
	return CombatComp;
}

FORCEINLINE_DEBUGGABLE UAnimMontage* ACPP_SkillBoxCharacter::GetReloadMontage() const
{
	return ReloadMontage;
}

FORCEINLINE_DEBUGGABLE USAttributeComponent* ACPP_SkillBoxCharacter::GetAttributeComp() const
{
	return AttributeComp;
}

FORCEINLINE_DEBUGGABLE UStaticMeshComponent* ACPP_SkillBoxCharacter::GetAttachedGrenade() const
{
	return AttachedGrenade;
}

FORCEINLINE_DEBUGGABLE UStaticMeshComponent* ACPP_SkillBoxCharacter::GetAttachedKnife() const
{
	return AttachedKnife;
}

FORCEINLINE_DEBUGGABLE USWorldUserWidget* ACPP_SkillBoxCharacter::GetHealthBar() const
{
	return ActivateHealthBar;
}

FORCEINLINE_DEBUGGABLE UTDSCharacterMovementComponent* ACPP_SkillBoxCharacter::GetCharacterMovementComp() const
{
	return CharacterMovementComp;
}

FORCEINLINE_DEBUGGABLE ETurningInPlace ACPP_SkillBoxCharacter::GetTurningInPlace() const
{
	return TurningInPlace;
}

FORCEINLINE_DEBUGGABLE bool ACPP_SkillBoxCharacter::IsLocallyPlayerControlled() const
{
	return IsLocallyControlled() && IsPlayerControlled();
}

FORCEINLINE_DEBUGGABLE bool ACPP_SkillBoxCharacter::IsPossessed() const
{
	return PossessedInfo.Possessed;
}

FORCEINLINE_DEBUGGABLE UDamageIndicatorComponent* ACPP_SkillBoxCharacter::GetDamageIndicatorComponent() const
{
	return DamageIndicatorComponent;
}

FORCEINLINE_DEBUGGABLE UInventoryComponent* ACPP_SkillBoxCharacter::GetInventoryComponent() const
{
	return InventoryComponent;
}

FORCEINLINE_DEBUGGABLE USActionComponent* ACPP_SkillBoxCharacter::GetActionComponent() const
{
	return ActionComp;
}

FORCEINLINE_DEBUGGABLE UFindTargetComponent* ACPP_SkillBoxCharacter::GetFindTargetComponent() const
{
	return FindTargetComponent;
}

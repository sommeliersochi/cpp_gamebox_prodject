// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_SkillBox/Character/TargetBoxActor.h"

#include "Components/BoxComponent.h"


ATargetBoxActor::ATargetBoxActor()
{
	PrimaryActorTick.bCanEverTick = true;
		
	bReplicates = true;

	Box = CreateDefaultSubobject<UBoxComponent>("TargetBox");
	SetRootComponent(Box);
	Box->SetHiddenInGame(true);

}



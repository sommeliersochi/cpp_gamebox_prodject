#pragma once

class SUtils
{
public:
	template<typename T>
	static T* FindComponent(AActor* Actor)
	{
		return Actor ? Actor->FindComponentByClass<T>() : nullptr;
	}
};
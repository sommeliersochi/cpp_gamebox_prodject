
#include "SAICharacterBase.h"

#include "AICombatComponent.h"
#include "SAIControllerBase.h"
#include "BrainComponent.h"
#include "SAIController.h"
#include "SInteractionComponent.h"
#include "DropItem/DropItemComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Camera/CameraComponent.h"
#include "DamageIndicator/DamageIndicatorComponent.h"
#include "FindTarget/FindTargetComponent.h"
#include "FirstAidKit/FirstAidKitComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "KnifeTarget/FindKnifeTargetComponent.h"
#include "Perception/AISense.h"
#include "Perception/PawnSensingComponent.h"

ASAICharacterBase::ASAICharacterBase(const FObjectInitializer& ObjectInit)
: Super(ObjectInit.SetDefaultSubobjectClass<UAICombatComponent>("CombatComp"))
{
	GetTopDownCameraComponent()->DestroyComponent();
	GetCameraBoom()->DestroyComponent();
	GetAttachedKnife()->DestroyComponent();
	GetDamageIndicatorComponent()->DestroyComponent();
	GetFindTargetComponent()->DestroyComponent();
	InteractComp->DestroyComponent();
	FirstAidKitComponent->DestroyComponent();
	FindTargetComponent->DestroyComponent();
	FindKnifeTargetComponent->DestroyComponent();
	
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AIControllerClass = ASAIControllerBase::StaticClass();

	//SensingComp = CreateDefaultSubobject<UPawnSensingComponent>("SensingComp");
	DropItemComponent = CreateDefaultSubobject<UDropItemComponent>("DropItemComp");

	bUseControllerRotationYaw = false;
	if(GetCharacterMovement())
	{
		GetCharacterMovement()->bUseControllerDesiredRotation = true;
		GetCharacterMovement()->RotationRate = FRotator(0.0f, 360.0f, 0.0f);
	}
	bIsPlayer = false;
}

void ASAICharacterBase::BeginPlay()
{
	Super::BeginPlay();
}

void ASAICharacterBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	//SensingComp->OnSeePawn.AddDynamic(this, &ASAICharacterBase::OnPawnSeen);
}

void ASAICharacterBase::UpdateFaction(EFaction NewFaction)
{
	Faction = NewFaction;
	SetGenericTeamId(int32(NewFaction));
}

void ASAICharacterBase::ToggleSitting(bool Value)
{
	
}

void ASAICharacterBase::OnDeath(AActor* VictimActor, AActor* Killer)
{
	Super::OnDeath(VictimActor, Killer);

	const auto SController = GetController<AAIController>();
	if(SController && SController->BrainComponent)
	{
		SController->BrainComponent->Cleanup();
	}
}

void ASAICharacterBase::OnPawnSeen(APawn* Pawn)
{
	/*if(AAIController* AIC = GetController<AAIController>())
	{
		UBlackboardComponent* BBComp = AIC->GetBlackboardComponent();
		BBComp->SetValueAsObject("TargetActor", Pawn);

		DrawDebugString(GetWorld(), GetActorLocation(), "PLAYER SPOTTED", nullptr, FColor::Black, 2.0f, true);
	}*/
}



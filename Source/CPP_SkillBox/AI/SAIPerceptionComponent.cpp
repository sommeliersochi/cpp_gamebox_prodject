// Fill out your copyright notice in the Description page of Project Settings.


#include "SAIPerceptionComponent.h"

#include <AIController.h>
#include <Perception/AISense_Sight.h>

#include "CPP_SkillBoxCharacter.h"
#include "SAIController.h"
#include "SAttributeComponent.h"
#include "CPP_SkillBox/Core/SUtils.h"

AActor* USAIPerceptionComponent::GetClosestEnemy() const
{
	TArray<AActor*> PerceivedActorsSight;
	TArray<AActor*> PerceivedActorsHearing;
	TArray<AActor*> PerceivedActorsDamage;
	TArray<AActor*> PerceivedActorsTeam;
	
	GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PerceivedActorsSight);
	GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PerceivedActorsHearing);
	GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PerceivedActorsDamage);
	GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PerceivedActorsTeam);
	
	if (PerceivedActorsSight.Num() == 0 && PerceivedActorsHearing.Num() == 0 && PerceivedActorsDamage.Num() == 0 && PerceivedActorsTeam.Num() == 0)
	{
		return nullptr;
	}

	const auto Controller = Cast<ASAIController>(GetOwner());
	if (!Controller)
	{
		return nullptr;
	}

	const auto Pawn = Controller->GetPawn<ASAICharacterBase>();
	if (!Pawn)
	{
		return nullptr;
	}
	ClearPerceivedArray(PerceivedActorsTeam);
	//ClearPerceivedArray(PerceivedActorsDamage);
	//ClearPerceivedArray(PerceivedActorsHearing);
	ClearPerceivedArray(PerceivedActorsSight);
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, FString::Printf(TEXT("PerceivedActorsSight: %s"),*FString::FromInt(PerceivedActorsSight.Num())));
	float BestDistance = MAX_FLT;
	AActor* BestActor = nullptr;

	for (const auto PerceiveActor : PerceivedActorsTeam)
	{
		const auto AttributeComp = SUtils::FindComponent<USAttributeComponent>(PerceiveActor);
		if (AttributeComp && !AttributeComp->IsDead())
		{
			const auto CurrentDistance = (PerceiveActor->GetActorLocation() - Pawn->GetActorLocation()).Size();
			if (CurrentDistance < BestDistance)
			{
				BestDistance = CurrentDistance;
				BestActor = PerceiveActor;
			}
		}
	}
	for (const auto PerceiveActor : PerceivedActorsSight)
	{
		const auto AttributeComp = SUtils::FindComponent<USAttributeComponent>(PerceiveActor);
		if (AttributeComp && !AttributeComp->IsDead())
		{
			const auto CurrentDistance = (PerceiveActor->GetActorLocation() - Pawn->GetActorLocation()).Size();
			if (CurrentDistance < BestDistance)
			{
				BestDistance = CurrentDistance;
				BestActor = PerceiveActor;
			}
		}
	}
	return BestActor;
}

void USAIPerceptionComponent::ClearPerceivedArray(TArray<AActor*> Arr) const
{
	if (const auto Controller = Cast<ASAIController>(GetOwner()))
	{
		if (const auto Pawn = Controller->GetPawn<ASAICharacterBase>())
		{
			for (const auto PerceiveActor : Arr)
			{
				ACPP_SkillBoxCharacter* EnemyChar = Cast<ACPP_SkillBoxCharacter>(PerceiveActor);
				if (EnemyChar)
				{
					if (PerceiveActor == Pawn || EnemyChar->Faction == Pawn->Faction || Pawn->GetAttributeComp()->IsDead())
					{
						Arr.Remove(PerceiveActor);
						ClearPerceivedArray(Arr);
						break;
					}
				}
			}
		}
	}
}

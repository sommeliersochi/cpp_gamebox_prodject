// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Actor.h>

#include "CPP_SkillBox.h"
#include "AIManager.generated.h"


class ASAIControllerBase;
class ASAICharacterBase;

UCLASS()
class CPP_SKILLBOX_API AAIManager : public AActor
{
	GENERATED_BODY()

public:

	AAIManager();

	UFUNCTION(BlueprintCallable)
	void CoverFire(bool ProvideCoverFire, ASAICharacterBase* Instgtr);
	
	UFUNCTION()
	void CreateAgentsList();

	UFUNCTION()
	void NotifyAIState(EAIState State);

	UFUNCTION()
	void RemoveAgent(ASAIControllerBase* ControllerToRemove);
protected:

	virtual void BeginPlay() override;

	UFUNCTION()
	bool Engaged();

	UFUNCTION()
	void RunCombatLoop();

	UFUNCTION()
	void RunSearchTimer();

	UFUNCTION()
	void UpdateCombatRole();

public:	
	
	UPROPERTY(Transient, BlueprintReadOnly)
	TArray<ASAIControllerBase*> Agents;

	UPROPERTY(Transient, BlueprintReadOnly)
	TArray<ASAICharacterBase*> Defenders;
	
	int32 ApproacherIndex = 0;

	UPROPERTY(Transient, BlueprintReadOnly)
	ASAICharacterBase* AgentProvidingCoverFire = nullptr;

	UPROPERTY(Transient, BlueprintReadOnly)
	FVector LastStimulusLocation = FVector::ZeroVector;

protected:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AI")
	EFaction Faction;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AI")
	float MaxStimulusTime_Combat = 10.f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AI")
	float MaxStimulusTime_Search = 10.f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AI")
	float ApproachDelay = 10.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName AICombatStateKey = TEXT("AICombatState");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName ShootFromCoverKey = TEXT("ShootFromCover");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName ContactKey = TEXT("Contact");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName TargetActorKey = TEXT("TargetActor");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName AIStateKey = TEXT("AIState");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName LastStimulusLocationKey = TEXT("LastStimulusLocation");

	FTimerHandle CombatTimer;
	FTimerHandle SearchTimer;

	float SearchTimeStamp = 0.f;

	int32 AgentIndex = 0;

};

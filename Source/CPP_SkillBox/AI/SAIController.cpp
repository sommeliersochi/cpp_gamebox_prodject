#include "SAIController.h"

#include "AISmartObject.h"
#include "CPP_SkillBoxCharacter.h"
#include "SAICharacterBase.h"
#include "SAIPerceptionComponent.h"
#include "SAttributeComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AIPerceptionSystem.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISense_Damage.h"
#include "Perception/AISense_Hearing.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AISense_Team.h"
#include "Navigation/CrowdFollowingComponent.h"

ASAIController::ASAIController(const FObjectInitializer& ObjectInitializer):Super(ObjectInitializer.SetDefaultSubobjectClass<UCrowdFollowingComponent>(TEXT("PathFollowingComponent")))
{
	AIPerceptionComp = CreateDefaultSubobject<USAIPerceptionComponent>(TEXT("AIPerception"));
	AIPerceptionComp->OnTargetPerceptionUpdated.AddDynamic(this, &ASAIController::OnTargetPerceptionUpdated);
	AIPerceptionComp->OnPerceptionUpdated.AddDynamic(this, &ASAIController::OnPerceptionUpdated);
	AIPerceptionComp->bAutoRegister = true;
	BBC = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard Component"));
	BTC = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("behaviorTree Compponent"));
	TeamId = FGenericTeamId(255);
}

void ASAIController::BeginPlay()
{
	Super::BeginPlay();

	if (ensureMsgf(BehaviorTree, TEXT("BehaviorTree is nullptr! Please assign BehaviorTree in your AI Controller.")))
	{
		RunBehaviorTree(BehaviorTree);
	}
	FTimerHandle TimerHandle;
	//GetWorldTimerManager().SetTimer(TimerHandle,this,&ThisClass::ClearDeadActors,1.f,true);
}

void ASAIController::AnyDamage(AActor* Actor, float X, const UDamageType* Damage, AController* Controller,
	AActor* Actor1)
{
	ReceiveAnyDamage(X, Damage, Controller, Actor1);
}

void ASAIController::OnPawnDeath(AActor* Owne, AActor* Instigato)
{
	AIPawn->SetGenericTeamId(2);
	SetGenericTeamId(2);
	UAIPerceptionSystem::GetCurrent(GetWorld())->UpdateListener(*AIPerceptionComp);
}

void ASAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	AIPawn = Cast<ASAICharacterBase>(InPawn);
	if (AIPawn)
	{
		AIPawn->OnTakeAnyDamage.AddDynamic(this,&ASAIController::AnyDamage);
		AIPawn->GetAttributeComp()->OnDeath.AddDynamic(this, &ASAIController::OnPawnDeath);
		FTimerHandle Timer;
		GetWorldTimerManager().SetTimer(Timer,this,&ThisClass::UpdatePatrolLocation,1.f,false);
		AIPawn->GetCharacterMovement()->bOrientRotationToMovement = true;
		if (AIPawn->SmartObject && AIPawn->SmartObject->SubTree)
		{
			BTC->SetDynamicSubtree(FGameplayTag(), AIPawn->SmartObject->SubTree);
		}
	}
}

void ASAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (PerceivedActor != nullptr)
	{
		/*
		if (!PerceivedActor->GetAttributeComp()->IsAlive())
		{
			UpdateSightKey(false);
			UpdateTargetKey(AIPerceptionComp->GetClosestEnemy());
		}
		else
		{
			if (GetBlackboardComponent())
			{
				if (GetBlackboardComponent()->GetValueAsBool("HasLineOfSight"))
					SetFocalPoint(PerceivedActor->GetActorLocation());
			}
		}*/
		if (BBC->GetValueAsBool("HasLineOfSight"))
			SetFocalPoint(PerceivedActor->GetActorLocation());
	}
}

ETeamAttitude::Type ASAIController::GetTeamAttitudeTowards(const AActor& Other) const
{
	auto OtherPawn = Cast<APawn>(&Other);
	if (!OtherPawn)
		return ETeamAttitude::Neutral;

	const auto AITeamAgentInterface = Cast<IGenericTeamAgentInterface>(OtherPawn->GetController());
	const auto PlayerTeamAgentInterface = Cast<IGenericTeamAgentInterface>(&Other);
	if (!AITeamAgentInterface && !PlayerTeamAgentInterface)
		return ETeamAttitude::Neutral;
	/*
	if (AITeamAgentInterface)
	{
		FString StringAITeamId = FString::FromInt(AITeamAgentInterface->GetGenericTeamId());
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, FString::Printf(TEXT("AITeamId: %s"),*StringAITeamId));
	}

	if (PlayerTeamAgentInterface)
	{
		const FString StringPlayerTeamId = FString::FromInt(PlayerTeamAgentInterface->GetGenericTeamId());
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, FString::Printf(TEXT("PlayerTeamId: %s"),*StringPlayerTeamId));
	}
*/
	FGenericTeamId OtherActorTeamID(NULL);
	if(AITeamAgentInterface)
		OtherActorTeamID = AITeamAgentInterface->GetGenericTeamId();
	else if (PlayerTeamAgentInterface)
		OtherActorTeamID = PlayerTeamAgentInterface->GetGenericTeamId();

	FGenericTeamId ThisAITeamId = GetGenericTeamId();
	if (OtherActorTeamID == 2)
	{
		return ETeamAttitude::Neutral;
	}
	else if (ThisAITeamId == OtherActorTeamID)
	{
		return ETeamAttitude::Friendly;
	}
	else
	{
		return ETeamAttitude::Hostile;
	}
}

void ASAIController::BroadcastTarget(AActor* Target)
{
	const FAITeamStimulusEvent TeamEvent = FAITeamStimulusEvent(this, Target, Target->GetActorLocation(), 4000.0f);
	UAIPerceptionSystem::OnEvent<FAITeamStimulusEvent,FAITeamStimulusEvent::FSenseClass>(GetWorld(), TeamEvent);
}

void ASAIController::OnPerceptionUpdated(const TArray<AActor*>& Actors)
{
	
}

void ASAIController::OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus)
{
	ClearDeadActors();
	ACPP_SkillBoxCharacter* EnemyChar = Cast<ACPP_SkillBoxCharacter>(Actor);
	if (EnemyChar)
	{
		if (AIPawn && EnemyChar->Faction == AIPawn->Faction)
		{
			return;
		}
	}
	
	/*bool bIsAlive = true;
	if (EnemyChar)
	{
		bIsAlive = EnemyChar->GetAttributeComp()->IsAlive();
		if (EnemyChar->GetAttributeComp()->IsDead())
		{
			UpdateSightKey(false);
			UpdateTargetKey(GetClosestTarget());
			return;
		}
	}*/
	if (EnemyChar->GetAttributeComp()->IsDead())
	{
		UpdateSightKey(false);
		//UpdateTargetKey(GetClosestTarget());
		return;
	}
	
	if (Actor != GetPawn())
	{
		UpdatePerceivedActors(Actor);
		if (Stimulus.WasSuccessfullySensed())
		{
			if (Stimulus.Type == UAISense::GetSenseID(UAISense_Team::StaticClass()) && PerceivedActor == nullptr)
			{
				if (DrawDebug)
					UKismetSystemLibrary::DrawDebugSphere(GetWorld(), Actor->GetActorLocation(), 30.f, 12,FColor::Purple, 2.f, 1.f);
				UpdateTargetKey(Actor);
				SetFocalPoint(Actor->GetActorLocation());
				GetWorldTimerManager().SetTimer(LostSightTimer, this, &ThisClass::LostSight, 5.f, false);
				//UpdateLastLocation(GetClosestTarget().StimulusInfo.StimulusLocation);
				/*if (AActor* TargetActor = GetClosestTarget())
				{
					UpdateTargetKey(TargetActor);
					SetFocalPoint(TargetActor->GetActorLocation());
				}*/
			}
			if (Stimulus.Type == UAISense::GetSenseID(UAISense_Damage::StaticClass()))
			{
				BroadcastTarget(Actor);
				if (!PerceivedActor)
				{
					UpdateTargetKey(Actor);
					SetFocalPoint(Actor->GetActorLocation());
				}
				//UpdateLastLocation(ActorInfo.StimulusInfo.StimulusLocation);
				if (DrawDebug)
					UKismetSystemLibrary::DrawDebugSphere(GetWorld(), Stimulus.StimulusLocation, 30.f, 12,FColor::Red, 2.f, 1.f);
				GetWorldTimerManager().SetTimer(LostSightTimer, this, &ThisClass::LostSight, SearchTime, false);
			}
			if (Stimulus.Type == UAISense::GetSenseID(UAISense_Sight::StaticClass()))
			{
				BroadcastTarget(Actor);
				GetWorldTimerManager().ClearTimer(LostSightTimer);
				UpdateSightKey(true);
				if (AActor* TargetActor = GetClosestTarget())
				{
					UpdateTargetKey(TargetActor);
					SetFocalPoint(TargetActor->GetActorLocation());
				}
				//UKismetSystemLibrary::DrawDebugSphere(GetWorld(), PerceivedActor->GetActorLocation(), 30.f, 12,FColor::Green, 2.f, 1.f);
			}
			if (Stimulus.Type == UAISense::GetSenseID(UAISense_Hearing::StaticClass()))
			{
				bIsHearing = true;
				BroadcastTarget(Actor);
				if (!PerceivedActor || !BBC->GetValueAsBool("HasLineOfSight"))
				{
					UpdateTargetKey(Actor);
					SetFocalPoint(Actor->GetActorLocation());
				}
				GetWorldTimerManager().SetTimer(LostSightTimer, this, &ThisClass::LostSight, SearchTime, false);
				
				if (DrawDebug)
					UKismetSystemLibrary::DrawDebugSphere(GetWorld(), Actor->GetActorLocation(), 30.f, 12,FColor::Yellow, 4.f, 1.f);
			}
		}
		else
		{
			if (Stimulus.Type == UAISense::GetSenseID(UAISense_Sight::StaticClass()))
			{
				GetWorldTimerManager().SetTimer(LostSightTimer, this, &ThisClass::LostSight, SearchTime, false);
				GetBlackboardComponent()->SetValueAsVector("GrenadeLocation", Actor->GetActorLocation());
				
				if (DrawDebug)
				{
					UKismetSystemLibrary::DrawDebugSphere(GetWorld(), Actor->GetActorLocation(), 50.f, 12,
													  FColor::Green, 3.f, 1.f);
					UKismetSystemLibrary::DrawDebugString(GetWorld(), Actor->GetActorLocation(),
														  FString(TEXT("LostTargetHere")), 0, FColor::Blue, 3.f);
				}
				if (AActor* TargetActor = GetClosestTarget())
				{
					SetFocalPoint(TargetActor->GetActorLocation());
					UpdateTargetKey(TargetActor);
				}
				else
				{
					UpdateTargetKey(Actor);
					SetFocalPoint(Actor->GetActorLocation());
				}
				UpdateSightKey(false);
			}
			if (Stimulus.Type == UAISense::GetSenseID(UAISense_Hearing::StaticClass()))
			{
				bIsHearing = false;
			}
		}
	}
}

void ASAIController::ClearDeadActors()
{
	for (AActor* PerceiveActor : PerceivedActors)
	{
		if (PerceiveActor == nullptr)
		{
			PerceivedActors.Remove(PerceiveActor);
			ClearDeadActors();
			break;
		}
		const ACPP_SkillBoxCharacter* TargetChar = Cast<ACPP_SkillBoxCharacter>(PerceiveActor);
		if (TargetChar != nullptr && TargetChar->GetAttributeComp()->IsDead())
		{
			PerceivedActors.Remove(PerceiveActor);
			ClearDeadActors();
			break;
		}
	}
}

AActor* ASAIController::GetClosestTarget()
{
	float BestDistance = MAX_FLT;
	AActor* BestActor = nullptr;
	ClearDeadActors();
	for (AActor* PerceiveActor : PerceivedActors)
	{
		const ACPP_SkillBoxCharacter* TargetChar = Cast<ACPP_SkillBoxCharacter>(PerceiveActor);
		if (TargetChar != nullptr && TargetChar->GetAttributeComp()->IsAlive())
		{
			const auto CurrentDistance = (PerceiveActor->GetActorLocation() - AIPawn->GetActorLocation()).Size();
			if (CurrentDistance < BestDistance)
			{
				BestDistance = CurrentDistance;
				BestActor = PerceiveActor;
			}
		}
	}
	return BestActor;
}

void ASAIController::UpdatePerceivedActors(AActor* Actor)
{
	for (const AActor* Perceive : PerceivedActors)
	{
		if (Perceive == Actor)
		{
			return;
		}
	}
	PerceivedActors.Add(Actor);
}

void ASAIController::UpdatePatrolLocation()
{
	SetGenericTeamId(AIPawn->GetGenericTeamId());
	UAIPerceptionSystem::GetCurrent(GetWorld())->UpdateListener(*AIPerceptionComp);
	if (AIPawn)
		BBC->SetValueAsVector("PatrolLocation",AIPawn->PatrolLocation);
}

void ASAIController::UpdateSightKey(bool HasLineOfSight)
{
	BBC->SetValueAsBool("HasLineOfSight", HasLineOfSight);
}

void ASAIController::PerceivedActorOnDeath(AActor* ActorOwner, AActor* Inst)
{
	UpdateTargetKey(GetClosestTarget());
}

void ASAIController::UpdateTargetKey(AActor* Actor)
{
	if (Actor)
	{
		if (ACPP_SkillBoxCharacter* TargetActor = Cast<ACPP_SkillBoxCharacter>(Actor))
		{
			PerceivedActor = TargetActor;
			if (PerceivedActor)
				PerceivedActor->GetAttributeComp()->OnDeath.AddUniqueDynamic(this, &ASAIController::PerceivedActorOnDeath);
		}
		AIPawn->GetCharacterMovement()->bOrientRotationToMovement = false;
		SetFocalPoint(Actor->GetActorLocation());
		BBC->SetValueAsObject("TargetActor", Actor);      
	}
	else
	{
		BBC->SetValueAsObject("TargetActor", nullptr);
		PerceivedActor = nullptr;
		AIPawn->GetCharacterMovement()->bOrientRotationToMovement = true;
	}
}

void ASAIController::UpdateLastLocation(FVector Location)
{
	BBC->SetValueAsVector("LastTargetLocation", Location);
}

void ASAIController::LostSight()
{
	UpdateTargetKey(nullptr);
	//PercievedActorsInfo.Empty();
}

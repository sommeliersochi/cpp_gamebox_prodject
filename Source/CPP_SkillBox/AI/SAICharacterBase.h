// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/CPP_SkillBoxCharacter.h"

#include "SAICharacterBase.generated.h"

class UDropItemComponent;
class UPawnSensingComponent;
class UBehaviorTree;
class UCombatComponent;
class ASAIControllerBase;

UCLASS()
class CPP_SKILLBOX_API ASAICharacterBase : public ACPP_SkillBoxCharacter
{
	GENERATED_BODY()
public:
	ASAICharacterBase(const FObjectInitializer& ObjectInit);

	virtual void BeginPlay() override;

	virtual void PostInitializeComponents() override;

	UBehaviorTree* GetBehaviorTreeAsset() const;

	void UpdateFaction(EFaction NewFaction);

	void ToggleSitting(bool Value);


protected:
	virtual void OnDeath(AActor* VictimActor, AActor* Killer) override;
	
	UFUNCTION()
	void OnPawnSeen(APawn* Pawn);

public:
	UPROPERTY(Transient, BlueprintReadOnly)
	ASAIControllerBase* AIControllerRef = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AI")
	ECombatRole CombatRole;

	FVector PatrolLocation{0};
	
	UPROPERTY(BlueprintReadWrite)
	bool CanFire = true;

protected:
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AI")
	class UBehaviorTree* TreeAsset;

	UPROPERTY(VisibleAnywhere, Category= "Components")
	UPawnSensingComponent* SensingComp = nullptr;

	UPROPERTY(VisibleAnywhere, Category= "Components")
	UDropItemComponent* DropItemComponent = nullptr;
};

FORCEINLINE_DEBUGGABLE UBehaviorTree* ASAICharacterBase::GetBehaviorTreeAsset() const
{
	return TreeAsset;
}

UCLASS()
class CPP_SKILLBOX_API AAICharacterGuardian : public ASAICharacterBase
{
	GENERATED_BODY()
};

UCLASS()
class CPP_SKILLBOX_API AAICharacterPatrol : public ASAICharacterBase
{
	GENERATED_BODY()
};


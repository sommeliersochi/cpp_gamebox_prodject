// Fill out your copyright notice in the Description page of Project Settings.


#include "SEnemyEnvQueryContext.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"


void USEnemyEnvQueryContext::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
	Super::ProvideContext(QueryInstance, ContextData);

	const auto QueryOwner = Cast<AActor>(QueryInstance.Owner.Get());

	const auto BlackBoard = UAIBlueprintHelperLibrary::GetBlackboard(QueryOwner);
	if (!BlackBoard)
	{
		return;
	}

	const auto ContextActor = Cast<AActor>(BlackBoard->GetValueAsObject(EnemyActorKey));
	if (IsValid(ContextActor))
	{
		UEnvQueryItemType_Actor::SetContextHelper(ContextData, ContextActor);
	}
}

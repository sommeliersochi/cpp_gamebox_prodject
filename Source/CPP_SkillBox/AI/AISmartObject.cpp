// Fill out your copyright notice in the Description page of Project Settings.


#include "AISmartObject.h"

#include <Components/ArrowComponent.h>
#include <Components/BillboardComponent.h>


AAISmartObject::AAISmartObject(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	
	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("DefaultComponent")));

	Billboard = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billboard"));
	Billboard->SetupAttachment(GetRootComponent());

	FacingDirection = CreateDefaultSubobject<UArrowComponent>(TEXT("Facing Direction"));
	FacingDirection->SetupAttachment(GetRootComponent());
}



// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>

#include "AISmartObject.h"
#include "AIPatrolPath.generated.h"

UCLASS()
class CPP_SKILLBOX_API AAIPatrolPath : public AAISmartObject
{
	GENERATED_BODY()

public:

	AAIPatrolPath(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

protected:
	void CachedSplinePoints();

public:
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "SplinePath")
	class USplineComponent* Path;

	TArray<FVector> Locations;
};

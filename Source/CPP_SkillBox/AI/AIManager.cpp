#include "AIManager.h"

#include "CPP_SkillBoxCharacter.h"
#include "EngineUtils.h"
#include "SAICharacterBase.h"
#include "SAIControllerBase.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "CPP_SkillBox/CPP_SkillBox.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

AAIManager::AAIManager()
{
	PrimaryActorTick.bCanEverTick = false;
}


void AAIManager::BeginPlay()
{
	Super::BeginPlay();
	CreateAgentsList();
	UpdateCombatRole();
}

void AAIManager::CoverFire(bool ProvideCoverFire, ASAICharacterBase* Instgtr)
{
	if (!Instgtr || Defenders.Find(Instgtr) < 0)
	{
		return;
	}

	if (ProvideCoverFire)
	{
		for (const auto& Agnt : Defenders)
		{
			if (!IsValid(Agnt))
			{
				continue;
			}

			UBlackboardComponent* AgntBlackboardComponent = Agnt->AIControllerRef->BBC;
			if (Agnt != Instgtr && AgntBlackboardComponent->GetValueAsEnum(AICombatStateKey) == (uint8)EAICombatState::HoldCover)
			{
				AgentProvidingCoverFire = Agnt;
				AgntBlackboardComponent->SetValueAsBool(ShootFromCoverKey, true);
				break;
			}
		}
	}
	else if (IsValid(AgentProvidingCoverFire))
	{
		UBlackboardComponent* AgntBlackboardComponent = AgentProvidingCoverFire->AIControllerRef->BBC;
		if (AgntBlackboardComponent->GetValueAsEnum(AICombatStateKey) == (uint8)EAICombatState::HoldCover)
		{
			AgntBlackboardComponent->SetValueAsBool(ShootFromCoverKey, false);
		}
		AgentProvidingCoverFire = nullptr;
	}
}

bool AAIManager::Engaged()
{
	for (const auto& Cntrlr : Agents)
	{
		if (!IsValid(Cntrlr))
		{
			continue;
		}

		if (Cntrlr->BBC->GetValueAsBool(ContactKey))
		{
			return true;
		}

		if (Cntrlr->BBC->GetValueAsObject(TargetActorKey))
		{
			return true;
		}

		if (UKismetSystemLibrary::GetGameTimeInSeconds(GetWorld()) - Cntrlr->LastSightTimeStamp < MaxStimulusTime_Combat)
		{
			return true;
		}
	}

	return false;
}

void AAIManager::RunCombatLoop()
{
	if (Engaged())
	{
		//Approach
		if (Defenders.Num() > 0 && Defenders.IsValidIndex(ApproacherIndex))
		{
			Defenders[ApproacherIndex]->AIControllerRef->BBC->SetValueAsEnum(AICombatStateKey, (uint8)EAICombatState::ApproachingCover);
			CoverFire(true, Defenders[ApproacherIndex]);
			ApproacherIndex = ApproacherIndex + 1 % Defenders.Num();
		}
	}
	else
	{
		NotifyAIState(EAIState::LostEnemy);
		GetWorldTimerManager().ClearTimer(CombatTimer);
	}
}

void AAIManager::RunSearchTimer()
{
	if (UKismetSystemLibrary::GetGameTimeInSeconds(GetWorld()) - SearchTimeStamp < MaxStimulusTime_Search)
	{
		return;
	}

	GetWorldTimerManager().ClearTimer(SearchTimer);
	NotifyAIState(EAIState::Idle);
}

void AAIManager::CreateAgentsList()
{
	const UWorld* World = GetWorld();
	if (!World)
	{
		return;
	}

	for (TActorIterator<ASAIControllerBase> It(World); It; ++It)
	{
		ASAIControllerBase* Cn = *It; 
		if (Cn && Cn->Agent->Faction == Faction)
		{
			Agents.AddUnique(Cn);
			Cn->AIManager = this;
		}
	}
}

void AAIManager::NotifyAIState(EAIState State)
{
	EAIState NeededState = Engaged() ? EAIState::Attack : State;
	for (const auto& Cntrlr : Agents)
	{
		if (!IsValid(Cntrlr))
		{
			continue;
		}

		Cntrlr->BBC->SetValueAsEnum(AIStateKey, (uint8)NeededState);
		Cntrlr->BBC->SetValueAsVector(LastStimulusLocationKey, LastStimulusLocation);
	}

	switch (State)
	{
	case EAIState::Attack:
		GetWorldTimerManager().ClearTimer(SearchTimer);
		GetWorldTimerManager().SetTimer(CombatTimer, this, &AAIManager::RunCombatLoop, ApproachDelay, true);
		break;
	case EAIState::Search:
		SearchTimeStamp = UKismetSystemLibrary::GetGameTimeInSeconds(GetWorld());
		GetWorldTimerManager().SetTimer(SearchTimer, this, &AAIManager::RunSearchTimer, 1.f, true);
		break;
	default:
		break;
	}
}

void AAIManager::RemoveAgent(ASAIControllerBase* ControllerToRemove)
{
	if (Agents.Contains(ControllerToRemove))
	{
		Agents.RemoveSingle(ControllerToRemove);
		UpdateCombatRole();
	}
}

void AAIManager::UpdateCombatRole()
{
	for (const auto& Cntrlr : Agents)
	{
		if (!IsValid(Cntrlr))
		{
			continue;
		}

		if (Cntrlr->Agent->CombatRole == ECombatRole::Defender)
		{
			Defenders.AddUnique(Cntrlr->Agent);
		}
	}
}

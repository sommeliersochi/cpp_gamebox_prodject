// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <AIController.h>
#include <Perception/AIPerceptionTypes.h>

#include "SAIControllerBase.generated.h"

class ASAICharacterBase;
class UBehaviorTreeComponent;
class UAISenseConfig_Hearing;
class UAISenseConfig_Sight;
class USAIPerceptionComponent;

UCLASS()
class CPP_SKILLBOX_API ASAIControllerBase : public AAIController
{
	GENERATED_BODY()
	
public:
	
	ASAIControllerBase();
	
	virtual void BeginPlay() override;

	virtual void PostInitializeComponents() override;

protected:
	virtual void OnPossess(APawn* InPawn) override;

	UFUNCTION()
	void OnPerception(AActor* Actor, FAIStimulus Stimulus);

	AActor* GetFocusOnActor() const;
	
	void SetDetectionLevel();

public:
	UPROPERTY(Transient, BlueprintReadOnly)
	class AAIManager* AIManager = nullptr;

	UPROPERTY(Transient, BlueprintReadWrite)
	float DetectionLevel = 0.f;

	UPROPERTY(Transient, BlueprintReadWrite)
	float LastSightTimeStamp = 0.f;

	UPROPERTY(Transient)
	class UBehaviorTreeComponent* BTC;

	UPROPERTY(Transient)
	class UBlackboardComponent* BBC;

	UPROPERTY(Transient)
	ASAICharacterBase* Agent = nullptr;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category= "Components")
	USAIPerceptionComponent* PerceptionComp;

	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly, Category= "Components")
	UAISenseConfig_Sight* Sight;
	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly, Category= "Components")
	UAISenseConfig_Hearing* Hearing;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "AI")
	FName FocusOnKeyName = "EnemyActor";

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName AICombatStateKey = TEXT("AICombatState");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName ShootFromCoverKey = TEXT("ShootFromCover");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName ContactKey = TEXT("Contact");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName TargetActorKey = TEXT("TargetActor");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName AIStateKey = TEXT("AIState");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName LastStimulusLocationKey = TEXT("LastStimulusLocation");

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI|Keys")
	FName CombatRoleKey = TEXT("CombatRole");

	FTimerHandle DetectionTime;
	
	float Rate = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DetectionThreshold = 5.f;

	AActor* Target = nullptr;

	FVector LastStimulusLocation = FVector::ZeroVector;
	
};

#include "AI/CoverActor.h"

#include "SAICharacterBase.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"

ACoverActor::ACoverActor(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("DefaultComponent")));

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);

	Collider = CreateDefaultSubobject<UBoxComponent>(TEXT("Collider"));
	Collider->SetupAttachment(RootComponent);
	Collider->OnComponentBeginOverlap.AddDynamic(this, &ACoverActor::OnOverlapBegin);
	Collider->OnComponentEndOverlap.AddDynamic(this, &ACoverActor::OnOverlapEnd);
}

void ACoverActor::BeginPlay()
{
	Super::BeginPlay();
	GetComponents<UArrowComponent>(CoverSpots, true);
	Debug();
}

void ACoverActor::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ASAICharacterBase* Chr = Cast<ASAICharacterBase>(OtherActor);
	if (Chr && Character == nullptr)
	{
		Character = Chr;
		Available = false;
	}
}

void ACoverActor::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
	if (OtherActor == Character)
	{
		Available = true;
		Character = nullptr;
	}
}


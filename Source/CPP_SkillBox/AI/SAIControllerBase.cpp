// Fill out your copyright notice in the Description page of Project Settings.


#include "SAIControllerBase.h"

#include "AIManager.h"
#include "AISmartObject.h"
#include "SAICharacterBase.h"
#include "SAIPerceptionComponent.h"
#include "TDSCharacterMovementComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Perception/AISenseConfig_Hearing.h"
#include "Perception/AISenseConfig_Sight.h"
#include "CPP_SkillBox/CPP_SkillBox.h"

ASAIControllerBase::ASAIControllerBase()
{
	PerceptionComp = CreateDefaultSubobject<USAIPerceptionComponent>("PerceptionComp");

	Sight = CreateDefaultSubobject<UAISenseConfig_Sight>("Sight Config");
	Sight->SightRadius = 2000.f;
	Sight->LoseSightRadius = Sight->SightRadius + 500.f;
	Sight->PeripheralVisionAngleDegrees = 90.f;
	Sight->DetectionByAffiliation.bDetectEnemies = true;
	Sight->DetectionByAffiliation.bDetectFriendlies = true;
	Sight->DetectionByAffiliation.bDetectNeutrals = true;

	Hearing = CreateDefaultSubobject<UAISenseConfig_Hearing>("Hearing Config");
	Hearing->HearingRange = 2000.f;
	Hearing->DetectionByAffiliation.bDetectEnemies = true;
	Hearing->DetectionByAffiliation.bDetectFriendlies = true;
	Hearing->DetectionByAffiliation.bDetectNeutrals = true;

	PerceptionComp->ConfigureSense(*Sight);
	PerceptionComp->ConfigureSense(*Hearing);
	PerceptionComp->SetDominantSense(Sight->GetSenseImplementation());

	BBC = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard Component"));
	BTC = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("behaviorTree Compponent"));
}

/*
void ASAIControllerBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	const auto AimActor = GetFocusOnActor();
	SetFocus(AimActor);
}
*/

void ASAIControllerBase::BeginPlay()
{
	Super::BeginPlay();
	if (!Agent)
	{
		Agent = GetPawn<ASAICharacterBase>();
	}

	if (Agent)
	{
		Agent->AIControllerRef = this;

		if (Agent->SmartObject && Agent->SmartObject->SubTree)
		{
			BTC->SetDynamicSubtree(FGameplayTag(), Agent->SmartObject->SubTree);
		}

		BBC->SetValueAsEnum(CombatRoleKey, static_cast<uint8>(Agent->CombatRole));
	}
}

void ASAIControllerBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (PerceptionComp)
	{
		PerceptionComp->OnTargetPerceptionUpdated.AddDynamic(this, &ASAIControllerBase::OnPerception);
	}
}

void ASAIControllerBase::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	const auto AICharacter = Cast<ASAICharacterBase>(InPawn);
	if (AICharacter != nullptr && AICharacter->GetBehaviorTreeAsset() != nullptr)
	{
		Agent = AICharacter;
		Agent->AIControllerRef = this;

		// Initialize the Blackboard
		RunBehaviorTree(AICharacter->GetBehaviorTreeAsset());

		if (Agent->SmartObject && Agent->SmartObject->SubTree)
		{
			BTC->SetDynamicSubtree(FGameplayTag(), Agent->SmartObject->SubTree);
		}

		BBC->SetValueAsEnum(CombatRoleKey, static_cast<uint8>(Agent->CombatRole));
	}
}

void ASAIControllerBase::OnPerception(AActor* Actor, FAIStimulus Stimulus)
{
	const auto Char = Cast<ACPP_SkillBoxCharacter>(Actor);
	if (UAIPerceptionSystem::GetSenseClassForStimulus(GetWorld(), Stimulus) == UAISense_Sight::StaticClass())
	{
		if (Char && Char->IsHostile(Agent))
		{
			BBC->SetValueAsBool(ContactKey, Stimulus.WasSuccessfullySensed());

			if (BBC->GetValueAsEnum(AIStateKey) != static_cast<uint8>(EAIState::Attack))
			{
				Agent->GetCharacterMovement()->StopActiveMovement();
				BBC->SetValueAsObject(TargetActorKey, Actor);
			}

			Target = Actor;
			LastStimulusLocation = Stimulus.StimulusLocation;

			if (AIManager)
			{
				AIManager->LastStimulusLocation = LastStimulusLocation;
			}

			LastSightTimeStamp = UKismetSystemLibrary::GetGameTimeInSeconds(Agent);
		}

		if (!GetWorldTimerManager().IsTimerActive(DetectionTime) && BBC->GetValueAsBool(ContactKey) && BBC->
			GetValueAsEnum(AIStateKey) == static_cast<uint8>(EAIState::Idle))
		{
			DetectionLevel = 0.0f;
			GetWorldTimerManager().SetTimer(DetectionTime, this, &ASAIControllerBase::SetDetectionLevel, Rate, true,
			                                0.f);
		}
	}
	else
	{
		if (BBC->GetValueAsEnum(AIStateKey) == static_cast<uint8>(EAIState::Attack))
		{
			return;
		}

		if (Char && Char->IsHostile(Agent))
		{
			BBC->SetValueAsEnum(AIStateKey, static_cast<uint8>(EAIState::Investigate));
			BBC->SetValueAsVector(LastStimulusLocationKey, Stimulus.StimulusLocation);
		}
	}
}

AActor* ASAIControllerBase::GetFocusOnActor() const
{
	if (!GetBlackboardComponent())
	{
		return nullptr;
	}

	return Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(FocusOnKeyName));
}

void ASAIControllerBase::SetDetectionLevel()
{
	if (!Target || !BBC->GetValueAsBool(ContactKey))
	{
		GetWorldTimerManager().ClearTimer(DetectionTime);

		if (BBC->GetValueAsEnum(AIStateKey) != static_cast<uint8>(EAIState::Idle))
		{
			return;
		}

		if (DetectionLevel > 0.f)
		{
			DetectionLevel -= 1;
			return;
		}
		return;
	}

	const float Distance = GetPawn()->GetDistanceTo(Target);
	Rate = (Distance <= 500.f) ? 1.f : 2.f;
	DetectionLevel += 1;

	if (DetectionLevel >= DetectionThreshold)
	{
		if (AIManager)
		{
			AIManager->NotifyAIState(EAIState::Attack);
		}

		GetWorldTimerManager().ClearTimer(DetectionTime);
		return;
	}

	if (DetectionLevel >= DetectionThreshold / 2)
	{
		BBC->SetValueAsEnum(AIStateKey, (uint8)EAIState::Investigate);
		BBC->SetValueAsVector(LastStimulusLocationKey, LastStimulusLocation);
	}
}

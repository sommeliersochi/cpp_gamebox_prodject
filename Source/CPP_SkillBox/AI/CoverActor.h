// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CoverActor.generated.h"

UCLASS()
class CPP_SKILLBOX_API ACoverActor : public AActor
{
	GENERATED_BODY()

public:
	ACoverActor(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void Debug();

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
	                    bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

public:
	UPROPERTY(Transient, BlueprintReadOnly)
	class ASAICharacterBase* Character;

	UPROPERTY(VisibleAnywhere, Transient, BlueprintReadOnly, Category = "CoverActor")
	TArray<class UArrowComponent*> CoverSpots;

	bool Available = true;
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	class UStaticMeshComponent* Mesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	class UBoxComponent* Collider;
};

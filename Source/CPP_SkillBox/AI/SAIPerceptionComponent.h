// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Perception/AIPerceptionComponent.h"
#include "SAIPerceptionComponent.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API USAIPerceptionComponent : public UAIPerceptionComponent
{
	GENERATED_BODY()

	public:
	AActor* GetClosestEnemy() const;
	UFUNCTION()
	void ClearPerceivedArray(TArray<AActor*> Arr) const;
};

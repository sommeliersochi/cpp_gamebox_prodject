// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AISmartObject.generated.h"

UCLASS()
class CPP_SKILLBOX_API AAISmartObject : public AActor
{
	GENERATED_BODY()

public:
	AAISmartObject(const FObjectInitializer& ObjectInitializer);

public:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Components")
	class UBillboardComponent* Billboard;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Components")
	class UArrowComponent* FacingDirection;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AI")
	class UBehaviorTree* SubTree;
};

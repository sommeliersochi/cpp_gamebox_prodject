// Fill out your copyright notice in the Description page of Project Settings.


#include "AIPatrolPath.h"

#include <Components/SplineComponent.h>


AAIPatrolPath::AAIPatrolPath(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	Path = CreateDefaultSubobject<USplineComponent>(TEXT("Path"));
	Path->SetupAttachment(GetRootComponent());
}

void AAIPatrolPath::BeginPlay()
{
	Super::BeginPlay();
	
	CachedSplinePoints();
}

void AAIPatrolPath::CachedSplinePoints()
{
	const int32 NumberOfSplinePoints = Path->GetNumberOfSplinePoints();
	Locations.Reserve(NumberOfSplinePoints);
	for (int i = 0; i < NumberOfSplinePoints; ++i)
	{
		Locations.Add(Path->GetLocationAtSplinePoint(i, ESplineCoordinateSpace::World));
	}
}


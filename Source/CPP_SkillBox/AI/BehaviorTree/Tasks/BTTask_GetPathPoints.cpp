// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_GetPathPoints.h"

#include "CPP_SkillBoxCharacter.h"
#include "AI/AIPatrolPath.h"
#include "AI/SAIController.h"
#include "AI/SAIControllerBase.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"


UBTTask_GetPathPoints::UBTTask_GetPathPoints(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bCreateNodeInstance = true;
	NodeName = "Get Path Points";
}

EBTNodeResult::Type UBTTask_GetPathPoints::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const UBlackboardComponent* MyBlackboard = OwnerComp.GetBlackboardComponent();
	//ASAIControllerBase* MyController = Cast<ASAIControllerBase>(OwnerComp.GetAIOwner());
	const ASAIController* MyController = Cast<ASAIController>(OwnerComp.GetAIOwner());
	if (!MyController || !MyBlackboard)
	{
		return EBTNodeResult::Failed;
	}

	ACPP_SkillBoxCharacter* Chr = MyController->GetPawn<ACPP_SkillBoxCharacter>();
	if (!Chr)
	{
		return EBTNodeResult::Failed;
	}

	AAIPatrolPath* PathRef = Cast<AAIPatrolPath>(Chr->SmartObject);
	if (!PathRef || PathRef->Locations.Num() < 1)
	{
		return EBTNodeResult::Succeeded;
	}

	OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>("MoveToLocation", PathRef->Locations[Index]);

	if (Index < PathRef->Locations.Num() - 1)
	{
		Index++;
		return EBTNodeResult::Succeeded;
	}
	Index = 0;
	return EBTNodeResult::Succeeded;
}
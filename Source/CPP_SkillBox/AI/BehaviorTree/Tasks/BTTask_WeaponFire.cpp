// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_WeaponFire.h"
#include "CombatComponent.h"
#include "AI/SAICharacterBase.h"
#include "AI/SAIControllerBase.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"

UBTTask_WeaponFire::UBTTask_WeaponFire(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bCreateNodeInstance = true;
	NodeName = "Fire Weapon";
}

EBTNodeResult::Type UBTTask_WeaponFire::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ASAIControllerBase* MyController = Cast<ASAIControllerBase>(OwnerComp.GetAIOwner());
	if (!MyController)
	{
		return EBTNodeResult::Failed;
	}

	Chr = Cast<ACPP_SkillBoxCharacter>(MyController->GetPawn());
	if (!Chr)
	{
		return EBTNodeResult::Failed;
	}

	const UBlackboardComponent* MyBlackboard = OwnerComp.GetBlackboardComponent();

	ACPP_SkillBoxCharacter* EnemyActor = Cast<ACPP_SkillBoxCharacter>(MyBlackboard->GetValue<UBlackboardKeyType_Object>(BlackboardKey.SelectedKeyName));
	if (!EnemyActor || EnemyActor->Dead || !ShouldFire) 
	{
		Shoot(false);
		return EBTNodeResult::Succeeded;
	}

	
	FHitResult OutHit = MyController->Agent->CapsuleTrace();
	ACPP_SkillBoxCharacter* HitActor = Cast<ACPP_SkillBoxCharacter>(OutHit.GetActor());
	if (HitActor && HitActor->Faction == MyController->Agent->Faction)
	{
		Shoot(false);
		return EBTNodeResult::Succeeded;
	}

	Shoot(true);
	return EBTNodeResult::Succeeded;
}

void UBTTask_WeaponFire::Shoot(bool NewBool)
{
	Chr->ToggleADS(NewBool);
	if (NewBool)
	{
		Chr->GetCombat()->FireButtonPressed(true);
	}
	else
	{
		Chr->GetCombat()->FireButtonPressed(false);
	}
}

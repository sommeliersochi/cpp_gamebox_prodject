#include "BTTask_SelectTarget.h"

#include "AI/SAICharacterBase.h"
#include "AI/SAIControllerBase.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTTask_SelectTarget::UBTTask_SelectTarget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bCreateNodeInstance = true;
	NodeName = "Select Target";
}

EBTNodeResult::Type UBTTask_SelectTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Cntrlr = Cast<ASAIControllerBase>(OwnerComp.GetAIOwner());

	if (Cntrlr && EnemySeekerQuery)
	{
		EnemySeekerQueryRequest = FEnvQueryRequest(EnemySeekerQuery, Cntrlr->Agent);
		EnemySeekerQueryRequest.Execute(EEnvQueryRunMode::AllMatching, this, &UBTTask_SelectTarget::EnemySeekerQueryFinished);
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}

void UBTTask_SelectTarget::EnemySeekerQueryFinished(TSharedPtr<FEnvQueryResult> Result)
{
	BestTarget = nullptr;
	Cntrlr->BBC->SetValueAsObject("TargetActor", nullptr);

	float CurrentBestScore = 0.f;
	int32 Index = 0;
	TArray<AActor*> AllDetectedActors;
	Result->GetAllAsActors(AllDetectedActors);

	for (auto& DetectedActor : AllDetectedActors)
	{
		ACPP_SkillBoxCharacter* Chr = Cast<ACPP_SkillBoxCharacter>(DetectedActor);
		if (Chr && Chr->IsHostile(Cntrlr->Agent) && !Chr->Dead)
		{
			if (Result->GetItemScore(Index) > CurrentBestScore && Result->GetItemScore(Index) >0.f)
			{
				BestTarget = Chr;
				CurrentBestScore = Result->GetItemScore(Index);
			}

			Index++;
		}
	}

	if (BestTarget)
	{
		Cntrlr->BBC->SetValueAsObject("TargetActor", BestTarget);
	}
}
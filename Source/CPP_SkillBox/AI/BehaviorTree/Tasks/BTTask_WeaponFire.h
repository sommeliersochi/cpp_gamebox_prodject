// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_WeaponFire.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UBTTask_WeaponFire : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
public:

	UBTTask_WeaponFire(const FObjectInitializer& ObjectInitializer);

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	void Shoot(bool NewBool);

	UPROPERTY(Transient)
	class ACPP_SkillBoxCharacter* Chr = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Blackboard")
	bool ShouldFire = true;
	
};

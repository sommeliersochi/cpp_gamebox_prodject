#include "AI/BehaviorTree/Tasks/BTTask_GetCoverSpot.h"

#include "AI/CoverActor.h"
#include "AI/SAIControllerBase.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Components/ArrowComponent.h"
#include "Kismet/KismetMathLibrary.h"

UBTTask_GetCoverSpot::UBTTask_GetCoverSpot(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bCreateNodeInstance = true;
	NodeName = "Get CoverSpot";
}

void UBTTask_GetCoverSpot::InitializeFromAsset(UBehaviorTree& Asset)
{
	Super::InitializeFromAsset(Asset);
}

EBTNodeResult::Type UBTTask_GetCoverSpot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (const ASAIControllerBase* Cntrlr = Cast<ASAIControllerBase>(OwnerComp.GetAIOwner()))
	{
		ACoverActor* ThisCover = Cast<ACoverActor>(Cntrlr->BBC->GetValueAsObject(CoverActorKey.SelectedKeyName));
		const AActor* Target = Cast<AActor>(Cntrlr->BBC->GetValueAsObject(TargetActorKey.SelectedKeyName));

		if (ThisCover && ThisCover->CoverSpots.Num() > 0 && Target)
		{
			const FVector TargetLocation = Target->GetActorLocation();

			FVector Destination = FVector::ZeroVector;

			for (const auto& Arw : ThisCover->CoverSpots)
			{
				const FVector ArrowForwardVector = Arw->GetForwardVector();
				const FVector ArrowLocation = Arw->GetComponentLocation();
				const FVector DirectionToTarget = (TargetLocation - ArrowLocation).GetSafeNormal();
				const float AngleToTarget = UKismetMathLibrary::DegAcos(ArrowForwardVector | DirectionToTarget);

				if (AngleToTarget <= DesiredAngleToTarget)
				{
					Destination = ArrowLocation;
					break;
				}
			}

			Cntrlr->BBC->SetValueAsVector(MoveToLocationKey.SelectedKeyName, Destination);
		}
	}

	return EBTNodeResult::Succeeded;
}


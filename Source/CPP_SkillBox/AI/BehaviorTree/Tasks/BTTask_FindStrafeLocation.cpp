// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_FindStrafeLocation.h"

#include "AI/AIManager.h"
#include "AI/SAICharacterBase.h"
#include "AI/SAIControllerBase.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTTask_FindStrafeLocation::UBTTask_FindStrafeLocation(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bCreateNodeInstance = true;
	NodeName = "Find Strafe Location";
}

EBTNodeResult::Type UBTTask_FindStrafeLocation::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Cntrlr = Cast<ASAIControllerBase>(OwnerComp.GetAIOwner());

	if (Cntrlr && LocationSeekerQuery)
	{
		LocationSeekerQueryRequest = FEnvQueryRequest(LocationSeekerQuery, Cntrlr->Agent);
		LocationSeekerQueryRequest.Execute(EEnvQueryRunMode::AllMatching, this, &UBTTask_FindStrafeLocation::LocationSeekerQueryFinished);
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}

void UBTTask_FindStrafeLocation::LocationSeekerQueryFinished(TSharedPtr<FEnvQueryResult> Result)
{
	int32 Index = 0;
	float CurrentBestScore = 0;
	TArray<FVector> Locations;
	Result->GetAllAsLocations(Locations);

	for (auto& Loc : Locations)
	{
		if (IsDistanceGreaterThanX(Loc) && Result->GetItemScore(Index) > CurrentBestScore)
		{
			StrafeLocation = Loc;
			CurrentBestScore = Result->GetItemScore(Index);
		}

		Index++;
	}

	Cntrlr->BBC->SetValueAsVector("MoveToLocation", StrafeLocation);
}

bool UBTTask_FindStrafeLocation::IsDistanceGreaterThanX(FVector Location)
{
	if (!Cntrlr || !Cntrlr->AIManager || Cntrlr->AIManager->Agents.Num() <= 1)
	{
		return true;
	}

	for (const auto& Agnt : Cntrlr->AIManager->Agents)
	{
		if (Agnt->Agent)
		{
			const float CalculatedDistance = (Location - Agnt->Agent->GetActorLocation()).Size();
			if (CalculatedDistance <= Distance)
			{
				return false;
			}
		}
	}
	
	return true;
}
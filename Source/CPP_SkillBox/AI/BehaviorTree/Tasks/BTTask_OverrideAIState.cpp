// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_OverrideAIState.h"

#include "AI/AIManager.h"
#include "AI/SAIControllerBase.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Enum.h"

UBTTask_OverrideAIState::UBTTask_OverrideAIState(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bCreateNodeInstance = true;
	NodeName = "AIState Override";
}

EBTNodeResult::Type UBTTask_OverrideAIState::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ASAIControllerBase* MyController = Cast<ASAIControllerBase>(OwnerComp.GetAIOwner());
	if (!MyController || !MyController->AIManager)
	{
		return EBTNodeResult::Failed;
	}

	if (RunMode == ERunMode::ThisAgent)
	{
		OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Enum>("AIState", static_cast<uint8>(DesiredState));
	}
	else
	{
		MyController->AIManager->NotifyAIState(DesiredState);
	}
	return EBTNodeResult::Succeeded;
}

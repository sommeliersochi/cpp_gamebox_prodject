#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_GetCoverSpot.generated.h"

UCLASS()
class CPP_SKILLBOX_API UBTTask_GetCoverSpot : public UBTTaskNode
{
	GENERATED_BODY()

public:

	UBTTask_GetCoverSpot(const FObjectInitializer& ObjectInitializer);

	/** initialize any asset related data */
	virtual void InitializeFromAsset(UBehaviorTree& Asset) override;

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Blackboard")
	float DesiredAngleToTarget = 45.f;

	/** blackboard key selector */
	UPROPERTY(EditAnywhere, Category=Blackboard)
	struct FBlackboardKeySelector CoverActorKey;

	/** blackboard key selector */
	UPROPERTY(EditAnywhere, Category=Blackboard)
	struct FBlackboardKeySelector TargetActorKey;

	/** blackboard key selector */
	UPROPERTY(EditAnywhere, Category=Blackboard)
	struct FBlackboardKeySelector MoveToLocationKey;
	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_SetBoolValue.h"

#include "BehaviorTree/BlackboardComponent.h"

UBTTask_SetBoolValue::UBTTask_SetBoolValue(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bCreateNodeInstance = true;
	NodeName = "Override Bool Value";
}

EBTNodeResult::Type UBTTask_SetBoolValue::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent())
	{
		BlackboardComponent->SetValueAsBool(BlackboardKey.SelectedKeyName, OverrideValue);
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}

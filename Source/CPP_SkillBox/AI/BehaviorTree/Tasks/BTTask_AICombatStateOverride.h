// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/AIDefinitions.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_AICombatStateOverride.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UBTTask_AICombatStateOverride : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

public:
	UBTTask_AICombatStateOverride(const FObjectInitializer& ObjectInitializer);

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Blackboard")
	EAICombatState DesiredState;
	
};

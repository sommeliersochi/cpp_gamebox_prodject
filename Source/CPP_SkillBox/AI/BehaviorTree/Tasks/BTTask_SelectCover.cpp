#include "AI/BehaviorTree/Tasks/BTTask_SelectCover.h"

#include "AI/CoverActor.h"
#include "AI/SAICharacterBase.h"
#include "AI/SAIControllerBase.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTTask_SelectCover::UBTTask_SelectCover(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bCreateNodeInstance = true;
	NodeName = "Select Cover";
}

EBTNodeResult::Type UBTTask_SelectCover::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Cntrlr = Cast<ASAIControllerBase>(OwnerComp.GetAIOwner());

	if (Cntrlr && CoverSeekerQuery)
	{
		CoverSeekerQueryRequest = FEnvQueryRequest(CoverSeekerQuery, Cntrlr->Agent);
		CoverSeekerQueryRequest.Execute(EEnvQueryRunMode::AllMatching, this, &UBTTask_SelectCover::CoverSeekerQueryFinished);
	}

	return EBTNodeResult::Succeeded;
}

void UBTTask_SelectCover::CoverSeekerQueryFinished(TSharedPtr<FEnvQueryResult> Result)
{
	if (!IsValid(Cntrlr))
	{
		return;
	}
	ACoverActor* BestCover = nullptr;

	AActor* Target = Cast<AActor>(Cntrlr->BBC->GetValueAsObject("TargetActor"));

	float CurrentBestScore = 0.f;
	TArray<AActor*> AllDetectedActors;
	Result->GetAllAsActors(AllDetectedActors);

	switch (RunMode)
	{
	case ECoverType::Approach:
		for (int32 Index = 0; Index < AllDetectedActors.Num(); ++Index)
		{
			ACoverActor* ThisCover = Cast<ACoverActor>(AllDetectedActors[Index]);
			if (ThisCover && ThisCover->GetDistanceTo(Target) >= DesiredDistance && ThisCover->Available && ThisCover->Character == nullptr)
			{
				const float CalculatedDirectness = (Cntrlr->Agent->GetDistanceTo(Target) - ThisCover->GetDistanceTo(Target)) /
					Cntrlr->Agent->GetDistanceTo(ThisCover);

				if (Result->GetItemScore(Index) > CurrentBestScore && CalculatedDirectness > DesiredDirectness)
				{
					BestCover = ThisCover;
					CurrentBestScore = Result->GetItemScore(Index);
				}
			}
		}
		break;
	case ECoverType::Reposition:
		if (Result->GetItemScore(0) > 0.0f) // i don't know this condition
		{
			for (int32 Index = 0; Index < AllDetectedActors.Num(); ++Index)
			{
				ACoverActor* ThisCover = Cast<ACoverActor>(AllDetectedActors[Index]);
				if (ThisCover && ThisCover->Available && ThisCover->Character == nullptr && Result->GetItemScore(Index) > CurrentBestScore)
				{
					BestCover = ThisCover;
					CurrentBestScore = Result->GetItemScore(Index);
				}
			}
		}
		break;
	case ECoverType::Retreat:
		for (int32 Index = 0; Index < AllDetectedActors.Num(); ++Index)
		{
			ACoverActor* ThisCover = Cast<ACoverActor>(AllDetectedActors[Index]);
			if (ThisCover && ThisCover->GetDistanceTo(Target) >= DesiredDistance && ThisCover->Available && ThisCover->Character == nullptr)
			{
				const float CalculatedDirectness = (Cntrlr->Agent->GetDistanceTo(Target) - ThisCover->GetDistanceTo(Target)) /
					Cntrlr->Agent->GetDistanceTo(ThisCover);

				if (Result->GetItemScore(Index) > CurrentBestScore && CalculatedDirectness < DesiredDirectness)
				{
					BestCover = ThisCover;
					CurrentBestScore = Result->GetItemScore(Index);
				}
			}
		}
		break;
	}

	if (BestCover)
	{
		ACoverActor* PreviousCover = Cast<ACoverActor>(Cntrlr->BBC->GetValueAsObject("CoverActor"));
		if (PreviousCover && PreviousCover != BestCover)
		{
			PreviousCover->Available = true;
			PreviousCover->Character = nullptr;
		}

		Cntrlr->BBC->SetValueAsObject("CoverActor", BestCover);
		BestCover->Available = false;
		BestCover->Character = Cntrlr->Agent;
	}
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "SBTTask_RangedAttack.h"

#include <AIController.h>
#include <BehaviorTree/BlackboardComponent.h>
#include <GameFramework/Character.h>

#include "CombatComponent.h"
#include "CPP_SkillBoxCharacter.h"
#include "CPP_SkillBox/Core/SUtils.h"
#include "SAttributeComponent.h"
#include "AI/SAIControllerBase.h"

USBTTask_RangedAttack::USBTTask_RangedAttack()
{
}

EBTNodeResult::Type USBTTask_RangedAttack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	return EBTNodeResult::Succeeded;
}

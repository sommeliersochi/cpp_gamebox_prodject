#include "AI/BehaviorTree/Tasks/BTTask_AICombatStateOverride.h"

#include "AI/SAIControllerBase.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Enum.h"

UBTTask_AICombatStateOverride::UBTTask_AICombatStateOverride(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bCreateNodeInstance = true;
	NodeName = "CombatState Override";
}

EBTNodeResult::Type UBTTask_AICombatStateOverride::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ASAIControllerBase* MyController = Cast<ASAIControllerBase>(OwnerComp.GetAIOwner());
	if (!MyController || !MyController->AIManager)
	{
		return EBTNodeResult::Failed;
	}

	OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Enum>(GetSelectedBlackboardKey(), static_cast<uint8>(DesiredState));
	return EBTNodeResult::Succeeded;
}

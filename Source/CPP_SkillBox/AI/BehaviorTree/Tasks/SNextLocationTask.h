// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "SNextLocationTask.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API USNextLocationTask : public UBTTaskNode
{
	GENERATED_BODY()
public:

	USNextLocationTask();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "AI")
	float Radius = 1000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "AI")
	bool SelfCenter = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "AI")
	FBlackboardKeySelector MoveToLocation;//AimLocationKey;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "AI", meta = (EditCondition = "!SelfCenter"))
	FBlackboardKeySelector CenterActorKey; 

	
	
	
};

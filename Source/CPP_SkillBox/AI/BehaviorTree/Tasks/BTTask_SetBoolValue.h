// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_SetBoolValue.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UBTTask_SetBoolValue : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
	
public:

	UBTTask_SetBoolValue(const FObjectInitializer& ObjectInitializer);

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Blackboard")
	bool OverrideValue = false;
};

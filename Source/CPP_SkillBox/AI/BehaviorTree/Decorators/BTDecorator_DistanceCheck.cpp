
#include "BTDecorator_DistanceCheck.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"

UBTDecorator_DistanceCheck::UBTDecorator_DistanceCheck(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NodeName = "Distance Check";
	bCreateNodeInstance = true;

	BlackboardKey.AddObjectFilter(this, *NodeName, AActor::StaticClass());
}

bool UBTDecorator_DistanceCheck::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const UBlackboardComponent* MyBlackboard = OwnerComp.GetBlackboardComponent();
	const AAIController* MyController = OwnerComp.GetAIOwner();

	if (!MyController || !MyBlackboard)
	{
		return false;
	}

	const AActor* TargetActor = Cast<AActor>(MyBlackboard->GetValueAsObject(BlackboardKey.SelectedKeyName));
	if (!TargetActor)
	{
		return false;
	}

	if (const APawn* AIPawn = MyController->GetPawn())
	{
		return AIPawn->GetDistanceTo(TargetActor) >= TestDistance;
	}

	return false;
}
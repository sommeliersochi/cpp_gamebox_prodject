// Fill out your copyright notice in the Description page of Project Settings.

#include "SFindEnemyService.h"
#include <BehaviorTree/BlackboardComponent.h>


#include "AI/SAIPerceptionComponent.h"
#include "CPP_SkillBox/Core/SUtils.h"

USFindEnemyService::USFindEnemyService()
{
	NodeName = "Find Enemy";
}

void USFindEnemyService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto Blackboard = OwnerComp.GetBlackboardComponent();
	if(Blackboard)
	{
		const auto Controller = OwnerComp.GetOwner();

		const auto PerceptionComp = SUtils::FindComponent<USAIPerceptionComponent>(Controller);
		if(PerceptionComp)
		{
			Blackboard->SetValueAsObject(EnemyActorKey.SelectedKeyName, PerceptionComp->GetClosestEnemy());
			/*ASAICharacterBase* Owner = Cast<ASAICharacterBase>(Controller);
			ACPP_SkillBoxCharacter* TargetChar = Cast<ACPP_SkillBoxCharacter>(PerceptionComp->GetClosestEnemy());
			if(TargetChar && Owner )
			{
				if(TargetChar->Faction != Owner->Faction)
				{
					Blackboard->SetValueAsObject(EnemyActorKey.SelectedKeyName, TargetChar);
				}
				else
				{
					Blackboard->SetValueAsObject(EnemyActorKey.SelectedKeyName, nullptr);
				}
			}*/
			
		}
	}
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}

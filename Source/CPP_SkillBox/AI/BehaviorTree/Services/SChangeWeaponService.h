// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "SChangeWeaponService.generated.h"


UCLASS()
class CPP_SKILLBOX_API USChangeWeaponService : public UBTService
{
	GENERATED_BODY()

public:
	USChangeWeaponService();

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "AI", meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float Probability = 0.3f;
	
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
};

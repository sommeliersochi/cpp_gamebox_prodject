// Fill out your copyright notice in the Description page of Project Settings.


#include "SFireService.h"

#include <BehaviorTree/BlackboardComponent.h>
#include <AIController.h>

#include "AI/SAICharacterBase.h"
#include "CombatComponent.h"

USFireService::USFireService()
{
	NodeName = "Fire";
}

void USFireService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto Controller = OwnerComp.GetAIOwner();

	const auto Blacboard = OwnerComp.GetBlackboardComponent();

	const auto HasAim = Blacboard && Blacboard->GetValueAsObject(EnemyActorKey.SelectedKeyName);

	if (Controller)
	{
		const auto OwnerChar = Controller->GetPawn<ASAICharacterBase>();
		if (OwnerChar)
		{
			if (HasAim)
			{
				const auto WeaponComp = OwnerChar->GetCombat();
				if (WeaponComp && WeaponComp->GetEquippedWeapon())
				{
					WeaponComp->Fire();
				}
			}
			else
			{
				OwnerChar->FireButtonRealised();
			}
		}
	}

	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}

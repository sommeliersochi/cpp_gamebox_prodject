// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "SFindEnemyService.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API USFindEnemyService : public UBTService
{
	GENERATED_BODY()
	
public:
	
	USFindEnemyService();
	
protected:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "AI")
	FBlackboardKeySelector EnemyActorKey;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
};

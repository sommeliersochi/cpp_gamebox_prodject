#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "SAICharacterBase.h"
#include "Perception/AIPerceptionTypes.h"
#include "SAIController.generated.h"


USTRUCT(BlueprintType)
struct FPercievedActorInfo
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadOnly)
	AActor* Actor = nullptr;
	UPROPERTY(BlueprintReadOnly)
	FAIStimulus StimulusInfo;
};

class USAIPerceptionComponent;
UCLASS()
class CPP_SKILLBOX_API ASAIController : public AAIController
{
	GENERATED_BODY()

	ASAIController(const FObjectInitializer& ObjectInitializer);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USAIPerceptionComponent* AIPerceptionComp;
	UPROPERTY(EditDefaultsOnly, Category= "AI")
	UBehaviorTree* BehaviorTree;
	UPROPERTY(EditDefaultsOnly, Category= "AI")
	float SearchTime = 10.f;
	UPROPERTY(EditDefaultsOnly, Category= "AI")
	bool DrawDebug = false;
	
	UPROPERTY()
	ASAICharacterBase* AIPawn;

	UPROPERTY(BlueprintReadOnly)
	FGenericTeamId TeamId;


	virtual void BeginPlay() override;

	UFUNCTION()
	void AnyDamage(AActor* Actor, float X, const UDamageType* Damage, AController* Controller, AActor* Actor1);

	UFUNCTION()
	void OnPawnDeath(AActor* Owne, AActor* Instigato);
	virtual void OnPossess(APawn* InPawn) override;

public:

	UPROPERTY(Transient)
	class UBehaviorTreeComponent* BTC;

	UPROPERTY(Transient)
	class UBlackboardComponent* BBC;

	UPROPERTY(BlueprintReadOnly)
	TArray<AActor*> PerceivedActors;
	
	virtual void Tick(float DeltaSeconds) override;
	
	virtual ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;

	void BroadcastTarget(AActor* Target);
	
	UFUNCTION()
	void OnPerceptionUpdated(const TArray<AActor*>& Actors);
	UFUNCTION()
	void OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus);

	void ClearDeadActors();
	UPROPERTY(BlueprintReadOnly)
	ACPP_SkillBoxCharacter* PerceivedActor;
	UPROPERTY(BlueprintReadOnly)
	FTimerHandle LostSightTimer;
	
	bool bIsHearing = false;
	
	AActor* GetClosestTarget();
	void UpdatePerceivedActors(AActor* Actor);
	void UpdatePatrolLocation();
	void UpdateSightKey(bool HasLineOfSight);
	UFUNCTION()
	void PerceivedActorOnDeath(AActor* ActorOwner, AActor* Inst);
	void UpdateTargetKey(AActor* Actor);
	void UpdateLastLocation(FVector Location);
	void LostSight();
};

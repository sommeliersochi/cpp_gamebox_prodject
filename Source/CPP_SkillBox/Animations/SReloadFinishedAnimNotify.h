// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SAnimNotify.h"
#include "SReloadFinishedAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API USReloadFinishedAnimNotify : public USAnimNotify
{
	GENERATED_BODY()
	
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>

#include <Animation/AnimInstance.h>

#include "TurningInPlace.h"
#include "SAnimInstance.generated.h"

class USActionComponent;
class UCombatComponent;
class AWeapon;
class ACPP_SkillBoxCharacter;

UCLASS()
class CPP_SKILLBOX_API USAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

protected:
	void NativeInitializeAnimation() override;

	void NativeUpdateAnimation(float DeltaSeconds) override;

protected:
	UPROPERTY(BlueprintReadOnly, Transient, Category= "Animations")
	TObjectPtr<USActionComponent> ActionComp;

	UPROPERTY(BlueprintReadOnly, Transient, Category= "Animations")
	TObjectPtr<UCombatComponent> OwnerCombatComp;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category= "Animations")
	bool bIsStunned;

	UPROPERTY(BlueprintReadOnly, Category = Character)
	TObjectPtr<ACPP_SkillBoxCharacter> Character;

	UPROPERTY(BlueprintReadOnly, Transient, Category= "Movement")
	FTransform LeftHandTransform;

	UPROPERTY(BlueprintReadOnly, Transient, Category = Movement)
	bool bUseFABRIK;

	UPROPERTY(BlueprintReadOnly, Transient, Category = Movement)
	bool bIsCrouched;

	UPROPERTY(BlueprintReadOnly, Transient, Category = Movement)
	bool bSprinting;

	UPROPERTY(BlueprintReadOnly, Transient, Category = Movement)
	bool bRolling;

	UPROPERTY(BlueprintReadOnly, Transient, Category = Movement)
	double Direction;

	UPROPERTY(BlueprintReadOnly, Transient, Category = Movement)
	double Speed;

	UPROPERTY(BlueprintReadOnly, Transient, Category = Movement)
	ETurningInPlace TurningInPlace;

	UPROPERTY(BlueprintReadOnly, Transient, Category = Combat)
	bool bAiming;

	UPROPERTY(BlueprintReadOnly, Transient, Category = Combat)
	bool bIsFiring;
};

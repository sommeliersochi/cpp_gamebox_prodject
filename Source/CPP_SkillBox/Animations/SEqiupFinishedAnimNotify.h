// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SAnimNotify.h"
#include "SEqiupFinishedAnimNotify.generated.h"


UCLASS()
class CPP_SKILLBOX_API USEqiupFinishedAnimNotify : public USAnimNotify
{
	GENERATED_BODY()
	
};

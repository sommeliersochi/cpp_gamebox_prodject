#include "SAnimInstance.h"

#include <GameplayTagContainer.h>

#include "CombatComponent.h"
#include "CPP_SkillBoxCharacter.h"
#include "SActionComponent.h"
#include "Weapon.h"

void USAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	Character = Cast<ACPP_SkillBoxCharacter>(TryGetPawnOwner());

	if (Character)
	{
		ActionComp = Character->GetActionComponent();
		OwnerCombatComp = Character->GetCombat();
	}
}

void USAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	if (!Character)
	{
		return;
	}

	bAiming = Character->IsAiming();
	TurningInPlace = Character->GetTurningInPlace();

	static FGameplayTag StunnedTag = FGameplayTag::RequestGameplayTag("Status.Stunned");
	if (ActionComp)
	{
		bIsStunned = ActionComp->ActiveGameplayTags.HasTag(StunnedTag);
	}

	AWeapon* EquippedWeapon = Character->GetEquippedWeapon();

	if (EquippedWeapon && EquippedWeapon->GetWeaponMesh() && Character->GetMesh())
	{
		const USkeletalMeshComponent* WeaponMesh = EquippedWeapon->GetWeaponMesh();
		LeftHandTransform = WeaponMesh->GetSocketTransform(FName("LeftHandSocket"), ERelativeTransformSpace::RTS_World);
		FVector OutPosition;
		FRotator OutRotation;

		Character->GetMesh()->TransformToBoneSpace(FName("hand_r"), LeftHandTransform.GetLocation(), FRotator::ZeroRotator, OutPosition,
		                                           OutRotation);

		LeftHandTransform.SetLocation(OutPosition);
		LeftHandTransform.SetRotation(FQuat(OutRotation));
	}

	bUseFABRIK = Character->GetCombatState() == ECombatState::ECS_Unoccupied;

	bIsCrouched = Character->bIsCrouched;

	Speed = Character->GetVelocity().Length();
	bIsFiring = Character->GetCombat()->bFireWeaponPressed;
/*
	if (Character->IsPlayerControlled())
	{
		bSprinting = Character->IsSprint(); 
		bRolling = Character->bIsRolling; 
		Direction = Character->Direction; 
	}*/
	bSprinting = Character->IsSprint(); 
	bRolling = Character->bIsRolling; 
	Direction = Character->Direction; 
}

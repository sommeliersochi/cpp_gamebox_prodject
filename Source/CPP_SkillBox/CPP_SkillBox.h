// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#define EPS_Metal EPhysicalSurface::SurfaceType1
#define EPS_Stone EPhysicalSurface::SurfaceType2
#define EPS_Tile EPhysicalSurface::SurfaceType3
#define EPS_Grass EPhysicalSurface::SurfaceType4
#define EPS_Water EPhysicalSurface::SurfaceType5

#define ECC_SkeletalMesh ECollisionChannel::ECC_GameTraceChannel2
#define ECC_FootstepTrace ECollisionChannel::ECC_GameTraceChannel3


DECLARE_LOG_CATEGORY_EXTERN(LogCPP_SkillBox, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogCPP_SkillBox_Net,Log,All);

UENUM(BlueprintType)
enum class EFaction: uint8
{
	Enemy		UMETA(DisplayName = "Enemy"),
	Friendly	UMETA(DisplayName = "Friendly"),
	Neutral		UMETA(DisplayName = "Neutral")
};
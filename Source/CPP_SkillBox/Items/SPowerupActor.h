// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CPP_SkillBox/Core/SGamplayInterface.h"
#include "GameFramework/Actor.h"
#include "SPowerupActor.generated.h"

class URotatingMovementComponent;
class USphereComponent;
UCLASS()
class CPP_SKILLBOX_API ASPowerupActor : public AActor, public ISGamplayInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASPowerupActor(const FObjectInitializer& ObjectInitializer);

protected:
	UPROPERTY(VisibleAnywhere, Category="Component")
	UStaticMeshComponent* MeshComp;
	
	UPROPERTY(EditAnywhere, Category="Powerup")
	float RespawnTime;

	UFUNCTION()
	void ShowPowerup();

	//TODO: refact for multiplayer
	UFUNCTION(Server, Reliable)
	void ServerHideAndCooldownPowerup();
	UFUNCTION(NetMulticast, Reliable)
	void NetMulticastHideAndCooldownPowerup();

	void HideAndCooldownPowerup();

	void SetPowerState(bool bNewIsActivate);
	
	UPROPERTY(VisibleAnywhere, Category="Component")
	USphereComponent* SphereComp;

	UPROPERTY(VisibleAnywhere, Category= "Component")
	URotatingMovementComponent* RotationComp;

public:
	void Interact_Implementation(APawn* InstigatorPawn) override;

};

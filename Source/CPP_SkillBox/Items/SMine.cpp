// Fill out your copyright notice in the Description page of Project Settings.


#include "SMine.h"

#include "CPP_SkillBoxCharacter.h"
#include "NiagaraFunctionLibrary.h"
#include "SAttributeComponent.h"
#include "Kismet/GameplayStatics.h"

ASMine::ASMine()
{
	PrimaryActorTick.bCanEverTick = true;
	MineBody=CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MineBody"));
	SetRootComponent(MineBody);
	MineHead=CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MineHead"));
	MineHead->SetupAttachment(MineBody);
	Collision=CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	Collision->SetupAttachment(MineBody);
	Collision->SetGenerateOverlapEvents(true);
	Collision->SetSphereRadius(100.f);
	RadialForce=CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForce"));
	RadialForce->SetupAttachment(MineBody);
	RadialForce->SetAutoActivate(false);
	RadialForce->bImpulseVelChange=true;

}

void ASMine::BeginPlay()
{
	Super::BeginPlay();
	Collision->SetSphereRadius(CollisionRadius);
	Collision->OnComponentBeginOverlap.AddDynamic(this, &ASMine::OnMineBeginOverlap);
}

bool ASMine::IsDropped()
{
	return bIsDropped;
}

void ASMine::StartTimerToExplose()
{
	if(GetWorld() && !GetWorld()->GetTimerManager().IsTimerActive(TimerToExlose))
	{
		GetWorld()->GetTimerManager().SetTimer(TimerToExlose, this,&ASMine::ExploseMine,DelayToExplose,false, -1.f);
	}
}

void ASMine::ExploseMine()
{
	RadialForce->Activate();
	RadialForce->FireImpulse();
	UGameplayStatics::SpawnSoundAtLocation(this,
			SoundEffect,
			GetActorLocation(),
			FRotator(0),
			1.f,
			1.f,
			0.f,
			nullptr,
			nullptr,
			true);
	UGameplayStatics::ApplyRadialDamageWithFalloff(this,
			BaseDamage,
			MinimumDamage,
			GetActorLocation(),
			CollisionRadius,
			ForceRadius,
			1.f,
			DamageType,
			IgnoredActors,
			this);
	UNiagaraFunctionLibrary::SpawnSystemAtLocation(
			this,
			ExploseEffect,
			GetActorLocation(),
			FRotator(0),
			FVector(1),
			true,
			true);
	Destroy();
}

void ASMine::OnMineBeginOverlap(UPrimitiveComponent* OverComp, AActor* OtherActor, UPrimitiveComponent* PrimComp,
	int Integer, bool Bool, const FHitResult& FHitResult)
{
	auto Target = Cast<ACPP_SkillBoxCharacter>(OtherActor);
	if(Target)
	{
		StartTimerToExplose();
	}
}

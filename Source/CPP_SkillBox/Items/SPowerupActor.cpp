// Fill out your copyright notice in the Description page of Project Settings.


#include "SPowerupActor.h"

#include "Components/SphereComponent.h"
#include "GameFramework/RotatingMovementComponent.h"

// Sets default values
ASPowerupActor::ASPowerupActor(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereComp = CreateDefaultSubobject<USphereComponent>("SphereComp");
	SphereComp->SetCollisionProfileName("Powerup");
	SphereComp->SetWorldLocation(FVector(0, 0, 20), false);
	RootComponent = SphereComp;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>("MeshComp");
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComp->SetupAttachment(RootComponent);

	RotationComp = CreateDefaultSubobject<URotatingMovementComponent>("RotationComp");	
	RotationComp->RotationRate = FRotator(0, 45.f,0);

	RespawnTime = 20.f;

	bReplicates = true;

}

void ASPowerupActor::Interact_Implementation(APawn* InstigatorPawn)
{
	//logic
}

void ASPowerupActor::ShowPowerup()
{
	SetPowerState(true);
}

void ASPowerupActor::ServerHideAndCooldownPowerup_Implementation()
{
	NetMulticastHideAndCooldownPowerup();
}

void ASPowerupActor::NetMulticastHideAndCooldownPowerup_Implementation()
{
	HideAndCooldownPowerup();
}

void ASPowerupActor::HideAndCooldownPowerup()
{
	SetPowerState(false);
	FTimerHandle TimerHandle_RespawnTimer;
	GetWorldTimerManager().SetTimer(TimerHandle_RespawnTimer, this, &ASPowerupActor::ShowPowerup, RespawnTime);
}

void ASPowerupActor::SetPowerState(bool bNewIsActivate)
{
	SetActorEnableCollision(bNewIsActivate);

	RootComponent->SetVisibility(bNewIsActivate, true);
}



// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NiagaraEmitter.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Sound/SoundCue.h"
#include "SMine.generated.h"

UCLASS()
class CPP_SKILLBOX_API ASMine : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASMine();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool IsDropped();

public:	
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	UStaticMeshComponent* MineBody;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	UStaticMeshComponent* MineHead;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	URadialForceComponent* RadialForce;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	USphereComponent* Collision;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	float CollisionRadius = 0.f;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	TSubclassOf<UDamageType> DamageType;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	UNiagaraSystem* ExploseEffect;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	USoundCue* SoundEffect;

	FTimerHandle TimerToExlose;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	float BaseDamage = 0.f;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	float MinimumDamage = 0.f;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	float DamageRadius = 0.f;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	TArray<AActor*> IgnoredActors;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	float ForceRadius = 0.f;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	float ImpulseStrange = 0.f;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	float ForceStrange = 0.f;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MineSettings")
	float DelayToExplose = 0.f;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="MineSettings",meta=(ExposeOnSpawn))
	bool bIsDropped = false;

	UFUNCTION()
	void StartTimerToExplose();

	void ExploseMine();

	UFUNCTION()
	void OnMineBeginOverlap(UPrimitiveComponent* OverComp, AActor* OtherActor, UPrimitiveComponent* PrimComp, int Integer, bool Bool, const FHitResult& FHitResult);
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SPowerupActor_Ammo.h"

#include "CombatComponent.h"
#include "CPP_SkillBox/Core/SUtils.h"
#include "Inventory/InventoryComponent.h"

ASPowerupActor_Ammo::ASPowerupActor_Ammo(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	
}

void ASPowerupActor_Ammo::Interact_Implementation(APawn* InstigatorPawn)
{
	auto InventoryComponent = SUtils::FindComponent<UInventoryComponent>(InstigatorPawn);

	if (InventoryComponent)
	{
		// InventoryComponent->AddAmmo();
		HideAndCooldownPowerup();
	}
}

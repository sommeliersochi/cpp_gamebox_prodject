// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "ArmorPlatePickup.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API AArmorPlatePickup : public APickup
{
	GENERATED_BODY()

protected:
	
	virtual void OnSphereOverlap (
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult
	);

private:
	
	UPROPERTY(EditAnywhere)
	float ArmorStrenght = 25.0f;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoPickup.h"

#include "CPP_SkillBoxCharacter.h"
#include "Inventory/InventoryComponent.h"
#include "SAttributeComponent.h"

void AAmmoPickup::SetAmmoAmount(int32 Value)
{
	AmmoAmount = Value;
}

void AAmmoPickup::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                  UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnSphereOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	if (const ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(OtherActor))
	{
		const USAttributeComponent* AttributeComponent = Character->GetAttributeComp();
		UInventoryComponent* InventoryComponent = Character->GetInventoryComponent();
		if (InventoryComponent && AttributeComponent && AttributeComponent->IsAlive())
		{
			InventoryComponent->AddAmmo(AmmoType, AmmoAmount);
			Destroy();
		}
	}
}

#pragma once

#include <CoreMinimal.h>

#include "ProjectileGrenade.h"
#include "SPowerupActor.h"
#include "SPowerupActor_GrenadeAmmo.generated.h"

UCLASS()
class CPP_SKILLBOX_API ASPowerupActor_GrenadeAmmo : public ASPowerupActor
{
	GENERATED_BODY()

public:
	ASPowerupActor_GrenadeAmmo(const FObjectInitializer& ObjectInitializer);

	void Interact_Implementation(APawn* InstigatorPawn) override;

	FGrenadeInfo GetGranadeInfo() const;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Settings")
	FGrenadeInfo GrenadeInfo;
};

FORCEINLINE_DEBUGGABLE FGrenadeInfo ASPowerupActor_GrenadeAmmo::GetGranadeInfo() const
{
	return GrenadeInfo;
}

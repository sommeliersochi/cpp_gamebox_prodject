// Fill out your copyright notice in the Description page of Project Settings.


#include "SPowerupActor_Health.h"

#include "CPP_SkillBoxCharacter.h"
#include "Inventory/InventoryComponent.h"


void ASPowerupActor_Health::Interact_Implementation(APawn* InstigatorPawn)
{
	if (!ensure(InstigatorPawn))
	{
		return;
	}

	if (!InstigatorPawn->HasAuthority())
	{
		return;
	}

	UE_LOG(LogTemp, Display, TEXT("Activate == ASPowerup_HealthPotion::Interact_Implementation"))

	UInventoryComponent* InventoryComponent = InstigatorPawn->FindComponentByClass<UInventoryComponent>();
	if (InventoryComponent)
	{
		int32 Remaining;
		if (InventoryComponent->CanAddFirstAidKits(NumFirstAidKits, Remaining))
		{
			InventoryComponent->AddFirstAidKits(NumFirstAidKits);
			ServerHideAndCooldownPowerup();
		}
	}
}

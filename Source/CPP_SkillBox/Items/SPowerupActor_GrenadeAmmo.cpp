#include "SPowerupActor_GrenadeAmmo.h"

#include "CombatComponent.h"
#include "CPP_SkillBox/Core/SUtils.h"

ASPowerupActor_GrenadeAmmo::ASPowerupActor_GrenadeAmmo(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
}

void ASPowerupActor_GrenadeAmmo::Interact_Implementation(APawn* InstigatorPawn)
{
	const auto CombatComponent = SUtils::FindComponent<UCombatComponent>(InstigatorPawn);

	if (CombatComponent)
	{
		CombatComponent->PickupGrenade(GetGranadeInfo());
		HideAndCooldownPowerup();
	}
}

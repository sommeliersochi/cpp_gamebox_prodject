// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Sound/SoundCue.h"
#include "Pickup.generated.h"

UCLASS()
class CPP_SKILLBOX_API APickup : public AActor
{
	GENERATED_BODY()

public:
	APickup();

	virtual void Destroyed() override;

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

protected:

	UFUNCTION()
	virtual void OnSphereOverlap (
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult
		);

protected:
	UPROPERTY(VisibleAnywhere, Category="Components")
	TObjectPtr<USphereComponent> OverlapSphere = nullptr;

	UPROPERTY(EditAnywhere, Category="Components")
	TObjectPtr<UStaticMeshComponent> PickupMesh = nullptr;

	UPROPERTY(EditAnywhere, Category="Settings")
	TObjectPtr<USoundCue> PickupSound = nullptr;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SPowerupActor.h"
#include "SPowerupActor_Credits.generated.h"

UCLASS()
class CPP_SKILLBOX_API ASPowerupActor_Credits : public ASPowerupActor
{
	GENERATED_BODY()

public:
	ASPowerupActor_Credits(const FObjectInitializer& ObjectInitializer);

	void Interact_Implementation(APawn* InstigatorPawn) override;

protected:

	int32 CreditAmount = 5000;
	
};

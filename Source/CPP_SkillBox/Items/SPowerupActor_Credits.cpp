// Fill out your copyright notice in the Description page of Project Settings.


#include "SPowerupActor_Credits.h"

#include "Inventory/InventoryComponent.h"

ASPowerupActor_Credits::ASPowerupActor_Credits(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
}

void ASPowerupActor_Credits::Interact_Implementation(APawn* InstigatorPawn)
{
	if(!ensure(InstigatorPawn)) return;

	if(UInventoryComponent* InventoryComponent = InstigatorPawn->FindComponentByClass<UInventoryComponent>())
	{
		HideAndCooldownPowerup();
		InventoryComponent->AddCredits(CreditAmount);
	}
}

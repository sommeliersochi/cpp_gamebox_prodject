// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SPowerupActor.h"
#include "SPowerupActor_Health.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API ASPowerupActor_Health : public ASPowerupActor
{
	GENERATED_BODY()

public:
	void Interact_Implementation(APawn* InstigatorPawn) override;

protected:
	UPROPERTY(EditDefaultsOnly, Category="Settings")
	int NumFirstAidKits = 1;
};

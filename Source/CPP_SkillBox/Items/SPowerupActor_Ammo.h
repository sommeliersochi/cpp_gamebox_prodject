// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SPowerupActor.h"
#include "SPowerupActor_Ammo.generated.h"

class AWeapon;
/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API ASPowerupActor_Ammo : public ASPowerupActor
{
	GENERATED_BODY()

public:
	ASPowerupActor_Ammo(const FObjectInitializer& ObjectInitializer);

	void Interact_Implementation(APawn* InstigatorPawn) override;
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category= "Pickup", meta = (ClampMin = 0))
	int32 ClipAmount = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Pickup")
	TSubclassOf<AWeapon> WeaponType;


};

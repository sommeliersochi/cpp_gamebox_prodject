// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>

#include "Pickup.h"
#include "WeaponTypes.h"
#include "AmmoPickup.generated.h"

UCLASS()
class CPP_SKILLBOX_API AAmmoPickup : public APickup
{
	GENERATED_BODY()

public:
	void SetAmmoAmount(int32 Value);

protected:
	virtual void OnSphereOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult
	) override;

protected:
	UPROPERTY(EditAnywhere, Category="Settings")
	int32 AmmoAmount = 0;

	UPROPERTY(EditAnywhere, Category="Settings")
	EAmmoType AmmoType = EAmmoType::EAT_None;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemChest.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AItemChest::AItemChest()
{
	BaseMash = CreateDefaultSubobject<UStaticMeshComponent>("BaseMash");
	RootComponent = BaseMash;

	LidMash = CreateDefaultSubobject<UStaticMeshComponent>("LidMash");
	LidMash->SetupAttachment(BaseMash);

	TargetPitch = 110;

	bReplicates = true;

}

void AItemChest::Interact_Implementation(APawn* InstigatorPawn)
{
	bLidOpened = !bLidOpened;
	OnRep_LidOpened();
}

void AItemChest::OnActorLoaded_Implementation()
{
	OnRep_LidOpened();
}

void AItemChest::OnRep_LidOpened()
{	
	float CurrPitch = bLidOpened ? TargetPitch : 0.0f;
	LidMash->SetRelativeRotation(FRotator(CurrPitch, 0, 0));
}

void AItemChest::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AItemChest, bLidOpened);
}
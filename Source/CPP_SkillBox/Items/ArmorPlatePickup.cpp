// Fill out your copyright notice in the Description page of Project Settings.


#include "ArmorPlatePickup.h"
#include "CPP_SkillBoxCharacter.h"
#include "Inventory/InventoryComponent.h"

void AArmorPlatePickup::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                        UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnSphereOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(OtherActor);
	if(Character && Character->bIsPlayer)
	{
		UInventoryComponent* Inventory = Character->GetInventoryComponent();
		if(Inventory)
		{
			if(Inventory->PickupArmor())
			{
				Destroy();
			}
		}
	}
}


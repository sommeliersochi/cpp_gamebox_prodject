﻿#pragma once

#include <CoreMinimal.h>
#include <Components/ActorComponent.h>

#include "FindKnifeTargetComponent.generated.h"


class ACPP_SkillBoxCharacter;

UCLASS( ClassGroup=(KnifeTarget), meta=(BlueprintSpawnableComponent) )
class CPP_SKILLBOX_API UFindKnifeTargetComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UFindKnifeTargetComponent(const FObjectInitializer& ObjectInitializer);

	virtual void InitializeComponent() override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Settings")
	float SphereTraceRadius = 50.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Settings")
	float TraceLength = 140.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Settings")
	TEnumAsByte<ETraceTypeQuery> TraceChannel = TraceTypeQuery3;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Settings|Debug")
	bool ShowDebug = false;

	UPROPERTY(Transient, BlueprintReadOnly, Category="KnifeTarget")
	TObjectPtr<ACPP_SkillBoxCharacter> KnifeTarget = nullptr;

	UPROPERTY(Transient, BlueprintReadOnly, Category="KnifeTarget")
	TObjectPtr<ACPP_SkillBoxCharacter> OwnerCharacter = nullptr;
};

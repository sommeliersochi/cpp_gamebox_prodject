﻿#include "Components/KnifeTarget/FindKnifeTargetComponent.h"

#include <Kismet/KismetSystemLibrary.h>
#include "CPP_SkillBoxCharacter.h"
#include "SAttributeComponent.h"

UFindKnifeTargetComponent::UFindKnifeTargetComponent(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = true;

	bAutoActivate = true;

	bWantsInitializeComponent = true;
}

void UFindKnifeTargetComponent::InitializeComponent()
{
	Super::InitializeComponent();

	OwnerCharacter = GetOwner<ACPP_SkillBoxCharacter>();
}

void UFindKnifeTargetComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (OwnerCharacter)
	{
		TArray<AActor*> IgnoreActors;
		IgnoreActors.Add(OwnerCharacter);
		FHitResult HitResult;

		EDrawDebugTrace::Type DrawDebugTrace = EDrawDebugTrace::None;
#if !UE_BUILD_SHIPPING
		if (ShowDebug)
		{
			DrawDebugTrace = EDrawDebugTrace::ForDuration;
		}
#endif

		FVector StartTrace = OwnerCharacter->GetActorLocation();
		FVector EndTrace = OwnerCharacter->GetActorLocation() + (OwnerCharacter->GetActorForwardVector() * TraceLength);

		UKismetSystemLibrary::SphereTraceSingle(
			OwnerCharacter,
			StartTrace,
			EndTrace,
			SphereTraceRadius,
			TraceChannel,
			false,
			IgnoreActors,
			DrawDebugTrace,
			HitResult,
			true
		);

		KnifeTarget = Cast<ACPP_SkillBoxCharacter>(HitResult.GetActor());
		if (KnifeTarget)
		{
			USAttributeComponent* TargetAttributeComponent = KnifeTarget->GetAttributeComp();
			if (!TargetAttributeComponent->IsAlive())
			{
				KnifeTarget = nullptr;
			}
		}
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Components/ActorComponent.h"
#include "SActionComponent.generated.h"


class USAction;
class USActionComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnActionStateChange, USActionComponent*, OwningComp, USAction*, Action);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CPP_SKILLBOX_API USActionComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Tags")
	FGameplayTagContainer ActiveGameplayTags;

	UFUNCTION(BlueprintCallable, Category= "Action")
	void AddAction(AActor* Instigator, TSubclassOf<USAction> ActionClass);

	UFUNCTION(BlueprintCallable, Category= "Action")
	bool StartActionByName(AActor* Instigator, FName ActionName);

	UFUNCTION(BlueprintCallable, Category= "Action")
	bool StopActionByName(AActor* Instigator, FName ActionName);

	UFUNCTION(BlueprintCallable, Category = "Actions")
	USAction* GetActionByName(FName ActionName);

	UFUNCTION(BlueprintCallable, Category= "Action")
	void RemoveAction(USAction* ActionToRemove);

	UFUNCTION(BlueprintCallable, Category = "Action")
	void RemoveAllActions();

	USActionComponent();

protected:

	UPROPERTY(EditAnywhere, Category= "Action")
	TArray<TSubclassOf<USAction>> DefaultAction;

	UPROPERTY(BlueprintReadOnly)
	TArray<USAction*> Actions;

	virtual void BeginPlay() override;
	
	UFUNCTION(Server, Reliable)
	void ServerStartActionByName(AActor* Instigator, FName ActionName);
	
	UFUNCTION(NetMulticast, Reliable)
	void NetMulticastStartActionByName(AActor* Instigator, FName ActionName);

	UFUNCTION(Server, Reliable)
	void ServerStopActionByName(AActor* Instigator, FName ActionName);

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticastStopActionByName(AActor* Instigator, FName ActionName);

public:

	UPROPERTY(BlueprintAssignable)
	FOnActionStateChange OnActionStarted;

	UPROPERTY(BlueprintAssignable)
	FOnActionStateChange OnActionStopped;

		
};

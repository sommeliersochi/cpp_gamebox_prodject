#pragma once

#include <CoreMinimal.h>
#include <Components/ActorComponent.h>

#include "SAttributeComponent.generated.h"

class USActionBleeding;
class USActionComponent;
//class USAttributeComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDeath, AActor*, Owner, AActor*, Instigator);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnHealthChanged, AActor*, InstigatorActor, USAttributeComponent*,
                                              OwningComp, float, NewHealth, float, Damage);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnStaminaChanged, USAttributeComponent*, OwningComp, float, NewStamina,
                                               float, Tired);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldPlateChanged, float, CurrentStrength, int32, Index);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class CPP_SKILLBOX_API USAttributeComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	USAttributeComponent();

	UFUNCTION(BlueprintCallable, Category = "Attributes|Health")
	bool IsAlive() const;

	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable, Category= "Attributes")
	static USAttributeComponent* GetAttributes(AActor* FromActor);

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	bool IsDead() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	bool IsFullHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	bool ApplyHealthChange(AActor* InstigatorActor, float Delta);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastHealthChanged(AActor* InstigatorActor, float NewHealth, float Delta);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastArmorChanged(float Damage, int32 Index);

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetHealthMax() const;

	UFUNCTION(BlueprintCallable)
	float GetHealth() const;

	UFUNCTION(BlueprintCallable)
	float GetStamina() const;

	UFUNCTION()
	void ChangeStamina(float NewTired);

	UFUNCTION(BlueprintCallable)
	void SetMaxHealth(float NewMaxHealth);

	UFUNCTION()
	float GetTired() const;

	UFUNCTION()
	bool IsNoTired() const;

	UFUNCTION()
	void ShieldChange(float Value);

	UFUNCTION()
	float ShieldPlateChange(float Damage, int32 Index);

	bool InstallNewShieldPlate(float StrengthValue);

private:
	UFUNCTION(BlueprintCallable, Category= "Attribute")
	void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType,
	                     class AController* InstigatedBy,
	                     AActor* DamageCauser);

	void StaminaUpdate();

	void HealthUpdate();

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated, Category= "Attributes|Health",
		meta = (ClampMin = "0", ClampMax = "10000.0"))
	float HealthMax = 100.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated, Category = "Attributes|Stamina")
	float StaminaMax = 100.0f;

	UPROPERTY(Replicated)
	float Health = 100.0f;

	UPROPERTY(Replicated)
	float Stamina = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attributes|Stamina")
	bool AutoTire = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attributes|Health")
	bool AutoHeal = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attributes|Stamina")
	float StaminaUpdateTime = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attributes|Health")
	float HealthUpdateTime = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attributes|Stamina")
	float StaminaUpdateDelay = 3.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attributes|Health")
	float HealthUpdateDelay = 3.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attributes|Stamina")
	float StaminaModifier = 4.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attributes|Health")
	float HealthModifier = 4.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attributes|Stamina")
	float Tired = 0.3f;

	UPROPERTY(EditDefaultsOnly, Replicated, BlueprintReadWrite, Category = "Attributes|Shield")
	float StrengthShieldPlate = 25.0f;

	UPROPERTY(EditDefaultsOnly, Replicated, BlueprintReadWrite, Category = "Attributes|Shield")
	int32 ArmorPlates = 3;

	UPROPERTY(EditDefaultsOnly, Replicated, BlueprintReadWrite, Category = "Attributes|Shield")
	TArray<float> CurrentPlates;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attributes|Shield")
	bool HaveShield = true;

	UPROPERTY(BlueprintReadOnly, Category= "Action")
	USActionComponent* ActionComp;

	UPROPERTY(EditDefaultsOnly, Category = "Action")
	TSubclassOf<USActionBleeding> StunActionClass;

	UPROPERTY()
	AActor* CompOwner = nullptr;

	UPROPERTY(BlueprintAssignable)
	FOnDeath OnDeath;

	UPROPERTY(BlueprintAssignable)
	FOnHealthChanged OnHealthChanged;

	UPROPERTY(BlueprintAssignable)
	FOnStaminaChanged OnStaminaChanged;

	UPROPERTY(BlueprintAssignable)
	FOnShieldPlateChanged OnShieldPlateChanged;

	FTimerHandle StaminaTimerHandle;

	FTimerHandle HealthTimerHandle;
};

FORCEINLINE_DEBUGGABLE bool USAttributeComponent::IsAlive() const
{
	return Health > 0.0f;
}

FORCEINLINE_DEBUGGABLE bool USAttributeComponent::IsDead() const
{
	return !IsAlive();
}

FORCEINLINE_DEBUGGABLE bool USAttributeComponent::IsFullHealth() const
{
	return Health == HealthMax;
}

FORCEINLINE_DEBUGGABLE float USAttributeComponent::GetHealthMax() const
{
	return HealthMax;
}

FORCEINLINE_DEBUGGABLE float USAttributeComponent::GetHealth() const
{
	return Health;
}

FORCEINLINE_DEBUGGABLE float USAttributeComponent::GetStamina() const
{
	return Stamina;
}

FORCEINLINE_DEBUGGABLE bool USAttributeComponent::IsNoTired() const
{
	return GetStamina() > 0.0f;
}

FORCEINLINE_DEBUGGABLE float USAttributeComponent::GetTired() const
{
	return Tired;
}

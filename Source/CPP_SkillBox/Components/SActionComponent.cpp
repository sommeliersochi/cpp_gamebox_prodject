// Fill out your copyright notice in the Description page of Project Settings.


#include "SActionComponent.h"

#include "SAction.h"

USActionComponent::USActionComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
}

void USActionComponent::BeginPlay()
{
	Super::BeginPlay();

	for (TSubclassOf<USAction> ActionClass : DefaultAction)
	{
		AddAction(GetOwner(), ActionClass);
	}
}

void USActionComponent::AddAction(AActor* Instigator, TSubclassOf<USAction> ActionClass)
{
	if (!ensure(ActionClass))
	{
		return;
	}

	USAction* NewAction = NewObject<USAction>(this, ActionClass);
	if (ensure(NewAction))
	{
		Actions.Add(NewAction);

		if (NewAction->bAutoStart && ensure(NewAction->CanStart(Instigator)))
		{
			NewAction->StartAction(Instigator);
		}
	}
}

bool USActionComponent::StartActionByName(AActor* Instigator, FName ActionName)
{
	for (USAction* Action : Actions)
	{
		if (Action && Action->ActionName == ActionName)
		{
			if (!Action->CanStart(Instigator))
			{
				FString FailedMsg = FString::Printf(TEXT("Failed to run Action: %s"), *ActionName.ToString());
				GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, FailedMsg);
				continue;
			}

			if (GetOwner()->HasLocalNetOwner())
			{
				ServerStartActionByName(Instigator, ActionName);
			}
			Action->StartAction(Instigator);
			return true;
		}
	}
	return false;
}

bool USActionComponent::StopActionByName(AActor* Instigator, FName ActionName)
{
	for (USAction* Action : Actions)
	{
		if (Action && Action->ActionName == ActionName)
		{
			if (Action->IsRunning())
			{
				if (GetOwner()->HasLocalNetOwner())
				{
					ServerStopActionByName(Instigator, ActionName);
				}

				Action->StopAction(Instigator);
				return true;
			}
		}
	}
	return false;
}

USAction* USActionComponent::GetActionByName(FName Action)
{
	if (Actions.Num() > 0)
	{
		for (int32 i = 0; i <= Actions.Num() - 1; i++)
		{
			//TODO Find Action By Name
		}
	}
	return nullptr;
}

void USActionComponent::RemoveAction(USAction* ActionToRemove)
{
	if (!ensure(ActionToRemove && !ActionToRemove->IsRunning()))
	{
		return;
	}

	Actions.Remove(ActionToRemove);
}

void USActionComponent::RemoveAllActions()
{
	if (Actions.Num() > 0)
	{
		for (int i = 0; i <= Actions.Num() - 1; i++)
		{
			RemoveAction(Actions[i]);
		}
		UE_LOG(LogTemp, Error, TEXT("All Actions REMOVED"));
	}
}

void USActionComponent::ServerStartActionByName_Implementation(AActor* Instigator, FName ActionName)
{
	NetMulticastStartActionByName(Instigator, ActionName);
}

void USActionComponent::NetMulticastStartActionByName_Implementation(AActor* Instigator, FName ActionName)
{
	if (!GetOwner()->HasLocalNetOwner())
	{
		StartActionByName(Instigator, ActionName);
	}
}

void USActionComponent::ServerStopActionByName_Implementation(AActor* Instigator, FName ActionName)
{
	NetMulticastStopActionByName(Instigator, ActionName);
}

void USActionComponent::NetMulticastStopActionByName_Implementation(AActor* Instigator, FName ActionName)
{
	if (!GetOwner()->HasLocalNetOwner())
	{
		StopActionByName(Instigator, ActionName);
	}
}

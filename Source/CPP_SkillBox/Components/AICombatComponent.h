// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CombatComponent.h"
#include "AICombatComponent.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UAICombatComponent : public UCombatComponent
{
	GENERATED_BODY()

protected:
	virtual bool CanDropWeapon() const override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Settings", meta=(UIMin = "0.0", UIMax = "1.0", ClampMin = "0.0", ClampMax = "1.0"))
	float DropWeaponChance = 0.3f;
};

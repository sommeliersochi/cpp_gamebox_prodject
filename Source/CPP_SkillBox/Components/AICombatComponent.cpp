// Fill out your copyright notice in the Description page of Project Settings.


#include "AICombatComponent.h"

bool UAICombatComponent::CanDropWeapon() const
{
	return DropWeaponChance >= FMath::FRandRange(0.0f, 1.0f);
}

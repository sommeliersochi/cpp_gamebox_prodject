// Fill out your copyright notice in the Description page of Project Settings.


#include "SInteractionComponent.h"

#include "SWorldUserWidget.h"
#include "CPP_SkillBoxCharacter.h"
#include "CPP_SkillBox/Core/SGamplayInterface.h"


USInteractionComponent::USInteractionComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void USInteractionComponent::PrimaryInteract()
{
	if (FocusActor == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, "No Focus Actor to interact.");
		return;
	}

	//TODO: refact for multiplayer
	if (OwnerCharacter && OwnerCharacter->HasLocalNetOwner())
	{
		ServerPrimaryInteract();
	}
}

void USInteractionComponent::ServerPrimaryInteract_Implementation()
{
	if (FocusActor == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, "No Focus Actor to interact.");
		return;
	}

	ISGamplayInterface::Execute_Interact(FocusActor, OwnerCharacter);
}

bool USInteractionComponent::ServerPrimaryInteract_Validate()
{
	return true;
}

void USInteractionComponent::BeginPlay()
{
	Super::BeginPlay();

	OwnerCharacter = GetOwner<ACPP_SkillBoxCharacter>();
	if (OwnerCharacter)
	{
		OwnerCharacter->OnPossessedEvent.RemoveDynamic(this, &USInteractionComponent::OnCharacterPossessed);
		OwnerCharacter->OnUnPossessedEvent.RemoveDynamic(this, &USInteractionComponent::OnCharacterUnPossessed);
		OwnerCharacter->OnPossessedEvent.AddDynamic(this, &USInteractionComponent::OnCharacterPossessed);
		OwnerCharacter->OnUnPossessedEvent.AddDynamic(this, &USInteractionComponent::OnCharacterUnPossessed);

		if (OwnerCharacter->IsPossessed())
		{
			OnCharacterPossessed(OwnerCharacter);
		}
	}
}

void USInteractionComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (OwnerCharacter)
	{
		OwnerCharacter->OnPossessedEvent.RemoveDynamic(this, &USInteractionComponent::OnCharacterPossessed);
		OwnerCharacter->OnUnPossessedEvent.RemoveDynamic(this, &USInteractionComponent::OnCharacterUnPossessed);
	}
}

void USInteractionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FindBestInteractable();
}

void USInteractionComponent::FindBestInteractable()
{
	if (!OwnerCharacter)
	{
		return;
	}

	FCollisionObjectQueryParams ObjectQueryParams;
	ObjectQueryParams.AddObjectTypesToQuery(CollisionChannel);

	FocusActor = nullptr;

	const FVector EyeLocation = OwnerCharacter->GetActorLocation() + FVector(0.0f,0.0f, -30.0f);
	const FRotator EyeRotation = OwnerCharacter->GetViewRotation();

	const FVector EndTrace = EyeLocation + (EyeRotation.Vector() * TraceDistance);

	TArray<FHitResult> Hits;

	FCollisionShape Shape;
	Shape.SetSphere(TraceRadius);

	GetWorld()->SweepMultiByObjectType(Hits, EyeLocation, EndTrace, FQuat::Identity, ObjectQueryParams, Shape);

	for (const auto& Hit : Hits)
	{
		AActor* HitActor = Hit.GetActor();
		if (HitActor && HitActor->Implements<USGamplayInterface>())
		{
			FocusActor = HitActor;
			break;
		}
	}

	if (OwnerCharacter->IsLocallyPlayerControlled() && DefaultWidgetInstance)
	{
		DefaultWidgetInstance->AttachedActor = FocusActor;
		if(FocusActor)
		{
			if(!DefaultWidgetInstance->IsInViewport())
			{
				DefaultWidgetInstance->AddToViewport();
			}
		}
		else
		{
			if(DefaultWidgetInstance->IsInViewport())
			{
				DefaultWidgetInstance->RemoveFromParent();
			}
		}
	}
}

void USInteractionComponent::FindBestInteractableActor()
{
	FCollisionObjectQueryParams ObjectQueryParams;
	ObjectQueryParams.AddObjectTypesToQuery(CollisionChannel);

	FVector EyeLocation;
	FRotator EyeRotation;

	OwnerCharacter->GetActorEyesViewPoint(EyeLocation, EyeRotation);

	FVector End = EyeLocation + (EyeRotation.Vector() * 2000.f);
	TArray<FHitResult> Hits;

	FCollisionShape Shape;
	Shape.SetSphere(TraceRadius);

	bool bBlockingHit = GetWorld()->SweepMultiByObjectType(Hits, EyeLocation, End, FQuat::Identity, ObjectQueryParams, Shape);

	FColor LineColor = bBlockingHit ? FColor::Green : FColor::Red;

	FocusActor = nullptr;

	for (FHitResult Hit : Hits)
	{
		AActor* HitActor = Hit.GetActor();
		if (HitActor)
		{
			FocusActor = HitActor;
			break;
		}
		DrawDebugSphere(GetWorld(), Hit.ImpactPoint, TraceRadius, 32,LineColor, false, 2.0f);
	}
}

void USInteractionComponent::OnCharacterPossessed(ACPP_SkillBoxCharacter* Character)
{
	if (OwnerCharacter && OwnerCharacter->IsLocallyPlayerControlled())
	{
		if (!DefaultWidgetInstance && DefaultWidgetClass)
		{
			DefaultWidgetInstance = CreateWidget<USWorldUserWidget>(GetWorld(), DefaultWidgetClass);
		}
	}
}

void USInteractionComponent::OnCharacterUnPossessed(ACPP_SkillBoxCharacter* Character)
{
	if (DefaultWidgetInstance && DefaultWidgetInstance->IsInViewport())
	{
		DefaultWidgetInstance->RemoveFromParent();
	}
}

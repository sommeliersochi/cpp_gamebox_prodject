#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FindTargetComponent.generated.h"


class ACPP_SkillBoxCharacter;
class UCombatComponent;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CPP_SKILLBOX_API UFindTargetComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UFindTargetComponent();

	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UPROPERTY(EditAnywhere, Category="Settings")
	FVector BoxExtent = FVector(130.0, 130.0, 1450.0);
	UPROPERTY(EditAnywhere, Category="Settings")
	double TraceLength = 3200.0;
	UPROPERTY(EditAnywhere, Category="Settings")
	double StartTraceOffset = 200.0;
	UPROPERTY(EditAnywhere, Category="Settings")
	TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjectTypes;
	UPROPERTY(EditAnywhere, Category="Settings|Debug")
	bool ShowDebug = false;

	FRotator NormalToTarget = FRotator::ZeroRotator;
	FVector CloseTargetLocation = FVector::ZeroVector;
	bool Overlap = false;

protected:
	TObjectPtr<UCombatComponent> OwnerCombatComponent = nullptr;
	TObjectPtr<ACPP_SkillBoxCharacter> OwnerCharacter = nullptr;
};

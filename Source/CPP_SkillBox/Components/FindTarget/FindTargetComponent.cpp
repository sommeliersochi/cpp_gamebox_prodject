#include "Components/FindTarget/FindTargetComponent.h"

#include <Kismet/KismetSystemLibrary.h>

#include "CombatComponent.h"
#include "CPP_SkillBoxCharacter.h"

UFindTargetComponent::UFindTargetComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	bAutoActivate = true;
}


void UFindTargetComponent::BeginPlay()
{
	Super::BeginPlay();

	OwnerCharacter = GetOwner<ACPP_SkillBoxCharacter>();
	if (OwnerCharacter)
	{
		OwnerCombatComponent = OwnerCharacter->GetCombat();
	}
}


void UFindTargetComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (OwnerCharacter && OwnerCombatComponent)
	{
		EDrawDebugTrace::Type DrawDebugType = EDrawDebugTrace::None;

#if !UE_BUILD_SHIPPING
		if (ShowDebug)
		{
			DrawDebugType = EDrawDebugTrace::ForDuration;
		}
#endif

		const FVector CharacterLocation = OwnerCharacter->GetActorLocation();
		const FVector CharacterForwardVector = OwnerCharacter->GetActorForwardVector();

		const TArray<AActor*> IgnoreActors {OwnerCharacter};
		const FVector StartLocation = CharacterLocation + CharacterForwardVector * StartTraceOffset;
		const FVector EndLocation = StartLocation + CharacterForwardVector * TraceLength;

		FHitResult HitResult;
		UKismetSystemLibrary::BoxTraceSingleForObjects(
			OwnerCharacter,
			StartLocation,
			EndLocation,
			BoxExtent,
			OwnerCharacter->GetActorRotation(),
			TraceObjectTypes,
			false,
			IgnoreActors,
			DrawDebugType,
			HitResult,
			true
		);

		if (HitResult.bBlockingHit && HitResult.GetActor())
		{
			Overlap = true;
			CloseTargetLocation = HitResult.GetActor()->GetActorLocation();
			FRotator Rot = (CloseTargetLocation - CharacterLocation).Rotation();
			Rot.Pitch += 90.f;
			NormalToTarget = Rot;
		}
		else
		{
			Overlap = false;
		}
	}
}

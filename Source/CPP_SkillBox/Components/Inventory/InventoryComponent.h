﻿#pragma once

#include <CoreMinimal.h>
#include <Components/ActorComponent.h>

#include "WeaponTypes.h"
#include "ProjectileGrenade.h"
#include "InventoryComponent.generated.h"

class ACPP_SkillBoxCharacter;
enum class EGrenadeSubType : uint8;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FInventoryComponentAmmoChanged, UInventoryComponent*, InventoryComponent,
                                               EAmmoType, Type, int32, NewAmmo);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FInventoryComponentGrenadeChanged, UInventoryComponent*,
                                               InventoryComponent, EGrenadeSubType, Type, int32, NewAmmo);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FInventoryComponentCreditsChanged, UInventoryComponent*,
                                               InventoryComponent, int32, NewCredits, int32, Delta);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FInventoryComponentNumFirstAidKitsChanged, UInventoryComponent*,
                                             InventoryComponent, int32, NewNumFirstAidKits);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnArmorPlatesChanged, int32, CurrentPlates);

UCLASS(ClassGroup=(Inventory), meta=(BlueprintSpawnableComponent))
class CPP_SKILLBOX_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UInventoryComponent();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintPure, Category="Inventory")
	int32 GetAmmo(const EAmmoType Type) const;
	UFUNCTION(BlueprintPure, Category="Inventory")
	int32 GetGrenade(const EGrenadeSubType Type) const;

	void AddAmmo(const EAmmoType Type, int32 Delta);

	void AddGrenade(EGrenadeSubType Type, int32 Delta);

	UFUNCTION(BlueprintCallable, Category = "PlayerState|Credits")
	void AddCredits(int32 Delta);

	UFUNCTION(BlueprintCallable, Category = "PlayerState|Credits")
	bool RemoveCredits(int32 Delta);

	UFUNCTION(BlueprintCallable, Category = "PlayerState|Credits")
	int32 GetCredits() const;

	UFUNCTION(BlueprintCallable, Category = "PlayerState|Credits")
	bool HasCredits(int32 CheckCredits) const;

	UFUNCTION(BlueprintCallable, Category = "PlayerState|Credits")
	bool CanAddFirstAidKits(int32 Delta, int32& Remaining);

	UFUNCTION(BlueprintCallable, Category = "PlayerState|Credits")
	bool AddFirstAidKits(int32 Delta = 1);

	UFUNCTION(BlueprintCallable, Category = "PlayerState|Credits")
	bool RemoveFirstAidKits(int32 Delta = 1);

	UFUNCTION(BlueprintCallable, Category = "PlayerState|Credits")
	int32 GetNumFirstAidKits() const;
	UFUNCTION(BlueprintCallable, Category = "Armor")
	int32 GetCurrentArmorPlates() const;

	bool PickupArmor(int32 Delta = 1);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ReloadArmor_OnServer();
	UFUNCTION(NetMulticast, Reliable)
	void ReloadArmor_Multicast();


public:
	UPROPERTY(BlueprintAssignable, Category="Inventory|Events")
	FInventoryComponentAmmoChanged OnAmmoChanged;
	UPROPERTY(BlueprintAssignable, Category = "Inventory|Events")
	FInventoryComponentCreditsChanged OnCreditsChanged;
	UPROPERTY(BlueprintAssignable, Category = "Inventory|Events")
	FInventoryComponentNumFirstAidKitsChanged OnNumFirstAidKitsChanged;
	UPROPERTY(BlueprintAssignable, Category = "Inventory|Events")
	FInventoryComponentGrenadeChanged OnGrenadeChanged;
	UPROPERTY(BlueprintAssignable, Category="Combat|Events")
	FOnArmorPlatesChanged OnArmorPlatesChanged;

	TObjectPtr<ACPP_SkillBoxCharacter> OwnerCharacter;

protected:
	UPROPERTY(EditDefaultsOnly, Category="Settings")
	TMap<EAmmoType, int32> AmmoMap;

	UPROPERTY(EditDefaultsOnly, Category="Settings")
	TMap<EGrenadeSubType, int32> GrenadeMap;

	UPROPERTY(EditDefaultsOnly, Category="Settings")
	int32 MaxCarriedAmmo = 500;

	UPROPERTY(EditDefaultsOnly, Category="Settings")
	int32 MaxCarriedGrenade = 6;

	UPROPERTY(EditDefaultsOnly, Category="Settings")
	int32 MaxFirstAidKits = 3;

	UPROPERTY(EditDefaultsOnly, ReplicatedUsing="OnRep_Credits", Category="Inventory")
	int32 Credits = 0;

	UPROPERTY(ReplicatedUsing="OnRep_CurrentArmorPlates")
	int32 CurrentArmorPlates = 0;

	UPROPERTY(EditDefaultsOnly, Category="Settings")
	int32 MaxArmorPlates = 3;

	UFUNCTION()
	void OnRep_Credits(int32 OldCredits);

	UPROPERTY(ReplicatedUsing="OnRep_NumFirstAidKits")
	int32 NumFirstAidKits = 0;

	UFUNCTION()
	void OnRep_NumFirstAidKits();

	UFUNCTION()
	void OnRep_CurrentArmorPlates();
};

FORCEINLINE_DEBUGGABLE int32 UInventoryComponent::GetAmmo(const EAmmoType Type) const
{
	return AmmoMap.Contains(Type) ? AmmoMap[Type] : 0;
}

FORCEINLINE_DEBUGGABLE int32 UInventoryComponent::GetGrenade(const EGrenadeSubType Type) const
{
	return GrenadeMap.Contains(Type) ? GrenadeMap[Type] : 0;
}

FORCEINLINE_DEBUGGABLE int32 UInventoryComponent::GetCredits() const
{
	return Credits;
}

FORCEINLINE_DEBUGGABLE int32 UInventoryComponent::GetNumFirstAidKits() const
{
	return NumFirstAidKits;
}

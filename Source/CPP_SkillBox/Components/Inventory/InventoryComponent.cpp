﻿
#include "InventoryComponent.h"

#include "CPP_SkillBoxCharacter.h"
#include "SAttributeComponent.h"
#include "Net/UnrealNetwork.h"

UInventoryComponent::UInventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	bAutoActivate = true;

	SetIsReplicatedByDefault(true);

	AmmoMap.Emplace(EAmmoType::EAT_5_45, 60);
	AmmoMap.Emplace(EAmmoType::EAT_5_56, 60);
	AmmoMap.Emplace(EAmmoType::EAT_7_62, 60);
	AmmoMap.Emplace(EAmmoType::EAT_9, 100);
	AmmoMap.Emplace(EAmmoType::EAT_12, 21);
	AmmoMap.Emplace(EAmmoType::EAT_45, 100);
	AmmoMap.Emplace(EAmmoType::EAT_300, 15);
	AmmoMap.Emplace(EAmmoType::EAT_GrenadeLauncher, 10);
	AmmoMap.Emplace(EAmmoType::EAT_RPG, 0);
}

void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;
	Params.RepNotifyCondition = REPNOTIFY_Always;
	DOREPLIFETIME_WITH_PARAMS(UInventoryComponent, Credits, Params);
	DOREPLIFETIME_WITH_PARAMS(UInventoryComponent, NumFirstAidKits, Params);
	DOREPLIFETIME(UInventoryComponent, CurrentArmorPlates);
}

void UInventoryComponent::AddAmmo(const EAmmoType Type, int32 Delta)
{
	const AActor* Owner = GetOwner();
	if (!Owner || !Owner->HasAuthority())
	{
		return;
	}

	if (AmmoMap.Contains(Type))
	{
		AmmoMap[Type] = FMath::Clamp(AmmoMap[Type] + Delta, 0, MaxCarriedAmmo);

		OnAmmoChanged.Broadcast(this, Type, AmmoMap[Type]);
	}
}

void UInventoryComponent::AddGrenade(const EGrenadeSubType Type, int32 Delta)
{
	const AActor* Owner = GetOwner();
	if (!Owner || !Owner->HasAuthority())
	{
		return;
	}

	if (GrenadeMap.Contains(Type))
	{
		GrenadeMap[Type] = FMath::Clamp(GrenadeMap[Type] + Delta, 0, MaxCarriedGrenade);

		OnGrenadeChanged.Broadcast(this, Type, GrenadeMap[Type]);
	}
}

void UInventoryComponent::AddCredits(int32 Delta)
{
	if (!ensure(Delta > 0.0f))
	{
		return;
	}

	const int32 OldCredits = Credits;
	Credits += Delta;
	OnRep_Credits(OldCredits);
	MARK_PROPERTY_DIRTY_FROM_NAME(UInventoryComponent, Credits, this);
}


bool UInventoryComponent::RemoveCredits(int32 Delta)
{
	if (!ensure(Delta > 0.0f))
	{
		return false;
	}

	if (Credits < Delta)
	{
		return false;
	}

	const int32 OldCredits = Credits;
	Credits -= Delta;
	OnRep_Credits(OldCredits);
	MARK_PROPERTY_DIRTY_FROM_NAME(UInventoryComponent, Credits, this);

	return true;
}

bool UInventoryComponent::HasCredits(int32 CheckCredits) const
{
	return Credits >= CheckCredits;
}

bool UInventoryComponent::CanAddFirstAidKits(int32 Delta, int32& Remaining)
{
	if (Delta > 0 && MaxFirstAidKits > 0)
	{
		Remaining = FMath::Max((Delta + NumFirstAidKits) - MaxFirstAidKits, 0);

		return NumFirstAidKits < MaxFirstAidKits;
	}

	Remaining = Delta;
	return false;
}

bool UInventoryComponent::AddFirstAidKits(int32 Delta)
{
	if (Delta > 0)
	{
		NumFirstAidKits = FMath::Clamp(NumFirstAidKits + Delta, 0, MaxFirstAidKits);
		MARK_PROPERTY_DIRTY_FROM_NAME(UInventoryComponent, NumFirstAidKits, this);
		OnRep_NumFirstAidKits();

		return true;
	}
	return false;
}

bool UInventoryComponent::RemoveFirstAidKits(int32 Delta)
{
	if (Delta > 0)
	{
		NumFirstAidKits = FMath::Clamp(NumFirstAidKits - Delta, 0, MaxFirstAidKits);
		MARK_PROPERTY_DIRTY_FROM_NAME(UInventoryComponent, NumFirstAidKits, this);
		OnRep_NumFirstAidKits();

		return true;
	}
	return false;
}

void UInventoryComponent::OnRep_Credits(int32 OldCredits)
{
	OnCreditsChanged.Broadcast(this, Credits, Credits - OldCredits);
}

void UInventoryComponent::OnRep_NumFirstAidKits()
{
	OnNumFirstAidKitsChanged.Broadcast(this, NumFirstAidKits);
}

void UInventoryComponent::OnRep_CurrentArmorPlates()
{
	OnArmorPlatesChanged.Broadcast(CurrentArmorPlates);
}

int32 UInventoryComponent::GetCurrentArmorPlates() const
{
	return CurrentArmorPlates;
}

bool UInventoryComponent::PickupArmor(int32 Delta)
{
	if (CurrentArmorPlates < MaxArmorPlates)
	{
		CurrentArmorPlates = FMath::Clamp(CurrentArmorPlates + Delta, 0, MaxArmorPlates);
		OnArmorPlatesChanged.Broadcast(CurrentArmorPlates);
		return true;
	}
	return false;
}

void UInventoryComponent::ReloadArmor_OnServer_Implementation(){ReloadArmor_Multicast();}
void UInventoryComponent::ReloadArmor_Multicast_Implementation()
{
	OwnerCharacter = Cast<ACPP_SkillBoxCharacter>(GetOwner()); 
	if (OwnerCharacter && CurrentArmorPlates > 0)
	{
		if (OwnerCharacter->GetAttributeComp()->InstallNewShieldPlate(25.0f))
		{
			CurrentArmorPlates--;
			OnArmorPlatesChanged.Broadcast(CurrentArmorPlates);
		}
	}
}


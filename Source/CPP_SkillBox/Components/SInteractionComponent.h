// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <Components/ActorComponent.h>

#include "SInteractionComponent.generated.h"

class USWorldUserWidget;
class ACPP_SkillBoxCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CPP_SKILLBOX_API USInteractionComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	USInteractionComponent(const FObjectInitializer& ObjectInitializer);

	void PrimaryInteract();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerPrimaryInteract();

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	void FindBestInteractable();

	void FindBestInteractableActor();

	UFUNCTION()
	void OnCharacterPossessed(ACPP_SkillBoxCharacter* Character);

	UFUNCTION()
	void OnCharacterUnPossessed(ACPP_SkillBoxCharacter* Character);

	UPROPERTY(EditDefaultsOnly, Category= "Settings")
	float TraceDistance = 200.0f;

	UPROPERTY(EditDefaultsOnly, Category= "Settings")
	float TraceRadius = 30.0f;

	UPROPERTY(EditDefaultsOnly, Category= "Settings")
	TEnumAsByte<ECollisionChannel> CollisionChannel = ECC_WorldDynamic;

	UPROPERTY(EditDefaultsOnly, Category= "Settings")
	TSubclassOf<USWorldUserWidget> DefaultWidgetClass;

	UPROPERTY(Transient)
	TObjectPtr<AActor> FocusActor = nullptr;

	UPROPERTY(Transient)
	TObjectPtr<USWorldUserWidget> DefaultWidgetInstance = nullptr;

	UPROPERTY(Transient)
	TObjectPtr<ACPP_SkillBoxCharacter> OwnerCharacter = nullptr;
};

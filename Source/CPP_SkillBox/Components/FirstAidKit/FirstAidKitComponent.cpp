// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/FirstAidKit/FirstAidKitComponent.h"

#include <Net/UnrealNetwork.h>
#include <GameFramework/GameStateBase.h>

#include "CombatComponent.h"
#include "CPP_SkillBoxCharacter.h"
#include "SAttributeComponent.h"
#include "Inventory/InventoryComponent.h"

UFirstAidKitComponent::UFirstAidKitComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	bAutoActivate = true;
	SetIsReplicatedByDefault(true);
}

void UFirstAidKitComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;
	Params.RepNotifyCondition = REPNOTIFY_Always;
	DOREPLIFETIME_WITH_PARAMS(UFirstAidKitComponent, LastPackedFirstAidKitParams, Params);
}


void UFirstAidKitComponent::BeginPlay()
{
	Super::BeginPlay();
	OwnerCharacter = GetOwner<ACPP_SkillBoxCharacter>();
	if (OwnerCharacter)
	{
		OwnerInventory = OwnerCharacter->GetInventoryComponent();
		OwnerCombatComponent = OwnerCharacter->GetCombat();
		OwnerAttributeComponent = OwnerCharacter->GetAttributeComp();
	}
}

void UFirstAidKitComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (IsUseFirstAidKitInProgress())
	{
		bool Canceled = false;
		if (!LastFirstAidKitParams.Target)
		{
			Canceled = true;
			CancelUseFirstAidKit();
		}

		if (OwnerCharacter)
		{
			const float DistanceToTargetSq = OwnerCharacter->GetSquaredDistanceTo(LastFirstAidKitParams.Target);
			if (DistanceToTargetSq > MaxDistance * MaxDistance)
			{
				Canceled = true;
				CancelUseFirstAidKit();
			}
		}

		if (!Canceled)
		{
			const USAttributeComponent* AttributeComponent = LastFirstAidKitParams.Target->GetAttributeComp();
			if (AttributeComponent->IsFullHealth())
			{
				Canceled = true;
				CancelUseFirstAidKit();
			}
		}

		if (!Canceled)
		{
			RemainingTime -= DeltaTime;
			if (RemainingTime <= 0.0f)
			{
				HealTarget();
				EndUseFirstAidKit();
			}
		}
	}
}

void UFirstAidKitComponent::UseFirstAidKit()
{
	if (OwnerCharacter)
	{
		//TODO: added heal teammate
		FFirstAidKitParams FirstAidKitParams;
		FirstAidKitParams.Instigator = OwnerCharacter;
		FirstAidKitParams.Target = OwnerCharacter;
		StartUseFirstAidKit(FirstAidKitParams);
	}
}

bool UFirstAidKitComponent::CanStartUseFirstAidKit_Implementation(const FFirstAidKitParams& FirstAidKitParams)
{
	if (!OwnerCharacter)
	{
		return false;
	}

	if (!FirstAidKitParams.Target)
	{
		return false;
	}

	if (IsUseFirstAidKitInProgress())
	{
		return false;
	}

	if (!OwnerCharacter->HasLocalNetOwner())
	{
		return false;
	}

	if (OwnerCharacter != FirstAidKitParams.Instigator)
	{
		return false;
	}

	if (OwnerCharacter->GetDistanceTo(FirstAidKitParams.Target) > MaxDistance)
	{
		return false;
	}

	const USAttributeComponent* AttributeComponent = FirstAidKitParams.Target->GetAttributeComp();
	if (!AttributeComponent || AttributeComponent->IsFullHealth())
	{
		return false;
	}

	if (!OwnerCombatComponent || OwnerCombatComponent->GetCombatState() != ECombatState::ECS_Unoccupied)
	{
		return false;
	}

	if (!OwnerInventory || OwnerInventory->GetNumFirstAidKits() <= 0)
	{
		return false;
	}

	return IsActive();
}

bool UFirstAidKitComponent::StartUseFirstAidKit(const FFirstAidKitParams& FirstAidKitParams)
{
	if (!OwnerCharacter)
	{
		return false;
	}

	const bool bHasAuthority = OwnerCharacter->HasAuthority();
	const bool bHasLocalNetOwner = OwnerCharacter->HasLocalNetOwner();
	if (bHasAuthority && bHasLocalNetOwner)
	{
		if (CanStartUseFirstAidKit(FirstAidKitParams))
		{
			LastPackedFirstAidKitParams = FPackedFirstAidKitParams::PackFirstAidKitParams(FirstAidKitParams, GetTimestamp());
			ServerStartUseFirstAidKit(LastPackedFirstAidKitParams);
		}
	}
	else if (bHasLocalNetOwner)
	{
		if (CanStartUseFirstAidKit(FirstAidKitParams))
		{
			LastPackedFirstAidKitParams = FPackedFirstAidKitParams::PackFirstAidKitParams(FirstAidKitParams, GetTimestamp());
			OnRep_LastPackedFirstAidKitParams();
			ServerStartUseFirstAidKit(LastPackedFirstAidKitParams);
		}
	}
	else if (bHasAuthority)
	{
		if (ValidateCanStartUseFirstAidKit(FirstAidKitParams))
		{
			LastPackedFirstAidKitParams = FPackedFirstAidKitParams::PackFirstAidKitParams(FirstAidKitParams, GetTimestamp());
			ServerStartUseFirstAidKit(LastPackedFirstAidKitParams);
		}
	}

	return false;
}

void UFirstAidKitComponent::CancelUseFirstAidKit()
{
	if (OwnerCharacter && OwnerCharacter->HasAuthority())
	{
		LastPackedFirstAidKitParams.UseFirstAidKitCanceled = true;
		LastPackedFirstAidKitParams.UseFirstAidKitInProgress = false;
		OnRep_LastPackedFirstAidKitParams();
		MARK_PROPERTY_DIRTY_FROM_NAME(UFirstAidKitComponent, LastPackedFirstAidKitParams, this);
	}
}

void UFirstAidKitComponent::EndUseFirstAidKit()
{
	if (OwnerCharacter && OwnerCharacter->HasAuthority())
	{
		LastPackedFirstAidKitParams.UseFirstAidKitCanceled = false;
		LastPackedFirstAidKitParams.UseFirstAidKitInProgress = false;
		OnRep_LastPackedFirstAidKitParams();
		MARK_PROPERTY_DIRTY_FROM_NAME(UFirstAidKitComponent, LastPackedFirstAidKitParams, this);
	}
}

float UFirstAidKitComponent::GetTimestamp() const
{
	if (const UWorld* World = GetWorld())
	{
		if (World->GetGameState())
		{
			return World->GetGameState()->GetServerWorldTimeSeconds();;
		}
	}

	return 0.0f;
}

bool UFirstAidKitComponent::ValidateTimestamp(float Timestamp)
{
	const float CurrentTimestamp = GetTimestamp();
	return (CurrentTimestamp - Timestamp) <= MaxDiffActivation;
}

bool UFirstAidKitComponent::ValidateCanStartUseFirstAidKit(const FFirstAidKitParams& FirstAidKitParams)
{
	if (IsUseFirstAidKitInProgress())
	{
		return false;
	}

	if (OwnerCharacter != FirstAidKitParams.Instigator)
	{
		return false;
	}

	if (OwnerCharacter->GetDistanceTo(FirstAidKitParams.Target) > MaxDistance)
	{
		return false;
	}

	const USAttributeComponent* AttributeComponent = FirstAidKitParams.Target->GetAttributeComp();
	if (!AttributeComponent || AttributeComponent->IsFullHealth())
	{
		return false;
	}

	if (!OwnerCombatComponent || OwnerCombatComponent->GetCombatState() != ECombatState::ECS_Unoccupied)
	{
		return false;
	}

	if (!OwnerInventory || OwnerInventory->GetNumFirstAidKits() <= 0)
	{
		return false;
	}

	return true;
}

void UFirstAidKitComponent::ServerStartUseFirstAidKit_Implementation(const FPackedFirstAidKitParams& PackedFirstAidKitParams)
{
	FFirstAidKitParams FirstAidKitParams = FPackedFirstAidKitParams::UnPackFirstAidKitParams(PackedFirstAidKitParams);
	if (ValidateTimestamp(PackedFirstAidKitParams.Timestamp) && ValidateCanStartUseFirstAidKit(FirstAidKitParams))
	{
		LastPackedFirstAidKitParams = PackedFirstAidKitParams;
		OnRep_LastPackedFirstAidKitParams();
		MARK_PROPERTY_DIRTY_FROM_NAME(UFirstAidKitComponent, LastPackedFirstAidKitParams, this);
	}
	else
	{
		//TODO: added abort
	}
}

bool UFirstAidKitComponent::ServerStartUseFirstAidKit_Validate(const FPackedFirstAidKitParams& PackedFirstAidKitParams)
{
	return true;
}

void UFirstAidKitComponent::DoStartUseFirstAidKit(const FFirstAidKitParams& FirstAidKitParams)
{
	UseFirstAidKitInProgress = true;
	LastFirstAidKitParams = FirstAidKitParams;

	InternalStartUseFirstAidKit(FirstAidKitParams);
	OnStartedUseFirstAidKit(FirstAidKitParams);
	OnStartedUseFirstAidKitEvent.Broadcast(this, FirstAidKitParams);
}

void UFirstAidKitComponent::InternalStartUseFirstAidKit_Implementation(const FFirstAidKitParams& FirstAidKitParams)
{
	if (OwnerCombatComponent)
	{
		OwnerCombatComponent->SetCombatState(ECombatState::ECS_UseFirstAidKit);
	}

	RemainingTime = UseTime;
}

void UFirstAidKitComponent::OnStartedUseFirstAidKit_Implementation(const FFirstAidKitParams& FirstAidKitParams)
{
}

void UFirstAidKitComponent::DoCancelUseFirstAidKit()
{
	UseFirstAidKitInProgress = false;

	InternalCancelUseFirstAidKit();
	OnCanceledUseFirstAidKit();
	OnCanceledUseFirstAidKitEvent.Broadcast(this);
}

void UFirstAidKitComponent::InternalCancelUseFirstAidKit_Implementation()
{
	if (OwnerCombatComponent && OwnerCombatComponent->GetCombatState() == ECombatState::ECS_UseFirstAidKit)
	{
		OwnerCombatComponent->SetCombatState(ECombatState::ECS_Unoccupied);
	}
}

void UFirstAidKitComponent::OnCanceledUseFirstAidKit_Implementation()
{
}

void UFirstAidKitComponent::DoEndUseFirstAidKit()
{
	UseFirstAidKitInProgress = false;

	InternalEndUseFirstAidKit();
	OnEndedUseFirstAidKit();
	OnEndedUseFirstAidKitEvent.Broadcast(this);
}

void UFirstAidKitComponent::InternalEndUseFirstAidKit_Implementation()
{
	if (OwnerCombatComponent && OwnerCombatComponent->GetCombatState() == ECombatState::ECS_UseFirstAidKit)
	{
		OwnerCombatComponent->SetCombatState(ECombatState::ECS_Unoccupied);
	}
}

void UFirstAidKitComponent::OnEndedUseFirstAidKit_Implementation()
{
}

void UFirstAidKitComponent::HealTarget()
{
	const TObjectPtr<ACPP_SkillBoxCharacter> Character = LastFirstAidKitParams.Target;
	if (Character && Character->HasAuthority() && OwnerInventory && OwnerInventory->GetNumFirstAidKits() > 0)
	{
		if (USAttributeComponent* AttributeComponent = Character->GetAttributeComp())
		{
			AttributeComponent->ApplyHealthChange(OwnerCharacter, Health);

			OwnerInventory->RemoveFirstAidKits();
		}
	}
}

void UFirstAidKitComponent::OnRep_LastPackedFirstAidKitParams()
{
	if (UseFirstAidKitInProgress != LastPackedFirstAidKitParams.UseFirstAidKitInProgress)
	{
		FFirstAidKitParams FirstAidKitParams = FPackedFirstAidKitParams::UnPackFirstAidKitParams(LastPackedFirstAidKitParams);
		if (LastPackedFirstAidKitParams.UseFirstAidKitInProgress)
		{
			DoStartUseFirstAidKit(FirstAidKitParams);
		}
		else if (LastPackedFirstAidKitParams.UseFirstAidKitCanceled)
		{
			DoCancelUseFirstAidKit();
		}
		else
		{
			DoEndUseFirstAidKit();
		}
	}
}

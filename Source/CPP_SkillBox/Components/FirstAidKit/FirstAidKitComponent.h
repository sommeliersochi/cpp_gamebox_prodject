// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FirstAidKitDefinitions.h"
#include "Components/ActorComponent.h"
#include "FirstAidKitComponent.generated.h"


class UInventoryComponent;
class UCombatComponent;
class USAttributeComponent;
class ACPP_SkillBoxCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FFirstAidKitComponentStartUseFirstAidKitSignature,  UFirstAidKitComponent*, FirstAidKitComponent, const FFirstAidKitParams&, FirstAidKitParams);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFirstAidKitComponentEndUseFirstAidKitSignature,  UFirstAidKitComponent*, FirstAidKitComponent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFirstAidKitComponentCancelUseFirstAidKitSignature,  UFirstAidKitComponent*, FirstAidKitComponent);

UCLASS(ClassGroup=(FirstAidKit), meta=(BlueprintSpawnableComponent))
class CPP_SKILLBOX_API UFirstAidKitComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UFirstAidKitComponent();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void UseFirstAidKit();

	UFUNCTION(BlueprintNativeEvent, Category="FirstAidKit")
	bool CanStartUseFirstAidKit(const FFirstAidKitParams& FirstAidKitParams);

	bool StartUseFirstAidKit(const FFirstAidKitParams& FirstAidKitParams);

	void CancelUseFirstAidKit();

	void EndUseFirstAidKit();

	bool IsUseFirstAidKitInProgress() const;

protected:
	float GetTimestamp() const;
	bool ValidateTimestamp(float Timestamp);

	bool ValidateCanStartUseFirstAidKit(const FFirstAidKitParams& FirstAidKitParams);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerStartUseFirstAidKit(const FPackedFirstAidKitParams& FirstAidKitParams);

	void DoStartUseFirstAidKit(const FFirstAidKitParams& FirstAidKitParams);

	UFUNCTION(BlueprintNativeEvent, Category="FirstAidKit")
	void InternalStartUseFirstAidKit(const FFirstAidKitParams& FirstAidKitParams);

	UFUNCTION(BlueprintNativeEvent, Category="FirstAidKit")
	void OnStartedUseFirstAidKit(const FFirstAidKitParams& FirstAidKitParams);

	void DoCancelUseFirstAidKit();

	UFUNCTION(BlueprintNativeEvent, Category="FirstAidKit")
	void InternalCancelUseFirstAidKit();

	UFUNCTION(BlueprintNativeEvent, Category="FirstAidKit")
	void OnCanceledUseFirstAidKit();

	void DoEndUseFirstAidKit();

	UFUNCTION(BlueprintNativeEvent, Category="FirstAidKit")
	void InternalEndUseFirstAidKit();

	UFUNCTION(BlueprintNativeEvent, Category="FirstAidKit")
	void OnEndedUseFirstAidKit();

	void HealTarget();

public:
	UPROPERTY(BlueprintAssignable, Category="FirstAidKit|Events")
	FFirstAidKitComponentStartUseFirstAidKitSignature OnStartedUseFirstAidKitEvent;
	UPROPERTY(BlueprintAssignable, Category="FirstAidKit|Events")
	FFirstAidKitComponentEndUseFirstAidKitSignature OnEndedUseFirstAidKitEvent;
	UPROPERTY(BlueprintAssignable, Category="FirstAidKit|Events")
	FFirstAidKitComponentCancelUseFirstAidKitSignature OnCanceledUseFirstAidKitEvent;
protected:
	TObjectPtr<ACPP_SkillBoxCharacter> OwnerCharacter;
	TObjectPtr<UInventoryComponent> OwnerInventory;
	TObjectPtr<UCombatComponent> OwnerCombatComponent;
	TObjectPtr<USAttributeComponent> OwnerAttributeComponent;

	bool UseFirstAidKitInProgress = false;

	UPROPERTY(ReplicatedUsing=OnRep_LastPackedFirstAidKitParams)
	FPackedFirstAidKitParams LastPackedFirstAidKitParams;

	UFUNCTION()
	void OnRep_LastPackedFirstAidKitParams();

	FFirstAidKitParams LastFirstAidKitParams;

	UPROPERTY(EditDefaultsOnly, Category="Settings")
	float UseTime = 1.0f;

	UPROPERTY(EditDefaultsOnly, Category="Settings")
	float MaxDistance = 200.0f;

	UPROPERTY(EditDefaultsOnly, Category="Settings")
	float Health = 100.0f;

	UPROPERTY(EditDefaultsOnly, Category="Settings")
	float MaxDiffActivation = 1.0f;

	float RemainingTime = 0.0f;
};

FORCEINLINE_DEBUGGABLE bool UFirstAidKitComponent::IsUseFirstAidKitInProgress() const
{
	return UseFirstAidKitInProgress;
}
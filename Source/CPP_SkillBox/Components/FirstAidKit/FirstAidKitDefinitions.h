// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "FirstAidKitDefinitions.generated.h"

class ACPP_SkillBoxCharacter;

USTRUCT(BlueprintType)
struct FFirstAidKitParams
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, Category="FirstAidKit")
	TObjectPtr<ACPP_SkillBoxCharacter> Target = nullptr;

	UPROPERTY(BlueprintReadWrite, Category="FirstAidKit")
	TObjectPtr<ACPP_SkillBoxCharacter> Instigator = nullptr;
};

USTRUCT()
struct FPackedFirstAidKitParams
{
	GENERATED_BODY()

	UPROPERTY()
	TObjectPtr<ACPP_SkillBoxCharacter> Target = nullptr;

	UPROPERTY()
	TObjectPtr<ACPP_SkillBoxCharacter> Instigator = nullptr;

	UPROPERTY()
	float Timestamp = 0.0f;

	UPROPERTY()
	bool UseFirstAidKitInProgress = true;

	UPROPERTY()
	bool UseFirstAidKitCanceled = false;

	static FPackedFirstAidKitParams PackFirstAidKitParams(FFirstAidKitParams FirstAidKitParams, float UseFirstAidKitTimestamp)
	{
		FPackedFirstAidKitParams Result;
		Result.Target = FirstAidKitParams.Target;
		Result.Instigator = FirstAidKitParams.Instigator;
		Result.Timestamp = UseFirstAidKitTimestamp;
		Result.UseFirstAidKitInProgress = true;
		Result.UseFirstAidKitCanceled = false;

		return Result;
	}

	static FFirstAidKitParams UnPackFirstAidKitParams(FPackedFirstAidKitParams FirstAidKitParams)
	{
		FFirstAidKitParams Result;
		Result.Target = FirstAidKitParams.Target;
		Result.Instigator = FirstAidKitParams.Instigator;

		return Result;
	}
};

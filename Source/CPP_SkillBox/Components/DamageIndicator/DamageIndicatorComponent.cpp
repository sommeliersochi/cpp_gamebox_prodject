#include "DamageIndicatorComponent.h"

UDamageIndicatorComponent::UDamageIndicatorComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UDamageIndicatorComponent::BeginPlay()
{
	Super::BeginPlay();

	if (AActor* Owner = GetOwner())
	{
		Owner->OnTakeAnyDamage.RemoveDynamic(this, &UDamageIndicatorComponent::OnOwnerTakeDamage);
		Owner->OnTakeAnyDamage.AddDynamic(this, &UDamageIndicatorComponent::OnOwnerTakeDamage);
	}
}

void UDamageIndicatorComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (AActor* Owner = GetOwner())
	{
		Owner->OnTakeAnyDamage.RemoveDynamic(this, &UDamageIndicatorComponent::OnOwnerTakeDamage);
	}
}

void UDamageIndicatorComponent::OnOwnerTakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	APawn* Instigator = InstigatedBy ? InstigatedBy->GetPawn() : nullptr;
	ClientDamageIndicator(DamagedActor, Instigator, DamageCauser);
}

void UDamageIndicatorComponent::ClientDamageIndicator_Implementation(AActor* DamagedActor, APawn* Instigator, AActor* DamageCauser)
{
	OnDamageIndicatorEvent.Broadcast(this, DamagedActor, Instigator, DamageCauser);
}

#pragma once

#include <CoreMinimal.h>
#include <Components/ActorComponent.h>

#include "DamageIndicatorComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnDamageIndicatorSimpleSignature, UDamageIndicatorComponent*, DamageIndicatorComponent, AActor*, DamagedActor, APawn*, Instigator, AActor*, DamageCauser);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CPP_SKILLBOX_API UDamageIndicatorComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UDamageIndicatorComponent();

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	UFUNCTION()
	void OnOwnerTakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION(Client, Reliable)
	void ClientDamageIndicator(AActor* DamagedActor, APawn* Instigator, AActor* DamageCauser);

public:
	UPROPERTY(BlueprintAssignable)
	FOnDamageIndicatorSimpleSignature OnDamageIndicatorEvent;
};

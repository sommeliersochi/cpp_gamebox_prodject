#pragma once

#include <CoreMinimal.h>
#include <Components/ActorComponent.h>

#include "CombatState.h"
#include "CPP_SkillBoxCharacter.h"
#include "PlayerHUD.h"
#include "ProjectileGrenade.h"
#include "WeaponTypes.h"
#include "CombatComponent.generated.h"

class UFindTargetComponent;
class UInventoryComponent;
class APlayerHUD;
class ACPP_SkillBoxPlayerController;
class ACPP_SkillBoxCharacter;
class ASAICharacterBase;
class AWeapon;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCombatComponentSimpleSignature, UCombatComponent*, CombatComponent);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCombatComponentChangedAmmo, UCombatComponent*, CombatComponent, int32, NewValue);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnFoundGrenades, FGrenadeInfo, GrenadeInfo);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnGrenadeChanges, int32, GrenadeIndex, int32, Count, UTexture2D*, ItemIcon, FText, ItemName);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnActiveSlotChange, int32, CurrentGrenadeSlot);

UCLASS(Blueprintable, ClassGroup=(Combat), meta=(BlueprintSpawnableComponent))
class CPP_SKILLBOX_API UCombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UCombatComponent();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void InitializeComponent() override;
	virtual void UninitializeComponent() override;
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void EquipWeapon(AWeapon* WeaponToEquip);
	
	void AttachActorToRightHand(AActor* ActorToAttach);
	void AttachActorToArmory(AActor* ActorToAttach);
	
	void Reload();

	UFUNCTION(BlueprintCallable)
	void FinishReloading();

	UFUNCTION(BlueprintCallable)
	void FinishSwap();

	UFUNCTION(BlueprintCallable)
	void FinishSwapAttachWeapon();

	void SwapWeapon();
	void EquipPrimaryWeapon(AWeapon* WeaponToEquip);
	void EquipSecondaryWeapon(AWeapon* WeaponToEquip);

	UFUNCTION()
	void OnRep_SecondaryWeapon();

	bool ShouldSwapWeapons() const;

	FVector GetMuzzleWorldLocation() const;

	void Fire();
	
	UFUNCTION(BlueprintCallable)
	void FireButtonPressed(bool bPressed);

	UFUNCTION(BlueprintCallable)
	void ShotgunShellReload();

	void JumpToShotgunEnd();

	UFUNCTION(BlueprintCallable)
	void ApplyKnifeAttack();

	void PickupGrenade(FGrenadeInfo GrenadeInfo);
	
	UFUNCTION(BlueprintCallable)
	AWeapon* GetEquippedWeapon() const;

	AWeapon* GetLastEquippedWeapon() const;

	int32 GetCarriedAmmo() const;

	ECombatState GetCombatState() const;

	ASAICharacterBase* GetTargetActor() const;

	int32 GetActiveGrenadeSlot() const;

	void SetCombatState(ECombatState NewCombatState);

	void SetCanFire(bool bNewCanFire);

	bool GetTraceData(FVector& TraceStart, FVector& TraceEnd, float TraceDistance, bool IsPrint) const;
	
	UFUNCTION(BlueprintCallable)
	void SetAiming(bool bIsAiming);

	bool IsAiming() const;
	
	UFUNCTION(BlueprintCallable)
	FVector GetHitTarget() const;
	UFUNCTION(BlueprintCallable)
	void SetHitTarget(FVector Target);

	void SetActiveGrenadeSlot(int32 NewActiveGrenadeSlot);

	int32 FindIndexGrenadeByType(const EGrenadeType GrenadeType);

	void SelectGrenadeSlot(int32 ActiveGrenade);

	void ThrowGrenade(int32 SelectedGrenade);

	void KnifeAttack();
	
	bool CanFire();
	
	virtual bool CanAutoFire();

protected:
	void DropEquippedWeapon();

	void AttachActorToLeftHand(AActor* ActorToAttach);
	void PlayEquipWeaponSound(AWeapon* WeapoToEquip);
	void ReloadEmptyWeapon();
	void ShowAttachedKnife(bool bShowKnife);
	void ShowAttachedGrenade(bool bShowGrenade);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	TObjectPtr<UAnimMontage> AttackAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	TObjectPtr<UAnimMontage> IronsightsAttackAnim;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category= "Weapone")
	FName MuzzleSocketName = "MuzzleSocket";

	UFUNCTION(Server, Reliable)
	void ServerFire(const FVector_NetQuantize& TraceHitTarget);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastFire(const FVector_NetQuantize& TraceHitTarget);

	UFUNCTION(Server, Reliable)
	void ServerSetAiming(bool bIsAiming);

	//Crosshire===============
	void ChangeHitTarget();

	void PlayerChangeHitTarget();
	void AIChangeHitTarget();

	void TraceUnderCrosshairs(FHitResult& TraceHitResult);

	void SetHUDCrosshairs(float DeltaTime);

	UFUNCTION(Server, Reliable)
	void ServerReload();

	int32 AmountToReload();

	//======================================================Grenades
	UPROPERTY(BlueprintReadWrite, Category="Combat|Grenade")
	TArray<FGrenadeInfo> GrenadeList;
	UPROPERTY(BlueprintAssignable, Category="Combat|Grenade|Events")
	FOnFoundGrenades OnFoundGrenades;
	UPROPERTY(BlueprintAssignable, Category="Combat|Grenade|Events")
	FOnGrenadeChanges OnGrenadeChanges;
	UPROPERTY(BlueprintAssignable, Category="Combat|Grenade|Events")
	FOnActiveSlotChange OnActiveSlotChange;
	void AddNewGrenade(int32 Slot, FGrenadeInfo GrenadeInfo);
	void DecreaseGrenadeCount();
	UFUNCTION(Server, Reliable)
	void ServerThrowGrenade(int32 SelectedGrenade);
	UFUNCTION(BlueprintCallable)
	void ThrowGrenadeFinished();
	UFUNCTION(BlueprintCallable)
	void LaunchGrenade();
	//======================================================

	virtual bool CanDropWeapon() const;

	UFUNCTION()
	void OnCharacterDead(AActor* Owner, AActor* Instigator);
	UFUNCTION()
	void OnCharacterPossessed(ACPP_SkillBoxCharacter* Character);
	UFUNCTION()
	void OnCharacterUnPossessed(ACPP_SkillBoxCharacter* Character);

	UFUNCTION()
	void OnInventoryAmmoChanged(UInventoryComponent* InventoryComponent, EAmmoType Type, int32 NewValue);

private:
	void UpdateCarriedAmmo();
	void UpdateAmmoAfterReloading();
	void UpdateAmmoReloadShotgun();

	void StartFireTimer();
	void FireTimerFinished();

	UFUNCTION()
	void OnChangedWeaponAmmo(AWeapon* Weapon, int32 NewValue);

	void TryActivateWeaponLaser();

public:
	UPROPERTY(BlueprintAssignable, Category="Combat|Events")
	FCombatComponentSimpleSignature OnChangeEquippedWeapon;
	UPROPERTY(BlueprintAssignable, Category="Combat|Events")
	FCombatComponentChangedAmmo OnChangedCarriedAmmoEvent;
	UPROPERTY(BlueprintAssignable, Category="Combat|Events")
	FCombatComponentChangedAmmo OnChangedWeaponAmmoEvent;
	
	UPROPERTY(EditAnywhere, Category="Settings")
	int32 StartingKnife = -1;
	
	bool bIsImpactPawnTrace; //Crosshire

	UPROPERTY(ReplicatedUsing= OnRep_EquippedWeapon)
	TObjectPtr<AWeapon> EquippedWeapon = nullptr;

	UFUNCTION()
	void OnRep_EquippedWeapon(AWeapon* OldWeapon);

	TObjectPtr<AWeapon> LastEquippedWeapon = nullptr;
	
	UPROPERTY(BlueprintReadOnly)
	bool bFireWeaponPressed;

private:
	TObjectPtr<ACPP_SkillBoxCharacter> OwnerCharacter;
	TObjectPtr<ACPP_SkillBoxPlayerController> Controller;
	TObjectPtr<ASAICharacterBase> TargetActor;
	TObjectPtr<APlayerHUD> HUD;
	TObjectPtr<UInventoryComponent> OwnerInventory;
	TObjectPtr<UFindTargetComponent> OwnerFindTargetComponent;

	UPROPERTY(ReplicatedUsing= OnRep_SecondaryWeapon)
	TObjectPtr<AWeapon> SecondaryWeapon;

	float CrosshairVelocityFactor;
	float CrosshairAimFactor;
	float CrosshairShootingFactor;

	FHUDPackage HUDPackage;

	UPROPERTY(ReplicatedUsing=OnRep_bAiming)
	bool bAiming = false;

	UFUNCTION()
	void OnRep_bAiming();

	FVector HitTarget;

	FTimerHandle FireTimerHandle;

	bool bCanFire = true;

	UPROPERTY(ReplicatedUsing=OnRep_CarriedAmmo)
	int32 CarriedAmmo;

	UFUNCTION()
	void OnRep_CarriedAmmo();

	UPROPERTY(ReplicatedUsing = OnRep_CombatState)
	ECombatState CombatState = ECombatState::ECS_Unoccupied;

	UFUNCTION()
	void OnRep_CombatState();

	int32 ActiveGrenadeSlot = INDEX_NONE;
};

FORCEINLINE_DEBUGGABLE AWeapon* UCombatComponent::GetEquippedWeapon() const
{
	return EquippedWeapon;
}

FORCEINLINE_DEBUGGABLE AWeapon* UCombatComponent::GetLastEquippedWeapon() const
{
	return LastEquippedWeapon;
}

FORCEINLINE_DEBUGGABLE int32 UCombatComponent::GetCarriedAmmo() const
{
	return CarriedAmmo;
}

FORCEINLINE_DEBUGGABLE ECombatState UCombatComponent::GetCombatState() const
{
	return CombatState;
}

FORCEINLINE_DEBUGGABLE ASAICharacterBase* UCombatComponent::GetTargetActor() const
{
	return TargetActor;
}

FORCEINLINE_DEBUGGABLE int32 UCombatComponent::GetActiveGrenadeSlot() const
{
	return ActiveGrenadeSlot;
}

FORCEINLINE_DEBUGGABLE bool UCombatComponent::IsAiming() const
{
	return bAiming;
}

FORCEINLINE_DEBUGGABLE FVector UCombatComponent::GetHitTarget() const
{
	return HitTarget;
}

inline void UCombatComponent::SetHitTarget(FVector Target)
{
	HitTarget = Target;
}

#include "CombatComponent.h"

#include <Components/ArrowComponent.h>
#include <Components/CapsuleComponent.h>
#include <Engine/SkeletalMeshSocket.h>
#include <GameFramework/CharacterMovementComponent.h>
#include <GameFramework/ProjectileMovementComponent.h>
#include <Kismet/GameplayStatics.h>
#include <Kismet/KismetMathLibrary.h>
#include <Net/UnrealNetwork.h>
#include <Sound/SoundCue.h>

#include "Weapon.h"
#include "CPP_SkillBoxCharacter.h"
#include "CPP_SkillBoxPlayerController.h"
#include "SAttributeComponent.h"
#include "AI/SAICharacterBase.h"
#include "FindTarget/FindTargetComponent.h"
#include "Inventory/InventoryComponent.h"

UCombatComponent::UCombatComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	bWantsInitializeComponent = true;
}

void UCombatComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UCombatComponent, EquippedWeapon);
	DOREPLIFETIME(UCombatComponent, SecondaryWeapon);
	DOREPLIFETIME(UCombatComponent, bAiming);
	DOREPLIFETIME_CONDITION(UCombatComponent, CarriedAmmo, COND_OwnerOnly);
	DOREPLIFETIME(UCombatComponent, CombatState);
}

void UCombatComponent::InitializeComponent()
{
	Super::InitializeComponent();

	OwnerCharacter = GetOwner<ACPP_SkillBoxCharacter>();
	if (OwnerCharacter)
	{
		Controller = OwnerCharacter->GetController<ACPP_SkillBoxPlayerController>();

		OwnerCharacter->OnPossessedEvent.RemoveDynamic(this, &UCombatComponent::OnCharacterPossessed);
		OwnerCharacter->OnPossessedEvent.AddDynamic(this, &UCombatComponent::OnCharacterPossessed);

		OwnerCharacter->OnUnPossessedEvent.RemoveDynamic(this, &UCombatComponent::OnCharacterUnPossessed);
		OwnerCharacter->OnUnPossessedEvent.AddDynamic(this, &UCombatComponent::OnCharacterUnPossessed);
	}

	if (Controller)
	{
		HUD = Controller->GetHUD<APlayerHUD>();
	}
}

void UCombatComponent::UninitializeComponent()
{
	Super::UninitializeComponent();

	if (OwnerCharacter)
	{
		OwnerCharacter->OnPossessedEvent.RemoveDynamic(this, &UCombatComponent::OnCharacterPossessed);

		OwnerCharacter->OnUnPossessedEvent.RemoveDynamic(this, &UCombatComponent::OnCharacterUnPossessed);
	}
}

void UCombatComponent::BeginPlay()
{
	Super::BeginPlay();

	if (OwnerCharacter)
	{
		if (OwnerCharacter->GetAttributeComp())
		{
			OwnerCharacter->GetAttributeComp()->OnDeath.RemoveDynamic(this, &UCombatComponent::OnCharacterDead);
			OwnerCharacter->GetAttributeComp()->OnDeath.AddDynamic(this, &UCombatComponent::OnCharacterDead);
		}

		OwnerInventory = OwnerCharacter->GetInventoryComponent();

		if (OwnerInventory)
		{
			OwnerInventory->OnAmmoChanged.RemoveDynamic(this, &UCombatComponent::OnInventoryAmmoChanged);
			OwnerInventory->OnAmmoChanged.AddDynamic(this, &UCombatComponent::OnInventoryAmmoChanged);
		}

		OwnerFindTargetComponent = OwnerCharacter->GetFindTargetComponent();
	}
}

void UCombatComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (OwnerCharacter->GetAttributeComp())
	{
		OwnerCharacter->GetAttributeComp()->OnDeath.RemoveDynamic(this, &UCombatComponent::OnCharacterDead);
	}

	if (OwnerInventory)
	{
		OwnerInventory->OnAmmoChanged.RemoveDynamic(this, &UCombatComponent::OnInventoryAmmoChanged);
		OwnerInventory = nullptr;
	}
}

void UCombatComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                     FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	FHitResult HitResult;
	TraceUnderCrosshairs(HitResult);

	ChangeHitTarget();

	SetHUDCrosshairs(DeltaTime);
}

void UCombatComponent::PickupGrenade(FGrenadeInfo GrenadeInfo)
{
	const int32 Index = FindIndexGrenadeByType(GrenadeInfo.GrenadeType);
	if (GrenadeList.IsValidIndex(Index) && GrenadeInfo.GrenadeSubType == GrenadeList[Index].GrenadeSubType)
	{
		FGrenadeInfo& Grenade = GrenadeList[Index];
		Grenade.Count += GrenadeInfo.Count;
		OnGrenadeChanges.Broadcast(Index, Grenade.Count, Grenade.ItemIcon.LoadSynchronous(), Grenade.ItemName);
	}
	else
	{
		AddNewGrenade(Index, GrenadeInfo);
	}
}

void UCombatComponent::AddNewGrenade(int32 Slot, FGrenadeInfo GrenadeInfo)
{
	if (Slot <= -1)
	{
		GrenadeList.Add(GrenadeInfo);
		OnFoundGrenades.Broadcast(GrenadeInfo);
	}
	else
	{
		GrenadeList.RemoveAt(Slot);
		GrenadeList.EmplaceAt(Slot, GrenadeInfo);
		OnGrenadeChanges.Broadcast(Slot, GrenadeInfo.Count, GrenadeInfo.ItemIcon.LoadSynchronous(), GrenadeInfo.ItemName);
	}
}

int32 UCombatComponent::FindIndexGrenadeByType(const EGrenadeType GrenadeType)
{
	for (int32 i = 0; i < GrenadeList.Num(); ++i)
	{
		if (GrenadeList[i].GrenadeType == GrenadeType)
		{
			return i;
		}
	}

	return INDEX_NONE;
}

void UCombatComponent::DecreaseGrenadeCount()
{
	FGrenadeInfo& GrenadeInfo = GrenadeList[ActiveGrenadeSlot];

	GrenadeInfo.Count -= 1;
	if (GrenadeInfo.Count <= 0)
	{
		GrenadeList.RemoveAt(ActiveGrenadeSlot);
	}
	OnGrenadeChanges.Broadcast(ActiveGrenadeSlot, GrenadeInfo.Count, GrenadeInfo.ItemIcon.LoadSynchronous(), GrenadeInfo.ItemName);
}

void UCombatComponent::SelectGrenadeSlot(int32 ActiveGrenade)
{
	if (GrenadeList.Num() > 0)
	{
		UE_LOG(LogTemp, Log, TEXT("ACTIVE SLOT = %i"), ActiveGrenade);
		OnActiveSlotChange.Broadcast(ActiveGrenade);
	}
}

void UCombatComponent::SetHUDCrosshairs(float DeltaTime)
{
	if (HUD)
	{
		//TODO: change on equipped
		if (EquippedWeapon)
		{
			HUDPackage.CrosshairsCenter = EquippedWeapon->CrosshairsCenter;
			HUDPackage.CrosshairsLeft = EquippedWeapon->CrosshairsLeft;
			HUDPackage.CrosshairsRight = EquippedWeapon->CrosshairsRight;
			HUDPackage.CrosshairsTop = EquippedWeapon->CrosshairsTop;
			HUDPackage.CrosshairsBottom = EquippedWeapon->CrosshairsBottom;
		}
		else
		{
			HUDPackage.CrosshairsCenter = nullptr;
			HUDPackage.CrosshairsLeft = nullptr;
			HUDPackage.CrosshairsRight = nullptr;
			HUDPackage.CrosshairsTop = nullptr;
			HUDPackage.CrosshairsBottom = nullptr;
		}

		const FVector2D WalkSpeedRange(0.f, OwnerCharacter->GetCharacterMovement()->MaxWalkSpeed + 300.f);
		const FVector2D VelocityMultiplierRange(0.f, 1.f);
		const FVector Velocity = OwnerCharacter->GetVelocity();
		CrosshairVelocityFactor = FMath::GetMappedRangeValueClamped(WalkSpeedRange, VelocityMultiplierRange, Velocity.Size2D());

		if (bAiming)
		{
			CrosshairAimFactor = FMath::FInterpTo(CrosshairAimFactor, 0.58f, DeltaTime, 30.f);
		}
		else
		{
			CrosshairAimFactor = FMath::FInterpTo(CrosshairAimFactor, 0.f, DeltaTime, 30.f);
		}

		CrosshairShootingFactor = FMath::FInterpTo(CrosshairShootingFactor, 0.f, DeltaTime, 40.f);

		HUDPackage.CrosshairSpread = 0.5f + CrosshairVelocityFactor - CrosshairAimFactor + CrosshairShootingFactor;
		HUD->SetHUDPackage(HUDPackage);
	}
}

void UCombatComponent::DropEquippedWeapon()
{
	if (EquippedWeapon)
	{
		EquippedWeapon->Dropped();
	}
}

void UCombatComponent::AttachActorToRightHand(AActor* ActorToAttach)
{
	if (OwnerCharacter == nullptr || OwnerCharacter->GetMesh() == nullptr)
	{
		return;
	}

	if (ActorToAttach == nullptr || EquippedWeapon == nullptr)
	{
		return;
	}

	if (const USkeletalMeshSocket* HandSocket = OwnerCharacter->GetMesh()->GetSocketByName(FName("RightHandSocket")))
	{
		HandSocket->AttachActor(ActorToAttach, OwnerCharacter->GetMesh());
	}
}

void UCombatComponent::AttachActorToLeftHand(AActor* ActorToAttach)
{
	if (OwnerCharacter == nullptr || OwnerCharacter->GetMesh() == nullptr)
	{
		return;
	}

	if (ActorToAttach == nullptr || EquippedWeapon == nullptr)
	{
		return;
	}

	bool bUsePistolSocket =
		EquippedWeapon->GetWeaponType() == EWeaponType::EWT_Pistol ||
		EquippedWeapon->GetWeaponType() == EWeaponType::EWT_SubmachinGun;

	FName SocketName = bUsePistolSocket ? FName("PistolSocket") : FName("LeftHandSocket");

	if (const USkeletalMeshSocket* HandSocket = OwnerCharacter->GetMesh()->GetSocketByName(SocketName))
	{
		HandSocket->AttachActor(ActorToAttach, OwnerCharacter->GetMesh());
	}
}

void UCombatComponent::AttachActorToArmory(AActor* ActorToAttach)
{
	if (OwnerCharacter == nullptr || OwnerCharacter->GetMesh() == nullptr || ActorToAttach == nullptr)
	{
		return;
	}

	if (const USkeletalMeshSocket* ArmorySocket = OwnerCharacter->GetMesh()->GetSocketByName(FName("ArmorySocket")))
	{
		ArmorySocket->AttachActor(ActorToAttach, OwnerCharacter->GetMesh());
	}
}

void UCombatComponent::ChangeHitTarget()
{
	if (OwnerCharacter && OwnerCharacter->GetAttributeComp() && OwnerCharacter->GetAttributeComp()->IsAlive())
	{
		if (OwnerCharacter->bIsPlayer)
		{
			PlayerChangeHitTarget();
		}
		else
		{
			//AIChangeHitTarget();
		}
	}
}

void UCombatComponent::PlayerChangeHitTarget()
{
	if (!OwnerCharacter->IsLocallyPlayerControlled() || !OwnerFindTargetComponent)
	{
		return;
	}

	const FVector CharacterForwardVector = OwnerCharacter->GetActorForwardVector();
	const FVector CharacterLocation = OwnerCharacter->GetActorLocation();
	if (!OwnerFindTargetComponent->Overlap)
	{
		HUDPackage.CrosshairColor = FLinearColor::White;
		if (Controller && EquippedWeapon)
		{
			HitTarget = Controller->MousePose;
		}
		else
		{
			HitTarget = CharacterForwardVector * 1000.0f + CharacterLocation;
		}
	}
	else
	{
		if (Controller && EquippedWeapon)
		{
			FVector MouseLocation, MouseDirection;
			Controller->DeprojectMousePositionToWorld(MouseLocation, MouseDirection);

			FVector TraceEndLocation = MouseDirection * 10000.f + MouseLocation;

			FHitResult HitResult;
			FCollisionQueryParams Params;
			Params.AddIgnoredActor(GetOwner());

			GetWorld()->LineTraceSingleByObjectType(HitResult, MouseLocation, TraceEndLocation, ECC_SkeletalMesh, Params);
			if (HitResult.bBlockingHit)
			{
				HitTarget = HitResult.ImpactPoint;
				HUDPackage.CrosshairColor = FLinearColor::Red;
				OwnerFindTargetComponent->CloseTargetLocation = HitResult.GetActor()->GetActorLocation();
			}
			else
			{
				HUDPackage.CrosshairColor = FLinearColor::White;

				float T;
				FVector NewHitTarget;
				const bool Intersect = UKismetMathLibrary::LinePlaneIntersection_OriginNormal(
					MouseLocation,
					TraceEndLocation,
					CharacterLocation,
					OwnerFindTargetComponent->NormalToTarget.Vector(),
					T,
					NewHitTarget
				);

				if (Intersect)
				{
					HitTarget = NewHitTarget;
				}
				else
				{
					HitTarget = FVector(Controller->MousePose.X, Controller->MousePose.Y, OwnerFindTargetComponent->CloseTargetLocation.Z);
				}
			}
		}
	}
}

void UCombatComponent::AIChangeHitTarget()
{
	if (OwnerCharacter && !OwnerCharacter->bIsPlayer && EquippedWeapon && OwnerFindTargetComponent)
	{
		if (OwnerFindTargetComponent->Overlap)
		{
			const auto HalfRad = FMath::DegreesToRadians(EquippedWeapon->BulletSpread);
			const FVector AbsoluteDirection = OwnerFindTargetComponent->CloseTargetLocation - OwnerCharacter->GetActorLocation();
			const FVector PlayerShotDir = FMath::VRandCone(AbsoluteDirection, HalfRad);

			HitTarget = OwnerCharacter->GetActorLocation() + PlayerShotDir;
		}
		else
		{
			HitTarget = OwnerCharacter->GetActorLocation() + OwnerCharacter->GetActorForwardVector() * 700.f; //TODO: magic number
		}
	}
}

void UCombatComponent::TraceUnderCrosshairs(FHitResult& TraceHitResult)
{
	FVector Start, End;
	GetTraceData(Start, End, 700.f, false); //TODO: magic number
	GetWorld()->LineTraceSingleByChannel(TraceHitResult, Start, End, ECollisionChannel::ECC_Pawn);

	if (OwnerCharacter && !OwnerCharacter->bIsPlayer && OwnerCharacter->GetAttributeComp()->IsAlive())
	{
		//DrawDebugLine(GetWorld(), Start, End, FColor::Orange, false, 0.0f, 0, 4.0f);
	}

	if (!TraceHitResult.bBlockingHit)
	{
		TraceHitResult.ImpactPoint = End;
	}

	HUDPackage.CrosshairColor = FLinearColor::White;
	if (TraceHitResult.GetActor() && TraceHitResult.GetActor()->Implements<UInteractWithCrosshairsInterface>())
	{
		TargetActor = Cast<ASAICharacterBase>(TraceHitResult.GetActor());
	}
	else
	{
		TargetActor = nullptr;
	}
}

FVector UCombatComponent::GetMuzzleWorldLocation() const
{
	if (!EquippedWeapon || !EquippedWeapon->GetWeaponMesh())
	{
		return FVector(0.0f, 0.0f, 0.0f);
	}

	return EquippedWeapon->GetWeaponMesh()->GetSocketLocation(MuzzleSocketName);
}

void UCombatComponent::SetCombatState(ECombatState NewCombatState)
{
	CombatState = NewCombatState;
}

void UCombatComponent::SetCanFire(bool bNewCanFire)
{
	bCanFire = bNewCanFire;
}

bool UCombatComponent::GetTraceData(FVector& TraceStart, FVector& TraceEnd, float TraceDistance, bool IsPrint) const
{
	if (OwnerCharacter == nullptr && EquippedWeapon == nullptr && Controller == nullptr)
	{
		return false;
	}

	FVector PlayerLoc = OwnerCharacter->GetActorLocation();
	FVector PlayerLocStart = PlayerLoc + FVector(50.0f, 0.0f, 50.f); //TODO: magic number

	if (EquippedWeapon)
	{
		const USkeletalMeshSocket* MuzzleFlashSocket = EquippedWeapon->GetWeaponMesh()->GetSocketByName(FName("MuzzleFlash"));
		if (MuzzleFlashSocket)
		{
			TraceStart = MuzzleFlashSocket->GetSocketLocation(EquippedWeapon->GetWeaponMesh());
		}

		if (!OwnerCharacter->bIsPlayer)
		{
			const auto HalfRad = FMath::DegreesToRadians(EquippedWeapon->BulletSpread);
			const FVector PlayerShotDir = FMath::VRandCone(OwnerCharacter->GetCapsuleComponent()->GetForwardVector(), HalfRad);
			TraceEnd = PlayerLocStart + PlayerShotDir * TraceDistance;
		}
		else if (Controller)
		{
			TraceEnd = Controller->MousePose; /*PlayerLocStart + PlayerShotDir * TraceDistance*/
		}
	}

	if (IsPrint)
	{
		DrawDebugSphere(GetWorld(), TraceEnd, 6.f, 6.f, FColor::Red, false, 0.0f);

		FString EndString = TraceEnd.ToString();
		FString ActionMsg = FString::Printf(TEXT(" TraceEnd: %s "), *EndString);
		GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Yellow, ActionMsg);
	}

	return true;
}

void UCombatComponent::FireButtonPressed(bool bPressed)
{
	bFireWeaponPressed = bPressed;

	Fire();
}

void UCombatComponent::ShotgunShellReload()
{
	if (OwnerCharacter && OwnerCharacter->HasAuthority())
	{
		UpdateAmmoReloadShotgun();
	}
}

void UCombatComponent::Fire()
{
	if (CanFire())
	{
		bCanFire = false;

		ServerFire(HitTarget);
		StartFireTimer();

		if (EquippedWeapon && !EquippedWeapon->IsEmpty())
		{
			CrosshairShootingFactor = 0.75f;
			UE_LOG(LogTemp, Log, TEXT("FIRE"));
			UE_LOG(LogTemp, Log, TEXT("BULLETS  ---   %i"), EquippedWeapon->GetAmmo());
		}
	}
}

void UCombatComponent::OnRep_bAiming()
{
	if (OwnerCharacter)
	{
		OwnerCharacter->ChangeMovementState();
	}

	TryActivateWeaponLaser();
}

void UCombatComponent::StartFireTimer()
{
	if (EquippedWeapon == nullptr || OwnerCharacter == nullptr)
	{
		return;
	}

	OwnerCharacter->GetWorldTimerManager().SetTimer(FireTimerHandle, this, &UCombatComponent::FireTimerFinished, EquippedWeapon->FireDelay);
}

void UCombatComponent::FireTimerFinished()
{
	if (EquippedWeapon == nullptr)
	{
		return;
	}

	bCanFire = true;

	if (CanAutoFire())
	{
		Fire();
	}

	ReloadEmptyWeapon();
}

void UCombatComponent::ReloadEmptyWeapon()
{
	if (EquippedWeapon && EquippedWeapon->IsEmpty())
	{
		Reload();
	}
}

bool UCombatComponent::CanFire()
{
	if (EquippedWeapon == nullptr)
	{
		return false;
	}

	if (!EquippedWeapon->IsEmpty() && bCanFire && bFireWeaponPressed)
	{
		if (CombatState == ECombatState::ECS_Reloading && EquippedWeapon->GetAmmoType() == EAmmoType::EAT_12)
		{
			return true;
		}

		return CombatState == ECombatState::ECS_Unoccupied;
	}

	return false;
}

bool UCombatComponent::CanAutoFire()
{
	return EquippedWeapon && EquippedWeapon->bAutomatic && !EquippedWeapon->IsEmpty();
}

void UCombatComponent::OnChangedWeaponAmmo(AWeapon* Weapon, int32 NewValue)
{
	OnChangedWeaponAmmoEvent.Broadcast(this, NewValue);
}

void UCombatComponent::TryActivateWeaponLaser()
{
	if (EquippedWeapon)
	{
		if (CombatState != ECombatState::ECS_Unoccupied || !IsAiming())
		{
			EquippedWeapon->DeactivateLaser();
		}
		else if (IsAiming())
		{
			EquippedWeapon->ActivateLaser();
		}
	}
}

void UCombatComponent::OnRep_CarriedAmmo()
{
	OnChangedCarriedAmmoEvent.Broadcast(this, CarriedAmmo);

	const bool bJumpToShotgunEnd =
		CombatState == ECombatState::ECS_Reloading &&
		EquippedWeapon != nullptr &&
		EquippedWeapon->GetAmmoType() == EAmmoType::EAT_12 &&
		CarriedAmmo == 0 && !EquippedWeapon->IsFull();

	if (bJumpToShotgunEnd)
	{
		JumpToShotgunEnd();
	}
}

void UCombatComponent::ServerFire_Implementation(const FVector_NetQuantize& TraceHitTarget)
{
	MulticastFire(TraceHitTarget);
}

void UCombatComponent::MulticastFire_Implementation(const FVector_NetQuantize& TraceHitTarget)
{
	if (EquippedWeapon == nullptr)
	{
		return;
	}

	if (!OwnerCharacter)
	{
		return;
	}

	if (CombatState == ECombatState::ECS_Reloading && EquippedWeapon->GetAmmoType() == EAmmoType::EAT_12)
	{
		CombatState = ECombatState::ECS_Unoccupied;
	}

	if (CombatState == ECombatState::ECS_Unoccupied)
	{
		OwnerCharacter->PlayFireAnim(bAiming ? IronsightsAttackAnim : AttackAnim);
		EquippedWeapon->Fire(TraceHitTarget);
	}
}

void UCombatComponent::SetAiming(bool bIsAiming)
{
	if (OwnerCharacter && OwnerCharacter->HasLocalNetOwner())
	{
		ServerSetAiming(bIsAiming);
	}
}

void UCombatComponent::SetActiveGrenadeSlot(int32 NewActiveGrenadeSlot)
{
	ActiveGrenadeSlot = NewActiveGrenadeSlot;
}

void UCombatComponent::ServerSetAiming_Implementation(bool bIsAiming)
{
	bAiming = bIsAiming;
	OnRep_bAiming();
}

void UCombatComponent::EquipWeapon(AWeapon* WeaponToEquip)
{
	if (OwnerCharacter == nullptr || WeaponToEquip == nullptr)
	{
		return;
	}

	if (EquippedWeapon != nullptr && SecondaryWeapon == nullptr)
	{
		EquipSecondaryWeapon(WeaponToEquip);
	}
	else
	{
		EquipPrimaryWeapon(WeaponToEquip);
	}

	if (EquippedWeapon->IsEmpty())
	{
		Reload();
	}
}

void UCombatComponent::Reload()
{
	if (CarriedAmmo > 0 && CombatState == ECombatState::ECS_Unoccupied && EquippedWeapon && !EquippedWeapon->IsFull())
	{
		ServerReload();
	}
}

void UCombatComponent::ServerReload_Implementation()
{
	if (OwnerCharacter == nullptr || EquippedWeapon == nullptr)
	{
		return;
	}

	CombatState = ECombatState::ECS_Reloading;
	OnRep_CombatState();
}

void UCombatComponent::FinishReloading()
{
	if (OwnerCharacter == nullptr)
	{
		return;
	}

	if (OwnerCharacter->HasAuthority())
	{
		UpdateAmmoAfterReloading();
		CombatState = ECombatState::ECS_Unoccupied;
		TryActivateWeaponLaser();
	}

	Fire();
}

void UCombatComponent::FinishSwap()
{
	if (OwnerCharacter == nullptr)
	{
		return;
	}

	if (IsAiming() && EquippedWeapon && OwnerCharacter->IsLocallyPlayerControlled())
	{
		EquippedWeapon->ActivateLaser();
	}

	if (OwnerCharacter->HasAuthority())
	{
		CombatState = ECombatState::ECS_Unoccupied;
	}

	OwnerCharacter->bFinishedSwapping = true;
}

void UCombatComponent::FinishSwapAttachWeapon()
{
	AWeapon* TempWeapon = EquippedWeapon;

	if (TempWeapon)
	{
		TempWeapon->OnChangedAmmo.RemoveDynamic(this, &UCombatComponent::OnChangedWeaponAmmo);
	}

	EquippedWeapon = SecondaryWeapon;

	SecondaryWeapon = TempWeapon;

	EquippedWeapon->SetWeaponState(EWeaponState::EWS_Equipped);

	OnChangedWeaponAmmo(EquippedWeapon, EquippedWeapon->GetAmmo());

	if (EquippedWeapon)
	{
		EquippedWeapon->OnChangedAmmo.RemoveDynamic(this, &UCombatComponent::OnChangedWeaponAmmo);
		EquippedWeapon->OnChangedAmmo.AddDynamic(this, &UCombatComponent::OnChangedWeaponAmmo);
	}

	AttachActorToRightHand(EquippedWeapon);
	UpdateCarriedAmmo();
	PlayEquipWeaponSound(EquippedWeapon);

	SecondaryWeapon->SetWeaponState(EWeaponState::EWS_EquippedSecondary);
	AttachActorToArmory(SecondaryWeapon);

	LastEquippedWeapon = EquippedWeapon;
	OnChangeEquippedWeapon.Broadcast(this);
}

void UCombatComponent::UpdateAmmoAfterReloading()
{
	if (OwnerCharacter == nullptr || EquippedWeapon == nullptr || OwnerInventory == nullptr)
	{
		return;
	}

	const int32 ReloadAmount = AmountToReload();

	OwnerInventory->AddAmmo(EquippedWeapon->GetAmmoType(), -ReloadAmount);
	CarriedAmmo = OwnerInventory->GetAmmo(EquippedWeapon->GetAmmoType());
	if (OwnerCharacter->HasAuthority())
	{
		OnRep_CarriedAmmo();
	}

	EquippedWeapon->AddAmmo(ReloadAmount);
}

void UCombatComponent::UpdateCarriedAmmo()
{
	if (OwnerCharacter == nullptr || EquippedWeapon == nullptr || OwnerInventory == nullptr)
	{
		return;
	}

	CarriedAmmo = OwnerInventory->GetAmmo(EquippedWeapon->GetAmmoType());
	if (OwnerCharacter->HasAuthority())
	{
		OnRep_CarriedAmmo();
	}
}

void UCombatComponent::PlayEquipWeaponSound(AWeapon* WeapoToEquip)
{
	if (OwnerCharacter && WeapoToEquip && WeapoToEquip->EquipSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, WeapoToEquip->EquipSound, OwnerCharacter->GetActorLocation());
	}
}

void UCombatComponent::UpdateAmmoReloadShotgun()
{
	if (OwnerCharacter == nullptr || EquippedWeapon == nullptr || OwnerInventory == nullptr)
	{
		return;
	}

	EquippedWeapon->AddAmmo(1);

	OwnerInventory->AddAmmo(EquippedWeapon->GetAmmoType(), -1);

	CarriedAmmo = OwnerInventory->GetAmmo(EquippedWeapon->GetAmmoType());

	if (OwnerCharacter->HasAuthority())
	{
		OnRep_CarriedAmmo();
	}

	bCanFire = true;

	if (EquippedWeapon->IsFull())
	{
		JumpToShotgunEnd();
	}
}

void UCombatComponent::JumpToShotgunEnd()
{
	UAnimInstance* AnimInstance = OwnerCharacter->GetMesh()->GetAnimInstance();
	if (AnimInstance && OwnerCharacter->GetReloadMontage())
	{
		AnimInstance->Montage_JumpToSection(FName("ShotgunEnd"));
	}
}

void UCombatComponent::ThrowGrenadeFinished()
{
	CombatState = ECombatState::ECS_Unoccupied;
	AttachActorToRightHand(EquippedWeapon);
	UE_LOG(LogTemp, Warning, TEXT("TROW GRENADE FINISHED"));
}

void UCombatComponent::LaunchGrenade()
{
	ShowAttachedGrenade(false);
	if (OwnerCharacter && OwnerCharacter->IsLocallyControlled()) // TODO: replace spawn grenade on server
	{
		UE_LOG(LogTemp, Warning, TEXT("LAUNCH GRENADE  --- SPAWN HIRE"));
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
		FVector SocketLocation = OwnerCharacter->GetMesh()->GetSocketLocation(FName("RightHandSocket"));
		FTransform SpawnPoint = FTransform(FRotator(0), SocketLocation + FVector(0.f,0.f,15.f), FVector(1));
		//TODO Need END SPAWN
		auto Spawned = GetWorld()->SpawnActorDeferred<AProjectileGrenade>(GrenadeList[ActiveGrenadeSlot].GrenadeClass.LoadSynchronous(), SpawnPoint,
		                                                                  OwnerCharacter,
		                                                                  nullptr, SpawnParams.SpawnCollisionHandlingOverride);
		if(Spawned)
			Spawned->GetProjectileMovementComponent()->Velocity = FVector(OwnerCharacter->GetActorForwardVector() * 300);
		UGameplayStatics::FinishSpawningActor(Spawned, SpawnPoint);
		//GrenadeList[ActiveGrenadeSlot].Count - 1;
		DecreaseGrenadeCount();
		ActiveGrenadeSlot = INDEX_NONE; //clear index -- remove HighlightSlot
		OnActiveSlotChange.Broadcast(ActiveGrenadeSlot);
	}
}

void UCombatComponent::ApplyKnifeAttack()
{
	ShowAttachedKnife(false);
	if (OwnerCharacter && OwnerCharacter->IsLocallyControlled())
	{
		UE_LOG(LogTemp, Warning, TEXT("SERVER - END KNIFE ATTACK"));

		CombatState = ECombatState::ECS_Unoccupied;
		AttachActorToRightHand(EquippedWeapon);
	}
}

void UCombatComponent::OnRep_CombatState()
{
	switch (CombatState)
	{
	case ECombatState::ECS_Reloading:
		if (OwnerCharacter)
		{
			OwnerCharacter->PlayReloadMontage();
		}
		break;
	case ECombatState::ECS_Unoccupied:
		Fire();
		break;
	case ECombatState::ECS_SwappingWeapons:
		if (OwnerCharacter && !OwnerCharacter->HasAuthority())
		{
			if (EquippedWeapon && OwnerCharacter->IsLocallyPlayerControlled())
			{
				EquippedWeapon->DeactivateLaser();
			}
			
			OwnerCharacter->PlaySwapMontage();
		}
		break;
	case ECombatState::ECS_ThrowingGrenade:
		if (OwnerCharacter && !OwnerCharacter->IsLocallyControlled())
		{
			OwnerCharacter->PlayThrowGrenadeMontage();
			AttachActorToLeftHand(EquippedWeapon);
			ShowAttachedGrenade(true);
		}
		break;
	default:
		break;
	}

	TryActivateWeaponLaser();
}

int32 UCombatComponent::AmountToReload()
{
	if (EquippedWeapon == nullptr || OwnerInventory == nullptr)
	{
		return 0;
	}

	const int32 NeededToFull = EquippedWeapon->GetMagCapacity() - EquippedWeapon->GetAmmo();

	const int32 AmountCarried = OwnerInventory->GetAmmo(EquippedWeapon->GetAmmoType());
	return FMath::Min(NeededToFull, AmountCarried);
}

void UCombatComponent::ThrowGrenade(int32 SelectedGrenade)
{
	if (CombatState != ECombatState::ECS_Unoccupied)
	{
		return;
	}

	CombatState = ECombatState::ECS_ThrowingGrenade;
	if (OwnerCharacter)
	{
		OwnerCharacter->PlayThrowGrenadeMontage();
		AttachActorToLeftHand(EquippedWeapon);
		ShowAttachedGrenade(true);
		if (!OwnerCharacter->HasAuthority())
		{
			ServerThrowGrenade(SelectedGrenade);
		}
	}
}

void UCombatComponent::KnifeAttack()
{
	if (CombatState != ECombatState::ECS_Unoccupied)
	{
		return;
	}

	CombatState = ECombatState::ECS_KnifeAttack;
	if (OwnerCharacter)
	{
		ShowAttachedKnife(true);
		OwnerCharacter->PlayKnifeAttackMontage();
		AttachActorToLeftHand(EquippedWeapon);
	}
}

void UCombatComponent::ServerThrowGrenade_Implementation(int32 SelectedGrenade)
{
	CombatState = ECombatState::ECS_ThrowingGrenade;
	if (OwnerCharacter)
	{
		OwnerCharacter->PlayThrowGrenadeMontage();
		AttachActorToLeftHand(EquippedWeapon);
		ShowAttachedGrenade(true);
	}
}

bool UCombatComponent::CanDropWeapon() const
{
	return EquippedWeapon && !EquippedWeapon->bDestroyWeapon;
}

void UCombatComponent::OnCharacterDead(AActor* Owner, AActor* Instigator)
{
	if (EquippedWeapon)
	{
		if (CanDropWeapon())
		{
			EquippedWeapon->Dropped();
		}
		else
		{
			EquippedWeapon->SetLifeSpan(0.1f);
		}
	}
}

void UCombatComponent::OnCharacterPossessed(ACPP_SkillBoxCharacter* Character)
{
	if (OwnerCharacter)
	{
		Controller = OwnerCharacter->GetController<ACPP_SkillBoxPlayerController>();
	}

	if (Controller)
	{
		HUD = Controller->GetHUD<APlayerHUD>();
	}
}

void UCombatComponent::OnCharacterUnPossessed(ACPP_SkillBoxCharacter* Character)
{
	Controller = nullptr;
	HUD = nullptr;
}

void UCombatComponent::OnInventoryAmmoChanged(UInventoryComponent* InventoryComponent, EAmmoType Type, int32 NewValue)
{
	UpdateCarriedAmmo();

	if (EquippedWeapon && EquippedWeapon->IsEmpty() && EquippedWeapon->GetAmmoType() == Type)
	{
		Reload();
	}
}

void UCombatComponent::ShowAttachedKnife(bool bShowKnife)
{
	if (OwnerCharacter && OwnerCharacter->GetAttachedKnife())
	{
		OwnerCharacter->GetAttachedKnife()->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		OwnerCharacter->GetAttachedKnife()->SetVisibility(bShowKnife);
	}
}

void UCombatComponent::ShowAttachedGrenade(bool bShowGrenade)
{
	if (OwnerCharacter && OwnerCharacter->GetAttachedGrenade())
	{
		OwnerCharacter->GetAttachedGrenade()->SetVisibility(bShowGrenade);
	}
}

void UCombatComponent::EquipPrimaryWeapon(AWeapon* WeaponToEquip)
{
	if (OwnerCharacter == nullptr || WeaponToEquip == nullptr)
	{
		return;
	}

	if (CombatState != ECombatState::ECS_Unoccupied)
	{
		return;
	}

	if (EquippedWeapon)
	{
		EquippedWeapon->OnChangedAmmo.RemoveDynamic(this, &UCombatComponent::OnChangedWeaponAmmo);
	}

	DropEquippedWeapon();
	EquippedWeapon = WeaponToEquip;
	EquippedWeapon->SetWeaponState(EWeaponState::EWS_Equipped);
	EquippedWeapon->SetInstigator(OwnerCharacter);
	EquippedWeapon->SetOwner(OwnerCharacter);
	EquippedWeapon->EnableCustomDepth(false);

	OnChangedWeaponAmmo(EquippedWeapon, EquippedWeapon->GetAmmo());

	EquippedWeapon->OnChangedAmmo.RemoveDynamic(this, &UCombatComponent::OnChangedWeaponAmmo);
	EquippedWeapon->OnChangedAmmo.AddDynamic(this, &UCombatComponent::OnChangedWeaponAmmo);

	AttachActorToRightHand(EquippedWeapon);

	UpdateCarriedAmmo();
	PlayEquipWeaponSound(WeaponToEquip);
	ReloadEmptyWeapon();

	LastEquippedWeapon = EquippedWeapon;
	OnChangeEquippedWeapon.Broadcast(this);
}

void UCombatComponent::EquipSecondaryWeapon(AWeapon* WeaponToEquip)
{
	if (WeaponToEquip)
	{
		SecondaryWeapon = WeaponToEquip;
		SecondaryWeapon->SetWeaponState(EWeaponState::EWS_EquippedSecondary);
		SecondaryWeapon->SetInstigator(OwnerCharacter);
		AttachActorToArmory(WeaponToEquip);
		PlayEquipWeaponSound(WeaponToEquip);
		SecondaryWeapon->EnableCustomDepth(false);
		SecondaryWeapon->SetOwner(OwnerCharacter);
	}
}

void UCombatComponent::OnRep_EquippedWeapon(AWeapon* OldWeapon)
{
	if (EquippedWeapon && OwnerCharacter)
	{
		if (OldWeapon)
		{
			OldWeapon->OnChangedAmmo.RemoveDynamic(this, &UCombatComponent::OnChangedWeaponAmmo);
		}

		EquippedWeapon->SetWeaponState(EWeaponState::EWS_Equipped);
		EquippedWeapon->SetInstigator(OwnerCharacter);
		EquippedWeapon->EnableCustomDepth(false);

		OnChangedWeaponAmmo(EquippedWeapon, EquippedWeapon->GetAmmo());

		EquippedWeapon->OnChangedAmmo.RemoveDynamic(this, &UCombatComponent::OnChangedWeaponAmmo);
		EquippedWeapon->OnChangedAmmo.AddDynamic(this, &UCombatComponent::OnChangedWeaponAmmo);

		AttachActorToRightHand(EquippedWeapon);
		PlayEquipWeaponSound(EquippedWeapon);
	}
}

void UCombatComponent::OnRep_SecondaryWeapon()
{
	if (SecondaryWeapon && OwnerCharacter)
	{
		SecondaryWeapon->SetWeaponState(EWeaponState::EWS_EquippedSecondary);
		SecondaryWeapon->SetInstigator(OwnerCharacter);
		AttachActorToArmory(SecondaryWeapon);
		PlayEquipWeaponSound(EquippedWeapon);
	}
}

void UCombatComponent::SwapWeapon()
{
	if (CombatState != ECombatState::ECS_Unoccupied || OwnerCharacter == nullptr || !OwnerCharacter->HasAuthority())
	{
		return;
	}

	if (EquippedWeapon && OwnerCharacter->IsLocallyPlayerControlled())
	{
		EquippedWeapon->DeactivateLaser();
	}

	OwnerCharacter->PlaySwapMontage();
	CombatState = ECombatState::ECS_SwappingWeapons;
	OwnerCharacter->bFinishedSwapping = false;
}

bool UCombatComponent::ShouldSwapWeapons() const
{
	return (EquippedWeapon != nullptr && SecondaryWeapon != nullptr);
}

#include "DropItemComponent.h"

#include "AmmoPickup.h"
#include "CombatComponent.h"
#include "CPP_SkillBoxCharacter.h"
#include "SAttributeComponent.h"
#include "Weapon.h"
#include "Kismet/GameplayStatics.h"

UDropItemComponent::UDropItemComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UDropItemComponent::BeginPlay()
{
	Super::BeginPlay();

	OwnerCharacter = GetOwner<ACPP_SkillBoxCharacter>();
	if (OwnerCharacter)
	{
		OwnerCombatComponent = OwnerCharacter->GetCombat();

		OwnerAttributeComponent = OwnerCharacter->GetAttributeComp();
		if (OwnerAttributeComponent)
		{
			OwnerAttributeComponent->OnDeath.AddDynamic(this, &UDropItemComponent::OnDeath);
		}
	}
}

void UDropItemComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (OwnerAttributeComponent)
	{
		OwnerAttributeComponent->OnDeath.RemoveDynamic(this, &UDropItemComponent::OnDeath);
	}

	OwnerCharacter = nullptr;
	OwnerCombatComponent = nullptr;
	OwnerAttributeComponent = nullptr;
}

bool UDropItemComponent::CanDropAmmo() const
{
	return bDropAmmo && DropAmmoChance >= FMath::FRandRange(0.0f, 1.0f);
}

void UDropItemComponent::OnDeath(AActor* Owner, AActor* Instigator)
{
	if (!CanDropAmmo())
	{
		return;
	}

	if (!GetWorld())
	{
		return;
	}

	if (!GetWorld()->IsNetMode(NM_ListenServer) && !GetWorld()->IsNetMode(NM_DedicatedServer) && !GetWorld()->IsNetMode(NM_Standalone))
	{
		return;
	}

	if (!OwnerCombatComponent)
	{
		return;
	}

	AWeapon* LastEquippedWeapon = OwnerCombatComponent->GetLastEquippedWeapon();
	if (!LastEquippedWeapon || LastEquippedWeapon->GetAmmo() <= 0)
	{
		return;
	}

	if (UClass* PickAmmoClass = LastEquippedWeapon->GetPickupAmmoClass().LoadSynchronous())
	{
		FHitResult Hit;
		FVector StartTrace = GetOwner()->GetActorLocation();
		constexpr float LineTraceDownLength = 1000.0f;
		FVector EndTrace = StartTrace * FVector::DownVector * LineTraceDownLength;

		FCollisionObjectQueryParams ObjectQueryParams;
		ObjectQueryParams.AddObjectTypesToQuery(ECC_WorldStatic);

		GetWorld()->LineTraceSingleByObjectType(Hit, StartTrace, EndTrace, ObjectQueryParams);
		if (Hit.bBlockingHit)
		{
			FTransform SpawnTransform = FTransform(FRotator::ZeroRotator, Hit.ImpactPoint);

			FActorSpawnParameters SpawnParameters;
			SpawnParameters.ObjectFlags = RF_Transient;
			SpawnParameters.bDeferConstruction = true;

			AAmmoPickup* PickupAmmo = GetWorld()->SpawnActor<AAmmoPickup>(PickAmmoClass, SpawnParameters);
			PickupAmmo->SetAmmoAmount(LastEquippedWeapon->GetAmmo());
			UGameplayStatics::FinishSpawningActor(PickupAmmo, SpawnTransform);

			LastEquippedWeapon->AddAmmo(-LastEquippedWeapon->GetAmmo());
		}
	}
}

#pragma once

#include <CoreMinimal.h>
#include <Components/ActorComponent.h>

#include "WeaponTypes.h"
#include "DropItemComponent.generated.h"

class AAmmoPickup;
class USAttributeComponent;
class ACPP_SkillBoxCharacter;
class UCombatComponent;

UCLASS(ClassGroup=(DropItem), meta=(BlueprintSpawnableComponent) )
class CPP_SKILLBOX_API UDropItemComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UDropItemComponent();

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	bool CanDropAmmo() const;

protected:
	UFUNCTION()
	void OnDeath(AActor* Owner, AActor* Instigator);

protected:
	UPROPERTY(EditAnywhere, Category="Settings")
	bool bDropAmmo = true;

	UPROPERTY(EditAnywhere, Category="Settings", meta=(EditCondition="bDropAmmo", ClampMin="0.0", ClampMax="1.0", UIMin="0.0", UIMax="1.0"))
	float DropAmmoChance = 0.3f;

private:
	TObjectPtr<ACPP_SkillBoxCharacter> OwnerCharacter = nullptr;
	TObjectPtr<UCombatComponent> OwnerCombatComponent = nullptr;
	TObjectPtr<USAttributeComponent> OwnerAttributeComponent = nullptr;

	TSoftClassPtr<AAmmoPickup> PickupAmmoClass;
};

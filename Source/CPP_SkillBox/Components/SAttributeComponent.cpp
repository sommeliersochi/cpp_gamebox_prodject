
#include "SAttributeComponent.h"

#include <Net/UnrealNetwork.h>

#include "CPP_SkillBoxCharacter.h"
#include "CPP_SkillBoxGameMode.h"
#include "SActionComponent.h"
#include "SActionBleeding.h"

//for stun effect TODO:: Add Stun Effect
/*
#include "GameFramework/CharacterMovementComponent.h" 
*/


USAttributeComponent::USAttributeComponent()
{
	CurrentPlates.Init(StrengthShieldPlate, ArmorPlates);

	SetIsReplicatedByDefault(true);
}

void USAttributeComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USAttributeComponent, Health)
	DOREPLIFETIME(USAttributeComponent, HealthMax)
	DOREPLIFETIME(USAttributeComponent, Stamina)
	DOREPLIFETIME(USAttributeComponent, StaminaMax)
	DOREPLIFETIME(USAttributeComponent, StrengthShieldPlate)
	DOREPLIFETIME(USAttributeComponent, ArmorPlates)
	DOREPLIFETIME(USAttributeComponent, CurrentPlates)
}

void USAttributeComponent::BeginPlay()
{
	Super::BeginPlay();

	Health = HealthMax;
	OnHealthChanged.Broadcast(nullptr, this, Health, HealthMax);

	Stamina = StaminaMax;
	OnStaminaChanged.Broadcast(this, Stamina, StaminaMax);

	CompOwner = GetOwner();
	if (CompOwner)
	{
		ActionComp = Cast<USActionComponent>(CompOwner->GetComponentByClass(USActionComponent::StaticClass()));
		CompOwner->OnTakeAnyDamage.AddDynamic(this, &USAttributeComponent::OnTakeAnyDamage);
	}
}

void USAttributeComponent::ShieldChange(float Value)
{
	float CurrentDamage = Value;
	for (int32 i = CurrentPlates.Num() - 1; i >= 0; --i)
	{
		CurrentDamage = ShieldPlateChange(CurrentDamage, i);
		if (CurrentDamage >= 0.0f)
		{
			break;
		}
	}
	if (CurrentPlates[0] == 0.0f)
	{
		HaveShield = false;
	}
}

float USAttributeComponent::ShieldPlateChange(float Damage, int Index)
{
	float NewStrength = CurrentPlates[Index] + Damage;
	if (NewStrength >= 0.0f)
	{
		CurrentPlates[Index] = NewStrength;
		MulticastArmorChanged(NewStrength, Index);
		return 0.0f;
	}

	CurrentPlates[Index] = 0.0f;
	MulticastArmorChanged(0.0f, Index);
	return NewStrength;
}

bool USAttributeComponent::InstallNewShieldPlate(float StrengthValue)
{
	for (int i = 0; i < CurrentPlates.Num(); ++i)
	{
		if (CurrentPlates[i] < StrengthValue)
		{
			CurrentPlates[i] = StrengthValue;
			MulticastArmorChanged(StrengthValue, i);
			HaveShield = true;
			return true;
		}
	}
	return false;
}

void USAttributeComponent::OnTakeAnyDamage(
	AActor* DamagedActor,
	float Damage,
	const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser
)
{
	if (HaveShield)
	{
		ShieldChange(-Damage);
	}
	else
	{
		ApplyHealthChange(DamageCauser, -Damage);
	}
	//ActionComp->AddAction(InstigatedBy, StunActionClass);

	/*const auto Char = Cast<ACharacter>(DamagedActor);
	if(Char)
	{
		Char->GetCharacterMovement()->DisableMovement();
	}*/ //TODO:: Add more actions in game
}

void USAttributeComponent::ChangeStamina(float NewTired)
{
	if (NewTired < 0.0f || IsDead() || !GetWorld())
	{
		return;
	}

	Stamina = FMath::Clamp(Stamina - NewTired, 0.0f, StaminaMax);
	OnStaminaChanged.Broadcast(this, Stamina, NewTired);

	if (AutoTire)
	{
		GetWorld()->GetTimerManager().SetTimer(StaminaTimerHandle, this, &USAttributeComponent::StaminaUpdate,
		                                       StaminaUpdateTime, true, StaminaUpdateDelay);
	}
}

void USAttributeComponent::SetMaxHealth(float NewMaxHealth)
{
	HealthMax = NewMaxHealth;
}

USAttributeComponent* USAttributeComponent::GetAttributes(AActor* FromActor)
{
	if (FromActor)
	{
		return Cast<USAttributeComponent>(FromActor->GetComponentByClass(USAttributeComponent::StaticClass()));
	}
	return nullptr;
}

void USAttributeComponent::StaminaUpdate()
{
	Stamina = FMath::Min(Stamina + StaminaModifier, StaminaMax);
	OnStaminaChanged.Broadcast(this, Stamina, StaminaModifier);

	if (Stamina == StaminaMax && GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(StaminaTimerHandle);
	}
}

void USAttributeComponent::HealthUpdate()
{
	Health = FMath::Min(Health + HealthModifier, HealthMax);
	OnHealthChanged.Broadcast(nullptr, this, Health, HealthModifier);

	if (Health == HealthMax && GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(HealthTimerHandle);

		auto Char = Cast<ACPP_SkillBoxCharacter>(GetOwner());
		if (Char)
		{
			Char->SetHealthBarVisibility(false);
		}
	}
}

bool USAttributeComponent::ApplyHealthChange(AActor* InstigatorActor, float Delta)
{
	if (!GetOwner()->CanBeDamaged() && Delta < 0.0f)
	{
		return false;
	}

	float OldHealth = Health;
	float NewHealth = FMath::Clamp<float>(Health + Delta, 0.0f, HealthMax);

	float ActualDelta = NewHealth - OldHealth;

	Health = NewHealth;
	if (ActualDelta != 0.0f)
	{
		//OnHealthChanged.Broadcast(InstigatorActor, this, Health, ActualDelta);
		MulticastHealthChanged(InstigatorActor, Health, ActualDelta);
	}

	if (ActualDelta < 0.0f && Health == 0.0f)
	{
		auto ProjectOwner = InstigatorActor ? InstigatorActor->GetInstigator() : nullptr;
		ACPP_SkillBoxGameMode* GM = GetWorld()->GetAuthGameMode<ACPP_SkillBoxGameMode>();
		ACPP_SkillBoxPlayerController* Controller = ProjectOwner
			                                            ? Cast<ACPP_SkillBoxPlayerController>(
				                                            ProjectOwner->GetController())
			                                            : nullptr;

		if (GM)
		{
			GM->OnActorKilled(GetOwner(), ProjectOwner, Controller);
		}
	}
	if (IsDead())
	{
		OnDeath.Broadcast(GetOwner(), InstigatorActor);
	}
	else if (AutoHeal)
	{
		GetWorld()->GetTimerManager().SetTimer(
			HealthTimerHandle,
			this, &USAttributeComponent::HealthUpdate,
			HealthUpdateTime,
			true,
			HealthUpdateDelay);

		OnHealthChanged.Broadcast(nullptr, this, Health, HealthMax);
	}
	return ActualDelta != 0;
}

void USAttributeComponent::MulticastHealthChanged_Implementation(AActor* InstigatorActor, float NewHealth, float Delta)
{
	OnHealthChanged.Broadcast(InstigatorActor, this, NewHealth, Delta);
}


void USAttributeComponent::MulticastArmorChanged_Implementation(float Damage, int Index)
{
	OnShieldPlateChanged.Broadcast(Damage, Index);
}

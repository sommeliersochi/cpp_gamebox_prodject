// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "UObject/NoExportTypes.h"
#include "SAction.generated.h"

class USActionComponent;
/**
 * 
 */
UCLASS(Blueprintable)
class CPP_SKILLBOX_API USAction : public UObject
{
	GENERATED_BODY()
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category= "UI")
	UTexture2D* Icon;

	float TimeStarted;;
	
	UFUNCTION(BlueprintCallable, Category= "Action")
	USActionComponent* GetOwningComponent() const;

	UPROPERTY(EditDefaultsOnly, Category= "Tags")
	FGameplayTagContainer GrantsTags;

	UPROPERTY(EditDefaultsOnly, Category= "Tags")
	FGameplayTagContainer BlockedTags;

	bool bIsRunning;
	
public:

	UPROPERTY(EditDefaultsOnly, Category= "Action")
	bool bAutoStart;

	UFUNCTION(BlueprintCallable, Category= "Action")
	bool IsRunning() const;
	
	UFUNCTION(BlueprintNativeEvent, Category= "Action")
	bool CanStart(AActor* Instigator);

	UFUNCTION(BlueprintNativeEvent, Category= "Action")
	void StartAction(AActor* Instigator);

	UFUNCTION(BlueprintNativeEvent, Category= "Action")
	void StopAction(AActor* Instigator);

	UPROPERTY(EditDefaultsOnly, Category= "Action")
	FName ActionName;

	virtual UWorld* GetWorld() const override;
	

};

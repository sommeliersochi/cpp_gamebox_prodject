#include "SActionBleeding.h"

#include "SActionComponent.h"


USActionBleeding::USActionBleeding()
{
	bAutoStart = true;
}

void USActionBleeding::StartAction_Implementation(AActor* Instigator)
{
	Super::StartAction_Implementation(Instigator);

	if (Duration > 0.0f)
	{
		FTimerDelegate Delegate;
		Delegate.BindUObject(this, &USActionBleeding::StopAction, Instigator);

		GetWorld()->GetTimerManager().SetTimer(DurationHandle, Delegate, Duration, false);
	}

	if (Period > 0.0f)
	{
		FTimerDelegate Delegate;
		Delegate.BindUObject(this, &USActionBleeding::ExecutePeriodicEffect, Instigator);

		GetWorld()->GetTimerManager().SetTimer(PeriodHandle, Delegate, Period, true);
	}
}

void USActionBleeding::StopAction_Implementation(AActor* Instigator)
{
	if (GetWorld()->GetTimerManager().GetTimerRemaining(PeriodHandle) < KINDA_SMALL_NUMBER)
	{
		ExecutePeriodicEffect(Instigator);
	}

	Super::StopAction_Implementation(Instigator);

	GetWorld()->GetTimerManager().ClearTimer(PeriodHandle);
	GetWorld()->GetTimerManager().ClearTimer(DurationHandle);

	USActionComponent* Comp = GetOwningComponent();
	if (Comp)
	{
		Comp->RemoveAction(this);
	}
}

float USActionBleeding::GetTimeRemaining() const
{
	float EndTime = TimeStarted + Duration;

	return EndTime - GetWorld()->TimeSeconds;
}

void USActionBleeding::ExecutePeriodicEffect_Implementation(AActor* Instigator)
{
}

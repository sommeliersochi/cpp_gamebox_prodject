// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>

#include "PlayerHUDBaseWidget.h"
#include "CharacterOverlay.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UCharacterOverlay : public UPlayerHUDBaseWidget
{
	GENERATED_BODY()
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <Blueprint/UserWidget.h>

#include "SPlayerHUDWidget.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API USPlayerHUDWidget : public UUserWidget
{
	GENERATED_BODY()

};

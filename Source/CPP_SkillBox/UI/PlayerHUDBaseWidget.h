// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerHUDBaseWidget.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UPlayerHUDBaseWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;
	virtual void NativeDestruct() override;

	UFUNCTION()
	void OnOwningPawnChanged(APawn* OldPawn, APawn* NewPawn);

	UFUNCTION(BlueprintNativeEvent, Category="Widget")
	void ClearController(APlayerController* Controller);
	UFUNCTION(BlueprintNativeEvent, Category="Widget")
	void BindController(APlayerController* Controller);

	UFUNCTION(BlueprintNativeEvent, Category="Widget")
	void ClearPawn(APawn* Pawn);
	UFUNCTION(BlueprintNativeEvent, Category="Widget")
	void BindPawn(APawn* Pawn);

protected:
	TObjectPtr<APlayerController> OwnerPlayerController = nullptr;

	TObjectPtr<APawn> OwnerPawn = nullptr;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SWorldUserWidget.generated.h"

class USizeBox;
/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API USWorldUserWidget : public UUserWidget
{
	GENERATED_BODY()
protected:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UPROPERTY(meta = (BindWidget))
	TObjectPtr<USizeBox> ParentSizeBox;

public:

	UPROPERTY(EditAnywhere, Category= "UI")
	FVector WorldOffset = FVector(0.0f, 0.0f, 0.0f);
	
	UPROPERTY(BlueprintReadWrite, Category= "UI", meta = (ExposeOnSpawn = true))
	TObjectPtr<AActor> AttachedActor = nullptr;
};

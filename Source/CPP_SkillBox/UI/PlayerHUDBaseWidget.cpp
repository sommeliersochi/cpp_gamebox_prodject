// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/PlayerHUDBaseWidget.h"

void UPlayerHUDBaseWidget::NativeConstruct()
{
	Super::NativeConstruct();

	ClearController(OwnerPlayerController);
	OwnerPlayerController = GetOwningPlayer();
	BindController(OwnerPlayerController);

	if (OwnerPlayerController && OwnerPlayerController->GetPawn())
	{
		OnOwningPawnChanged(nullptr, OwnerPlayerController->GetPawn());
	}
}

void UPlayerHUDBaseWidget::NativeDestruct()
{
	ClearController(OwnerPlayerController);
	ClearPawn(OwnerPawn);

	Super::NativeDestruct();
}

void UPlayerHUDBaseWidget::OnOwningPawnChanged(APawn* OldPawn, APawn* NewPawn)
{
	ClearPawn(OldPawn);
	BindPawn(NewPawn);

	OwnerPawn = NewPawn;
}

void UPlayerHUDBaseWidget::ClearController_Implementation(APlayerController* Controller)
{
	if (Controller)
	{
		Controller->OnPossessedPawnChanged.RemoveDynamic(this, &UPlayerHUDBaseWidget::OnOwningPawnChanged);
	}
}

void UPlayerHUDBaseWidget::BindController_Implementation(APlayerController* Controller)
{
	if (Controller)
	{
		Controller->OnPossessedPawnChanged.RemoveDynamic(this, &UPlayerHUDBaseWidget::OnOwningPawnChanged);
		Controller->OnPossessedPawnChanged.AddDynamic(this, &UPlayerHUDBaseWidget::OnOwningPawnChanged);
	}
}

void UPlayerHUDBaseWidget::ClearPawn_Implementation(APawn* Pawn)
{
}

void UPlayerHUDBaseWidget::BindPawn_Implementation(APawn* Pawn)
{
}

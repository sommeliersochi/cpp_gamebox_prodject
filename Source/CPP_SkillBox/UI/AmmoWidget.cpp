// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/AmmoWidget.h"

#include "CombatComponent.h"
#include "CPP_SkillBoxCharacter.h"
#include "Weapon.h"

void UAmmoWidget::ClearPawn_Implementation(APawn* Pawn)
{
	if (const ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(Pawn))
	{
		if (UCombatComponent* CombatComponent = Character->GetCombat())
		{
			CombatComponent->OnChangedCarriedAmmoEvent.RemoveDynamic(this, &UAmmoWidget::OnChangedCarriedAmmo);
			CombatComponent->OnChangedWeaponAmmoEvent.RemoveDynamic(this, &UAmmoWidget::OnChangedWeaponAmmo);
		}
	}
	Super::ClearPawn_Implementation(Pawn);
}

void UAmmoWidget::BindPawn_Implementation(APawn* Pawn)
{
	Super::BindPawn_Implementation(Pawn);

	if (const ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(Pawn))
	{
		if (UCombatComponent* CombatComponent = Character->GetCombat())
		{
			CombatComponent->OnChangedCarriedAmmoEvent.RemoveDynamic(this, &UAmmoWidget::OnChangedCarriedAmmo);
			CombatComponent->OnChangedCarriedAmmoEvent.AddDynamic(this, &UAmmoWidget::OnChangedCarriedAmmo);

			CombatComponent->OnChangedWeaponAmmoEvent.RemoveDynamic(this, &UAmmoWidget::OnChangedWeaponAmmo);
			CombatComponent->OnChangedWeaponAmmoEvent.AddDynamic(this, &UAmmoWidget::OnChangedWeaponAmmo);

			OnChangedCarriedAmmo(CombatComponent, CombatComponent->GetCarriedAmmo());
			if (CombatComponent->GetEquippedWeapon())
			{
				OnChangedWeaponAmmo(CombatComponent, CombatComponent->GetEquippedWeapon()->GetAmmo());
			}
		}
	}
}

void UAmmoWidget::OnChangedCarriedAmmo(UCombatComponent* CombatComponent, int32 NewValue)
{
	CarriedAmmo = NewValue;
	UpdateAmmo();
}

void UAmmoWidget::OnChangedWeaponAmmo(UCombatComponent* CombatComponent, int32 NewValue)
{
	WeaponAmmo = NewValue;
	UpdateAmmo();
}

void UAmmoWidget::UpdateAmmo_Implementation()
{
}
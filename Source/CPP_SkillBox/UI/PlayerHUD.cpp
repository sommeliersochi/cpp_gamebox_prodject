// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHUD.h"

#include <Kismet/GameplayStatics.h>

#include "CPP_SkillBoxPlayerController.h"

void APlayerHUD::BeginPlay()
{
	Super::BeginPlay();

	AddCharacterOverlay();
}

void APlayerHUD::AddCharacterOverlay()
{
	ACPP_SkillBoxPlayerController* PlayerController = Cast<ACPP_SkillBoxPlayerController>(GetOwningPlayerController());
	if (PlayerController && CharacterOverlayClass)
	{
		CharacterOverlay = CreateWidget<UCharacterOverlay>(PlayerController, CharacterOverlayClass);
		CharacterOverlay->AddToViewport();
	}
}

void APlayerHUD::DrawHUD()
{
	Super::DrawHUD();

	if (GEngine)
	{
		FVector2D CrosshairCenter = HUDPackage.CrosshairPos;
		PlayerOwner->GetMousePosition(CrosshairCenter.X, CrosshairCenter.Y);

		float SpreadScale = CrosshairSpreadMax * HUDPackage.CrosshairSpread;

		if (HUDPackage.CrosshairsCenter)
		{
			FVector2D Spread(0.f, 0.f);
			DrawCrosshair(HUDPackage.CrosshairsCenter, CrosshairCenter, Spread, HUDPackage.CrosshairColor);
		}
		if (HUDPackage.CrosshairsLeft)
		{
			FVector2D Spread(-SpreadScale, 0.f);
			DrawCrosshair(HUDPackage.CrosshairsLeft, CrosshairCenter, Spread, HUDPackage.CrosshairColor);
		}
		if (HUDPackage.CrosshairsRight)
		{
			FVector2D Spread(SpreadScale, 0.f);
			DrawCrosshair(HUDPackage.CrosshairsRight, CrosshairCenter, Spread, HUDPackage.CrosshairColor);
		}
		if (HUDPackage.CrosshairsTop)
		{
			FVector2D Spread(0.f, -SpreadScale);
			DrawCrosshair(HUDPackage.CrosshairsTop, CrosshairCenter, Spread, HUDPackage.CrosshairColor);
		}
		if (HUDPackage.CrosshairsBottom)
		{
			FVector2D Spread(0.f, SpreadScale);
			DrawCrosshair(HUDPackage.CrosshairsBottom, CrosshairCenter, Spread, HUDPackage.CrosshairColor);
		}
	}
}

void APlayerHUD::DrawCrosshair(UTexture2D* Texture, const FVector2D ViewportCenter, const FVector2D Spread, const FLinearColor CrosshairColor)
{
	const float TextureWidth = Texture->GetSizeX();
	const float TextureHeight = Texture->GetSizeY();
	const FVector2D TextureDrawPoint(
		ViewportCenter.X - (TextureWidth / 2.f) + Spread.X,
		ViewportCenter.Y - (TextureHeight / 2.f) + Spread.Y
	);
	DrawTexture(
		Texture,
		TextureDrawPoint.X,
		TextureDrawPoint.Y,
		TextureWidth,
		TextureHeight,
		0.f,
		0.f,
		1.f,
		1.f,
		CrosshairColor
	);
}

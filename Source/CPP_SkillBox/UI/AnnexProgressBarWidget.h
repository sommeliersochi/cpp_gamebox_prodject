// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "AnnexProgressBarWidget.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UAnnexProgressBarWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintNativeEvent)
	void SetTime(float Time, float TimeToAnnex);
};

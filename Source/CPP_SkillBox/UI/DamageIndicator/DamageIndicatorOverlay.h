﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>

#include "PlayerHUDBaseWidget.h"
#include "DamageIndicatorDefinitions.h"
#include "DamageIndicatorOverlay.generated.h"

class UDamageIndicatorComponent;
class UDamageIndicator;
class UOverlay;
/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UDamageIndicatorOverlay : public UPlayerHUDBaseWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION()
	void OnDamageIndicator(UDamageIndicatorComponent* DamageIndicatorComponent, AActor* DamagedActor, APawn* Instigator, AActor* DamageCauser);

	virtual void ClearPawn_Implementation(APawn* Pawn) override;
	virtual void BindPawn_Implementation(APawn* Pawn) override;

	float GetTimestamp() const;

protected:
	UPROPERTY(EditDefaultsOnly, Category="Settings")
	TSubclassOf<UDamageIndicator> IndicatorWidgetClass = nullptr;

	UPROPERTY(EditDefaultsOnly, Category="Settings")
	float LifeTimeIndicator = 2.0f;

	UPROPERTY(meta=(BindWidget))
	TObjectPtr<UOverlay> IndicatorOverlay = nullptr;

	TMap<TObjectPtr<AActor>, FDamageIndicatorInfo> Indicators;
};

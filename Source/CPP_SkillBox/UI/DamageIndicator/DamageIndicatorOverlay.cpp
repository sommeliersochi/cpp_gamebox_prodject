﻿#include "DamageIndicatorOverlay.h"

#include <Components/Overlay.h>
#include <Components/OverlaySlot.h>
#include <GameFramework/GameStateBase.h>

#include "CPP_SkillBoxCharacter.h"
#include "DamageIndicator.h"
#include "DamageIndicator/DamageIndicatorComponent.h"

void UDamageIndicatorOverlay::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (OwnerPawn && OwnerPlayerController && OwnerPlayerController->PlayerCameraManager)
	{
		TArray<TObjectPtr<AActor>> Removed;

		const float CurrentTimestamp = GetTimestamp();
		const FVector OwnerPawnLocation = OwnerPawn->GetActorLocation();
		const FRotator CameraRotation = OwnerPlayerController->PlayerCameraManager->GetCameraRotation();
		for (const auto& Indicator : Indicators)
		{
			const TObjectPtr<AActor> ActorDamaged = Indicator.Key;
			const FDamageIndicatorInfo& DamageIndicatorInfo = Indicator.Value;

			const float Time = DamageIndicatorInfo.TimeDamage;
			const TObjectPtr<UDamageIndicator> DamageIndicator = DamageIndicatorInfo.WidgetIndicator;

			if (!DamageIndicator)
			{
				Removed.Add(ActorDamaged);
				continue;
			}

			if (CurrentTimestamp - Time > LifeTimeIndicator || !IsValid(ActorDamaged))
			{
				Removed.Add(ActorDamaged);
				DamageIndicator->RemoveFromParent();
			}
			else
			{
				const FRotator RotationToDamaged = (ActorDamaged->GetActorLocation() - OwnerPawnLocation).Rotation();
				DamageIndicator->SetRenderTransformAngle(RotationToDamaged.Yaw - CameraRotation.Yaw);
			}
		}

		for (const auto& RemoveItem: Removed)
		{
			Indicators.Remove(RemoveItem);
		}
	}
}

void UDamageIndicatorOverlay::OnDamageIndicator(UDamageIndicatorComponent* DamageIndicatorComponent, AActor* DamagedActor, APawn* Instigator, AActor* DamageCauser)
{
	if (!IndicatorWidgetClass)
	{
		return;
	}

	if (!OwnerPlayerController)
	{
		return;
	}

	if (!IndicatorOverlay)
	{
		return;
	}

	const UWorld* World = GetWorld();
	if (!World)
	{
		return;
	}

	const AGameStateBase* GameState = World->GetGameState();
	if (!GameState)
	{
		return;
	}

	if (Instigator)
	{
		if (Indicators.Contains(Instigator))
		{
			Indicators[Instigator].TimeDamage = GetTimestamp();
		}
		else
		{
			UDamageIndicator* DamageIndicator = CreateWidget<UDamageIndicator>(OwnerPlayerController.Get(), IndicatorWidgetClass);

			UOverlaySlot* OverlaySlot = IndicatorOverlay->AddChildToOverlay(DamageIndicator);
			OverlaySlot->SetHorizontalAlignment(HAlign_Center);
			OverlaySlot->SetVerticalAlignment(VAlign_Center);

			FDamageIndicatorInfo NewDamageIndicator;
			NewDamageIndicator.TimeDamage = GetTimestamp();
			NewDamageIndicator.WidgetIndicator = DamageIndicator;

			Indicators.Add(Instigator, NewDamageIndicator);
		}
	}
}

void UDamageIndicatorOverlay::ClearPawn_Implementation(APawn* Pawn)
{
	Super::ClearPawn_Implementation(Pawn);

	if (ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(Pawn))
	{
		UDamageIndicatorComponent* DamageIndicatorComponent = Character->GetDamageIndicatorComponent();
		if (DamageIndicatorComponent)
		{
			DamageIndicatorComponent->OnDamageIndicatorEvent.RemoveDynamic(this, &UDamageIndicatorOverlay::OnDamageIndicator);
		}
	}

	for (const auto& Indicator : Indicators)
	{
		const TObjectPtr<UDamageIndicator> DamageIndicator = Indicator.Value.WidgetIndicator;

		if (DamageIndicator && DamageIndicator->IsInViewport())
		{
			DamageIndicator->RemoveFromParent();
		}
	}

	Indicators.Reset();
}

void UDamageIndicatorOverlay::BindPawn_Implementation(APawn* Pawn)
{
	Super::BindPawn_Implementation(Pawn);

	if (Pawn)
	{
		if (ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(Pawn))
		{
			UDamageIndicatorComponent* DamageIndicatorComponent = Character->GetDamageIndicatorComponent();
			if (DamageIndicatorComponent)
			{
				DamageIndicatorComponent->OnDamageIndicatorEvent.RemoveDynamic(this, &UDamageIndicatorOverlay::OnDamageIndicator);
				DamageIndicatorComponent->OnDamageIndicatorEvent.AddDynamic(this, &UDamageIndicatorOverlay::OnDamageIndicator);
			}
		}
	}
}

float UDamageIndicatorOverlay::GetTimestamp() const
{
	const UWorld* World = GetWorld();
	if (!World)
	{
		return 0.0f;
	}

	const AGameStateBase* GameState = World->GetGameState();
	if (!GameState)
	{
		return 0.0f;
	}

	return GameState->GetServerWorldTimeSeconds();
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DamageIndicator.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UDamageIndicator : public UUserWidget
{
	GENERATED_BODY()
	
};

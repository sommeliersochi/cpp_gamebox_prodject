// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>

#include "DamageIndicatorDefinitions.generated.h"

class UDamageIndicator;

USTRUCT()
struct FDamageIndicatorInfo
{
	GENERATED_BODY()

	float TimeDamage = 0.0f;

	TObjectPtr<UDamageIndicator> WidgetIndicator = nullptr;
};
#pragma once

#include <CoreMinimal.h>
#include <GameFramework/HUD.h>

#include "CharacterOverlay.h"
#include "PlayerHUD.generated.h"

USTRUCT(BlueprintType)
struct FHUDPackage
{
	GENERATED_BODY()

public:
	TObjectPtr<UTexture2D> CrosshairsCenter;
	TObjectPtr<UTexture2D> CrosshairsLeft;
	TObjectPtr<UTexture2D> CrosshairsRight;
	TObjectPtr<UTexture2D> CrosshairsTop;
	TObjectPtr<UTexture2D> CrosshairsBottom;

	FVector2D CrosshairPos;
	
	float CrosshairSpread;

	FLinearColor CrosshairColor;
};

UCLASS()
class CPP_SKILLBOX_API APlayerHUD : public AHUD
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;

	virtual void DrawHUD() override;

	void SetHUDPackage(const FHUDPackage& Package);
	UFUNCTION(BlueprintPure, Category="HUD")
	UCharacterOverlay* GetCharacterOverlay();

protected:
	void AddCharacterOverlay();

private:
	void DrawCrosshair(UTexture2D* Texture, const FVector2D ViewportCenter, const FVector2D Spread, const FLinearColor CrosshairColor);

protected:
	UPROPERTY(EditAnywhere, Category= "HUD")
	TSubclassOf<UUserWidget> CharacterOverlayClass;

private:
	TObjectPtr<UCharacterOverlay> CharacterOverlay;

	FHUDPackage HUDPackage;

	float CrosshairSpreadMax = 16.f;
};

FORCEINLINE_DEBUGGABLE void APlayerHUD::SetHUDPackage(const FHUDPackage& Package)
{
	HUDPackage = Package;
}

FORCEINLINE UCharacterOverlay* APlayerHUD::GetCharacterOverlay()
{
	return CharacterOverlay;
}

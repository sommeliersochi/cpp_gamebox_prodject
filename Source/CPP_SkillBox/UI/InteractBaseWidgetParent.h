// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InteractBaseWidgetParent.generated.h"

class AInteractParentBase;
/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UInteractBaseWidgetParent : public UUserWidget
{
	GENERATED_BODY()
public:
	
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category="BaseOwner")
	AInteractParentBase* InteractParentBase;
	
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent)
	void CloseWidget();
	
	UFUNCTION(BlueprintNativeEvent)
	void UpdateSpecification(FName Specification);
	
	UFUNCTION(BlueprintNativeEvent)
	void InitTypeManager(bool bIsBarrier);
};

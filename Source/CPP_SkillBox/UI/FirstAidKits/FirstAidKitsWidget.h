// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/PlayerHUDBaseWidget.h"
#include "FirstAidKitsWidget.generated.h"

class UInventoryComponent;
/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UFirstAidKitsWidget : public UPlayerHUDBaseWidget
{
	GENERATED_BODY()

public:
	virtual void ClearPawn_Implementation(APawn* Pawn) override;
	virtual void BindPawn_Implementation(APawn* Pawn) override;

	UFUNCTION(BlueprintNativeEvent, Category="Widget")
	void OnFirstAidKitsChanged(UInventoryComponent* InventoryComponent, int32 NewValue);
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/FirstAidKits/FirstAidKitsWidget.h"

#include "CPP_SkillBoxCharacter.h"
#include "Inventory/InventoryComponent.h"

void UFirstAidKitsWidget::ClearPawn_Implementation(APawn* Pawn)
{
	Super::ClearPawn_Implementation(Pawn);

	if (Pawn)
	{
		if (ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(Pawn))
		{
			if (UInventoryComponent* InventoryComponent = Character->GetInventoryComponent())
			{
				InventoryComponent->OnNumFirstAidKitsChanged.RemoveDynamic(this, &UFirstAidKitsWidget::OnFirstAidKitsChanged);
			}
		}
	}
}

void UFirstAidKitsWidget::BindPawn_Implementation(APawn* Pawn)
{
	Super::BindPawn_Implementation(Pawn);

	if (Pawn)
	{
		if (ACPP_SkillBoxCharacter* Character = Cast<ACPP_SkillBoxCharacter>(Pawn))
		{
			if (UInventoryComponent* InventoryComponent = Character->GetInventoryComponent())
			{
				InventoryComponent->OnNumFirstAidKitsChanged.RemoveDynamic(this, &UFirstAidKitsWidget::OnFirstAidKitsChanged);
				InventoryComponent->OnNumFirstAidKitsChanged.AddDynamic(this, &UFirstAidKitsWidget::OnFirstAidKitsChanged);
			}
		}
	}
}

void UFirstAidKitsWidget::OnFirstAidKitsChanged_Implementation(UInventoryComponent* InventoryComponent, int32 NewValue)
{
}

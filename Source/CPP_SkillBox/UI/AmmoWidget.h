// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/PlayerHUDBaseWidget.h"
#include "AmmoWidget.generated.h"

class UCombatComponent;
class UTextBlock;
/**
 * 
 */
UCLASS()
class CPP_SKILLBOX_API UAmmoWidget : public UPlayerHUDBaseWidget
{
	GENERATED_BODY()

public:
	virtual void ClearPawn_Implementation(APawn* Pawn) override;
	virtual void BindPawn_Implementation(APawn* Pawn) override;

protected:
	UFUNCTION()
	void OnChangedCarriedAmmo(UCombatComponent* CombatComponent, int32 NewValue);
	UFUNCTION()
	void OnChangedWeaponAmmo(UCombatComponent* CombatComponent, int32 NewValue);

	UFUNCTION(BlueprintNativeEvent, Category="AmmoWidget")
	void UpdateAmmo();

protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category="AmmoWidget", meta=(BindWidget))
	TObjectPtr<UTextBlock> AmmoText = nullptr;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category="AmmoWidget")
	int32 CarriedAmmo;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category="AmmoWidget")
	int32 WeaponAmmo;
};


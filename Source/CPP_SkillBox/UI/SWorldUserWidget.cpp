// Fill out your copyright notice in the Description page of Project Settings.


#include "SWorldUserWidget.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "Components/SizeBox.h"
#include "Kismet/GameplayStatics.h"

void USWorldUserWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (!IsInViewport())
	{
		return;
	}

	if (!IsValid(AttachedActor))
	{
		RemoveFromParent();

		//UE_LOG(LogTemp, Warning, TEXT("AttahchedActor to long valid, remove Health widget."))
		return;
	}

	FVector2D ScreenPosition;
	if (UGameplayStatics::ProjectWorldToScreen(GetOwningPlayer(), AttachedActor->GetActorLocation() + WorldOffset, ScreenPosition))
	{
		const float Scale = UWidgetLayoutLibrary::GetViewportScale(this);

		if (!FMath::IsNearlyZero(Scale))
		{
			ScreenPosition /= Scale;
			if (ParentSizeBox)
			{
				ParentSizeBox->SetRenderTranslation(ScreenPosition);
			}
		}
	}
}
